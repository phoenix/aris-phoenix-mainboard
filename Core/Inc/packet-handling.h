/**
 *******************************************************************************
 * @file           packet-handling.h
 * @brief          Handler interface that acts on incoming packets.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include "tasks/packets.h"

/**
 * @brief Handler for incoming received packets.
 * This handler is called by the packets API. It will also run on the packet
 * API's CPU time, so it is tried to keep the block the processing of other
 * packets to a minimum.
 * @param packet The packet that was received.
 */
void packet_received(Packet *packet);

/**
 * @brief Process an incoming data payload from the ICSB.
 *
 * The data payload is converted to the contained sensor data and pushed into
 * the state estimation input.
 * @param payload The non-null received payload
 * @param origin The microcontroller that sent the payload.
 * @note Does not free the payload.
 */
void process_data_payload(void *payload, enum Location origin);