/**
 *******************************************************************************
 * @file           epos4.h
 * @brief          Driver talking to the EPOS4 motor controller.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#ifndef INC_EPOS4_H_
#define INC_EPOS4_H_

#include "main.h"
#include <stdio.h>
#include <string.h>

/// @addtogroup actuator_drivers
/// @{

typedef enum { EPOS4_OK = 0x00, EPOS4_ERROR = 0x01 } EPOS4_Status_t;

uint16_t calculateCRC(uint8_t *data, uint8_t len);

EPOS4_Status_t EnableMotor(UART_HandleTypeDef *huart);
EPOS4_Status_t DisableMotor(UART_HandleTypeDef *huart);
EPOS4_Status_t InitDisableMotor(UART_HandleTypeDef *huart);

EPOS4_Status_t SetCyclicPositionMode(UART_HandleTypeDef *huart);
EPOS4_Status_t GetPosition(UART_HandleTypeDef *huart, int32_t *position);
EPOS4_Status_t SetPositionProfilePPM(UART_HandleTypeDef *huart,
                                     int32_t velocity, int32_t acceleration,
                                     int32_t deceleration);
EPOS4_Status_t MoveToPosition(UART_HandleTypeDef *huart, int32_t position);
EPOS4_Status_t SetHomingModeHHM(UART_HandleTypeDef *huart,
                                uint32_t homing_acceleration,
                                uint32_t speed_switch, uint32_t speed_index,
                                int32_t home_offset, uint16_t current_threshold,
                                int32_t home_position);
EPOS4_Status_t FindHome(UART_HandleTypeDef *huart);

EPOS4_Status_t WriteCommand(uint8_t *command, uint8_t *data, uint8_t *rx_buffer,
                            UART_HandleTypeDef *huart);
EPOS4_Status_t ReadCommand(uint8_t *command, uint8_t *rx_buffer,
                           UART_HandleTypeDef *huart);

/**
 * @brief Callback that is invoked when the UART IT transfer has completed.
 */
void EPOS_UART_TxCplt_Callback(void);

/**
 * @brief Callback that is invoked when the UART IT transfer has completed.
 */
void EPOS_UART_RxCplt_Callback(void);

/* external */
extern UART_HandleTypeDef huart6;
extern UART_HandleTypeDef huart1;

/// @}

#endif /* INC_EPOS4_H_ */
