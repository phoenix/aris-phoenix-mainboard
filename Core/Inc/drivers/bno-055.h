/**
 *******************************************************************************
 * @file           bno-055.h
 * @brief          Driver for the BNO-055 9-axis absolute orientation sensor.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include <stdbool.h>

#include "stm32f4xx_hal.h"

#include "bno-055-bosch.h"
#include "data/data.h"
#include "main.h"

/// @addtogroup sensor_drivers
/// @{

/* BNO-055 configuration----------------------------------------------------- */

enum AccelFullscaleRange {
    ACCEL_FSR2G = 0x00,
    ACCEL_FSR4G = 0x01,
    ACCEL_FSR8G = 0x02,
    ACCEL_FSR16G = 0x03
};

enum GyroFullscaleRange {
    GYRO_FSR2000 = 0x00,
    GYRO_FSR1000 = 0x01,
    GYRO_FSR500 = 0x02,
    GYRO_FSR250 = 0x03,
    GYRO_FSR125 = 0x04
};

/**
 * @brief A handle that refers to a BNO055 9-DoF inertial measurement unit
 * This handle is passed to the API to discern between multiple instances of
 * BNO055 sensors on different buses.
 */
typedef struct BNO055_Struct {
    I2C_HandleTypeDef *hi2c;  ///< I2C handle where the BNO-055 is located.
    GPIO_TypeDef *reset_port; ///< Reset port of this BNO-055 sensor.
    uint16_t reset_pin;       ///< Reset pin of this BNO-055 sensor.
    bool rst_pin_setup;    ///< Whether the reset pin is set up. Do not change.
    bool use_primary_addr; ///< Whether this BNO-055 uses the primary or the
                           ///< secondary address
    enum AccelFullscaleRange afsr; ///< Full scale range of accelerometer
    enum GyroFullscaleRange gfsr;  ///< Full scale range of gyroscope
    struct bno055_t bno_priv;      ///< Private struct used in the BNO055 API.
    struct bno055_accel_offset_t acc_precalib; ///< Pre-calibration values
    struct bno055_mag_offset_t mag_precalib;   ///< Pre-calibration values
    struct bno055_gyro_offset_t gyro_precalib; ///< Pre-calibration values
} BNO055_Handle;

/* BNO-055 Sensor API ------------------------------------------------------- */

// These functions are all for use through the sensor task.

/**
 * Initialize the BNO-055 9-Axis IMU/Magnetometer sensor.
 *
 * This starts communication with the sensor.
 * @param bno055 A pointer to an already existing BNO055 handle.
 * @returns true if the setup was successful.
 */
bool bno055_setup(BNO055_Handle *bno055);

/**
 * Configure the BNO-055 as needed for our application. Clean reset.
 *
 * This method will first assert the NRST pin of the BNO-055 to make it boot
 * again, providing a clean slate for setting it up. It then configures the
 * BNO-055 with the parameters configured in the `menuconfig` tool (see
 * documentation on how to change the values).
 * @param bno055 The BNO-055 to reset.
 * @pre `bno_055_setup()` must have been called before invoking this method.
 * @returns true if the reset and following setup was successful.
 */
bool bno055_reset(BNO055_Handle *bno055);

/**
 * Put the BNO-055 in low-power mode. No registers will be written to, etc.
 *
 * The sleep mode must be lifted prior to sampling by calling bno055_wakeup().
 * @param bno055 The bno055 to put in sleep mode.
 * @returns true if the suspension was successful.
 */
bool bno055_lowpower_enable(BNO055_Handle *bno055);

/**
 * Wake up the BNO-055 from low-power mode.
 * @param bno055 The BNO055 to wake up from low-power mode.
 * @returns true if waking it up was successful.
 */
bool bno055_wakeup(BNO055_Handle *bno055);

/**
 * Take a sample of the BNO-055 sensor and stores the result at destination.
 * @param bno055 Handle of the BNO055 to be sampled.
 * @param dest Nonnull pointer to the struct where the sample shall be saved.
 * @returns true if taking the sample was successful.
 */
bool bno055_sample(BNO055_Handle *bno055, struct BNO055_Readout *dest);

/**
 * @brief Query whether the BNO-055 is fully calibrated
 *
 * @param bno055 The BNO-055 to examine.
 * @pre bno055 must not be NULL
 * @returns True if it is fully calibrated, false otherwise.
 */
bool bno055_is_calibrated(BNO055_Handle *bno055);

/**
 * @brief Push the predetermined calibration values to the BNO055.
 *
 * The calibration of the BNO055 can be sped up by pushing a known calibration
 * set that was previously found after calibration. The BNO055 will then use
 * that one as a starting point. It will keep updating the data as it gathers
 * more data. This will NOT be synchronized with the hardcoded data we provide.
 * @pre The BNO055 must have been woken from suspend mode. Otherwise the
 *          register writing will not succeed.
 * @returns true if it could successfully push the offsets/radii over the bus.
 */
bool bno055_push_calibration(BNO055_Handle *bno055);

/**
 * @brief Prints the current calibration values for the BNO055 to a serial port.
 *
 * This function gets the calibration values for accelerometer, gyroscope and
 * magnetometer and writes them to the serial monitor.
 */
bool bno055_print_calibration(BNO055_Handle *bno055);

/// @}
