/**
 *******************************************************************************
 * @file           neo-7m.h
 * @brief          Driver for the NEO-7M GPS receiver.
 * @author         Lukas Vogel
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "data/data.h"
#include "main.h"
#include "util/gps-parsing.h"

/// @addtogroup receiver_drivers
/// @{

/* Type definitions --------------------------------------------------------- */

/**
 * @brief A struct that refers to a NEO-7M GPS Receiver.
 *
 * The NEO-7M driver handles the receiving and the DMA driver itself. It will
 * notify the callback function provided in ready_cb when a new position is
 * available.
 *
 * How to use the driver: Provide a function pointer for the ready callback.
 * Also  make a DMA callback function. When it is is called (in ISR context),
 * call the corresponding NEO-7M receiver's "process_dma" function. If the data
 * is ready for processing and readout, the READY CB will be called (also in ISR
 * context), and you can readout in your task.
 *
 * It is the job of the managing task to determine which DMA corresponding to
 * which NEO-7M was called.
 */
struct NEO7M_Handle {

    /* To provide by user --------------------------------------------------- */

    UART_HandleTypeDef *huart; ///< The UART bus where the receiver sends data.
    void (*ready_cb)(int);     ///< Callback to call when a position is ready
    int arg;                   ///< Argument to pass to callback function

    /* Set by API ----------------------------------------------------------- */

    bool ready;             ///< Whether the NEO-7M has a readout available.
    bool has_fix;           ///< Whether the receiver has a fix on the GPS.
    uint8_t num_satellites; ///< Number of satellites used.
    uint8_t recv_buf[256];  ///< Receive buffer.
    uint8_t proc_buf[256];  ///< Processing buffer
    uint8_t recv_tmp;       ///< Temporary receive character
    uint8_t recv_index;     ///< Index where the current reception process is

    struct NMEA_State nmea; ///< State of the NMEA parser.
};

/* Public functions --------------------------------------------------------- */

/**
 * Initialize a NEO-7M GPS receiver.
 *
 * The initialization starts the DMA transfer on this NEO-7M receiver. It will
 * read the circular DMA buffer in 1B steps and process each character.
 * @returns true if the initialization succeeded, false otherwise
 */
bool neo7m_init(struct NEO7M_Handle *neo7m);

/**
 * @brief Make the driver process the received DMA characters.
 * @return true if the processing was successful, false otherwise
 */
bool neo7m_process(struct NEO7M_Handle *neo7m);

/**
 * @brief Get the readout from the NEO-7M GPS receiver.
 * @note Only call after being notified that a measurement is available.
 *      Otherwise the reported values are undefined.
 * @returns true if the readout was successful, false otherwise
 */
bool neo7m_get_readout(struct NEO7M_Handle *neo7m,
                       struct NEO7M_Readout *readout);

/// @}
