/**
 *******************************************************************************
 * @file           sd-driver.h
 * @brief          Driver that initializes and talks to the SD.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include <stdbool.h>

/// @addtogroup sd_drivers
/// @{

/* File types --------------------------------------------------------------- */

/**
 * @brief Denotes different file types that can be written to.
 * This handle is passed with a log string to indicate which file should be
 * written to.
 */
enum SDFile {
    EventFile = 1,
    ErrorFile = 2,
    SensorDataFile = 3,
};

/* Mounting and unmounting -------------------------------------------------- */

/**
 * Tries to mount the SD card and reports whether it was successful doing so.
 */
bool sd_card_mount(void);

/**
 * Tries to unmount the SD card and reports whether it was successful doing so.
 */
bool sd_card_unmount(void);

/* Files and folders -------------------------------------------------------- */

/**
 * @brief Creates a new folder and the necessary files for the log data.
 * @return Whether the action was successful.
 */
bool sd_setup_logging(void);

/**
 * @brief Write the character buffer to the SD card, using the correct file.
 * @param buf The null-terminated buffer to write to the card.
 * @param target_file The file into which the data is to be written.
 * @returns true if the writing was successful.
 */
bool sd_write(char *buf, enum SDFile target_file);

/**
 * @brief Sync the files to the SD card.
 *
 * Flushes the internal buffers that the SD Card implementation has over to the
 * actual card. To be called in regular intervals to ensure that even in case
 * something goes wrong, at least a portion of the log is available on disk.
 * @returns true if all syncs were successful and false if an error occurred.
 */
bool sd_sync(void);

/* Information about the file system ---------------------------------------- */

/**
 * Calculates the percentage of available disk space on the SD card.
 * @returns The percentage (from 0 to 1) of available disk space. Negative on
 *      error.
 * @pre The SD card must have previously been mounted.
 */
float free_diskspace_percentage(void);

/**
 * Counts the number of subdirectories of a directory at a given path.
 * @param path The path of the directory whose subdirectories shall be counted.
 * @returns The number of subdirectories. Negative on error.
 */
int count_subdirectories(char *path);

/// @}
