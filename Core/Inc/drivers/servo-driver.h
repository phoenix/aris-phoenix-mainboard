/**
 *******************************************************************************
 * @file           servo-driver.h
 * @brief          Contains the driver talking to the deployment servos.
 * @author         Helvijs Kiselis
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

/// @addtogroup actuator_drivers
/// @{

/**
 * @brief Triggers the deployment servos.
 */
void trigger_deployment_servos(void);

/// @}
