/**
 *
 *
 *
 *
 *
 */

#ifndef LORA_AIRBORNE_H
#define LORA_AIRBORNE_H

#include "data/lora_payloads.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_spi.h"
#include <stdint.h>

#include "tasks/finite_state_machine.h"

/// @addtogroup lora_drivers
/// @{

// Typedef

typedef struct {

    uint8_t error : 1; // reserved for further use (not currently implemented)
    uint8_t receiver_mcu : 1; // 0: inactive/not connected, 1: active
    uint8_t icsb1 : 1;        // 0: inactive/not connected, 1: active
    uint8_t icsb2 : 1;        // 0: inactive/not connected, 1: active
    uint8_t baro1 : 1;        // 0: inactive/not connected, 1: active
    uint8_t baro2 : 1;        // 0: inactive/not connected, 1: active
    uint8_t imu1 : 1;         // 0: inactive/not connected, 1: active
    uint8_t imu2 : 1;         // 0: inactive/not connected, 1: active

    uint8_t gps1 : 1;      // 0: inactive/not connected, 1: active
    uint8_t gps1_lock : 1; // 0: no lock, 1: lock acquired
    uint8_t gps2 : 1;      // 0: inactive/not connected, 1: active
    uint8_t gps2_lock : 1; // 0: no lock, 1: lock acquired
    uint8_t gps3 : 1;      // 0: inactive/not connected, 1: active
    uint8_t gps3_lock : 1; // 0: no lock, 1: lock acquired
    uint8_t sd : 1;        // 0: inactive/not connected, 1: active
    uint8_t reserved : 1;  // reserved for further use

    uint8_t error_code;

    uint8_t state; // current state of the FSM; for definition see enum
                   // SystemState in "tasks/finite_state_machine.h"

} StatusUpdate_t;

// describes status of system and peripherals
typedef struct {
    uint8_t sd : 1;
    uint8_t error : 1;
    uint8_t ready : 1;
    uint8_t receiver_mcu : 1;
    uint8_t icsb1 : 1;
    uint8_t icsb2 : 1;
    uint8_t baro1 : 1;
    uint8_t baro2 : 1;

    uint8_t imu1 : 1;
    uint8_t imu2 : 1;
    uint8_t gps1 : 1;
    uint8_t gps1_lock : 1;
    uint8_t gps2 : 1;
    uint8_t gps2_lock : 1;
    uint8_t gps3 : 1;
    uint8_t gps3_lock : 1;

    uint8_t error_code;

    FSMState_t fsm_state; // current state of the FSM; for definition see
                          // "lora_payloads.h"

} SystemStatus_t;

// most recent coordinates
typedef struct {

    int16_t x;  // in units of 0.2m -> range: -6553.6m to +6553.4m
    int16_t y;  // in units of 0.2m -> range: -6553.6m to +6553.4m
    uint16_t z; // in units of 0.1m -> range: 0m to +6553.5m

} Coordinates_t;

// requirements set to system; this is set by ARMING REQUEST transmission
typedef struct {

    uint8_t baro : 2;       // 0 - 2
    uint8_t imu : 2;        // 0 - 2
    uint8_t gps_active : 2; // 0 - 3
    uint8_t gps_lock : 2;   // 0 - 3

    uint8_t icsb : 2;         // 0 - 2
    uint8_t receiver_mcu : 1; // 0 or 1
    uint8_t sd : 1;           // 0 or 1
    uint8_t reserved : 4;     // reserved

} SystemRequirements_t;

// FUNCTIONS: TO BE CALLED EXTERNALLY
// initialise lora module in airborne mode
void lora_init_airborne(void);

// LoRa slave function entry, will be executed in endless loop
void lora_slave_function(void);

// Update system status as sent in system status report package
void lora_update_system_status(StatusUpdate_t status_update);

// Update coordinates as sent in telemetry data package
void lora_update_coordinates(float x, float y, float z);

// other/internal functions

// FUNCTIONS: INTERNAL

// process RX message and returns corresponding TX message
PayloadAirborne_t process_msg(PayloadGround_t rx_msg);

// sets tx message to pong message
void process_ping(PayloadAirborne_t *tx_msg);

// sets tx message to status report
void process_status_report(PayloadAirborne_t *tx_msg);

// sets tx message to telemetry data
void process_telemetry(PayloadAirborne_t *tx_msg);

// sets tx message to countdown ack package, starts internal countown for drop
void process_countdown(PayloadGround_t *rx_msg, PayloadAirborne_t *tx_msg);

// sets tx message to emergency ack package, triggers internal fsm transition to
// deploy main parachute
void process_emergency_deployment(PayloadAirborne_t *tx_msg);

// sets tx message to FSM command reply package, triggers internal fsm
// transition to disarmed state
void process_software_reset(void);

// process fsm command payload, sends signals for fsm transitions
void process_fsm_command(PayloadGround_t *rx_msg, PayloadAirborne_t *tx_msg);

// sets tx message to FSM command reply package, triggers internal fsm
// transition to calibrate
void process_calibrate(PayloadAirborne_t *tx_msg);

// sets tx message to FSM command reply package, triggers internal fsm
// transition to warmup
void process_warmup(PayloadAirborne_t *tx_msg);

// sets tx message to FSM command reply package, triggers internal fsm
// transition to armed state
void process_arm(PayloadAirborne_t *tx_msg);

// sets tx message to FSM command reply package, triggers internal fsm
// transition to approach state (maybe irrelevant placeholder function)
void process_trigger_approach(PayloadAirborne_t *tx_msg);

// sets tx message to FSM command reply package, manually triggers internal fsm
// transition to touchdown detected
void process_touchdown(PayloadAirborne_t *tx_msg);

// sets tx message to FSM command reply package, triggers internal fsm
// transition to disarmed state
void process_disarm(PayloadAirborne_t *tx_msg);

void set_pong_msg(void);

/// @}

#endif
