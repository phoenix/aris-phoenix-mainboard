/**
 ******************************************************************************
 * @file           bmp-388.h
 * @brief          Declaration of BMP-388 sensor implementation.
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

#pragma once

#include <stdbool.h>

#include "bmp3-bosch.h"
#include "data/data.h"
#include "main.h"

/// @addtogroup sensor_drivers
/// @{

/**
 * Handle for a BMP-388 absolute pressure sensor.
 * This handle is used to discern between different BMP-388 sensors at different
 * I2C interfaces.
 */
typedef struct BMP388_Struct {
    I2C_HandleTypeDef *hi2c; ///< I2C port where the BMP-388 is located
    uint8_t bmp388_i2c_addr; ///< I2C address of BMP-388 being sampled
    /** Private struct used to talk to the BMP-388 driver. Do not modify. */
    struct bmp3_dev bmp_internal;
} BMP388_Handle;

/**
 * Initialize the BMP-388 sensor.
 *
 * Keep in mind that the BMP388_Handle is not returned by this function, but
 * should be statically allocated somewhere in the caller's code. It must not go
 * out of scope, otherwise the Bosch Sensortec driver might crash.
 * @param bmp_388 A pointer to a non-null BMP388_Handle including the I2C handle
 * @pre bmp_388 and bmp_388->hi2c must not be NULL
 * @returns true if the BMP-388 could be set up.
 */
bool bmp388_init(BMP388_Handle *bmp_388);

/**
 * @brief Sample the BMP-388 absolute pressure and temperature sensor.
 * @param[in] bmp_388 The handle to the BMP-388 to sample.
 * @param[out] readout A pointer to the struct where the readout is stored.
 * @returns true if the sampling was successful. Only upon successful sampling
 *  will the data be stored in readout.
 */
bool bmp388_sample(BMP388_Handle *bmp_388, struct BMP388_Readout *readout);

/// @}