/**
 * Code based on examples by Semtech
 *
 *
 *
 *
 */

#ifndef LORA_MODULE_H
#define LORA_MODULE_H

#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_spi.h"
#include <stdint.h>

/// @addtogroup lora_drivers
/// @{

// TYPEDEF

// Flags set by IRQ -> poll flags to see which new action should be taken
typedef union {

    struct {
        uint8_t rxDone : 1;
        uint8_t rxError : 1;
        uint8_t txDone : 1;
        uint8_t rxTimeout : 1;
        uint8_t txTimeout : 1;
        uint8_t reserved : 3;
    } flags;

    uint8_t value;

} RadioFlags_t;

typedef enum {
    RF_IDLE = 0x00, //!< The radio is idle
    RF_RX_RUNNING,  //!< The radio is in reception state
    RF_TX_RUNNING,  //!< The radio is in transmission state
    RF_CAD,         //!< The radio is doing channel activity detection
} RadioStates_t;

// commands opcode for rf shield
typedef enum {
    RADIO_GET_STATUS = 0xC0,
    RADIO_WRITE_REGISTER = 0x0D,
    RADIO_READ_REGISTER = 0x1D,
    RADIO_WRITE_BUFFER = 0x0E,
    RADIO_READ_BUFFER = 0x1E,
    RADIO_SET_SLEEP = 0x84,
    RADIO_SET_STANDBY = 0x80,
    RADIO_SET_FS = 0xC1,
    RADIO_SET_TX = 0x83,
    RADIO_SET_RX = 0x82,
    RADIO_SET_RXDUTYCYCLE = 0x94,
    RADIO_SET_CAD = 0xC5,
    RADIO_SET_TXCONTINUOUSWAVE = 0xD1,
    RADIO_SET_TXCONTINUOUSPREAMBLE = 0xD2,
    RADIO_SET_PACKETTYPE = 0x8A,
    RADIO_GET_PACKETTYPE = 0x11,
    RADIO_SET_RFFREQUENCY = 0x86,
    RADIO_SET_TXPARAMS = 0x8E,
    RADIO_SET_PACONFIG = 0x95,
    RADIO_SET_CADPARAMS = 0x88,
    RADIO_SET_BUFFERBASEADDRESS = 0x8F,
    RADIO_SET_MODULATIONPARAMS = 0x8B,
    RADIO_SET_PACKETPARAMS = 0x8C,
    RADIO_GET_RXBUFFERSTATUS = 0x13,
    RADIO_GET_PACKETSTATUS = 0x14,
    RADIO_GET_RSSIINST = 0x15,
    RADIO_GET_STATS = 0x10,
    RADIO_RESET_STATS = 0x00,
    RADIO_CFG_DIOIRQ = 0x08,
    RADIO_GET_IRQSTATUS = 0x12,
    RADIO_CLR_IRQSTATUS = 0x02,
    RADIO_CALIBRATE = 0x89,
    RADIO_CALIBRATEIMAGE = 0x98,
    RADIO_SET_REGULATORMODE = 0x96,
    RADIO_GET_ERROR = 0x17,
    RADIO_SET_TCXOMODE = 0x97,
    RADIO_SET_TXFALLBACKMODE = 0x93,
    RADIO_SET_RFSWITCHMODE = 0x9D,
    RADIO_SET_STOPRXTIMERONPREAMBLE = 0x9F,
    RADIO_SET_LORASYMBTIMEOUT = 0xA0,
} RadioCommands_t;

// Represents the possible packet type (i.e. modem) used
typedef enum {
    PACKET_TYPE_GFSK = 0x00,
    PACKET_TYPE_LORA = 0x01,
    PACKET_TYPE_NONE = 0x0F,
} RadioPacketTypes_t;

// Represents the ramping time for power amplifier
typedef enum {
    RADIO_RAMP_10_US = 0x00,
    RADIO_RAMP_20_US = 0x01,
    RADIO_RAMP_40_US = 0x02,
    RADIO_RAMP_80_US = 0x03,
    RADIO_RAMP_200_US = 0x04,
    RADIO_RAMP_800_US = 0x05,
    RADIO_RAMP_1700_US = 0x06,
    RADIO_RAMP_3400_US = 0x07,
} RadioRampTimes_t;

// Represents the number of symbols to be used for channel activity detection
// operation
typedef enum {
    LORA_CAD_01_SYMBOL = 0x00,
    LORA_CAD_02_SYMBOL = 0x01,
    LORA_CAD_04_SYMBOL = 0x02,
    LORA_CAD_08_SYMBOL = 0x03,
    LORA_CAD_16_SYMBOL = 0x04,
} RadioLoRaCadSymbols_t;

// Represents the Channel Activity Detection actions after the CAD operation is
// finished
typedef enum {
    LORA_CAD_ONLY = 0x00,
    LORA_CAD_RX = 0x01,
    LORA_CAD_LBT = 0x10,
} RadioCadExitModes_t;

// Represents the modulation shaping parameter
typedef enum {
    MOD_SHAPING_OFF = 0x00,
    MOD_SHAPING_G_BT_03 = 0x08,
    MOD_SHAPING_G_BT_05 = 0x09,
    MOD_SHAPING_G_BT_07 = 0x0A,
    MOD_SHAPING_G_BT_1 = 0x0B,
} RadioModShapings_t;

// Represents the modulation shaping parameter
typedef enum {
    RX_BW_4800 = 0x1F,
    RX_BW_5800 = 0x17,
    RX_BW_7300 = 0x0F,
    RX_BW_9700 = 0x1E,
    RX_BW_11700 = 0x16,
    RX_BW_14600 = 0x0E,
    RX_BW_19500 = 0x1D,
    RX_BW_23400 = 0x15,
    RX_BW_29300 = 0x0D,
    RX_BW_39000 = 0x1C,
    RX_BW_46900 = 0x14,
    RX_BW_58600 = 0x0C,
    RX_BW_78200 = 0x1B,
    RX_BW_93800 = 0x13,
    RX_BW_117300 = 0x0B,
    RX_BW_156200 = 0x1A,
    RX_BW_187200 = 0x12,
    RX_BW_234300 = 0x0A,
    RX_BW_312000 = 0x19,
    RX_BW_373600 = 0x11,
    RX_BW_467000 = 0x09,
} RadioRxBandwidth_t;

// Represents the possible spreading factor values in LoRa packet types
typedef enum {
    LORA_SF5 = 0x05,
    LORA_SF6 = 0x06,
    LORA_SF7 = 0x07,
    LORA_SF8 = 0x08,
    LORA_SF9 = 0x09,
    LORA_SF10 = 0x0A,
    LORA_SF11 = 0x0B,
    LORA_SF12 = 0x0C,
} RadioLoRaSpreadingFactors_t;

// Represents the bandwidth values for LoRa packet type
typedef enum {
    LORA_BW_500 = 6,
    LORA_BW_250 = 5,
    LORA_BW_125 = 4,
    LORA_BW_062 = 3,
    LORA_BW_041 = 10,
    LORA_BW_031 = 2,
    LORA_BW_020 = 9,
    LORA_BW_015 = 1,
    LORA_BW_010 = 8,
    LORA_BW_007 = 0,
} RadioLoRaBandwidths_t;

// Represents the coding rate values for LoRa packet type
typedef enum {
    LORA_CR_4_5 = 0x01,
    LORA_CR_4_6 = 0x02,
    LORA_CR_4_7 = 0x03,
    LORA_CR_4_8 = 0x04,
} RadioLoRaCodingRates_t;

// Represents the preamble length used to detect the packet on Rx side
typedef enum {
    RADIO_PREAMBLE_DETECTOR_OFF = 0x00, //!< Preamble detection length off
    RADIO_PREAMBLE_DETECTOR_08_BITS =
        0x04, //!< Preamble detection length 8 bits
    RADIO_PREAMBLE_DETECTOR_16_BITS =
        0x05, //!< Preamble detection length 16 bits
    RADIO_PREAMBLE_DETECTOR_24_BITS =
        0x06, //!< Preamble detection length 24 bits
    RADIO_PREAMBLE_DETECTOR_32_BITS =
        0x07, //!< Preamble detection length 32 bit
} RadioPreambleDetection_t;

// Represents the possible combinations of SyncWord correlators activated
typedef enum {
    RADIO_ADDRESSCOMP_FILT_OFF =
        0x00, //!< No correlator turned on, i.e. do not search for SyncWord
    RADIO_ADDRESSCOMP_FILT_NODE = 0x01,
    RADIO_ADDRESSCOMP_FILT_NODE_BROAD = 0x02,
} RadioAddressComp_t;

// Radio packet length mode
typedef enum {
    RADIO_PACKET_FIXED_LENGTH = 0x00, //!< The packet is known on both sides, no
                                      //!< header included in the packet
    RADIO_PACKET_VARIABLE_LENGTH =
        0x01, //!< The packet is on variable size, header included
} RadioPacketLengthModes_t;

// Represents the CRC length
typedef enum {
    RADIO_CRC_OFF = 0x01, //!< No CRC in use
    RADIO_CRC_1_BYTES = 0x00,
    RADIO_CRC_2_BYTES = 0x02,
    RADIO_CRC_1_BYTES_INV = 0x04,
    RADIO_CRC_2_BYTES_INV = 0x06,
    RADIO_CRC_2_BYTES_IBM = 0xF1,
    RADIO_CRC_2_BYTES_CCIT = 0xF2,
} RadioCrcTypes_t;

// Radio whitening mode activated or deactivated
typedef enum {
    RADIO_DC_FREE_OFF = 0x00,
    RADIO_DC_FREEWHITENING = 0x01,
} RadioDcFree_t;

// Holds the lengths mode of a LoRa packet type
typedef enum {
    LORA_PACKET_VARIABLE_LENGTH =
        0x00, //!< The packet is on variable size, header included
    LORA_PACKET_FIXED_LENGTH = 0x01, //!< The packet is known on both sides, no
                                     //!< header included in the packet
    LORA_PACKET_EXPLICIT = LORA_PACKET_VARIABLE_LENGTH,
    LORA_PACKET_IMPLICIT = LORA_PACKET_FIXED_LENGTH,
} RadioLoRaPacketLengthsMode_t;

// Represents the CRC mode for LoRa packet type
typedef enum {
    LORA_CRC_ON = 0x01,  //!< CRC activated
    LORA_CRC_OFF = 0x00, //!< CRC not used
} RadioLoRaCrcModes_t;

// Represents the IQ mode for LoRa packet type
typedef enum {
    LORA_IQ_NORMAL = 0x00,
    LORA_IQ_INVERTED = 0x01,
} RadioLoRaIQModes_t;

// Represents the voltage used to control the TCXO on/off from DIO3
typedef enum {
    TCXO_CTRL_1_6V = 0x00,
    TCXO_CTRL_1_7V = 0x01,
    TCXO_CTRL_1_8V = 0x02,
    TCXO_CTRL_2_2V = 0x03,
    TCXO_CTRL_2_4V = 0x04,
    TCXO_CTRL_2_7V = 0x05,
    TCXO_CTRL_3_0V = 0x06,
    TCXO_CTRL_3_3V = 0x07,
} RadioTcxoCtrlVoltage_t;

// Represents the interruption masks available for the radio
// remark: Note that not all these interruptions are available for all packet
// types
typedef enum {
    IRQ_RADIO_NONE = 0x0000,
    IRQ_TX_DONE = 0x0001,
    IRQ_RX_DONE = 0x0002,
    IRQ_PREAMBLE_DETECTED = 0x0004,
    IRQ_SYNCWORD_VALID = 0x0008,
    IRQ_HEADER_VALID = 0x0010,
    IRQ_HEADER_ERROR = 0x0020,
    IRQ_CRC_ERROR = 0x0040,
    IRQ_CAD_DONE = 0x0080,
    IRQ_CAD_ACTIVITY_DETECTED = 0x0100,
    IRQ_RX_TX_TIMEOUT = 0x0200,
    IRQ_RADIO_ALL = 0xFFFF,
} RadioIrqMasks_t;

/*Declares the oscillator in use while in standby mode
 *
 * Using the STDBY_RC standby mode allow to reduce the energy consumption
 * STDBY_XOSC should be used for time critical applications
 */
typedef enum {
    STDBY_RC = 0x00,
    STDBY_XOSC = 0x01,
} RadioStandbyModes_t;

/*!
 * \brief Represents the operating mode the radio is actually running
 */
typedef enum {
    MODE_SLEEP = 0x00, //! The radio is in sleep mode
    MODE_STDBY_RC,     //! The radio is in standby mode with RC oscillator
    MODE_STDBY_XOSC,   //! The radio is in standby mode with XOSC oscillator
    MODE_FS,           //! The radio is in frequency synthesis mode
    MODE_TX,           //! The radio is in transmit mode
    MODE_RX,           //! The radio is in receive mode
    MODE_RX_DC,        //! The radio is in receive duty cycle mode
    MODE_CAD           //! The radio is in channel activity detection mode
} RadioOperatingModes_t;

// The type describing the modulation parameters for every packet types
typedef struct {
    RadioPacketTypes_t PacketType; //!< Packet to which the modulation
                                   //!< parameters are referring to.
    struct {
        struct {
            uint32_t BitRate;
            uint32_t Fdev;
            RadioModShapings_t ModulationShaping;
            uint8_t Bandwidth;
        } Gfsk;
        struct {
            RadioLoRaSpreadingFactors_t
                SpreadingFactor; //!< Spreading Factor for the LoRa modulation
            RadioLoRaBandwidths_t
                Bandwidth; //!< Bandwidth for the LoRa modulation
            RadioLoRaCodingRates_t
                CodingRate; //!< Coding rate for the LoRa modulation
            uint8_t LowDatarateOptimize; //!< Indicates if the modem uses the
                                         //!< low datarate optimization
        } LoRa;
    } Params; //!< Holds the modulation parameters structure
} ModulationParams_t;

// The type describing the packet parameters for every packet types
typedef struct {
    RadioPacketTypes_t
        PacketType; //!< Packet to which the packet parameters are referring to.
    struct {
        /*!
         * \brief Holds the GFSK packet parameters
         */
        struct {
            uint16_t PreambleLength; //!< The preamble Tx length for GFSK packet
                                     //!< type in bit
            RadioPreambleDetection_t
                PreambleMinDetect;  //!< The preamble Rx length minimal for GFSK
                                    //!< packet type
            uint8_t SyncWordLength; //!< The synchronization word length for
                                    //!< GFSK packet type
            RadioAddressComp_t AddrComp; //!< Activated SyncWord correlators
            RadioPacketLengthModes_t
                HeaderType; //!< If the header is explicit, it will be
                            //!< transmitted in the GFSK packet. If the header
                            //!< is implicit, it will not be transmitted
            uint8_t PayloadLength; //!< Size of the payload in the GFSK packet
            RadioCrcTypes_t
                CrcLength; //!< Size of the CRC block in the GFSK packet
            RadioDcFree_t DcFree;
        } Gfsk;
        /*!
         * \brief Holds the LoRa packet parameters
         */
        struct {
            uint16_t PreambleLength; //!< The preamble length is the number of
                                     //!< LoRa symbols in the preamble
            RadioLoRaPacketLengthsMode_t
                HeaderType; //!< If the header is explicit, it will be
                            //!< transmitted in the LoRa packet. If the header
                            //!< is implicit, it will not be transmitted
            uint8_t PayloadLength; //!< Size of the payload in the LoRa packet
            RadioLoRaCrcModes_t CrcMode; //!< Size of CRC block in LoRa packet
            RadioLoRaIQModes_t InvertIQ; //!< Allows to swap IQ for LoRa packet
        } LoRa;
    } Params; //!< Holds the packet parameters structure
} PacketParams_t;

// Represents the packet status for every packet type
typedef struct {
    RadioPacketTypes_t
        packetType; //!< Packet to which the packet status are referring to.
    struct {
        struct {
            uint8_t RxStatus;
            int8_t RssiAvg;  //!< The averaged RSSI
            int8_t RssiSync; //!< The RSSI measured on last packet
            uint32_t FreqError;
        } Gfsk;
        struct {
            int8_t RssiPkt; //!< The RSSI of the last packet
            int8_t SnrPkt;  //!< The SNR of the last packet
            int8_t SignalRssiPkt;
            uint32_t FreqError;
        } LoRa;
    } Params;
} PacketStatus_t;

// Represents the Rx internal counters values when GFSK or LoRa packet type is
// used
typedef struct {
    RadioPacketTypes_t
        packetType; //!< Packet to which the packet status are referring to.
    uint16_t PacketReceived;
    uint16_t CrcOk;
    uint16_t LengthError;
} RxCounter_t;

// Represents a calibration configuration
typedef union {
    struct {
        uint8_t RC64KEnable : 1;    //!< Calibrate RC64K clock
        uint8_t RC13MEnable : 1;    //!< Calibrate RC13M clock
        uint8_t PLLEnable : 1;      //!< Calibrate PLL
        uint8_t ADCPulseEnable : 1; //!< Calibrate ADC Pulse
        uint8_t ADCBulkNEnable : 1; //!< Calibrate ADC bulkN
        uint8_t ADCBulkPEnable : 1; //!< Calibrate ADC bulkP
        uint8_t ImgEnable : 1;
        uint8_t : 1;
    } Fields;
    uint8_t Value;
} CalibrationParams_t;

// Represents a sleep mode configuration
typedef union {
    struct {
        uint8_t WakeUpRTC : 1; //!< Get out of sleep mode if wakeup signal
                               //!< received from RTC
        uint8_t Reset : 1;
        uint8_t WarmStart : 1;
        uint8_t Reserved : 5;
    } Fields;
    uint8_t Value;
} SleepParams_t;

// Represents the possible radio system error states
typedef union {
    struct {
        uint8_t Rc64kCalib : 1; //!< RC 64kHz oscillator calibration failed
        uint8_t Rc13mCalib : 1; //!< RC 13MHz oscillator calibration failed
        uint8_t PllCalib : 1;   //!< PLL calibration failed
        uint8_t AdcCalib : 1;   //!< ADC calibration failed
        uint8_t ImgCalib : 1;   //!< Image calibration failed
        uint8_t XoscStart : 1;  //!< XOSC oscillator failed to start
        uint8_t PllLock : 1;    //!< PLL lock failed
        uint8_t BuckStart : 1;  //!< Buck converter failed to start
        uint8_t PaRamp : 1;     //!< PA ramp failed
        uint8_t : 7;            //!< Reserved
    } Fields;
    uint16_t Value;
} RadioError_t;

// Structure describing the error codes for callback functions
typedef enum {
    IRQ_HEADER_ERROR_CODE = 0x01,
    IRQ_SYNCWORD_ERROR_CODE = 0x02,
    IRQ_CRC_ERROR_CODE = 0x04,
} IrqErrorCode_t;

typedef struct {
    RadioPacketTypes_t packetType;
    int8_t txPower;
    RadioRampTimes_t txRampTime;
    ModulationParams_t modParams;
    PacketParams_t packetParams;
    uint32_t rfFrequency;
    uint16_t irqTx;
    uint16_t irqRx;
    uint32_t txTimeout;
    uint32_t rxTimeout;
} RadioConfigurations_t;

// Functions called externally

// initialise LoRa module
// params: payload length of TX data package
void lora_init(uint8_t payload_length);

void lora_reset(void);

// only to test if it transmits at all, no further use
void lora_send_test(void);

// only to test if something is received, displays message over serial
void lora_receive_test(void);

// same test only with known predefined RX pointer and payload length of 4
void lora_fixed_receive_test(void);

// same as receive test but with boosted RX gain for ca. 3dB higher sensitivity
void lora_receive_test_boosted(void);

// input number of ping attempts, return value number of successful responses
uint8_t lora_ping(uint8_t count);

// test write and read buffer
void lora_payload_debug_test(void);

// Internal functions

void lora_pins_init(void);
// pins init
void nss_init(void);
void nreset_init(void);
void ant_power_init(void);
void busy_init(void);
void xtal_sel_init(void);

// void set_packet_type(RadioPacketTypes_t packet_type);

void set_payload(uint8_t *payload, uint8_t length);

/*!
 * \brief Reads the payload received. If the received payload is longer
 * than maxSize, then the method returns 1 and do not set size and payload.
 *
 * \param [out] payload       A pointer to a buffer into which the payload will
 * be copied \param [out] size          A pointer to the size of the payload
 * received \param [in]  maxSize       The maximal size allowed to copy into the
 * buffer
 */
uint8_t get_payload(uint8_t *buffer, uint8_t *size, uint8_t maxSize);

// get payload of fixed length at fixed rx pointer
void get_fixed_payload(uint8_t *buffer, uint8_t size);

/*!
 * \brief Gets the last received packet buffer status
 *
 * \param [out] payloadLength Last received packet payload length
 * \param [out] rxStartBuffer Last received packet buffer address pointer
 */
void get_rx_buffer_status(uint8_t *payloadLength, uint8_t *rxStartBuffer);

void write_buffer(uint8_t offset, uint8_t *buffer, uint8_t length);

/*!
 * \brief Reads Radio Data Buffer at offset to buffer of size
 *
 * \param [in]  offset        Offset where to start reading
 * \param [out] buffer        Buffer pointer
 * \param [in]  size          Buffer size
 */
void read_buffer(uint8_t offset, uint8_t *buffer, uint8_t size);

/*!
 * \brief Sets the data buffer base address for transmission and reception
 *
 * \param [in]  txBaseAddress Transmission base address
 * \param [in]  rxBaseAddress Reception base address
 */
void set_buffer_base_addresses(uint8_t txBaseAddress, uint8_t rxBaseAddress);

/*!
 * \brief Writes the given command to the radio
 *
 * \param [in]  opcode        Command opcode
 * \param [in]  buffer        Command parameters byte array
 * \param [in]  size          Command parameters byte array size
 */
void write_command(RadioCommands_t command, uint8_t *buffer, uint16_t size);

/*!
 * \brief Reads the given command from the radio
 *
 * \param [in]  opcode        Command opcode
 * \param [in]  buffer        Command parameters byte array
 * \param [in]  size          Command parameters byte array size
 */
void read_command(RadioCommands_t opcode, uint8_t *buffer, uint16_t size);

/*!
 * \brief Returns the current IRQ status
 *
 * \retval      irqStatus     IRQ status
 */
uint16_t get_irq_status(void);

// set module to transmission mode, transmits data stored in internal buffer
// configured by set_payload function
void transmit_buffer(uint32_t timeout);

// set module to receive mode, writes received data into internal buffer
void receive_buffer(uint32_t timeout);

// set module to receive mode with boosted gain, writes received data into
// internal buffer
void receive_buffer_boosted(uint32_t timeout);

// wait until ISR sets either txDone or txTimeout flags
// NOTE: after this function, poll the Radio Flags
void wait_tx_done(void);

// wait until ISR sets either rxDone, rxError or rxTimeout flags
// NOTE: after this function, poll the Radio Flags
void wait_rx_done(void);

// pins toggle functions
void nss_set(void); //-> active low
void nss_reset(void);

void nreset_set(void); //-> active low
void nreset_reset(void);

void ant_power_on(void); // antenna power switch
void ant_power_off(void);

// pins read functions
GPIO_PinState busy_read(void);
GPIO_PinState xtal_sel_read(void);

// Callback functions prototypes

// Function to be executed on Radio Tx Done event
void tx_done(void);

// Function to be executed on Radio Rx Done event
void rx_done(void);

// Function executed on Radio Tx Timeout event
void tx_timeout(void);

// Function executed on Radio Rx Timeout event
void rx_timeout(void);

// Function executed on Radio Rx Error event
void rx_error(IrqErrorCode_t errCode);

void cad_done(uint8_t cadFlag);

// Function executed on Radio Fhss Change Channel event
// void OnFhssChangeChannel( uint8_t channelIndex );

// delay in case HAL_Delay() doesn't work
void dummy_delay(void);

// wait until busy pin resets
void wait_busy(void);

// wake up from sleep mode
void wakeup(void);

void set_standby(RadioStandbyModes_t standby_config);

void set_fs(void);

void set_configuration(RadioConfigurations_t *config);

void configure_radio(RadioConfigurations_t *config);

void set_packet_type(RadioPacketTypes_t packetType);

void set_packet_params(PacketParams_t *packetParams);

void set_modulation_params(ModulationParams_t *modulationParams);

void set_rf_frequency(uint32_t frequency);

void calibrate_image(uint32_t freq);

void set_tx_params(int8_t power, RadioRampTimes_t rampTime);

void write_register(uint16_t address, uint8_t *buffer, uint16_t size);

void write_reg(uint16_t address, uint8_t value);

/*!
 * \brief Read data from the radio memory
 *
 * \param [in]  address       The address of the first byte to read from the
 * radio \param [out] buffer        The buffer that holds data read from radio
 * \param [in]  size          The number of bytes to read from radio's memory
 */
void read_register(uint16_t address, uint8_t *buffer, uint16_t size);

/*!
 * \brief Read a single byte of data from the radio memory
 *
 * \param [in]  address       The address of the first byte to write in the
 *                            radio
 *
 * \retval      value         The value of the byte at the given address in
 *                            radio's memory
 */
uint8_t read_reg(uint16_t address);

/*
 *	Sets the transmission parameters of Power Amplifier
 * \param [in]  paDutyCycle     Duty Cycle for the PA
 * \param [in]  HpMax           0 for sx1261, 7 for sx1262
 * \param [in]  deviceSel       1 for sx1261, 0 for sx1262
 * \param [in]  paLUT           0 for 14dBm LUT, 1 for 22dBm LUT
 */
void set_pa_config(uint8_t paDutyCycle, uint8_t HpMax, uint8_t deviceSel,
                   uint8_t paLUT);

void set_irq_tx(void);

void reset_tx_done_flag(void);

void reset_tx_timeout_flag(void);

void reset_rx_done_flag(void);

void reset_rx_timeout_flag(void);

void reset_rx_error_flag(void);

void reset_all_flags(void);

void set_irq_rx(void);

void set_irq_tx_rx(void);

void reset_all_irq(void);

void set_dio_irq_params(uint16_t irqMask, uint16_t dio1Mask, uint16_t dio2Mask,
                        uint16_t dio3Mask);

// get RSSI and SNR values of last received package
void get_rssi_snr(int8_t *rssi, int8_t *snr);

// print RSSI and SNR values of last package over serial
void print_rssi_snr(void);

// use RF module to get a random 32 bit number
uint32_t get_random_number(void);

// single long LD2 user LED blink to signal success of some sort, to use for
// simple display in case Serial is not connected
void led_blink_complete_success(void);

// two long blinks
void led_blink_partial_success(void);

// three shorter blinks
void led_blink_partial_failure(void);

// seven short blinks
void led_blink_complete_failure(void);

void user_led_init(void);

/*!
 * \brief Clears the IRQs on RF module
 *
 * \param [in]  irq           IRQ(s) to be cleared
 */
void clear_irq_status(uint16_t irq);

/*!
 * \brief Gets the last received packet payload length
 *
 * \param [out] pktStatus     A structure of packet status
 */
void get_packet_status(PacketStatus_t *pktStatus);

// process the IRQs and calls functions to set flags accordingly
// note: call when general IRQ flag is set by the ISR
void process_irqs(void);

// remove irq from the variable irqRegs
// NOTE: does NOT clear IRQ in RF module(refer to clear_irq_status), only from
// the variable which stores the IRQs locally
void remove_irq(uint16_t irq);

// irq callback for external irq
void lora_irq_callback(void);

/// @}

#endif
