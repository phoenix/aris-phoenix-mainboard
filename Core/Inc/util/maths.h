/**
 *******************************************************************************
 * @file           maths.h
 * @brief          Utility functions and definitions for maths
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include <stdbool.h>

/*******************************************************************************
 * Global macros
 ******************************************************************************/

/** Explicit floating-point pi for efficient floating-point calculations */
#define M_PIf 3.14159265358979323846f
/** Explicit floating-point 2*pi constant */
#define M_2PIf 6.28318530717958647692f
/** Macro for squaring a number */
#define sq(x) ((x) * (x))

/*******************************************************************************
 * Vectors
 ******************************************************************************/

/**
 * @brief Universal vector in 3D space.
 * Used for geometric operations like quaternion rotations.
 */
union Vector3 {
    float array[3]; ///< array form of the vector
    struct __attribute__((packed)) {
        float x; ///< x component of the vector
        float y; ///< y component of the vector
        float z; ///< z component of the vector
    };
};

/*******************************************************************************
 * Quaternions
 ******************************************************************************/


/**
 * A union that represents a quaternion.
 */
union Quaternion {
    float array[4]; ///< Array containing the x,y,z,w components of the quatrn
    /** Shortcut struct to access the different array members by names. */
    struct __attribute__((packed)) {
        float x; ///< x component of the quaternion
        float y; ///< y component of the quaternion
        float z; ///< z component of the quaternion
        float w; ///< w component of the quaternion
    };
};


/**
 * @brief Returns the norm of the quaternion.
 * @returns the norm of the quaternion or negative if q is NULL.
 */
float quaternion_norm(union Quaternion *q);

/**
 * @brief Normalizes the quaternion passed to it.
 *
 * The function will try to normalize the quaternion by calculating its norm and
 * then multiplying by the inverse of the norm. If the norm happens to be zero,
 * i.e. the quaternion is non-normalizable, it will not change the quaternion
 * and instead return false.
 * @param q Pointer to a quaternion.
 * @returns true if the quaternion could be normalized, false if otherwise
 */
bool normalize_quaternion(union Quaternion *q);

/**
 * @brief Inverts a quaternion into its conjugate.
 *
 * A quaternion [a + ib + jc + kd] has a conjugate that is defined by
 * [a + ib + jc + kd]^* = [a - ib -jc - kd]
 * @returns true if the conjugation was sucessful (q != NULL), false otherwise
 */
bool conjugate_quaternion(union Quaternion *q);

/**
 * @brief Computes the hamilton product of the two quaternions a and b.
 *
 * If either one of the pointers is equal to NULL, the return value will be a
 * zeroed out quaternion.
 * @returns the hamilton product or a zeroed struct if an error occurred.
 */
union Quaternion hamilton_product(union Quaternion *a, union Quaternion *b);

/**
 * @brief Rotates a vector by a unit quaternion.
 * @param vec The vector to be rotated.
 * @param q The unit quaternion that describes the vector.
 * @pre None of the arguments must be NULL. The quaternion q must be normalized.
 * @returns true if the rotation could be completed, false if an error occurred
 */
bool rotate_by_quaternion(union Vector3 *vec, union Quaternion *q);

/*******************************************************************************
 * Angle mathematics: Conversion and mean
 ******************************************************************************/

/**
 * Convert angle in degrees to radians
 */
double deg2rad(double angle);

/**
 * Convert angle in radians to degrees
 */
double rad2deg(double radians);


/**
 * Finds the mean of the two angles a and b, both given in radians
 * @param a The first angle, given in radians
 * @param b The second angle, given in radians
 * @returns The average angle, wrapped to an interval from -pi to pi
 */
float mean_angle(float a, float b);

/**
 * Wraps an angle in radians to the interval [-pi, pi].
 * @param angle The angle to wrap.
 * @returns The angle, wrapped to the [-pi, pi] interval.
 */
float wrap_to_pi(float angle);

/**
 * Wraps an angle in radians to the interval [0, 2pi].
 * @param angle The angle to wrap.
 * @returns The angle, wrapped to the [0, 2pi] interval.
 */
float wrap_to_2pi(float angle);