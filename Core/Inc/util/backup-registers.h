/**
 *******************************************************************************
 * @file           backup-registers.h
 * @brief          Interface for persisting variables through a system reset
 *                      by using RTC backup registers.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

/* Exported constants ------------------------------------------------------- */

/** The backup register where the current state of the watchdog is stored. */
#define BACKUP_REGISTER_WATCHDOG_STATE 12

/* Public functions --------------------------------------------------------- */

/**
 * @brief Write to one 32-bit backup register.
 * @param reg_no The backup register number, from 0 to 19
 * @param value The value that should be written to that register.
 * @returns true if successfully written, false if wrong parameters or on error
 */
bool rtc_backup_register_write(uint8_t reg_no, uint32_t value);

/**
 * @brief Read from one 32-bit backup register.
 * @param reg_no The backup register number, from 0 to 19
 * @param value The location where the read value should be written to.
 * @returns true if successfully read, false if wrong parameters or on error
 */
bool rtc_backup_register_read(uint8_t reg_no, uint32_t *value);
