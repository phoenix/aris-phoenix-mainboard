/**
 *******************************************************************************
 * @file           watchdog.h
 * @brief          Interface for managing the safety watchdog.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

/* Public functions --------------------------------------------------------- */

/**
 * @brief Enable the watchdog after a timer has elapsed.
 *
 * This function calls the generated IWDG initialization code after a timer has
 * elapsed. Before the timer elapses, the initialization can be aborted, i.e.
 * the watchdog will not be enabled. After the timer has elapsed and the IWDG is
 * enabled, a "Watchdog State" variable will be written to the RTC backup
 * memory. On restart, the main code will call @see
 * watchdog_should_trigger_deployment to trigger the deployment servos if this
 * variable has not been reset in the mean time.
 */
void watchdog_enable_on_timer(uint32_t ms);

/**
 * @brief Disable the watchdog (if the timer has not elapsed) or stop deployment
 *
 * This function will try to stop the timer that was started with @see
 * watchdog_enable_on_timer. If the timer has already elapsed, the IWDG can't be
 * stopped, but the RTC backup register will be cleared such that on potential
 * reset, the deployment will NOT happen.
 */
void watchdog_disable(void);

/**
 * @brief Checks if deployment should immediately happen.
 *
 * The preconditions for deployment to happen are:
 * - The current startup comes after a watchdog-triggered RESET, which can be
 *      detected in the registers.
 * - The "Watchdog State" RTC backup register has not been cleared, i.e. it
 *      hasn't been tried to stop the watchdog.
 * @returns true if the deployment should be triggered, false otherwise
 */
bool watchdog_should_trigger_deployment(void);

/**
 * @brief Refreshes the watchdog.
 */
void watchdog_refresh(void);
