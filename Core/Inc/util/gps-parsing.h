/**
 *******************************************************************************
 * @file           gps-parsing.h
 * @brief          Contains parsing for NMEA-formatted GPS messages.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

// DISCLAIMER: This code is based on the GPS implementation in the BetaFlight
// open source flight controller software. The code has been adapted to use with
// our software.

#define GPS_SV_MAXSATS 16

/* BetaFlight Implementation Structs ---------------------------------------- */

enum GPSFrame {
    NO_FRAME = 0,
    FRAME_GGA = 1,
    FRAME_RMC = 2,
    FRAME_GSV = 3,
};

/**
 * @brief Internal struct used by the NMEA parsing.
 *
 * Also to be zero-initialized.
 */
typedef struct gpsDataNmea_s {
    int32_t latitude;
    int32_t longitude;
    uint8_t numSat;
    int32_t altitudeCm;
    uint16_t speed;
    uint16_t hdop;
    uint16_t ground_course;
    uint32_t time;
    uint32_t date;
} gpsDataNmea_t;

/**
 * @brief Latitude, longitude and height position in the NEU system.
 */
typedef struct gpsLocation_s {
    int32_t lat;   // latitude * 1e+7
    int32_t lon;   // longitude * 1e+7
    int32_t altCm; // altitude in 0.01m
} gpsLocation_t;

typedef struct gpsSolutionData_s {
    gpsLocation_t llh;
    uint16_t speed3d;      // speed in 0.1m/s
    uint16_t groundSpeed;  // speed in 0.1m/s
    uint16_t groundCourse; // degrees * 10
    uint16_t hdop;         // generic HDOP value (*100)
    uint8_t numSat;
} gpsSolutionData_t;

/**
 * @brief This struct saves the state of the NMEA parsing of one UART stream.
 *
 * This struct saves all the variables needed for saving the state of the NMEA
 * parsing that has been taken over from the BetaFlight flight controller
 * software.
 */
struct NMEA_State {
    uint8_t param;
    uint8_t offset;
    uint8_t parity;
    uint8_t checksum_param;
    enum GPSFrame gps_frame;
    uint8_t svMessageNum;
    gpsDataNmea_t gps_Msg;
    gpsSolutionData_t gpsSol;
    uint32_t GPS_packetCount;
    uint32_t GPS_svInfoReceivedCount;
    uint8_t GPS_update;
    uint8_t GPS_numCh;                          // Number of channels
    uint8_t GPS_svinfo_chn[GPS_SV_MAXSATS];     // Channel number
    uint8_t GPS_svinfo_svid[GPS_SV_MAXSATS];    // Satellite ID
    uint8_t GPS_svinfo_quality[GPS_SV_MAXSATS]; // Bitfield Qualtity
    uint8_t GPS_svinfo_cno[GPS_SV_MAXSATS]; // Carrier to Noise Ratio (Signal
                                            // Strength)
    char string[15];
    uint8_t GPS_FIX;
};

/* Public functions --------------------------------------------------------- */

/**
 * @brief Parse a new character that has been received.
 * @param c The new character that was received.
 * @param state The current state of the NMEA. Zero-initialized on first call.
 * @returns true if the data set was completed, false otherwise.
 */
bool nmea_parse_new_char(uint8_t c, struct NMEA_State *state);