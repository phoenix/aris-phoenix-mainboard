/**
 *
 *
 *
 *
 *
 */

#ifndef LORA_PAYLOADS_H
#define LORA_PAYLOADS_H

#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_spi.h"
#include <stdint.h>

#define GROUND_TX_PAYLOAD_SIZE   8 // Define fixed Ground TX payload size here
#define AIRBORNE_TX_PAYLOAD_SIZE 8 // Define fixed Airborne X payload size here

// payload identifiers to announce payload type
// identifier consists of 2x 4 bits, whereas the latter are mirrored to make the
// header symmetrical so that a shift of the payload automatically results in a
// header that won't be processed

typedef enum {

    PING = 0x00,
    PONG = 0x18,
    ARMING_REQUEST = 0x24,
    ARMING_ACK = 0x3C,
    STATUS_REPORT_REQUEST = 0x42,
    STATUS_REPORT = 0x5A,
    COUNTDOWN_REQUEST = 0x66,
    COUNTDOWN_ACK = 0x7E,
    EMERGENCY_DEPLOYMENT_REQUEST = 0x81,
    EMERGENCY_DEPLOYMENT_ACK = 0x99, // NOT USED
    TELEMETRY_REQUEST = 0xA5,
    TELEMETRY_DATA = 0xBD,
    FSM_COMMAND = 0xC3,
    FSM_COMMAND_ACK = 0xDB,
    SOFTWARE_RESET_REQUEST = 0xE7,
    INVALID = 0xFF, // can be set to invalid to avoid transmission in certain
                    // implementations

    // MANUAL_STEERING_REQUEST			=	0xE1,	//only a reminder, this header has been
    // reused
    // MANUAL_STEERING_ACK				= 	0xF0,	//only a reminder, this header has been
    // reused

} Identifiers_t;

// used to manually send commands for system FSM
typedef enum {

    Transition_Calibrate = 0x80,
    Transition_WarmUp = 0x81,
    Transition_Arm = 0x82,  // done in separate payload type ARMING REQUEST
    Transition_Drop = 0x83, // done in separate payload type COUNTDOWN REQUEST
    Transition_TriggerDeployment =
        0x84, // done in separate payload type EMERGENCY DEPLOYMENT REQUEST
    Transition_TriggerApproach = 0x85,
    Transition_TouchdownDetected = 0x86,
    Transition_Disarm = 0x87,
    Transition_Increment = 0x88,
    COMMAND_1 = 0x89,
    COMMAND_2 = 0x8A,
    COMMAND_3 = 0x8B,
    COMMAND_4 = 0x8C,
    COMMAND_5 = 0x8D,
    COMMAND_6 = 0x8E,
    COMMAND_SOFTWARE_RESET = 0x8F,

} FSMCommand_t;

typedef enum {

    fsm_Stuck = 0x80,
    fsm_InitialState = 0x81,
    fsm_CalibratedState = 0x82,
    fsm_LiveState = 0x83,
    fsm_ArmedState = 0x84,
    fsm_DrogueDescentState = 0x85,
    fsm_GuidedDescentState = 0x86,
    fsm_FinalApproachState = 0x87,
    fsm_TouchdownState = 0x88,
    fsm_DisarmedState = 0x89,

} FSMState_t;



typedef union{

	union{

		struct{
			uint8_t PayloadType;
			uint8_t dat0;
			uint8_t dat1;
			uint8_t dat2;
			uint8_t dat3;
			uint8_t dat4;
			uint8_t dat5;
			uint8_t checksum;
		}Raw;

		struct{
			uint8_t PayloadType;

			int8_t			RSSI;
			int8_t			SNR;
			uint8_t 		on_air_time0;
			uint8_t			on_air_time1;
			uint8_t			on_air_time2;
			uint8_t			on_air_time3;
			uint8_t			checksum;

		}Ping;

		struct{
			uint8_t PayloadType;

			uint8_t	fsm_state;			//8 bits

			uint8_t sd				:1;
			uint8_t error			:1;
			uint8_t ready			:1;
			uint8_t receiver_mcu	:1;
			uint8_t icsb1			:1;
			uint8_t icsb2			:1;
			uint8_t baro1			:1;
			uint8_t baro2			:1;

			uint8_t imu1			:1;
			uint8_t imu2			:1;
			uint8_t gps1			:1;
			uint8_t gps1_lock		:1;
			uint8_t gps2			:1;
			uint8_t gps2_lock		:1;
			uint8_t gps3			:1;
			uint8_t gps3_lock		:1;

			uint8_t error_code;

			uint8_t			on_air_time0;			//this value contains total lora on-air time of in ms in current session
			uint8_t			on_air_time1;
			uint8_t			checksum;

		}StatusReport;


		struct{
			uint8_t PayloadType;

			uint8_t 		fsm_command;
			uint8_t 		fsm_state;
			uint8_t 		on_air_time0;
			uint8_t			on_air_time1;
			uint8_t			on_air_time2;
			uint8_t			on_air_time3;
			uint8_t 		checksum;

		}FSMCommand;


		struct{
			uint8_t PayloadType;

			uint8_t z0;
			uint8_t z1;

			uint8_t x0;
			uint8_t x1;

			uint8_t y0;
			uint8_t y1;

			uint8_t checksum;

		}Coordinates;

	}Data;

	uint8_t Raw[AIRBORNE_TX_PAYLOAD_SIZE];

}PayloadAirborne_t;



typedef union{

	union{

		struct{
			uint8_t PayloadType;
			uint8_t dat0;
			uint8_t dat1;
			uint8_t dat2;
			uint8_t dat3;
			uint8_t dat4;
			uint8_t dat5;
			uint8_t checksum;
		}Raw;

		struct{
			uint8_t PayloadType;

			uint8_t baro			:2;			// 0 - 2
			uint8_t imu				:2;			// 0 - 2
			uint8_t gps_active		:2;			// 0 - 3
			uint8_t gps_lock		:2;			// 0 - 3

			uint8_t icsb			:2;			// 0 - 2
			uint8_t receiver_mcu	:1;			// 0 or 1
			uint8_t sd				:1;			// 0 or 1
			uint8_t reserved		:4;			// reserved

			uint8_t reserved0;
			uint8_t reserved1;
			uint8_t reserved2;
			uint8_t reserved3;

			uint8_t checksum;

		}ArmingRequirements;


		struct{
			uint8_t PayloadType;

			uint8_t cmd;
			uint8_t reserved;

			uint8_t reserved0;
			uint8_t reserved1;
			uint8_t reserved2;
			uint8_t reserved3;

			uint8_t checksum;

		}FSMCommand;


		struct{
			uint8_t PayloadType;

			uint8_t timestamp;
			uint8_t reserved;

			uint8_t reserved0;
			uint8_t reserved1;
			uint8_t reserved2;
			uint8_t reserved3;

			uint8_t checksum;

		}Countdown;

	}Data;

	uint8_t Raw[GROUND_TX_PAYLOAD_SIZE];

}PayloadGround_t;




//whitening by xor-ing signature onto message
//NOTE: assumes maximum payload length of 14 bytes
//params: payload, length
void signature_whitening(uint8_t* msg, uint8_t length);

//encrypt air message
void encrypt_air(uint8_t* msg);


//decrypt air message
//return val bool true if payload seems valid, false if invalid
//NOTE: does not check the identifier, only the checksum!
uint8_t decrypt_air(uint8_t* msg);


//encrypt ground message
void encrypt_ground(uint8_t* msg);

//decrypt ground message
//return val bool true if payload seems valid, false if invalid
//NOTE: does not check the identifier, only the checksum!
uint8_t decrypt_ground(uint8_t* msg);

uint8_t identifier_check(uint8_t identifier);

void set_checksum(uint8_t* msg, uint8_t length);

uint8_t check_checksum(uint8_t* msg, uint8_t length);

void write_u16(uint16_t data, uint8_t* dest);
uint16_t read_u16(uint8_t* source);

void write_u32(uint32_t data, uint8_t* dest);
uint32_t read_u32(uint8_t* source);


void write_s16(int16_t data, uint8_t* dest);
int16_t read_s16(uint8_t* source);

void write_s32(int32_t data, uint8_t* dest);
int32_t read_s32(uint8_t* source);


#endif
