/**
 *******************************************************************************
 * @file           data.h
 * @brief          Declares the data types for the data being collected.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-20211 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "Data_C/Data_C.h"
#include "config.h"
#include "util/maths.h"

/* Data Types needed for control systems ------------------------------------ */

/** The output of the attitude controller. */
struct AttitudeControllerOutput {
    float p_ref; ///< Reference yaw rate.
    float q_ref; ///< Reference pitch rate.
};

/** The output of the bodyrate controller. */
struct BodyRateControllerOutput {
    float delta_a; ///< Asynchronous pulling of the lines
    float delta_f; ///< Synchronous pulling of the lines
    float i_1;
    float i_2;
};

/** Reference attitude set by the guidance for the attitude controller. */
struct ReferenceAttitude {
    float pitch_ref; ///< Reference pitch angle [rad]
    float yaw_ref;   ///< Reference yaw angle [rad]
    int guidance_mode;
    bool controller_enabled;
};

/** Reference for the motor controller, avoiding saturation effects */
struct InputHandlingOutput {
    float delta_a_clean;
    float delta_f_clean;

    float Delta_L;
    float Delta_R;
};

/** Struct describing the wind */
struct Wind {
    float x;
    float y;
    float z;
};

/* Raw register data -------------------------------------------------------- */

// This section represents one physical quantity measured by a sensor in its raw
// register form. These do not have units, but must first be converted using the
// methods provided in this header file.

/**
 * @brief A union containing heading information gathered from a sensor.
 * @note This struct is labelled "raw", meaning it must not yet contain
 * physically meaningful data. The appropriate conversion function should be
 * used to transform it into a non-raw version.
 */
union RawHeading {
    int16_t array[3]; ///< Array containing the yaw, pitch and roll as uint16_ts
    // Packed struct to ensure that no padding is inserted and the array/struct
    // share the exact same memory
    /** Shortcut struct to access the different array members by names. */
    struct __attribute__((packed)) {
        int16_t yaw;   ///< The yaw angle as measured by the sensor.
        int16_t roll;  ///< The roll angle as measured by the sensor.
        int16_t pitch; ///< The pitch angle as measured by the sensor.
    };
};

/**
 * @brief A union containing linear acceleration information from a sensor.
 * @note This struct is labelled "raw", meaning it must not yet contain
 * physically meaningful data. The appropriate conversion function should be
 * used to transform it into a non-raw version.
 */
union RawLinearAcceleration {
    int16_t array[3]; ///< Array containing the x-, y- and z-acceleration.
    // Packed struct to ensure that no padding is inserted and the array/struct
    // share the exact same memory
    /** Shortcut struct to access the different array members by names. */
    struct __attribute__((packed)) {
        int16_t x; ///< The linear acceleration in the x direction.
        int16_t y; ///< The linear acceleration in the y direction.
        int16_t z; ///< The linear acceleration in the z direction.
    };
};

/**
 * @brief Union containing angular velocity information from a sensor.
 * @note This struct is labelled "raw", meaning it must not yet contain
 * physically meaningful data. The appropriate conversion function should be
 * used to transform it into a non-raw version.
 */
union RawAngularVelocity {
    int16_t array[3]; ///< Array containing the x, y and z angular velocity.
    // Packed struct to ensure that no padding is inserted and the array/struct
    // share the exact same memory
    /** Shortcut struct to access the different array members by names. */
    struct __attribute__((packed)) {
        int16_t p; ///< The angular velocity component in the x dir.
        int16_t q; ///< The angular velocity component in the y dir.
        int16_t r; ///< The angular velocity component in the z dir.
    };
};

/**
 * @brief A union containing quaternion information from a sensor.
 * @note This struct is labelled "raw", meaning it must not yet contain
 * physically meaningful data. The appropriate conversion function should be
 * used to transform it into a non-raw version.
 */
union RawQuaternion {
    int16_t array[4]; ///< Array containing the x,y,z,w components of the quatrn
    /** Shortcut struct to access the different array members by names. */
    struct __attribute__((packed)) {
        int16_t x; ///< x component of the quaternion
        int16_t y; ///< y component of the quaternion
        int16_t z; ///< z component of the quaternion
        int16_t w; ///< w component of the quaternion
    };
};

/**
 * @brief The raw pressure output as measured by a pressure sensor.
 */
typedef uint32_t RawPressure;

/**
 * @brief The raw temperature output as measured by a temperature sensor.
 */
typedef int16_t RawTemperature;

/* Physical data ------------------------------------------------------------ */
// This section declares types that represent a physical quantity.

/**
 * @brief The position of the guided recovery system with respect to the
 * desired landing position.
 *
 * The desired landing position is in the origin of the coordinate frame.
 * Coordinates are expressed with respect to the NED frame.
 */
union Position {
    float array[3]; ///< Array containing the positions as floats.
    struct __attribute__((packed)) {
        float north; ///< North position [m]
        float east;  ///< East position [m]
        float down;  ///< Down position [m]
    };
};

/**
 * @brief The position of the system w.r.t. the geodetic coordinate system.
 *
 * This is the position as given by the GPS receiver. It can be converted to a
 * NED position using the conversion functions @see geo2ned.
 */
union GeodeticPosition {
    double array[3]; ///< Array containing the geodetic position as floats
    /** Shortcut struct to access the array members by names. */
    struct __attribute__((packed)) {
        double latitude;  ///< Latitudinal position of the system
        double longitude; ///< Longitudinal position of the system
        double elevation; ///< Altitude above sea level
    };
};

/**
 * @brief Velocity of the system with respect to the NED axis system.
 */
union Velocity {
    float array[3]; ///< Array to access all three members at once
    struct __attribute__((packed)) {
        float u; ///< Velocity along north axis [m/s]
        float v; ///< Velocity along east axis [m/s]
        float w; ///< Velocity along down axis [m/s]
    };
};

/**
 * A union containing heading information gathered from sensor data.
 */
union Heading {
    float array[3]; ///< Array containing the yaw, pitch and roll as floats.
    /** Shortcut struct to access the different array members by names */
    struct __attribute__((packed)) {
        float yaw;   ///< The yaw angle as measured by the sensor [rad].
        float roll;  ///< The roll angle as measured by the sensor [rad].
        float pitch; ///< The pitch angle as measured by the sensor [rad].
    };
};

/**
 * A union containing a linear acceleration vector that describes the linear
 *  acceleration of the system (without the static gravitational acceleration).
 */
union LinearAcceleration {
    float array[3]; ///< Array containing the x-, y- and z-acceleration.
    /** Shortcut struct to access the different array members by names. */
    struct __attribute__((packed)) {
        float x; ///< The linear acceleration in the x direction [m/s^2]
        float y; ///< The linear acceleration in the y direction [m/s^2]
        float z; ///< The linear acceleration in the z direction [m/s^2]
    };
};

/**
 * A union containing an angular velocity vector.
 */
union AngularVelocity {
    float array[3]; ///< Array containing the x, y and z angular velocity.
    /** Shortcut struct to access the different array members by names. */
    struct __attribute__((packed)) {
        float p; ///< The angular velocity component in the x dir [rad/s]
        float q; ///< The angular velocity component in the y dir [rad/s]
        float r; ///< The angular velocity component in the z dir [rad/s]
    };
};


typedef float Temperature;
typedef float Pressure;
/** Altitude above ground level */
typedef float AltitudeAGL;

/* Sensor specific data ----------------------------------------------------- */

// This section groups the raw sensor reading types into structs that represent
// a readout of a particular sensor.

/**
 * @brief An enum characterizing which sensor output is contained in a packet.
 */
enum SensorType {
    BNO055_Sensor = 1, ///< The data comes from a BNO-055 9-axis IMU.
    BMP388_Sensor = 2  ///< The data comes from a BMP-388 pressure sensor.
};

/**
 * A struct containing the different readouts of the BNO-055.
 *
 * The BNO-055 9-Axis Sensor will do sensor fusion on its values. The values in
 * this struct are those that get read out in a single sample.
 */
struct BNO055_Readout {
    uint32_t tick;                       ///< Tick when the sample was taken.
    union RawQuaternion qtr_att;         ///< Quaternion describing the attitude
    union RawLinearAcceleration lin_acc; ///< The linear acceleration.
    union RawAngularVelocity ang_vel;    ///< The angular velocity.
};

/**
 * @brief A struct containing the readout of the BMP-388 from the registers.
 *
 * The values are already compensated, but not yet converted to the right units
 * nor floating point.
 */
struct BMP388_Readout {
    RawPressure pressure;       ///< The pressure register entry.
    RawTemperature temperature; ///< The temperature register entry.
};

/**
 * @brief A struct containing the readout of a GPS receiver.
 */
struct NEO7M_Readout {
    uint32_t tick;                   ///< Tick of the measurement
    uint8_t num_satellites;          ///< Number of connected satellites
    bool has_fix;                    ///< Whether the sensor has a fix
    union GeodeticPosition position; ///< Position of the GR system
};

/* Control input types ------------------------------------------------------ */
// This section contains the data types that are passed to state estimation and
// control tasks. They are collections of physical quantities (e. g. converted
// sensor data).

/**
 * @brief A struct storing a snapshot in time of all the sensor data available.
 */
struct CompleteSensorData {
    struct BNO055_Readout main_att_readout_1;   ///< BNO055 readout #1 (main)
    struct BNO055_Readout main_att_readout_2;   ///< BNO055 readout #2 (main)
    struct BNO055_Readout icsb_att_readout_1;   ///< BNO055 readout #1 (ICSB)
    struct BNO055_Readout icsb_att_readout_2;   ///< BNO055 readout #2 (ICSB)
    struct BMP388_Readout main_tube_pressure_1; ///< BMP388 readout #1 (main)
    struct BMP388_Readout main_tube_pressure_2; ///< BMP388 readout #2 (main)
    struct BMP388_Readout icsb_pressure_left;   ///< BMP388 readout #1 (ICSB)
    struct BMP388_Readout icsb_pressure_right;  ///< BMP388 readout #2 (ICSB)
    struct NEO7M_Readout geodetic_pos_1;        ///< NEO7m readout #1 (main)
    struct NEO7M_Readout geodetic_pos_2;        ///< NEO7M readout #2 (main)
    /** 16-bit flag indicating which measurements have changed since last
     * deadline. */
    uint16_t changed_readings;
};

///@{

/**
 * @brief Flag set to indicate that a readout has been updated since the last
 * deadline.
 *
 * On each deadline, the control loop tasks resets its "updated measurements"
 * value to zero. If a task acquires new data, it changes the input to the
 * control loop and sets the relevant flag to indicate that the measurement
 * arrived on time. The control loop can use these flags to determine whether to
 * run a 6 DoF or 9 DoF state estimation, depending on missing measurements.
 */

#define MAIN_ATT_READOUT_1_CHANGED (1 << 0)
#define MAIN_ATT_READOUT_2_CHANGED (1 << 1)
#define ICSB_ATT_READOUT_1_CHANGED (1 << 2)
#define ICSB_ATT_READOUT_2_CHANGED (1 << 3)
#define MAIN_PRS_READOUT_1_CHANGED (1 << 4)
#define MAIN_PRS_READOUT_2_CHANGED (1 << 5)
#define ICSB_PRS_READOUT_1_CHANGED (1 << 6)
#define ICSB_PRS_READOUT_2_CHANGED (1 << 7)
#define MAIN_GPS_READOUT_1_CHANGED (1 << 8)
#define MAIN_GPS_READOUT_2_CHANGED (1 << 9)
#define MAIN_GPS_READOUT_3_CHANGED (1 << 10)

///@}

/**
 * @brief Data type representing the system's current state, estimated or real.
 */
struct State {
    union Position position;
    union Velocity velocity;
    union AngularVelocity angular_velocity;
    union Heading attitude;
};

/* Conversion functions ----------------------------------------------------- */

// These functions convert the register readouts to the physical quantities.
// They also give the units to the measurements.

/**
 * Converts the raw heading received from the ICSB to a float-based struct
 * that can be used in calculations.
 * @param in The int16_t based reading from the BNO.
 * @param out The float based, converted output.
 */
void convert_raw_heading(union RawHeading *in, union Heading *out);

/**
 * Converts a raw acceleration reading to a float  struct in the correct frame.
 * @param in The int16_t based reading from the BNO.
 * @param out The float based, converted output.
 * @param correction Quaternion that describes the correcting rotation to
 *      compensate assembly inaccuracies. May be NULL.
 */
void convert_raw_acceleration(union RawLinearAcceleration *in,
                              union LinearAcceleration *out,
                              union Quaternion *correction);

/**
 * Converts the raw ang. velocity received from the ICSB to a float-based struct
 * that can be used in calculations.
 * @param in The int16_t based reading from the BNO.
 * @param out The float based, converted output.
 * @param correction Quaternion that describes the correcting rotation to
 *      compensate assembly inaccuracies. May be NULL.
 */
void convert_raw_angular_velocity(union RawAngularVelocity *in,
                                  union AngularVelocity *out,
                                  union Quaternion *correction);

/**
 * Converts the raw quaternion read from the IMU to a float-based RPY heading.
 * @param in The raw 16-bit integer quaternion data read from the absolute
 *      orientation sensor.
 * @param out The heading based on the roll-pitch-yaw convention.
 * @param correction Quaternion that describes the correcting rotation to
 *      compensate assembly inaccuracies. May be NULL.
 * @pre The quaternion must have a non-zero norm, otherwise the function will
 *          just return.
 */
void convert_raw_quaternion_to_heading(union RawQuaternion *in,
                                       union Heading *out,
                                       union Quaternion *correction);

/**
 * Converts the heading to raw quaternions.
 * @param in The heading angles in RPY fashion.
 * @param out The corresponding normalized quaternion.
 */
void convert_heading_to_raw_quaternion(
    union Heading *in, union RawQuaternion *out,
    union Quaternion *quat_assembly_correction);

/**
 * Converts the raw pressure measurement of a barometric sensor to an
 * altitude above ground level.
 */
AltitudeAGL convert_raw_pressure_to_altitude(RawPressure pressure);


/*
 *******************************************************************************
 * Geodetic coordinate system
 *******************************************************************************
 */

/**
 * Initialize NED system with defined zero point
 */
bool initNedSystem();

/**
 * Convert geodetic coordinates to NED system
 */
void geo2ned(union GeodeticPosition *geo_position,
             union Position *ned_position);

/**
 * Convert NED coordinates to geodetic system
 */
void ned2geo(union Position *ned_position,
             union GeodeticPosition *geo_position);


/* Unconversion function used in HAL simulations ---------------------------- */

#if USE_SENSOR_DATA_HAL

/// @{

/**
 * @brief These functions are used when doing processor-in-the-loop runs.
 * All these functions do is undo the changes done by the data conversion
 * functions so that we can "fake" the raw sensor data input with the output of
 * the nine-dof linearized model.
 */

uint32_t convert_altitude_to_raw_pressure(float down);
union RawHeading unconvert_raw_heading(union Heading in);
union RawLinearAcceleration
unconvert_raw_acceleration(union LinearAcceleration in,
                           union Quaternion *quat_assembly_correction);
union RawAngularVelocity
unconvert_raw_angular_velocity(union AngularVelocity in,
                               union Quaternion *quat_assembly_correction);

/// @}

#endif /* USE_SENSOR_HAL */
