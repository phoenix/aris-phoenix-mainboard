/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           main.h
 * @brief          Header for main.c file.
 *                   This file contains the common defines of the application.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
    #define __MAIN_H

    #ifdef __cplusplus
extern "C" {
    #endif

    /* Includes
     * ------------------------------------------------------------------*/
    #include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
    #include "cmsis_os.h"
    #include "tasks/lora-transmission.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

// Export handles for other parts of the application to use.
extern SPI_HandleTypeDef hspi2;
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart6;
extern UART_HandleTypeDef huart4;
extern UART_HandleTypeDef huart5;
extern UART_HandleTypeDef huart2;
extern I2C_HandleTypeDef hi2c1;
extern IWDG_HandleTypeDef hiwdg;

    /** The UART handle used for the 1st GPS receiver on board. */
    #define huart_MNG1 huart4
/** The UART handle used for the 2nd GPS receiver on board. */
    #define huart_MNG2 huart5

    /** The UART handle used for the 1st motor controller */
    #define huart_ACT1 huart1
    #define huart_ACT2 huart6
/** The UART handle used for the 2nd motor controller */

extern osThreadId_t spi_taskHandle;
extern osThreadId_t control_loopHandle;
extern osThreadId_t sd_log_taskHandle;
extern osThreadId_t sensor_taskHandle;
extern osThreadId_t lora_taskHandle;
extern osThreadId_t gps_taskHandle;
extern osThreadId_t sensor_fix_taskHandle;

extern osTimerId_t sensor_spl_timerHandle;
extern osTimerId_t safety_timerHandle;
extern osTimerId_t watchdog_pretimerHandle;

extern osMutexId_t i2c_bus_mutexHandle;

extern TIM_HandleTypeDef htim2;

/**
 * @brief Event flag set indicating functionality that has been set up.
 *
 * This mechanism ensures that tasks can wait for features such as the SD card
 * to become available before using them.
 * @see Exported macros in main.h for the values of the flags.
 */
extern osEventFlagsId_t setup_flagsHandle;

/**
 * The handle for the event flag raised on transitions.
 *
 * On transitions, raise this flag with the appropriate value to trigger a
 * transition in the finite state machine.
 */
extern osEventFlagsId_t fsm_transitionHandle;

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

    #define SETUP_FLAG_SD_READY           (1 << 0) ///< SD task ready.
    #define SETUP_FLAG_SENSORS_READY      (1 << 1) ///< Sensors ready for calib.
    #define SETUP_FLAG_SENSORS_CALIBRATED (1 << 2) ///< Sensors are calibrated.

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/**
 * @brief Starts the MX generated watchdog initialization.
 */
void watchdog_init(void);

    /* USER CODE END EFP */

    /* Private defines
     * -----------------------------------------------------------*/
    #define B1_Pin                GPIO_PIN_13
    #define B1_GPIO_Port          GPIOC
    #define B1_EXTI_IRQn          EXTI15_10_IRQn
    #define ICSB_WAKEUP_Pin       GPIO_PIN_0
    #define ICSB_WAKEUP_GPIO_Port GPIOC
    #define USART_TX_Pin          GPIO_PIN_2
    #define USART_TX_GPIO_Port    GPIOA
    #define USART_RX_Pin          GPIO_PIN_3
    #define USART_RX_GPIO_Port    GPIOA
    #define SD_DET_Pin            GPIO_PIN_14
    #define SD_DET_GPIO_Port      GPIOB
    #define SPI1_NSS_Pin          GPIO_PIN_8
    #define SPI1_NSS_GPIO_Port    GPIOA
    #define BNO_1_RST_Pin         GPIO_PIN_11
    #define BNO_1_RST_GPIO_Port   GPIOA
    #define BNO_2_RST_Pin         GPIO_PIN_12
    #define BNO_2_RST_GPIO_Port   GPIOA
    #define TMS_Pin               GPIO_PIN_13
    #define TMS_GPIO_Port         GPIOA
    #define TCK_Pin               GPIO_PIN_14
    #define TCK_GPIO_Port         GPIOA
    #define BUSY_Pin              GPIO_PIN_3
    #define BUSY_GPIO_Port        GPIOB
    #define DIO1_Pin              GPIO_PIN_4
    #define DIO1_GPIO_Port        GPIOB
    #define DIO1_EXTI_IRQn        EXTI4_IRQn
    #define ESP_INTR_Pin          GPIO_PIN_5
    #define ESP_INTR_GPIO_Port    GPIOB
    #define ESP_INTR_EXTI_IRQn    EXTI9_5_IRQn
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

    #ifdef __cplusplus
}
    #endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
