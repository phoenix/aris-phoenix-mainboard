/**
 *******************************************************************************
 * @file           control-loop.h
 * @brief          Header file for the state estimation task.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include <stdbool.h>

#include "cmsis_os.h"
#include "data/data.h"

/* Signal Definitions ------------------------------------------------------- */

/* Real-time OS entry function ---------------------------------------------- */

/**
 * Entry function of the control loop task.
 *
 * This entry function is automatically called by the FreeRTOS layer. Do not
 * manually call within code. Also, this function should never return.
 * @ingroup tasks
 * @param arg An optional parameter that could be passed by
 * @returns Doesn't return.
 */
void control_loop_entry(void *arg);

/* Input to the state estimation--------------------------------------------- */

/**
 * @brief The place where all the newest sensor data is stored.
 * This memory location is protected by the semaphore sensor_data_accessHandle.
 * Take it before writing the sensor data, and give as soon as possible.
 */
extern struct CompleteSensorData *newest_sensor_data;
extern struct CompleteSensorData *model_sensor_data;

/**
 * @brief The semaphore protecting the newest sensor data memory location.
 * This semaphore must be taken prior to writing to the location. The task
 * attempts to take this semaphore prior to reading the newest sensor data and
 * passing it on. Only take when you're ready to write and give as soon as you
 * can.
 */
extern osMutexId_t write_sensor_data_mutexHandle;

/* Output of the state estimation-------------------------------------------- */

/**
 * @brief The current state of the system as estimated by the state estimation
 * task.
 * @note To ensure that all data read is congruent, read only after taking the
 * mutex protecting this data.
 */
extern struct State *estimated_state;

/**
 * @brief A mutex protecting access to the estimated_state memory.
 * This is to ensure that no writing to this memory location occurs during
 * reads.
 */
extern osMutexId_t access_state_mutexHandle;

/**
 * Callback for the state estimation timers.
 * @param arg Argument identifying the timer that has gone off. Configurate in
 *  `aris-phoenix-mainboard.ioc` file.
 */
void st_est_timer_fired(void *arg);

/* Controlling the control loop task ---------------------------------------- */

/**
 * Set whether the state estimation algorithm should run the full state
 * estimation or not. Regardless, sensor data is being collected at the state
 * estimation frequency and logged.
 * @param running If set to true, the state estimation runs, otherwise not.
 */
void state_estimation_set_running(bool running);

/**
 * @brief Enables the height trigger.
 *
 * This sets a flag in the control loop task that will make it check the current
 * height estimate on each execution. If the current height estimate is lower
 * than the configured deployment height, it will send the "Trigger Deployment"
 * input to the FSM.
 * @returns true if the height trigger could be armed.
 */
bool height_trigger_arm(void);

/**
 * @brief Disables the height trigger.
 *
 * This removes the height trigger on the next execution of the control loop.
 * @returns true if the height trigger could be disarmed.
 */
bool height_trigger_disarm(void);

/* Macros ------------------------------------------------------------------- */

/** Arbitrary numerical value indicating a fired state estimation timer. */
#define STATE_EST_TIMER_FIRED (1 << 1)
