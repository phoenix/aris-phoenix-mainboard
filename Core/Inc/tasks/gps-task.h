/**
 *******************************************************************************
 * @file           gps-task.h
 * @brief          Task receiving measurements from the GPS receivers
 * @author         Lukas Vogel
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */


#pragma once

#include "main.h"

/* Public functions --------------------------------------------------------- */

/**
 * @brief FreeRTOS entry function of the GPS task.
 * @ingroup tasks
 * @note Do not call manually outside of task creation.
 * @param arg An optional argument passed by the FreeRTOS driver on creation.
 */
void gps_task_entry(void *arg);

/* Interrupt processing ----------------------------------------------------- */

/**
 * @brief Callback that is called by the DMA reception process.
 * @param huart The UART interface that sent the interrupt, set by STM32 HAL.
 */
void GPS_RxCpltCallback(UART_HandleTypeDef *huart);

/**
 * @brief Callback that is called by the NEO-7M driver when ready to read out.
 * @param arg An integer identifying which NEO-7M fired the readiness.
 */
void NEO7M_RxCpltCallback(int arg);