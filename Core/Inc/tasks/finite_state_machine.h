/**
 *******************************************************************************
 * @file           finite_state_machine.h
 * @brief          This file implements a finite state machine keeping
 *                         track of the Guided Recovery system's state.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include <stdbool.h>

/* Definitions and types ---------------------------------------------------- */
/**
 * @brief An enum containing the states the guided recovery could be in.
 *
 * The states the guided recovery system will be in are *always increasing*.
 * The system is not allowed to go back to a "previous" state. This also means
 * that once the DisarmedState is reached, no input will allow the system to
 * be reinitialized apart from restarting the microcontroller.
 */
enum SystemState {
    /** Default state for the enum. */
    Stuck = 0,

    /** The initial state. The MCU is booted, the SD is connected. Peripherals
     * are successfully initialized by auto-generated code. */
    InitialState,
    /** The sensors have been calibrated (e.g. attitude and pressure). The
     * wireless connection has been started. The GPS has successfully acquired a
     * lock. The ICSBs are put to sleep. */
    CalibratedState,
    /** 2mins before drop, the ICSBs are once again woken, then the system is
       "live" */
    LiveState,
    /** 1min before drop, the height trigger which can enable the deployment of
       parachutes is armed. */
    ArmedState,
    /** 5 seconds before the system is dropped, it is put in the "drogue
       descent" state, where it stays until the safety timer or the height
       trigger fires */
    DrogueDescentState,
    /** After deploying the parachutes. Guidance, state estimation and all
       controllers run. The actuators are unlocked and follow the reference
       position from the controllers. */
    GuidedDescentState,
    /** In the last few meters before touchdown, the system could perform a
       flaring maneuvre. */
    FinalApproachState,
    /** Touchdown on the ground. The motors are locked and the ICSBs are again
       put to sleep. */
    TouchdownState,
    /** A state that can be triggered by the user on error: The motors are
       locked, deployment of the parachutes is blocked by disabling the safety
       timer and the height trigger. */
    DisarmedState
};

/**
 * @brief An enum containing the possible external inputs to the finite state
 *    manager.
 *
 * Keep in mind that transitions are only valid for one state (see technical
 * reports for extended documentation). Invalid transition flags will be
 * ignored. To signal a needed transition to the finite state manager, set the
 *    FSM_TRANSITION event flag to the system input enum value.
 */
enum SystemInput {
    Calibrate = 0x01, ///< Calibrate the sensors and start WiFi.
    WarmUp = 0x02,    ///< Wake up the ICSBs and start the motors.
    Arm = 0x04,       ///< Arm the system, i.e. the height trigger.
    Drop = 0x08,      ///< Transition to drogue descent. Start safety timer.
    TriggerDeployment = 0x10, ///< Trigger the deployment of the main parachute.
    TriggerApproach = 0x20,   ///< Trigger the final approach sequence.
    TouchdownDetected = 0x40, ///< Detect touchdown.
    Disarm = 0x80,            ///< Disarm the system (on failure).
};

/**
 * @brief A struct declaring a possible transition of a system from one
 * state to another.
 *
 * A transition consists of two states and one input: The initial state, an
 * input that can be processed in that state and the next state the system
 * will be in. The handler designates the function that will execute the
 * transition.
 */
struct SystemTransition {
    enum SystemState initial_state; ///< The state the system should be in.
    enum SystemState final_state; ///< The state that the system should move to.
    enum SystemInput input;       ///< The correspondig system input.
    void (*handler)(void);        ///< The handler implementing the transition.
};

/* Public functions --------------------------------------------------------- */

/**
 * Entry function of the finite state machine task.
 *
 * N.B.: This function will be called by FreeRTOS in a new thread. It will not
 * return.
 * @ingroup tasks
 * @param argument A pointer to an optional argument.
 */
void fsm_task_entry(void *arg);

/**
 * Give an input to the finite state machine.
 * @param input The input for the FSM.
 */
void fsm_input(enum SystemInput input);

/**
 * Get the current system state.
 * @returns The current system state.
 */
enum SystemState fsm_get_state(void);

/**
 * Callback function for the safety timer.
 * @param arg An optional argument that could be passed, but it is NULL.
 */
void safety_timer_fired(void *arg);
