/**
 *******************************************************************************
 * @file           sd-log.h
 * @brief          Declares the SD logging task and its setup functions.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#ifndef __SD_LOG_H__
#define __SD_LOG_H__

#include <stdbool.h>

/* Exported logging macros -------------------------------------------------- */

/**
 * @brief A logging macro for printing fixed strings to the SD and tracing.
 *
 * The string will only be printed to tracing if tracing is enabled.
 * @param log_type An enum SDLogEntryType.
 * @param trace_chn The tracing channel to print this to.
 * @param msg The message to print, a null-terminated string.
 */
#define LOG_AND_TRACE(log_type, trace_chn, msg)                                \
    do {                                                                       \
        sd_log(msg, log_type);                                                 \
        if (configUSE_TRACE_FACILITY) {                                        \
            vTracePrint(trace_chn, msg);                                       \
        }                                                                      \
    } while (0)

/**
 * @brief A logging macro for printing formatted strings to the SD and tracing.
 *
 * The string will only be printed to tracing if tracing is enabled.
 */
#define LOG_AND_TRACEF(log_type, trace_chn, fmt, ...)                          \
    do {                                                                       \
        sd_printf(log_type, fmt, __VA_ARGS__);                                 \
        if (configUSE_TRACE_FACILITY) {                                        \
            vTracePrintF(trace_chn, fmt, __VA_ARGS__);                         \
        }                                                                      \
    } while (0)

/* Exported types ----------------------------------------------------------- */

/**
 * @brief Characterizes a log entry.
 * This categorization is used to split up the logging messages into
 * different files.
 */
enum SDLogEntryType {
    EventEntry = 1,  ///< Log entry is an event (e.g. FSM transition)
    ErrorEntry,      ///< Log entry is an error (e.g. sensor failure)
    SensorDataEntry, ///< Log entry contains data to be plotted later (sensors)
    EndLogEntry,     ///< Reserved! Ends the log and writes everything to disk
};

/**
 * Log a character string <= 255B (non-newline terminated) to the SD card.
 *
 * This will write the character string to the SD card. The current RTOS tick
 * will automatically be added.
 * @param buf The null-terminated buffer to write to the SD card.
 * @param type The type of log entry. Different types go into different files.
 * @returns true if it could be added to the log buffer or whether the buffer
 *  was full.
 * @note Do not call from ISR! Less than 255 bytes and null-terminated, so 256
 * with terminator
 */
bool sd_log(const char *buf, enum SDLogEntryType type);

/**
 * @brief Printf(.) to the SD card. Keep in mind that this can be slow.
 * @note The length of the entire formatted string must not exceed 255 char's.
 * @param type The log entry type
 * @param fmt The format string to be interpreted in printf(.) style syntax
 * @return Whether it could be added to the log buffer or whether the buffer was
 *  full.
 * @note Do not call from ISR!
 */
bool sd_printf(enum SDLogEntryType type, const char *fmt, ...);

/**
 * Pauses the log task.
 *
 * Writes the remaining buffer into memory and discards all further log entries.
 * Unmounts the SD card to allow for safe removal.
 * @param none
 * @returns none
 */
void sd_log_task_pause_logging(void);

/* Public functions --------------------------------------------------------- */

/**
 * @brief Entry function of the SD logging task.
 *
 * The SD logging task first tries to mount the SD card and then sets up the
 * buffers that can be used by other tasks to write data, logs or errors into.
 * This entry function is automatically called by the FreeRTOS configuration. Do
 * not call manually. Will never return.
 * @ingroup tasks
 * @param none
 * @returns Doesn't return.
 */
void sd_log_task_entry(void *argument);

/**
 * @brief Unlock the logging of data to the SD card.
 *
 * This sets a flag for SD task to enable logging to the stream buffer. Prints a
 * message that the log starts here.
 */
void sd_start_log(void);

/**
 * @brief Lock the logging of data to the SD card.
 *
 * This sets the flag that no more log entries can be written. Adds a message
 * that the end of the log has been reached.
 */
void sd_stop_log(void);

#endif /* __SD_LOG_H__ */
