/**
 *******************************************************************************
 * @file           packets.h
 * @brief          Platform-independent abstraction layer for sending data
 *                      and messages among devices (wirelessly or serially).
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include <stdint.h>

#include "data/data.h"

/* Packet interface definition ---------------------------------------------- */

/**
 * An enum outlining the microcontrollers involved in the packet transmission
 * chain, which can be both targets or senders.
 */
enum Location {
    ReceiverBoard = 1,      ///< The receiving ESP-32 board (in the main tube)
    InCanopyBoardLeft = 2,  ///< The left sending ESP-32 board (ICSB)
    InCanopyBoardRight = 3, ///< The right sending ESP-32 board (ICSB)
    MainControllerBoard = 4 ///< The main STM32 Nucleo board
};

/**
 * An enum outlining the different packet types that could be sent inbetween
 * microcontrollers.
 */
enum PacketType {
    Empty = 0,         ///< Packet is empty
    LogPacket = 1,     ///< The packet contains log information.
    DataPacket = 2,    ///< The packet contains data (e.g. sensor).
    CommandPacket = 3, ///< The packet contains commands.
    StatePacket = 4,   ///< The packet contains information about current state.
    MultiPacket = 8,   ///< The packet contains multiple sensor readings.
    KeepAlive = 9      ///< The packet is for testing connectivity.
};

/**
 * An enum outlining the priority of the packet. Packets with higher priority
 * should be transmitted first.
 */
enum PriorityLevel {
    PriorityNormal = 0, ///< The packet has normal priority.
    PriorityHigh = 1    ///< The packet has maximal priority.
};

/**
 * A struct representing a wireless packet.
 *
 * A wireless packet has metadata that identifies the sender and the destination
 * as well as an associated payload.
 */
typedef struct {
    enum Location origin;        ///< Which MC sent the packet?
    enum Location destination;   ///< To whom should it be transmitted?
    enum PacketType type;        ///< What kind of a packet is it?
    enum PriorityLevel priority; ///< Is it a high priority packet?
    uint8_t payload_size;        ///< Size (in bytes) of the payload.
    void *payload;               ///< Pointer to the payload to be sent.
} Packet;

/* Specific payload definitions --------------------------------------------- */

/**
 * A command sent to a microcontroller.
 */
enum Command {
    CommandDummy = 0x00,     ///< A dummy command meaning "do nothing"
    CommandActivate = 0x01,  ///< Activate.
    CommandSleep = 0x02,     ///< Go to power-saving / sleep mode.
    CommandCalibrate = 0x04, ///< Perform calibration steps.
    CommandRestart = 0x80    ///< Restart the board.
} __attribute__((packed));

/**
 * A struct for a command payload that can be cast into a byte-array.
 */
struct CommandPayload {
    enum Command cmd; ///< The command contained in the payload.
} __attribute__((packed));

/**
 * @brief A payload that contains data from a sensor.
 * The first field defines which sensor data struct is populated.
 * Then follows the data.
 */
struct DataPayload {
    enum SensorType sensor_type; ///< Which field is populated?
    union {
        struct BNO055_Readout bno_readout;
        struct BMP388_Readout bmp_readout;
    };
};

/* Packet task API  --------------------------------------------------------- */

/**
 * A struct that can be used to tell the API about a transmission handler.
 *
 * A transmission handler is a function that is capable of transmitting a packet
 * that the packets task hands to it to the location defined.
 * @note The packet tasks API won't copy the memory occupied by the payload.
 *  Cleanup of memory is the responsibility of the packet transmission handler.
 * @note The pointer to the `Packet *` must not be released, only the payload.
 */
struct PacketTransmissionHandler {
    enum Location destination;          ///< Which location the handler serves.
    int (*transmitter)(Packet *packet); ///< The function pointer handle.
};

struct PacketTaskConfig {
    enum Location here;                ///< Which location is "here"?
    uint8_t num_transmission_handlers; ///< Number of transmission handlers.
    /** Pointer to an array which contains the handlers. */
    struct PacketTransmissionHandler *tx_handlers;
    /** Reception handler to handle the received packets. */
    void (*reception_handler)(Packet *packet);
};

/**
 * Entry function of the packet task.
 * @ingroup tasks
 * @param config A pointer to a struct PacketTaskConfig
 * @returns Never returns, as it is an RTOS task.
 */
void packet_task_entry(void *config);

/**
 * Queue a packet for transmission.
 *
 * This function runs on the task that is calling it. Keep that in mind. It
 * will eat your CPU time (until the packet is in the queue).
 * @returns Zero when the packet could be put in the queue, non-zero otherwise.
 * @note This function will copy the packet struct by value. This means that
 *  the pointer to `payload` will not be modified. The pointer to the payload
 *  must remain valid, and only the transmission handler should clean up memory.
 */
int packet_add_to_queue(Packet *packet);
