/**
 *******************************************************************************
 * @file           lora-transmission.h
 * @brief          Interface for the task responsible for LoRa communication.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

/**
 * @brief Entry function of the LoRa transmission task.
 * @ingroup tasks
 * @note Do not call manually outside of task creation.
 * @param arg An optional argument passed through the RTOS task setup.
 */
void lora_transmission_task_entry(void *arg);

/* Exported macros ---------------------------------------------------------- */

/** Thread flag to set on the LoRa transmission task when an interrupt is
 * received. */
#define LORA_INTR_RECEIVED (1 << 0)