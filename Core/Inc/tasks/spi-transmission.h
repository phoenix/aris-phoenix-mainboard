/**
 *******************************************************************************
 * @file           spi-transmission.h
 * @brief          Declares the SPI master that talks to the receiver board.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include "tasks/packets.h"

/**
 * Entry function to the SPI transmission task. Called by FreeRTOS scheduler,
 * do not call manually.
 * @ingroup tasks
 * @param arg The argument (ignored for now).
 * @returns Doesn't return.
 */
void spi_task_entry(void *arg);

/**
 * Transmit a packet via SPI to the receiver board.
 *
 * @note Only one packet can be queued at a time. If this method is called again
 * before the previous packet has been transmitted, the new packet is dropped.
 * @returns true if the packet could successfully be scheduled for transmission.
 */
int transmit_packet_spi(Packet *packet);

/* Exported macros ---------------------------------------------------------- */

/** Thread flag to set on the SPI transmission task when an interrupt is
 * received. */
#define ESP_INTERRUPT_RECV (1 << 0)
