/**
 *******************************************************************************
 * @file           sensor-task.h
 * @brief          Interface for the sampling of IMU/barometer
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include <stdint.h>

#include "main.h"

/* Sensor Task Public Interface --------------------------------------------- */

/**
 * @brief Entry function of the sensor sampling task.
 *
 * This task starts by initializing the BNO-055 and BMP-388 sensors on the I2C1
 * bus and waits for the "Calibration" command to be received. It will then push
 * the calibration values into the sensors. On "Arming", it will start sampling
 * the sensors at the predefined rates and write their readings to the control
 * loop's memory.
 * @ingroup tasks
 * @note Do not call outside of RTOS task creation.
 * @param arg An optional argument that can be passed through RTOS creation.
 */
void sensor_task_entry(void *arg);

/**
 * @brief Start the sensor calibration process.
 *
 * This method will block the calling task until the calibratin process is over.
 * If the calibration task failed or timed out, it will return false.
 * @returns true if the calibration was successful.
 */
bool sensor_task_command_calibration(void);

/**
 * @brief Start sampling the sensors.
 * This function call is used to sync up the sensor sampling with the state
 * estimation such that sensor measurements should be available on deadline.
 */
void sensor_task_start_sampling(void);

/**
 * @brief The callback function that gets called when the sensors should sample.
 * @note Do not call.
 * @param argument An argument that can be passed through the timer.
 */
void sensor_spl_timer_fired(void *argument);

/* Hardware helper methods for sensors -------------------------------------- */

/**
 * Simple I2C register read method.
 *
 * Performs the multi-byte read sequence by redirecting to the HAL DMA transfer.
 * This method will block the calling task until the result is available.
 * @param dev_addr The 7-bit address of the device, non-shifted
 * @param reg_addr The register address to read
 * @param data A pointer to an array, where the data can be written.
 * @param cnt The number of bytes to read.
 * @returns Non-zero on error.
 */
int8_t i2c_master_read_slave_reg(uint8_t dev_addr, uint8_t reg_addr,
                                 uint8_t *data, uint8_t cnt);

/**
 * Simple I2C register write method.
 *
 * Performs the multi-byte write sequence by redirecting to HAL DMA transfer.
 * This method will block the calling task until the result is available.
 * @param dev_addr The 7-bit address of the device, non-shifted
 * @param reg_addr The register address to start writing to.
 * @param data A pointer to an array, where the data to be written is.
 * @param cnt The number of bytes to write.
 * @returns Non-zero on error.
 */
int8_t i2c_master_write_slave_reg(uint8_t dev_addr, uint8_t reg_addr,
                                  uint8_t *data, uint8_t cnt);

/**
 * @brief Check whether an error occurred in the I2C transfer.
 * @returns True if no error occurred.
 */
bool i2c_check_no_errors();
