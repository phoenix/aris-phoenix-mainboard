/**
 *******************************************************************************
 * @file           downlink-task.h
 * @brief          Interface for the various program parts to update the
 *                      status of certain system functions (e.g. GPS lock)
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include "data/data.h"

/* Public functions --------------------------------------------------------- */

/**
 * Entry function of the downlink / status reporting task.
 * @ingroup tasks
 * @note Do not call manually outside of RTOS task creation. Never returns.
 * @param arg An optional argument that can be passed through RTOS task creation
 */
void downlink_task_entry(void *arg);
