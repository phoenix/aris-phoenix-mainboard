/**
 *******************************************************************************
 * @file           bodyrate-controller.h
 * @brief          Header file for the bodyrate controller.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include <stdbool.h>

#include "config.h"
#include "data/data.h"

/// @addtogroup controllers
/// @{

void oneStep_BodyRateController(void);

/**
 * @brief Run the bodyrate controller once.
 * @param[in] estimated_state The current estimated state.
 * @param[in] ref_rate The output of the attitude controller.
 * @param[out] out The output that is then passed to the input handling.
 * @returns true if running the body rate controller was successful, else false
 */
bool bodyrate_controller_run(const struct State *estimated_state,
                             const struct AttitudeControllerOutput *ref_rate,
                             struct BodyRateControllerOutput *out);

/**
 * @brief Initialize body rate controller
 */
bool bodyrate_controller_init();

/**
 * @brief Terminate body rate controller
 */
bool bodyrate_controller_terminate();

/// @}
