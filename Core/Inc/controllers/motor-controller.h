/**
 *******************************************************************************
 * @file           motor-controller.h
 * @brief          Motor controller that takes the input handling output and
 *                      sends it to the EPOS driver.
 * @author         : Pascal Sutter
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include <stdbool.h>

#include "data/data.h"
#include "main.h"

/// @addtogroup actuator_drivers
/// @{

/**
 * @brief Prepare the motors for drop: Enable them so that they have enough
 * torque.
 * @returns true if the motors could successfully be enabled.
 */
bool motors_prepare_for_drop(void);

/**
 * @brief Release the motors from their braked position and release the steering
 * lines.
 * @returns true if the motors could successfully follow the commands.
 */
bool motors_release_brakes(void);

/**
 * @brief Send a new reference position to the motor.
 * @param reference The output of the InputHandling controller, containing the
 *  reference position of the lines.
 * @returns true if the action was successful.
 */
bool motor_controller_run(struct InputHandlingOutput *reference);

/**
 * @brief Perform a flare maneuvre on the parachute.
 * @returns true if the action was successful.
 */
bool motors_flare(void);

/**
 * @brief Disable the motors after touchdown.
 * @returns true if the motors could successfully be disabled.
 */
bool motors_disable(void);

/* Tracing support ---------------------------------------------------------- */

#if configUSE_TRACE_FACILITY
/**
 * @brief Allows registering the tracing strings before the arming transition.
 *
 * We want to make sure we don't start allocating "unneeded" memory during a
 * transition.
 */
void motors_register_traceStrings(void);

#endif

/// @}
