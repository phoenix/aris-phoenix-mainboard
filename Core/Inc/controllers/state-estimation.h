/**
 *******************************************************************************
 * @file           state-estimation.h
 * @brief          Bridges between the Matlab-generated SE and the rest.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include <stdbool.h>

#include "config.h"
#include "data/data.h"

/// @addtogroup controllers
/// @{

void oneStep_StateEst(void);

/**
 * A struct representing the input given to the state estimation algorithm on
 * each tick of the estimation.
 */
struct StateEstimationInput {
    /* Data ICSB ----------------------------------------------- */

    /** Average linear acceleration as measured by the ICSBs. */
    union LinearAcceleration lin_accel_icsb;

    /** Average angular velocity as measured by the left ICSB. */
    union AngularVelocity ang_vel_icsb;

    /** Euler angles as measured by the left ICSB. */
    union Heading heading_icsb;

    /** Whether the ICSB data is up to date or the deadline was missed. */
    bool icsb_imu_updated;

    /* Data from mainboard ------------------------------------- */

    /** Average linear acceleration as measured by the main board. */
    union LinearAcceleration lin_accel_main;
    /** Average angular velocity as measured by the main board. */
    union AngularVelocity ang_vel_main;
    /** Average heading as measured by the main board. */
    union Heading heading_main;

    /** Average altitude AGL derived from main board pressure sensors. */
    AltitudeAGL altitude_from_baro;

    /** Whether the main IMU data (lin acc, ang vel and heading) got updated. */
    bool main_imu_updated;
    /** Whether the main altitude derived from pressure data got updated. */
    bool altitude_updated;

    /* GPS Data ------------------------------------------------ */
    /** The newest GPS position measurement. */
    union Position gps_position;

    /** Whether the GPS position got updated since the last run. */
    bool gps_position_updated;
};

/**
 * @brief Run the next iteration of the state estimation.
 *
 * @param[input] input A pointer to where the input to the state estimation is
 * stored.
 * @param[estimated_state] estimated_state Output of the state estimation. Store
 * results there.
 * @returns true if running the state estimation was successful or an error
 * occurred.
 */
bool state_estimation_run(struct StateEstimationInput *input,
                          struct State *estimated_state,
                          struct InputHandlingOutput *steering_input,
                          struct ReferenceAttitude *guidance_output);

/**
 * @brief Initialize state estimation
 */
bool state_estimation_init();

/**
 * @brief Terminate state estimation
 */
bool state_estimation_terminate();

/// @}
