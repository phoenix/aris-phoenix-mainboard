/**
 *******************************************************************************
 * @file           input-handling.h
 * @brief          Header for the input handling filter between bodyrate and
 *                 motor controller
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include "config.h"
#include "data/data.h"

/// @addtogroup controllers
/// @{

void oneStep_InputHandling(void);

/**
 * @brief Run one iteration of the input handling algorithm.
 *
 * The input handling ensures that the output of the bodyrate controller always
 * stays actionable. If, for example the maximum synchronous pulling of the line
 * (for braking) is combined with asynchronous pulling (for making a turn), this
 * controller makes sure that this remains possible and does not "lock up".
 * @param[in] body_rate_ctrl_out The output of the bodyrate controller.
 * @param[out] out The output of the input handling algorithm for the motor.
 */
bool input_handling_run(
    const struct BodyRateControllerOutput *body_rate_ctrl_out,
    struct InputHandlingOutput *out);

/**
 * @brief Initialize input handling filter
 */
bool input_handling_init();

/**
 * @brief Terminate input handling filter
 */
bool input_handling_terminate();

/// @}
