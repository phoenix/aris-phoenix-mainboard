/**
 *******************************************************************************
 * @file           attitude-controller.h
 * @brief          Header file for the attitude controller.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include <stdbool.h>

#include "config.h"
#include "data/data.h"

/// @addtogroup controllers
/// @{

/**
 * @brief Run one step of the attitude controller.
 */
void oneStep_AttitudeController(void);

/**
 * @brief Run the attitude controller loop once.
 *
 * The attitude controller sets the reference for the bodyrate controller. It
 * takes as input the current estimated state, the reference set by the guidance
 * and then outputs the reference for the next bodyrate iteration.
 * @param[in] state The current estimated state.
 * @param[in] ref_att The reference set by the guidance
 * @param[in] body_rate_out The last output of the bodyrate controller.
 * @param[out] out The reference for the bodyrate controller.
 * @returns true if running the attitude controller was successful, else false
 */
bool attitude_controller_run(
    const struct State *state, const struct ReferenceAttitude *ref_att,
    const struct BodyRateControllerOutput *body_rate_out,
    struct AttitudeControllerOutput *out);

/**
 * @brief Initialize attitude controller.
 *
 * This calls the initialization function of the Matlab-generated C code.
 */
bool attitude_controller_init();

/**
 * @brief Terminate attitude controller.
 *
 * Performs any cleanup that the MATLAB-generated C code requires.
 */
bool attitude_controller_terminate();

/// @}
