/**
 *******************************************************************************
 * @file           guidance.h
 * @brief          Header file for the guidance algorithm.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include <stdbool.h>

#include "config.h"
#include "data/data.h"

/// @addtogroup controllers
/// @{

/* Public functions --------------------------------------------------------- */
void oneStep_Guidance(void);

/**
 * @brief Run one iteration of the guidance algorithm.
 * @param[in] estimated_state A pointer to the current estimated state
 * @param[in] estimated_wind The output of the wind estimation
 * @param[out] out The reference for the attitude controller.
 * @returns true if running the guidance was successful, false otherwise
 */
bool guidance_run(const struct State *estimated_state,
                  const struct Wind *estimated_wind,
                  struct ReferenceAttitude *out);

/**
 * @brief Initialize guidance algorithm
 */
bool guidance_init();

/**
 * @brief Terminate guidance algorithm
 */
bool guidance_terminate();

/// @}
