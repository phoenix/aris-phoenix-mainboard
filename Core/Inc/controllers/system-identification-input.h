/**
 *******************************************************************************
 * @file           system-identification-input.h
 * @brief          Header file for the system identification input generator
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include <stdbool.h>

#include "config.h"
#include "data/data.h"

/// @addtogroup controllers
/// @{

void oneStep_SystemIdentificationInput(void);

/**
 * @brief Run one iteration of the system identification input
 */

bool system_identification_input_run(
    struct ReferenceAttitude *reference_attitude);

/**
 * @brief Initialize system identification input
 */
bool system_identification_input_init();

/**
 * @brief Terminate system identification input
 */
bool system_identification_input_terminate();

/// @}
