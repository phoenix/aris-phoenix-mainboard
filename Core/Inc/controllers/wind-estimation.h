/**
 *******************************************************************************
 * @file           wind-estimation.h
 * @brief          Header file for the wind estimator.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include <stdbool.h>

#include "data/data.h"

/// @addtogroup controllers
/// @{

/* Public functions --------------------------------------------------------- */
void oneStep_WindEstimation(void);

/**
 * @brief Run one iteration of the wind estimation.
 * @param[in] estimated_state A pointer to the current estimated state.
 * @param[out] wind A pointer to where the wind estimation output should be
 * written.
 * @returns true if running the wind estimation could run successfully, else
 * false
 */
bool wind_estimation_run(const struct State *estimated_state,
                         struct Wind *wind_est);

/**
 * @brief Initialize wind estimation
 */
bool wind_estimation_init();

/**
 * @brief Terminate wind estimation
 */
bool wind_estimation_terminate();

/// @}
