/**
 *******************************************************************************
 * @file           system-status.h
 * @brief          A framework that keeps an overview over what's working on
 *                      the system and access to low-level system functions.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include "cmsis_os.h"

#include "drivers/lora_airborne.h"

/* Status for components ---------------------------------------------------- */

/**
 * @brief Describes why the system was reset.
 */
enum ResetReason {
    UnknownResetReason = 0,          ///< The reason for reset is unknown
    SoftwareResetReason = 1,         ///< The reset was triggered through SW
    LowPowerResetReason = 2,         ///< The reset was because of a power-out
    WatchdogResetReason = 3,         ///< The reset was caused by the watchdog
    PowerOnPowerDownResetReason = 4, ///< The reset was a power-on-power-down
    ResetPinResetReason = 5,         ///< The reset pin was pressed,
    BrownOutResetReason = 6,         ///< The reset was caused by a brown-out
};

/**
 * @brief Describes the state of the SD.
 */
enum SDStatus {
    UnknownSDStatus = 0,    ///< The state of the SD is unknown
    NotPresentSDStatus = 1, ///< The SD is not present (not detected)
    MountedSDStatus = 2,    ///< The SD was mounted
    ReadySDStatus = 3,      ///< The SD is ready to write to
};

/**
 * @brief Status of the GPS
 */
enum GPSStatus {
    NotRespondingGPSStatus = 0, ///< The GPS is not responding
    RespondingGPSStatus = 1,    ///< The GPS is sending data
    FixGPSStatus = 2,           ///< The GPS has a fix
};

/** State of an In-Canopy Sensor Board from the mainboard's perspective. */
enum ICSBStatus {
    NotConnectedICSBStatus = 0, ///< The ICSB is not connected / not responding
    CalibratedICSBStatus = 1,   ///< The ICSB is calibrated
    ActiveICSBStatus = 2,       ///< The ICSB is active
    SleepingICSBStatus = 3,     ///< The ICSB is sleeping
};

/**
 * @brief Status of a sensor attached to the mainboard.
 */
enum SensorStatus {
    NotRespondingSensorStatus = 0,  ///< The sensor is not responding
    InSetupSensorStatus = 1,        ///< The sensor is being set up
    SetupCompleteSensorStatus = 2,  ///< The sensor is completely set up
    ReadySensorStatus = 3,          ///< The sensor is calibrated and ready
    TemporaryErrorSensorStatus = 4, ///< Currently trying to resolve error
    PermanentErrorSensorStatus = 5, ///< Forget it, this sensor is done with
};

/* System overview ---------------------------------------------------------- */

/**
 * @brief A struct that contains an overview over the current system status.
 * @note Do not confuse with the state, which is related to the FSM. This is
 *  related to features such as SD.
 */
struct SystemStatus {
    enum ResetReason reset_reason;
    enum SDStatus sd_status;
    enum GPSStatus mng1_status;
    enum GPSStatus mng2_status;
    enum ICSBStatus icsb_1_status;
    enum ICSBStatus icsb_2_status;
    enum SensorStatus mna1_status; ///< Status of attitude sensor #1 (main)
    enum SensorStatus mna2_status; ///< Status of attitude sensor #2 (main)
    enum SensorStatus mnp1_status; ///< Status of pressure sensor #1 (main)
    enum SensorStatus mnp2_status; ///< Status of pressure sensor #2 (main)
    TickType_t icsb_1_last_update;
    TickType_t icsb_2_last_update;
};

/**
 * @brief Global variable that contains the status of all the components.
 */
extern struct SystemStatus global_system_status;

/* Public functions --------------------------------------------------------- */

/**
 * @brief Get a LoRa compatible struct that corresponds to the current system
 * state.
 * @note Thread safe to call.
 */
StatusUpdate_t system_get_lora_compatible_status(void);

/**
 * @brief Finds the reason for the reset, returns it and stores it.
 *
 * The reset reason is stored in the global @see global_system_status. Call this
 * function as soon as possible after startup, as the register could change.
 * @note Call as soon as possible, before RTOS setup.
 * @returns The reason for the system reset.
 */
enum ResetReason system_get_reset_reason(void);

/**
 * @brief Prints the system startup report to the SD card.
 */
void system_print_startup_report(void);

/**
 * @brief Restarts the system.
 * @note You won't get a callback from this. Because the system just restarts.
 */

void system_restart(void);
