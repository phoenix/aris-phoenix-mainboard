/**
 *******************************************************************************
 * @file           nine-dof-model.h
 * @brief          Header file for the linear 9Dof model
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include "config.h"
#include "controllers/state-estimation.h"
#include "data/data.h"

#if USE_SENSOR_DATA_HAL

/// @addtogroup hal_functions
/// @{

/* Public variables --------------------------------------------------------- */

/**
 * @brief This struct contains the sensor data output derived from running the
 * 9-DoF linearized model.
 */
extern struct CompleteSensorData *model_sensor_data;

/* Public functions --------------------------------------------------------- */
void oneStep_NineDofModel(void);

/**
 * @brief Run the nine dof model once.
 * @returns Returns whether running the nine dof model was successful.
 */
bool nine_dof_model_run(struct InputHandlingOutput *sys_input);

bool nine_dof_model_init();

bool nine_dof_model_terminate();

/// @}

#endif
