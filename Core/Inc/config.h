/**
 *******************************************************************************
 * @file           config.h
 * @brief          Contains global parameters that change which code parts are
 *                      compiled. Can be used during debugging to remove some
 *                      functionality if not all components are connected.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include "util/maths.h"

/*
 *******************************************************************************
 * Launch day configuration
 *******************************************************************************
 */

/* Drop and impact point, weather situation --------------------------------- */

/** Pressure at sea level at drop location in Pa */
#define SEA_LEVEL_PRESSURE_PA 102452
/** Height above sea level of ground at drop location in meters */
#define GROUND_LEVEL_METERS 1484.7f
/** Deployment height above ground level of the main parachute. */
#define DEPLOYMENT_HEIGHT_METERS_AGL 600.0f
/** Latitude of zero point */
#define LAT_0 46.88699573858463
/** Longitude of zero point */
#define LNG_0 9.107472948870935

/* Timers and watchdogs ----------------------------------------------------- */

/**
 * @brief Number of milliseconds the safety timer should run.
 *
 * Keep in mind that this includes both the waiting BEFORE the drop (20s) plus
 * the free-fall time.
 */
#define SAFETY_TIMER_DURATION_MS (20 * 1000 + 11 * 1000)

/**
 * @brief Number of milliseconds after invoking the "DROP" transition that the
 * watchdog will be enabled.
 *
 * After this duration has passed, the watchdog timer will not be able to be
 * stopped, so choose this value carefully.
 */
#define WATCHDOG_PRETIMER_MS (22 * 1000)

/*
 *******************************************************************************
 * Hardware assembly corrections
 *******************************************************************************
 */

/** Heading correction due to assembly inaccuracies. [rad] */
#define HEADING_CORRECTION_ASSEMBLY 10.0f * M_PIf / 180.0f

/* Downlink configuration --------------------------------------------------- */

/** Frequency with which the status register of the LoRa task is updated. */
#define DOWNLINK_UPDATE_FREQ 1

/* SD Card ------------------------------------------------------------------ */

/** Size of the stream buffer (in bytes) used for the logging. */
#define SD_CARD_STREAM_BUFFER_SIZE 2048

/** Number of bytes to write to the SD at once. Trigger level for stream buf. */
#define SD_CARD_WRITE_CHUNK 512

/** Number of writes to the SD before a sync happens. */
#define SD_CARD_SYNC_THRESHOLD 10

/* State estimation --------------------------------------------------------- */

/**
 * @brief Rate the state estimation algorithm is running at [Hz].
 * @note This must not be lower than the state estimation rate.
 */
#define STATE_ESTIMATION_RATE 20

/* Controls ----------------------------------------------------------------- */

/**
 * @brief Number of milliseconds that the finite state machine should wait after
 * triggering deployment to enable the state estimation and the controllers.
 */
#define CONTROLLER_POST_DROP_WAIT 10000

/* Debugging ---------------------------------------------------------------- */

#define ENABLE_DEBUG_PRINTING 1

/**
 * Select whether live tracing should, if enabled, start before the RTOS kernel
 * or "on demand" when pressing "record" in Tracealyzer.
 * If this is enabled, make sure to configure Tracealyzer accordingly, to use
 * "Target Starts Tracing" in the PSF settings.
 */

#define TRACING_STARTS_ON_LAUNCH 1

/*
 *******************************************************************************
 * Hardware Abstraction Layer (HAL) Settings
 *******************************************************************************
 */

/**
 * @brief Whether to use the hardware abstraction layer for the SD card instead
 * of a connected SD.
 *
 * If this is set to 1, all writes to the SD card will instead be rerouted to
 * the USART2 peripheral and subsequently to the serial console of your
 * computer.
 */
#define USE_SD_CARD_HAL 0

/**
 * @brief Whether to use the HAL for generating the BNO-055, BMP-388 and GPS
 * data instead of using data obtained from the real sensors.
 */
#define USE_SENSOR_DATA_HAL 0

/**
 * @brief Whether to disable the control loop and use system identification
 * input
 */
#define USE_SYS_ID_INPUT 0

/**
 * @brief Log before and after each control loop function.
 */
#define EXTENDED_CONTROL_LOOP_LOGGING 0

/**
 * @brief Whether to use the LoRa HAL.
 *
 * If this is set to 1, the finite state machine transitions can be triggered by
 * pushing the blue push button. The LoRa transmission task will then simulate
 * reception of all state transition commands, in order.
 */
#define USE_LORA_HAL 0

/**
 * @brief Whether to use the SPI HAL.
 *
 * If this is set to 1, the SPI transmission task will be replaced by a HAL
 * task. This task waits for the "Activate" packet to be queued by the finite
 * state machine and will then read out the model output data at 20 Hz and send
 * it as packets to the packets task.
 */
#define USE_SPI_HAL 0

/**
 * @brief Whether to us the servo HAL.
 * If this is set to 1, then the servos won't actually be actuated.
 */
#define USE_SERVO_HAL 0

/**
 * @brief Whether to use a HAL for the motors.
 */
#define USE_MOTOR_HAL 0
/*
 *******************************************************************************
 * Sensor configuration settings
 *******************************************************************************
 */

/* General sensor settings -------------------------------------------------- */

/**
 * @brief Sampling rate of the connected sensors.
 *
 * This should be, for best performance, a multiple of the state estimation
 * rate.
 */
#define SENSOR_SAMPLING_RATE 20

/* BNO055 Calibration ------------------------------------------------------- */

// The BNO055 sensor can use values found in precalibration to skip the tedious
// calibration. Only the gyroscope and the magnetometer need to be well
// calibrated for the absolute orientation to make sense. The accelerometer is
// factory pre-calibrated and does not need the pre-calibration.

/**
 * @brief Maximum amount of time the calibration of the sensors should take.
 * If timeout is reached, an error will be reported and the system should resume
 * into "Initial" state.
 * @note This must be larger than 5000ms because that's the amount of time that
 *  the sensor must stay in "calibrated" status before it counts as "calibrated"
 */
#define SENSOR_CALIBRATION_TIMEOUT_MS 600000

/**
 * @brief Controls whether pre-calibration is done on the BNO055.
 *
 * If this is set to 1, the calibration profile defined in config.h is pushed to
 * the sensor just after initialization (before "Calibrate"!)
 * @note Only set to 0 when you want to manually determine a calibration
 * profile.
 */
#define CONFIG_BNO055_PRECALIBRATION 1

/**
 * @brief Controls whether the accelerometer also gets precalibration values.
 *
 * The accelerometer doesn't really need the precalibration, since it is
 * factory-precalibrated. Nevertheless, we can get better results by pushing
 * values. These are hard to get right, so if we're not confident in them,
 * better not to push them.
 */
#define CONFIG_BNO055_ACCEL_PRECALIBRATION 1

// Side note: In testing, we always found both BNOs to arrive at the same
// calibration offsets. Seems to be quite deterministic (?)
// TODO: Double check whether this is correct.

#define CONFIG_BNO_ACCEL_OFFSET_X_1 11
#define CONFIG_BNO_ACCEL_OFFSET_Y_1 1
#define CONFIG_BNO_ACCEL_OFFSET_Z_1 79
#define CONFIG_BNO_ACCEL_OFFSET_R_1 1000

#define CONFIG_BNO_ACCEL_OFFSET_X_2 11
#define CONFIG_BNO_ACCEL_OFFSET_Y_2 -1
#define CONFIG_BNO_ACCEL_OFFSET_Z_2 66
#define CONFIG_BNO_ACCEL_OFFSET_R_2 1000

#define CONFIG_BNO_GYRO_OFFSET_X_1 -2
#define CONFIG_BNO_GYRO_OFFSET_Y_1 2
#define CONFIG_BNO_GYRO_OFFSET_Z_1 3

#define CONFIG_BNO_GYRO_OFFSET_X_2 0
#define CONFIG_BNO_GYRO_OFFSET_Y_2 -7
#define CONFIG_BNO_GYRO_OFFSET_Z_2 0

#define CONFIG_BNO_MAG_OFFSET_X_1 -3
#define CONFIG_BNO_MAG_OFFSET_Y_1 -7
#define CONFIG_BNO_MAG_OFFSET_Z_1 1
#define CONFIG_BNO_MAG_OFFSET_R_1 983

#define CONFIG_BNO_MAG_OFFSET_X_2 -37
#define CONFIG_BNO_MAG_OFFSET_Y_2 26
#define CONFIG_BNO_MAG_OFFSET_Z_2 -3
#define CONFIG_BNO_MAG_OFFSET_R_2 742
