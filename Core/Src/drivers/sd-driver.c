/**
 *******************************************************************************
 * @file           sd-driver.c
 * @brief          Driver that initializes and talks to the SD.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdio.h>
#include <string.h>

#include "bsp_driver_sd.h"
#include "cmsis_os.h"
#include "fatfs.h"

#include "config.h"
#include "drivers/sd-driver.h"

#if configUSE_TRACE_FACILITY
extern traceString sd_channel;
#endif

#if !USE_SD_CARD_HAL

/* Local macros ------------------------------------------------------------- */

    /** Number of distinct files that logging entries are written into. */
    #define NUM_FILES 3

/* Private variables -------------------------------------------------------- */

static char logging_dir_name[10];

static const char *logfile_names[NUM_FILES] = {
    "Events.log",
    "Errors.log",
    "Data.log",
};

static FIL logfile_handles[NUM_FILES];

/* Local macros ------------------------------------------------------------- */

    /** Convenience macro for unmounting, makes the code more understandable. */
    #define f_unmount(path) f_mount(NULL, path, 0)

/* Mounting and unmounting -------------------------------------------------- */

bool sd_card_mount(void) {

    #if configUSE_TRACE_FACILITY
    vTracePrint(sd_channel, "Detecting whether SD card is present...");
    #endif /* configUSE_TRACE_FACILITY */

    if (BSP_SD_IsDetected() != SD_PRESENT) {
    #if configUSE_TRACE_FACILITY
        vTracePrint(sd_channel, "No SD card detected! Skipping init.");
    #endif /* configUSE_TRACE_FACILITY */
        return false;
    }

    #if configUSE_TRACE_FACILITY
    vTracePrint(sd_channel, "SD detected. Initializing...");
    #endif /* configUSE_TRACE_FACILITY */
    osDelay(50 / portTICK_PERIOD_MS);

    // force mounting right now (third argument = 1), this allows early
    // detection in case it doesn't work
    FRESULT ret = FR_OK;
    if ((ret = f_mount(&SDFatFS, SDPath, 1)) != FR_OK) {
    #if configUSE_TRACE_FACILITY
        vTracePrintF(sd_channel, "Could not mount SD card. Reason: (%i)", ret);
    #endif /* configUSE_TRACE_FACILITY */
        return false;
    }

    #if configUSE_TRACE_FACILITY
    vTracePrint(sd_channel, "SD mounting successful.");
    #endif /* configUSE_TRACE_FACILITY */
    return true;
}

bool sd_card_unmount(void) {
    for (uint_fast8_t i = 0; i < NUM_FILES; i++) {
        f_close(&logfile_handles[i]);
    }
    return f_unmount(SDPath) == FR_OK;
}

/* Files and folders -------------------------------------------------------- */

bool sd_setup_logging() {
    /* find number of directories already created and create new directory
        with an increased name */
    int num_dir = count_subdirectories(SDPath);

    // make new folder with number as directory name, left-aligned as "0007"
    snprintf(logging_dir_name, 10, "%s%04i", SDPath, num_dir + 1);

    FRESULT ret;
    while ((ret = f_mkdir(logging_dir_name)) != FR_OK) {
        if (ret == FR_EXIST) {
            // for some reason we have a name collision. increase target name
            // by one and try again
            num_dir++;
            snprintf(logging_dir_name, 10, "%s%04i", SDPath, num_dir);
        } else {
            break;
        }
    }
    if (ret == FR_OK) {
    #if configUSE_TRACE_FACILITY
        vTracePrintF(sd_channel, "Created new directory %s for logging.",
                     logging_dir_name);
    #endif /* configUSE_TRACE_FACILITY */
    } else {
    #if configUSE_TRACE_FACILITY
        vTracePrintF(sd_channel,
                     "Could not create new directory '%s'."
                     " Reason: (%i).",
                     logging_dir_name, ret);
    #endif /* configUSE_TRACE_FACILITY */
        return false;
    }

    char file_name[30];

    for (uint_fast8_t i = 0; i < NUM_FILES; i++) {
        snprintf(file_name, 30, "%s/%s", logging_dir_name,
                 logfile_names[(uint8_t)i]);

        ret =
            f_open(&(logfile_handles[i]), file_name, FA_WRITE | FA_OPEN_APPEND);
        if (ret != FR_OK) {
    #if configUSE_TRACE_FACILITY
            vTracePrintF(sd_channel,
                         "Could not open log file. FATFS return code: (%i)",
                         ret);
    #endif
            return false;
        }
    }

    return ret == FR_OK;
}

bool sd_write(char *buf, enum SDFile target_file) {
    unsigned int bytes_to_write = strlen(buf);
    unsigned int bytes_written = 0;
    FRESULT ret = f_write(&logfile_handles[(uint8_t)target_file - 1], buf,
                          bytes_to_write, &bytes_written);

    if (ret != FR_OK) {
    #if configUSE_TRACE_FACILITY
        vTracePrintF(sd_channel,
                     "Could not write to file. "
                     "FATFS return code: (%i)",
                     ret);
    #endif /* configUSE_TRACE_FACILITY */
        // f_close(&logfile_handles[(uint8_t)target_file]);
        return false;
    } else {
        if (bytes_written < bytes_to_write) {
    #if configUSE_TRACE_FACILITY
            vTracePrint(sd_channel, "Volume full! Not all bytes written.");
    #endif /* configUSE_TRACE_FACILITY */
            return false;
        } else {
    #if configUSE_TRACE_FACILITY
            vTracePrintF(sd_channel, "Successfully wrote %u bytes to SD.",
                         bytes_written);
    #endif
            return true;
        }
    }
}

bool sd_sync(void) {
    bool all_succeeded = true;
    FRESULT ret;

    for (uint_fast8_t i = 0; i < NUM_FILES; i++) {
        ret = f_sync(&logfile_handles[i]);
        if (ret != FR_OK) {
            // error occurred in at least one file
    #if configUSE_TRACE_FACILITY
            vTracePrintF(
                sd_channel,
                "f_sync() failed for file with index %u. Return code: %i", i,
                ret);
    #endif
            all_succeeded = false;
        }
    }

    #if configUSE_TRACE_FACILITY
    if (all_succeeded)
        vTracePrint(sd_channel, "f_sync successful for all files");
    #endif

    return all_succeeded;
}

/* Information about the file system ---------------------------------------- */

float free_diskspace_percentage(void) {
    FATFS *fs = &SDFatFS; // need double pointer for API

    FRESULT ret;
    DWORD free_clust, free_sect, tot_sect;

    if ((ret = f_getfree(SDPath, &free_clust, &fs)) != FR_OK) {
    #if configUSE_TRACE_FACILITY
        vTracePrintF(sd_channel, "Could not get no. of free clusters. (%i)",
                     ret);
    #endif /* configUSE_TRACE_FACILITY */
        return -1;
    }

    /* Calculate total sectors and free sectors */
    tot_sect = (SDFatFS.n_fatent - 2) * SDFatFS.csize;
    free_sect = free_clust * SDFatFS.csize;

    return (float)free_sect / tot_sect;
}

int count_subdirectories(char *path) {
    DIR dir;
    FRESULT ret;

    // First step: open directory whose subdirectories should be counted
    if ((ret = f_opendir(&dir, path)) != FR_OK) {
    #if configUSE_TRACE_FACILITY
        vTracePrintF(sd_channel,
                     "Could not open path \"%s\". "
                     "FATFS return code: (%i)",
                     path, ret);
    #endif /* configUSE_TRACE_FACILITY */
        return -1;
    }

    int num_dir = 0;
    FILINFO file_info;

    // f_readdir() will just keep returning FR_OK; after the last directory has
    // been seen, it will just store an empty filename, but still return FR_OK
    while ((ret = f_readdir(&dir, &file_info)) == FR_OK) {
        if (file_info.fname[0] == '\0') { // detect end of directory
            break;
        }

        if ((file_info.fattrib & AM_DIR) && (file_info.fname[0] != '.')) {
            // We only want to count directories that uses created, excluding
            // system directories like '.Trash' and so on from e.g. macOS
    #if configUSE_TRACE_FACILITY
            vTracePrintF(sd_channel, "Recognized directory with name '%s'.",
                         file_info.fname);
    #endif /* configUSE_TRACE_FACILITY */
            num_dir++;
        }
    }

    #if configUSE_TRACE_FACILITY
    vTracePrintF(sd_channel, "Recognized %i user directories on disk.",
                 num_dir);
    #endif /* configUSE_TRACE_FACILITY */

    if (ret != FR_OK) {
    #if configUSE_TRACE_FACILITY
        vTracePrint(sd_channel,
                    "Some error apparently occurred while enumerating"
                    "directories.");
    #endif /* configUSE_TRACE_FACILITY */
        return -1;
    }

    return num_dir;
}

#endif /* USE_SD_CARD_HAL */
