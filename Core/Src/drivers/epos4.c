/**
 *******************************************************************************
 * @file           epos4.c
 * @brief          Driver talking to the EPOS4 motor controller.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include "drivers/epos4.h"
#include "config.h"
#include "tasks/sd-log.h"

#if !USE_MOTOR_HAL

uint16_t calculateCRC(uint8_t *data, uint8_t len);
EPOS4_Status_t WriteCommand(uint8_t *command, uint8_t *data, uint8_t *rx_buffer,
                            UART_HandleTypeDef *huart);
EPOS4_Status_t ReadCommand(uint8_t *command, uint8_t *rx_buffer,
                           UART_HandleTypeDef *huart);

    #define EPOS4_FLAG_TX_CPLT (1U << 27)
    #define EPOS4_FLAG_RX_CPLT (1U << 28)

osThreadId_t waiting_taskHandle = NULL;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// This function is the low level command to write to the EPOS4.
// To write something, first the command bytes have to be set, in what register
// A write is needed -> command argument
// What is written into that register is saved into Data
// Then the CRC is calculated and the command is sent per UART via the DMA
// The EPOS4 always returns something and as such we check if the returned value
// yielded an error. The returned value is also returned into the rx_buffer
// Write function from communication guide page 2-12
EPOS4_Status_t WriteCommand(uint8_t *command, uint8_t *data, uint8_t *rx_buffer,
                            UART_HandleTypeDef *huart) {

    EPOS4_Status_t status = EPOS4_ERROR;

    uint8_t byte_stream_write[14] = {0};
    uint8_t dma_buffer[10] = {0};

    byte_stream_write[0] = 0x90; // DLE - frame synchronization 0x90
    byte_stream_write[1] = 0x02; // STX - frame synchronization 0x02
    byte_stream_write[2] =
        0x68; // Write Object-OpCode from Header --> Request Frame
    byte_stream_write[3] =
        0x04; // Length of Data in Words - Length from Header --> Request Frame
    byte_stream_write[4] = 0x01; // Node ID
    byte_stream_write[5] =
        command[1]; // Index Low Byte - low Byte transmitted first
    byte_stream_write[6] = command[0]; // Index High byte
    byte_stream_write[7] = command[2]; // Subindex of object --> always 0x00
    byte_stream_write[8] = data[3];    // Data - low byte first
    byte_stream_write[9] = data[2];    // Data
    byte_stream_write[10] = data[1];   // Data
    byte_stream_write[11] = data[0];   // Data - high byte

    /* CRC Calculation */
    uint8_t crc_data_array[10] = {0};
    memcpy(crc_data_array, &byte_stream_write[2],
           10 * sizeof(*byte_stream_write));

    uint16_t crc_calc = 0;
    crc_calc = calculateCRC(crc_data_array, 10);

    byte_stream_write[12] = crc_calc & 0xFF;
    ; // CRC low byte
    byte_stream_write[13] = (crc_calc >> 8) & 0xFF;
    ; // CRC high byte

    waiting_taskHandle = osThreadGetId();

    HAL_UART_Transmit_IT(huart, byte_stream_write, 14);
    osThreadFlagsWait(EPOS4_FLAG_TX_CPLT, osFlagsWaitAll | osFlagsNoClear,
                      5 / portTICK_PERIOD_MS);

    HAL_UART_Receive_IT(huart, dma_buffer, 10);

    // Wait & clear the flags such that all other flags are preserved
    // Note: We're not waiting for the RX flag to be received, because currently
    // the receiving lines show weird behaviour. We opt to not get feedback
    // instead.
    osThreadFlagsWait(EPOS4_FLAG_TX_CPLT | EPOS4_FLAG_RX_CPLT,
                      osFlagsWaitAll | osFlagsNoClear, 5 / portTICK_PERIOD_MS);
    osThreadFlagsClear(EPOS4_FLAG_TX_CPLT | EPOS4_FLAG_RX_CPLT);

    memcpy(rx_buffer, dma_buffer, 10);

    /* Check if we have an error code */
    if ((rx_buffer[7] | rx_buffer[6] | rx_buffer[5] | rx_buffer[4]) == 0) {
        status = EPOS4_OK;
    }

    return status;
}

EPOS4_Status_t ReadCommand(uint8_t *command, uint8_t *rx_buffer,
                           UART_HandleTypeDef *huart) {

    EPOS4_Status_t status = EPOS4_ERROR;

    uint8_t byte_stream_read[10];
    uint8_t dma_buffer[14] = {0};

    byte_stream_read[0] = 0x90;       // DLE
    byte_stream_read[1] = 0x02;       // STX
    byte_stream_read[2] = 0x60;       // Read Object
    byte_stream_read[3] = 0x02;       // Length of stuff sent
    byte_stream_read[4] = 0x01;       // Node ID
    byte_stream_read[5] = command[1]; // Index Low Byte
    byte_stream_read[6] = command[0]; // Index High byte
    byte_stream_read[7] = command[2]; // Subindex of object

    /* CRC data array */
    uint8_t crc_data_array[6] = {0};
    memcpy(crc_data_array, &byte_stream_read[2], 6 * sizeof(*byte_stream_read));

    uint16_t crc_calc = 0;
    crc_calc = calculateCRC(crc_data_array, 6);

    byte_stream_read[8] = crc_calc & 0xFF;
    ; // CRC low byte
    byte_stream_read[9] = (crc_calc >> 8) & 0xFF;
    ; // CRC high byte

    waiting_taskHandle = osThreadGetId();

    HAL_UART_Transmit_IT(huart, byte_stream_read, 10);
    osThreadFlagsWait(EPOS4_FLAG_TX_CPLT, osFlagsWaitAll | osFlagsNoClear,
                      5 / portTICK_PERIOD_MS);
    HAL_UART_Receive_IT(huart, dma_buffer, 14);

    // Wait & clear the flags such that all other flags are preserved
    // Note: We're not waiting for the RX flag to be received, because currently
    // the receiving lines show weird behaviour. We opt to not get feedback
    // instead.
    osThreadFlagsWait(EPOS4_FLAG_TX_CPLT | EPOS4_FLAG_RX_CPLT,
                      osFlagsWaitAll | osFlagsNoClear, 5 / portTICK_PERIOD_MS);
    osThreadFlagsClear(EPOS4_FLAG_TX_CPLT | EPOS4_FLAG_RX_CPLT);

    memcpy(rx_buffer, dma_buffer, 14);

    /* check if we have an error code */
    if ((rx_buffer[7] | rx_buffer[6] | rx_buffer[5] | rx_buffer[4]) == 0) {
        status = EPOS4_OK;
    }
    return status;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// This function calculates the crc code which is needed at the end of the
// command To successfully send a command to the EPOS4 a CRC code needs to be
// calculated and attached to the command (last 2 bytes). See Datasheet
// Communication Guide, chapter 2.2.3 for more information
uint16_t calculateCRC(uint8_t *data, uint8_t len) {

    uint16_t shifter, c;
    uint16_t carry;
    uint16_t crc = 0;

    for (int i = 0; i < len + 2; i += 2) {
        shifter = 0x8000;
        if (i == len) {
            c = 0;
        } else {
            c = data[i + 1] << 8 | data[i];
        }
        do {
            carry = crc & 0x8000;
            crc <<= 1;
            if (c & shifter)
                crc++;
            if (carry)
                crc ^= 0x1021;
            shifter >>= 1;
        } while (shifter);
    }
    return crc;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// Enable Motor, if unsuccessful it returns an EPOS4_Error.
// Has to be called at beginning for further motor commands
EPOS4_Status_t EnableMotor(UART_HandleTypeDef *huart) {

    EPOS4_Status_t status = EPOS4_ERROR;

    uint8_t command[3];
    uint8_t data[4];
    uint8_t rx_buffer_write[20];

    /* Register for Motor Control */
    command[0] = 0x60;
    command[1] = 0x40;
    command[2] = 0x00;

    data[0] = 0x00;
    data[1] = 0x00;
    data[2] = 0x00;
    data[3] = 0x06;
    // command: [0x6040 0x00 0x06 0x00]
    status = WriteCommand(command, data, rx_buffer_write, huart);

    /* Register for Motor Control */
    command[0] = 0x60;
    command[1] = 0x40;
    command[2] = 0x00;

    /* Fully Enable Controller */
    data[0] = 0x00;
    data[1] = 0x00;
    data[2] = 0x00;
    data[3] = 0x0F;
    // command: [0x6040 0x00 0x0F 0x00]
    status = WriteCommand(command, data, rx_buffer_write, huart);

    /* Check if Motor is enabled */
    uint8_t rx_buffer_read[20];

    command[0] = 0x60;
    command[1] = 0x41;
    command[2] = 0x00;

    status = ReadCommand(command, rx_buffer_read, huart);

    if (rx_buffer_read[8] == 0x37 && rx_buffer_read[9] == 0x04) {
        status = EPOS4_OK;
    }

    return status;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

EPOS4_Status_t DisableMotor(UART_HandleTypeDef *huart) {

    EPOS4_Status_t status = EPOS4_ERROR;

    uint8_t command[3];
    uint8_t data[4];
    uint8_t rx_buffer_write[20];

    /* Register for Motor Control */
    command[0] = 0x60;
    command[1] = 0x40; // change from 0x41
    command[2] = 0x00;

    data[0] = 0x00;
    data[1] = 0x00;
    data[2] = 0x00;
    data[3] = 0x00;

    status = WriteCommand(command, data, rx_buffer_write, huart);

    sd_printf(EventEntry, "Status_Disable,%+01i", status);

    return status;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

EPOS4_Status_t InitDisableMotor(UART_HandleTypeDef *huart) {

    EPOS4_Status_t status = EPOS4_ERROR;

    uint8_t command[3];
    uint8_t data[4];
    uint8_t rx_buffer_write[20];

    /* Register for Motor Control */
    command[0] = 0x60;
    command[1] = 0x41;
    command[2] = 0x00;

    data[0] = 0x00;
    data[1] = 0x00;
    data[2] = 0x00;
    data[3] = 0x00;

    status = WriteCommand(command, data, rx_buffer_write, huart);

    sd_printf(EventEntry, "Status_InitDisbale,%+01i", status);

    return status;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// Cyclic Synchronous Position Mode (CPM) -> faster, less stable --> 0x08,
// smaller inputs, higher frequency
EPOS4_Status_t SetCyclicPositionMode(UART_HandleTypeDef *huart) {

    EPOS4_Status_t status = EPOS4_ERROR;

    uint8_t command[3];
    uint8_t data[4];
    uint8_t rx_buffer_write[20];

    /* Position Mode Register */
    command[0] = 0x60;
    command[1] = 0x60;
    command[2] = 0x00;

    /*Enable  Position Mode */
    data[0] = 0x00;
    data[1] = 0x00;
    data[2] = 0x00;
    data[3] = 0x08;

    status = WriteCommand(command, data, rx_buffer_write, huart);

    sd_printf(EventEntry, "Status_SetCyclicPositionMode,%+01i", status);

    return status;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// This function writes the desired position into the EPOS4 position register
// Afterwards it tells the EPOS 4 to update the position and then we disable
// The movement again. Position [increments], 360° == 618 increments
EPOS4_Status_t MoveToPosition(UART_HandleTypeDef *huart, int32_t position) {

    EPOS4_Status_t status = EPOS4_ERROR;

    uint8_t command[3];
    uint8_t data[4];
    uint8_t rx_buffer_write[20];

    /* Write Desired Position */
    command[0] = 0x60;
    command[1] = 0x7A;
    command[2] = 0x00;
    // position (eg. = 287454020) --> 287454020 = 0x11223344 (Hex, 4 Bytes
    // value) 0xFF = 00000000 00000000 00000000 11111111 (32bit)
    data[0] = (position >> 24) & 0xFF; // data[0]= 11, fist byte
    data[1] = (position >> 16) & 0xFF; // data[1]= 22, second byte
    data[2] = (position >> 8) & 0xFF;  // data[1]= 33, third byte
    data[3] = position & 0xFF;         // data[1]= 44, fourth byte

    status = WriteCommand(command, data, rx_buffer_write, huart);

    /* Goto Position */
    command[0] = 0x60;
    command[1] = 0x40;
    command[2] = 0x00;

    data[0] = 0x00;
    data[1] = 0x00;
    data[2] = 0x00;
    data[3] = 0x3F;

    status = WriteCommand(command, data, rx_buffer_write, huart);

    /* Disable Movement Again */
    command[0] = 0x60;
    command[1] = 0x40;
    command[2] = 0x00;

    data[0] = 0x00;
    data[1] = 0x00;
    data[2] = 0x00;
    data[3] = 0x0F;

    status = WriteCommand(command, data, rx_buffer_write, huart);

    sd_printf(EventEntry, "Status_MoveToPosition,%+01i", status);

    return status;
}

EPOS4_Status_t GetPosition(UART_HandleTypeDef *huart, int32_t *position) {

    EPOS4_Status_t status = EPOS4_ERROR;

    uint8_t command[3];
    uint8_t rx_buffer_read[20];

    /* Register where the Current Position is written */
    command[0] = 0x60;
    command[1] = 0x64;
    command[2] = 0x00;

    status = ReadCommand(command, rx_buffer_read, huart);

    *position = rx_buffer_read[8] + (rx_buffer_read[9] << 8) +
                (rx_buffer_read[10] << 16) + (rx_buffer_read[11] << 24);

    sd_printf(EventEntry, "Status_GetPosition,%+01i", status);

    return status;
}

EPOS4_Status_t SetPositionProfilePPM(UART_HandleTypeDef *huart,
                                     int32_t velocity, int32_t acceleration,
                                     int32_t deceleration) {

    EPOS4_Status_t status = EPOS4_ERROR;

    uint8_t command[3];
    uint8_t data[4];
    uint8_t rx_buffer_write[20];

    /* Position Mode Register */
    command[0] = 0x60;
    command[1] = 0x60;
    command[2] = 0x00;

    /*Enable  Position Mode */
    int8_t position_mode = 0x01;
    data[0] = 0x00;
    data[1] = 0x00;
    data[2] = 0x00;
    data[3] = position_mode;

    status = WriteCommand(command, data, rx_buffer_write, huart);

    /* Configure desired Velocity to be reached during Position Change */
    command[0] = 0x60;
    command[1] = 0x81;
    command[2] = 0x00;

    data[0] = (velocity >> 24) & 0xFF;
    data[1] = (velocity >> 16) & 0xFF;
    data[2] = (velocity >> 8) & 0xFF;
    data[3] = velocity & 0xFF;

    status = WriteCommand(command, data, rx_buffer_write, huart);

    /* Configure desired acceleration to be reached during Position Change */
    command[0] = 0x60;
    command[1] = 0x83;
    command[2] = 0x00;

    data[0] = (acceleration >> 24) & 0xFF;
    data[1] = (acceleration >> 16) & 0xFF;
    data[2] = (acceleration >> 8) & 0xFF;
    data[3] = acceleration & 0xFF;

    status = WriteCommand(command, data, rx_buffer_write, huart);

    /* Configure desired deceleration to be reached during Position Change */
    command[0] = 0x60;
    command[1] = 0x84;
    command[2] = 0x00;

    data[0] = (deceleration >> 24) & 0xFF;
    data[1] = (deceleration >> 16) & 0xFF;
    data[2] = (deceleration >> 8) & 0xFF;
    data[3] = deceleration & 0xFF;

    status = WriteCommand(command, data, rx_buffer_write, huart);

    sd_printf(EventEntry, "Status_SetProfilePositionMode,%+01i", status);

    return status;
}

/*Set homing parameter and define Homing position+method*/
EPOS4_Status_t SetHomingModeHHM(UART_HandleTypeDef *huart,
                                uint32_t homing_acceleration,
                                uint32_t speed_switch, uint32_t speed_index,
                                int32_t home_offset, uint16_t current_threshold,
                                int32_t home_position) {

    EPOS4_Status_t status = EPOS4_ERROR;

    uint8_t command[3];
    uint8_t data[4];
    uint8_t rx_buffer_write[20];

    /* Set Homing Mode */
    command[0] = 0x60;
    command[1] = 0x60;
    command[2] = 0x00;

    data[0] = 0x00;
    data[1] = 0x00;
    data[2] = 0x00;
    data[3] = 0x06;

    status = WriteCommand(command, data, rx_buffer_write, huart);

    /* Set Homing Parameter */
    /* homing acceleration */
    command[0] = 0x60;
    command[1] = 0x9A;
    command[2] = 0x00;

    data[0] = (homing_acceleration >> 24) & 0xFF;
    data[1] = (homing_acceleration >> 16) & 0xFF;
    data[2] = (homing_acceleration >> 8) & 0xFF;
    data[3] = homing_acceleration & 0xFF;

    status = WriteCommand(command, data, rx_buffer_write, huart);

    /* speed switch */
    command[0] = 0x60;
    command[1] = 0x99;
    command[2] = 0x01;

    data[0] = (speed_switch >> 24) & 0xFF;
    data[1] = (speed_switch >> 16) & 0xFF;
    data[2] = (speed_switch >> 8) & 0xFF;
    data[3] = speed_switch & 0xFF;

    status = WriteCommand(command, data, rx_buffer_write, huart);

    /* speed index */
    command[0] = 0x60;
    command[1] = 0x99;
    command[2] = 0x02;

    data[0] = (speed_index >> 24) & 0xFF;
    data[1] = (speed_index >> 16) & 0xFF;
    data[2] = (speed_index >> 8) & 0xFF;
    data[3] = speed_index & 0xFF;

    status = WriteCommand(command, data, rx_buffer_write, huart);

    /* home offset */
    command[0] = 0x30;
    command[1] = 0xB1;
    command[2] = 0x00;

    data[0] = (home_offset >> 24) & 0xFF;
    data[1] = (home_offset >> 16) & 0xFF;
    data[2] = (home_offset >> 8) & 0xFF;
    data[3] = home_offset & 0xFF;

    status = WriteCommand(command, data, rx_buffer_write, huart);

    /* current threshold */
    command[0] = 0x30;
    command[1] = 0xB2;
    command[2] = 0x00;

    data[0] = 0x00;
    data[1] = 0x00;
    data[2] = (current_threshold >> 8) & 0xFF;
    data[3] = current_threshold & 0xFF;

    status = WriteCommand(command, data, rx_buffer_write, huart);

    /* home position */
    command[0] = 0x30;
    command[1] = 0xB0;
    command[2] = 0x00;

    data[0] = (home_position >> 24) & 0xFF;
    data[1] = (home_position >> 16) & 0xFF;
    data[2] = (home_position >> 8) & 0xFF;
    data[3] = home_position & 0xFF;

    status = WriteCommand(command, data, rx_buffer_write, huart);

    sd_printf(EventEntry, "Status_SetHomingMode,%+01i", status);

    return status;
}

EPOS4_Status_t FindHome(UART_HandleTypeDef *huart) {

    EPOS4_Status_t status = EPOS4_ERROR;

    uint8_t command[3];
    uint8_t data[4];
    uint8_t rx_buffer_write[20];

    /* Set homing method - actual position */

    command[0] = 0x60;
    command[1] = 0x98;
    command[2] = 0x00;

    data[0] = 0x00;
    data[1] = 0x00;
    data[2] = 0x00;
    data[3] = 0x25; // defines actual position as home --> 0

    status = WriteCommand(command, data, rx_buffer_write, huart);

    command[0] = 0x60;
    command[1] = 0x40;
    command[2] = 0x00;

    data[0] = 0x00;
    data[1] = 0x00;
    data[2] = 0x00;
    data[3] = 0x0F;

    status = WriteCommand(command, data, rx_buffer_write, huart);

    command[0] = 0x60;
    command[1] = 0x40;
    command[2] = 0x00;

    data[0] = 0x00;
    data[1] = 0x00;
    data[2] = 0x00;
    data[3] = 0x1F;

    status = WriteCommand(command, data, rx_buffer_write, huart);

    sd_printf(EventEntry, "Status_FindHome,%+01i", status);

    return status;
}

/* UART interrupt callbacks ------------------------------------------------- */

void EPOS_UART_TxCplt_Callback(void) {
    if (waiting_taskHandle != NULL) {
        osThreadFlagsSet(waiting_taskHandle, EPOS4_FLAG_TX_CPLT);
    }
}

/**
 * @brief Callback that is invoked when the UART IT transfer has completed.
 */
void EPOS_UART_RxCplt_Callback(void) {
    if (waiting_taskHandle != NULL) {
        osThreadFlagsSet(waiting_taskHandle, EPOS4_FLAG_RX_CPLT);
    }
}

#endif
