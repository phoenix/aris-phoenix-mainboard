/**
 *******************************************************************************
 * @file           bno-055.c
 * @brief          Implements the sensor communication with Bosch Sensortec's
 *                      BNO055, based on the Bosch Reference Implementation.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "cmsis_os.h"

#include "bno-055-bosch.h"
#include "config.h"
#include "drivers/bno-055.h"
#include "main.h"
#include "tasks/sd-log.h"
#include "tasks/sensor-task.h"

#if !USE_SENSOR_DATA_HAL

/* Detect configuration mistakes in config.h -------------------------------- */

    #if CONFIG_BNO055_PRECALIBRATION == 0
        #if CONFIG_BNO055_ACCEL_PRECALIBRATION == 1
            #warning CONFIG_BNO055_ACCEL_PRECALIBRATION has not effect!
        #endif
    #endif

/* Tracing and Live Data View ----------------------------------------------- */

    #if configUSE_TRACE_FACILITY
traceString bno055_chn = NULL;
traceString acc_x_chn = NULL;
traceString acc_y_chn = NULL;
traceString acc_z_chn = NULL;
    #endif

/* Faulty sensor value detection -------------------------------------------- */

    //! Minimum value for the heading that is accepted: >= 0 radians <==>  > -1
    #define BNO055_MIN_QUAT_COMP (-16384)
    #define BNO055_MAX_QUAT_COMP (+16384)

    //! Minimum linear acceleration that is accepted: -160 m/s^2
    #define BNO055_MIN_ACCEL (-160 * 100)
    //! Maximum linear acceleration that is accepted: +160 m/s^2
    #define BNO055_MAX_ACCEL (160 * 100)

    //! Minimum angular velocity that is accepted: -35 rad/s
    #define BNO055_MIN_RPS (-31500)
    //! Maximum angular velocity that is accepted: 35 rad/s
    #define BNO055_MAX_RPS (31500)

/* Imported BNO-055 Sensortec "private" variable ---------------------------- */

/**
 * @brief This is an imported variable from the BNO-055 Bosch driver.
 *
 * This variable is used to control the settings of the sensor that the API
 * currently talks to. Unfortunately, the Bosch driver doesn't support multiple
 * devices. But one can just change this pointer to the instance we want the API
 * to talk to. This doesn't need to be done during the reset(), because
 * bno055_init() will automatically set this variable. But before any other API
 * call, one must set this to the bno_priv struct.
 */
extern struct bno055_t *p_bno055;

/* Low-level drivers for BNO-055 I2C communication -------------------------- */

/** I2C bus write function for the BNO055 driver */
int8_t bno055_write(uint8_t dev_addr, uint8_t reg_addr, uint8_t *data,
                    uint8_t cnt) {
    return i2c_master_write_slave_reg(dev_addr, reg_addr, data, cnt);
}

/** I2C bus read function for the BNO055 driver */
int8_t bno055_read(uint8_t dev_addr, uint8_t reg_addr, uint8_t *data,
                   uint8_t cnt) {
    return i2c_master_read_slave_reg(dev_addr, reg_addr, data, cnt);
}

/** Millisecond delay function for the BNO055 driver */
void bno055_delay(u32 delay) {
    osDelay(delay / portTICK_PERIOD_MS);
}

/* BNO-055 Sensor API ------------------------------------------------------- */

bool bno055_setup(BNO055_Handle *bno055) {
    #if configUSE_TRACE_FACILITY
    if (!bno055_chn) {
        bno055_chn = xTraceRegisterString("BNO-055 Events");
        acc_x_chn = xTraceRegisterString("Accel X");
        acc_y_chn = xTraceRegisterString("Accel Y");
        acc_z_chn = xTraceRegisterString("Accel Z");
    }
    vTracePrint(bno055_chn, "Setting up a BNO-055 sensor");
    #endif

    assert_param(bno055 != NULL);

    // Set up the pin connected to BNO's NRST as open drain output with pull-up
    bno055->rst_pin_setup = true;

    return bno055_reset(bno055);
}

bool bno055_reset(BNO055_Handle *bno055) {
    assert_param(bno055 != NULL);

    // make sure that the NRST gpio has been properly set up
    if (!bno055->rst_pin_setup)
        return false;

    // hold down NRST pin for at least 20ns, so round it up to 1ms wait (p. 18)
    HAL_GPIO_WritePin(bno055->reset_port, bno055->reset_pin, GPIO_PIN_RESET);
    osDelay(1);
    HAL_GPIO_WritePin(bno055->reset_port, bno055->reset_pin, GPIO_PIN_SET);

    // wait 650ms for BNO to start up (p. 13 datasheet)
    osDelay(650 / portTICK_RATE_MS);

    // This is the BNO055 initialization struct that helps the Bosch API
    // communicate with our implementation.
    memset(&(bno055->bno_priv), 0, sizeof(struct bno055_t)); // zero out
    bno055->bno_priv.bus_write = &bno055_write;
    bno055->bno_priv.bus_read = &bno055_read;
    bno055->bno_priv.delay_msec = &bno055_delay;
    bno055->bno_priv.dev_addr =
        bno055->use_primary_addr ? BNO055_I2C_ADDR1 : BNO055_I2C_ADDR2;

    int32_t status;
    status = bno055_init(&(bno055->bno_priv));

    if (status != 0) {
    #if configUSE_TRACE_FACILITY
        vTracePrintF(bno055_chn, "BNO-055 init failed with status (%i)",
                     status);
    #endif
        sd_log("BNO-055 init failed.", ErrorEntry);
        return false;
    }

    // Perform self test of the BNO's gyro, magnetometer and accelerometer
    if (bno055_set_selftest(BNO055_BIT_ENABLE) != BNO055_SUCCESS) {
        sd_log("BNO-055 self-test could not be started!", ErrorEntry);
        return false;
    } else {
        // wait 400ms for the self-test to be performed
        osDelay(400 / portTICK_PERIOD_MS);

        uint8_t selftest_accel = 0;
        uint8_t selftest_gyro = 0;
        uint8_t selftest_mag = 0;
        uint8_t selftest_mcu = 0;

        bno055_get_selftest_accel(&selftest_accel);
        bno055_get_selftest_gyro(&selftest_gyro);
        bno055_get_selftest_mag(&selftest_mag);
        bno055_get_selftest_mcu(&selftest_mcu);

        // all get set to 1 if the selftest was successful
        if (selftest_accel && selftest_gyro && selftest_mag && selftest_mcu) {
            sd_log("BNO-055 self-test passed.", EventEntry);
        } else {
            sd_log("BNO-055 self-test failed.", ErrorEntry);
            return false;
        }
    }

    // Set to active power mode to start
    bno055_set_power_mode(BNO055_POWER_MODE_NORMAL);

    uint16_t sw_version;
    bno055_read_sw_rev_id(&sw_version);
    #if configUSE_TRACE_FACILITY
    vTracePrintF(bno055_chn, "BNO-055 software version is %x", sw_version);
    #endif
    sd_printf(EventEntry, "BNO-055 software version is %x", sw_version);

    // Set units for the measurements
    if ((status = bno055_set_accel_unit(BNO055_ACCEL_UNIT_MSQ)) !=
        BNO055_SUCCESS) {
    #if configUSE_TRACE_FACILITY
        vTracePrintF(bno055_chn,
                     "Error in setting lin. acc. to m/s^2. Return code: (%i)",
                     status);
    #endif
        sd_log("Failed to set linear acceleration unit to m/s^2", ErrorEntry);
    }
    if ((status = bno055_set_gyro_unit(BNO055_GYRO_UNIT_RPS)) !=
        BNO055_SUCCESS) {
    #if configUSE_TRACE_FACILITY
        vTracePrintF(
            bno055_chn,
            "Could not set angular velocity unit to rad/s. Status: (%i)",
            status);
    #endif
        sd_log("Failed to set angular velocity unit to rad/s", ErrorEntry);
    }
    if ((status = bno055_set_euler_unit(BNO055_EULER_UNIT_RAD)) !=
        BNO055_SUCCESS) {
    #if configUSE_TRACE_FACILITY
        vTracePrintF(bno055_chn,
                     "Could not set euler angles unit to rad. Status: (%i)",
                     status);
    #endif
        sd_log("Could not set euler angles unit to rad.", ErrorEntry);
    }

    // Remap axes: The sensor's (+X)(+Y)(+Z) corresponds to our (+X)(-Z)(+Y)
    if ((status = bno055_set_axis_remap_value(BNO055_REMAP_Y_Z)) != 0) {
        sd_log("Could not remap Y-Z axes!", ErrorEntry);
    }

    if ((status = bno055_set_remap_y_sign(BNO055_REMAP_AXIS_POSITIVE)) != 0) {
        sd_log("Could not set positive y axis sign!", ErrorEntry);
    }

    if ((status = bno055_set_remap_z_sign(BNO055_REMAP_AXIS_NEGATIVE)) != 0) {
        sd_log("Could not set negative z axis sign!", ErrorEntry);
    }

    // set operation mode to nine-dof sensor fusion
    if ((status = bno055_set_operation_mode(BNO055_OPERATION_MODE_NDOF)) != 0) {
    #if configUSE_TRACE_FACILITY
        vTracePrintF(bno055_chn,
                     "Error setting BNO-055 into NDOF mode. Status: (%i)",
                     status);
    #endif
        sd_log("Error setting BNO-055 into NDOF mode.", ErrorEntry);
    }

    // setting the ranges for the accelerometer etc. isn't possible, since it's
    // managed by the 9-DoF sensor fusion algorithm

    return true;
}

bool bno055_lowpower_enable(BNO055_Handle *bno055) {
    assert_param(bno055 != NULL);
    p_bno055 = &bno055->bno_priv; // switch Bosch API pointer
    bno055_set_operation_mode(BNO055_OPERATION_MODE_CONFIG);
    return bno055_set_power_mode(BNO055_POWER_MODE_SUSPEND) == BNO055_SUCCESS;
}

bool bno055_wakeup(BNO055_Handle *bno055) {
    assert_param(bno055 != NULL);
    p_bno055 = &bno055->bno_priv; // switch Bosch API pointer
    bno055_set_power_mode(BNO055_POWER_MODE_NORMAL);
    return bno055_set_operation_mode(BNO055_OPERATION_MODE_NDOF) ==
           BNO055_SUCCESS;
}

bool bno055_sample(BNO055_Handle *bno055, struct BNO055_Readout *dest) {
    assert_param(bno055 != NULL);
    assert_param(dest != NULL);
    p_bno055 = &bno055->bno_priv; // switch Bosch API pointer

    struct bno055_linear_accel_t accel = {0};
    struct bno055_gyro_t angvel = {0};
    struct bno055_quaternion_t qtrn = {0};

    if (bno055_read_linear_accel_xyz(&accel) == BNO055_ERROR) {
    #if configUSE_TRACE_FACILITY
        vTracePrint(bno055_chn, "Failed to read acceleration.");
    #endif
        sd_log("Failed to read acceleration.", ErrorEntry);
        return false;
    }
    if (bno055_read_gyro_xyz(&angvel) == BNO055_ERROR) {
    #if configUSE_TRACE_FACILITY
        vTracePrint(bno055_chn, "Failed to read angular velocity.");
    #endif
        sd_log("Failed to read angular velocity.", ErrorEntry);
        return false;
    }
    if (bno055_read_quaternion_wxyz(&qtrn) == BNO055_ERROR) {
    #if configUSE_TRACE_FACILITY
        vTracePrint(bno055_chn, "Failed to read quaternion output.");
    #endif
        sd_log("Failed to read quaternion output.", ErrorEntry);
        return false;
    }

    // Check whether all values make sense
    if (!(accel.x > BNO055_MIN_ACCEL && accel.x < BNO055_MAX_ACCEL &&
          accel.y > BNO055_MIN_ACCEL && accel.y < BNO055_MAX_ACCEL &&
          accel.z > BNO055_MIN_ACCEL && accel.z < BNO055_MAX_ACCEL)) {
        sd_log("Faulty accelerometer reading of BNO-055 detected!", ErrorEntry);
        return false;
    }

    if (!(angvel.x > BNO055_MIN_RPS && angvel.x < BNO055_MAX_RPS &&
          angvel.y > BNO055_MIN_RPS && angvel.y < BNO055_MAX_RPS &&
          angvel.z > BNO055_MIN_RPS && angvel.z < BNO055_MAX_RPS)) {
        sd_log("Faulty gyroscope reading of BNO-055 detected!", ErrorEntry);
        return false;
    }

    if (!(qtrn.x >= BNO055_MIN_QUAT_COMP && qtrn.x <= BNO055_MAX_QUAT_COMP &&
          qtrn.y >= BNO055_MIN_QUAT_COMP && qtrn.y <= BNO055_MAX_QUAT_COMP &&
          qtrn.z >= BNO055_MIN_QUAT_COMP && qtrn.z <= BNO055_MAX_QUAT_COMP &&
          qtrn.w >= BNO055_MIN_QUAT_COMP && qtrn.w <= BNO055_MAX_QUAT_COMP)) {
        // One of the sensor values is out of bounds
        sd_log("Faulty quaternion reading of BNO-055 detected!", ErrorEntry);
        return false;
    } else if (qtrn.x == 0 && qtrn.y == 0 && qtrn.z == 0 && qtrn.w == 0) {
        // Read a zero quaternion, a great indicator that something is wrong
        sd_log("Zero quaternion reading of BNO-055 detected!", ErrorEntry);
        return false;
    }

    memcpy(dest->lin_acc.array, &accel, 3 * sizeof(int16_t));
    memcpy(dest->ang_vel.array, &angvel, 3 * sizeof(int16_t));
    memcpy(dest->qtr_att.array, &qtrn, 4 * sizeof(int16_t));
    dest->tick = (uint32_t)xTaskGetTickCount();

    #if configUSE_TRACE_FACILITY
    vTracePrintF(acc_x_chn, "Accel x: %7d", dest->lin_acc.x);
    vTracePrintF(acc_y_chn, "Accel y: %7d", dest->lin_acc.y);
    vTracePrintF(acc_z_chn, "Accel z: %7d", dest->lin_acc.z);
    #endif

    return true;
}

bool bno055_is_calibrated(BNO055_Handle *bno055) {
    assert_param(bno055 != NULL);
    p_bno055 = &bno055->bno_priv; // switch API pointer

    uint8_t magn_calib_stat = 0;
    uint8_t gyro_calib_stat = 0;
    uint8_t accl_calib_stat = 0;

    // As per recommendation by Bosch Sensortec, don't check the system
    // calibration status, because there is a bug in firmware 3.11. Instead,
    // if gyroscope and magnetometer are 3/3, the system can be considered
    // "calibrated"

    if ((bno055_get_mag_calib_stat(&magn_calib_stat) != BNO055_SUCCESS) ||
        (bno055_get_gyro_calib_stat(&gyro_calib_stat) != BNO055_SUCCESS) ||
        (bno055_get_accel_calib_stat(&accl_calib_stat) != BNO055_SUCCESS)) {
        sd_printf(EventEntry,
                  "Failed to receive the calib status of one sensor.");
        return false;
    }

    sd_printf(EventEntry,
              "BNO-055 Calibration Status. %u/3 (accel), %u/3 (gyro), %u/3 "
              "(magneto)",
              accl_calib_stat, gyro_calib_stat, magn_calib_stat);

    return magn_calib_stat == 3 && gyro_calib_stat == 3;
    // Source:
    // https://community.bosch-sensortec.com/t5/MEMS-sensors-forum/BNO055-Calibration-Staus-not-stable/m-p/8379/highlight/true#M926
}

bool bno055_push_calibration(BNO055_Handle *bno055) {
    assert(bno055 != NULL);
    p_bno055 = &bno055->bno_priv; // switch API pointer

    // If these fail, fail gracefully. Why? Because the sensor will be able
    // to calibrate itself, it will just take some time
    bool success = true;

    // The acceleration calibration we only want to write when we're confident
    // that we have a good profile.
    #if CONFIG_BNO055_ACCEL_PRECALIBRATION
    if (bno055_write_accel_offset(&(bno055->acc_precalib)) != BNO055_SUCCESS) {
        sd_printf(ErrorEntry, "Couldn't write accel offset!");
        success = false;
    }
    #endif /* CONFIG_BNO055_ACCEL_PRECALIBRATION */

    if (bno055_write_gyro_offset(&(bno055->gyro_precalib)) != BNO055_SUCCESS) {
        sd_printf(ErrorEntry, "Couldn't write gyro offset!");
        success = false;
    }

    if (bno055_write_mag_offset(&(bno055->mag_precalib)) != BNO055_SUCCESS) {
        sd_printf(ErrorEntry, "Couldn't write mag offset!");
        success = false;
    }

    if (success)
        sd_printf(EventEntry, "Wrote these offsets: ");
    else
        sd_printf(EventEntry, "Didn't write these offsets:");

    #if CONFIG_BNO055_ACCEL_PRECALIBRATION
    sd_printf(EventEntry,
              "A: .x=%i, .y=%i, .z = %i, .r=%i, G: .x=%i, .y=%i, .z=%i, "
              "M: .x=%i, .y=%i, .z=%i, .r=%i",
              bno055->acc_precalib.x, bno055->acc_precalib.y,
              bno055->acc_precalib.z, bno055->acc_precalib.r,
              bno055->gyro_precalib.x, bno055->gyro_precalib.y,
              bno055->gyro_precalib.z, bno055->mag_precalib.x,
              bno055->mag_precalib.y, bno055->mag_precalib.z,
              bno055->mag_precalib.r);
    #else
    sd_printf(
        EventEntry, "G: .x=%i, .y=%i, .z=%i, M: .x=%i, .y=%i, .z=%i, .r=%i",
        bno055->gyro_precalib.x, bno055->gyro_precalib.y,
        bno055->gyro_precalib.z, bno055->mag_precalib.x, bno055->mag_precalib.y,
        bno055->mag_precalib.z, bno055->mag_precalib.r);
    #endif /* CONFIG_BNO_ACCEL_PRECALIBRATION */

    return success;
}

/**
 * @brief Prints the current calibration values for the BNO055 to a serial
 * port.
 *
 * This function gets the calibration values for accelerometer, gyroscope
 * and magnetometer and writes them to the serial monitor.
 */
bool bno055_print_calibration(BNO055_Handle *bno055) {
    assert(bno055 != NULL);
    p_bno055 = &bno055->bno_priv; // switch API pointer

    // Current offsets will be read into these structs

    bool success = true;
    if (bno055_read_accel_offset(&(bno055->acc_precalib)) != BNO055_SUCCESS) {
        sd_printf(ErrorEntry, "Couldn't read accel offset!");
        success = false;
    }

    if (bno055_read_gyro_offset(&(bno055->gyro_precalib)) != BNO055_SUCCESS) {
        sd_printf(ErrorEntry, "Couldn't read gyro offset!");
        success = false;
    }

    if (bno055_read_mag_offset(&(bno055->mag_precalib)) != BNO055_SUCCESS) {
        sd_printf(ErrorEntry, "Couldn't read mag offset!");
        success = false;
    }

    if (success) {
        sd_printf(EventEntry, "Read those these offsets: ");
        sd_printf(EventEntry,
                  "A: .x=%i, .y=%i, .z = %i, .r=%i, G: .x=%i, .y=%i, .z=%i, "
                  "M: .x=%i, .y=%i, .z=%i, .r=%i",
                  bno055->acc_precalib.x, bno055->acc_precalib.y,
                  bno055->acc_precalib.z, bno055->acc_precalib.r,
                  bno055->gyro_precalib.x, bno055->gyro_precalib.y,
                  bno055->gyro_precalib.z, bno055->mag_precalib.x,
                  bno055->mag_precalib.y, bno055->mag_precalib.z,
                  bno055->mag_precalib.r);
    } else {
        sd_printf(ErrorEntry, "Couldn't read offsets.");
    }

    return success;
}

#endif
