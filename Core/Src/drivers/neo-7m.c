/**
 *******************************************************************************
 * @file           neo-7m.c
 * @brief          Driver for the NEO-7M GPS receiver.
 * @author         Lukas Vogel
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cmsis_os.h"
#include "config.h"
#include "drivers/neo-7m.h"
#include "main.h"
#include "tasks/sd-log.h"

#if !USE_SENSOR_DATA_HAL

/* Sensor plausibility check boundaries ------------------------------------- */

    #define GPS_LONGITUDE_LOWER_LIMIT 5.955906  ///< West-most point of CH
    #define GPS_LONGITUDE_UPPER_LIMIT 10.489279 ///< East-most point of CH
    #define GPS_LATITUDE_LOWER_LIMIT  45.817763 ///< South-most point of CH
    #define GPS_LATITUDE_UPPER_LIMIT  47.808355 ///< North-most point of CH
    #define GPS_ALTITUDE_LOWER_LIMIT  0.0       ///< 0.0m above sea level
    #define GPS_ALTITUDE_UPPER_LIMIT  10000.0   ///< 10km above sea level

/* Public function implementation ------------------------------------------- */

bool neo7m_init(struct NEO7M_Handle *neo7m) {
    if (!neo7m || !neo7m->huart)
        return false;

    neo7m->ready = false;
    neo7m->has_fix = false;
    neo7m->num_satellites = 0;
    memset(neo7m->recv_buf, 0, 255);
    neo7m->recv_index = 0;

    HAL_StatusTypeDef ret = HAL_OK;

    // Enable the half duplex receiver
    if ((ret = HAL_HalfDuplex_EnableReceiver(neo7m->huart)) != HAL_OK) {
        sd_printf(ErrorEntry,
                  "HAL DMA for NEO-7M could not be enabled. HAL status: (%i)",
                  ret);
        return false;
    }

    // Start the first byte transfer
    if ((ret = HAL_UART_Receive_DMA(neo7m->huart, &neo7m->recv_tmp, 1))) {
        sd_printf(ErrorEntry,
                  "Could not start NEO-7M DMA transfer. HAL status: (%i)", ret);
        return false;
    }

    return true;
}

bool neo7m_process(struct NEO7M_Handle *neo7m) {
    if (!neo7m)
        return false;

    // Process the received character. Rules:
    // - If it is a $, reset the buffer. We missed the last start and we don't
    //      want to mess up anything inside. No bad feelings, the next received
    //      message will be even better.
    // - If it is a \n, this marks the end of the NMEA message. Copy the recv
    //      buffer to the proc buffer (without the newline, the NMEA parser
    //      doesn't need it) and reset the recv buffer
    // - If recv_index is 255, we are at the end of the buffer (we seem also to
    //      have missed something?). Reset the recv buffer and wait for next $

    switch (neo7m->recv_tmp) {
        case '$':
            neo7m->recv_buf[0] = '$';
            neo7m->recv_index = 1;
            break;
        case '\n':
            // comment: I don't trigger on \r because that resets the NMEA
            // engine correctly, which I want to keep
            if (neo7m->recv_index > 0) {
                // recv_index will always be <= 254, so no overflow problems
                memcpy(neo7m->proc_buf, neo7m->recv_buf, neo7m->recv_index);
                neo7m->proc_buf[neo7m->recv_index] = '\0'; // nul-terminate
            }
            neo7m->recv_index = 0;
            neo7m->ready = true;
            if (neo7m->ready_cb != NULL)
                neo7m->ready_cb(neo7m->arg);
            break;
        default:
            // other character: add it to the buf and ensure no overflow happens
            if (neo7m->recv_index >= 254) {
                // arrived at 254, emergency reset and wrap around
                neo7m->recv_index = 0;
            }
            neo7m->recv_buf[neo7m->recv_index++] = neo7m->recv_tmp;
            break;
    }
    return true;
}

bool neo7m_get_readout(struct NEO7M_Handle *neo7m,
                       struct NEO7M_Readout *readout) {
    if (!neo7m || !neo7m->ready)
        return false;

    bool success = false;
    // it is harmless to cast this byte buffer since it is null-terminated
    for (uint_fast8_t i = 0; i < strlen((char *)neo7m->proc_buf); i++) {
        if (nmea_parse_new_char(neo7m->recv_buf[i], &neo7m->nmea) == true) {
            // a batch has been successfully processed, can stop
            success = true;
            break;
        }
    }

    if (success && neo7m->nmea.gps_frame == FRAME_GGA) {

        // Copy data into the target memory location
        neo7m->num_satellites = neo7m->nmea.gpsSol.numSat;
        neo7m->has_fix = neo7m->nmea.GPS_FIX == 1;
        readout->has_fix = neo7m->has_fix;
        readout->num_satellites = neo7m->num_satellites;
        readout->tick = osKernelGetTickCount();
        readout->position.elevation = neo7m->nmea.gpsSol.llh.altCm / 100.0;
        readout->position.latitude = neo7m->nmea.gpsSol.llh.lat / 1.0e+7;
        readout->position.longitude = neo7m->nmea.gpsSol.llh.lon / 1.0e+7;

        // Check whether the sensor value is within reasonable boundaries, if
        // not, return false.
        if (readout->position.elevation > GPS_ALTITUDE_UPPER_LIMIT ||
            readout->position.elevation < GPS_LATITUDE_LOWER_LIMIT ||
            readout->position.longitude > GPS_LONGITUDE_UPPER_LIMIT ||
            readout->position.longitude < GPS_LONGITUDE_LOWER_LIMIT ||
            readout->position.latitude > GPS_LATITUDE_UPPER_LIMIT ||
            readout->position.latitude < GPS_LATITUDE_LOWER_LIMIT) {
            sd_printf(ErrorEntry,
                      "GPS readout out of bounds, reject measurement.");
            return false;
        }
    }

    return success;
}

#endif /* USE_SENSOR_DATA_HAL */
