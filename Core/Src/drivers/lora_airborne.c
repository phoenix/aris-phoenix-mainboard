/**
 *
 *
 *
 *
 *
 */

#include "drivers/lora_airborne.h"
#include "cmsis_os.h"
#include "drivers/lora_module.h"

#include "main.h"
#include "tasks/finite_state_machine.h"
#include "tasks/packets.h"
#include <stdio.h>
#include <string.h>

#include "system-status.h"

// TIMEOUT duration = TIMEOUT * 15.625us
#define RX_TIMEOUT_SINGLE_MODE                                                 \
    0x00 // RX single mode: wait indefinitely until reception of message
#define RX_TIMEOUT                                                             \
    50000 // RX Timeout value for irq in case no rxDone event occured
#define TX_TIMEOUT_SINGLE_MODE                                                 \
    0x00 // TX single mode: wait indefinitely for txDone event; no Timeout event
         // will be generated
#define TX_TIMEOUT                                                             \
    77000 // TX Timeout value for irq in case no txDone event occured

#define RX_BOOSTED_MODE                                                        \
    1 // if set to 1, use receive mode with boosted gain, if 0 use standard
      // power-saving gain

#define USE_ENCRYPTION 1

#define DIO1_GPIO GPIOB
#define DIO1_PIN  GPIO_PIN_4

// Status of peripherals
static SystemStatus_t system_status;

// Coordinates of airborne system
static Coordinates_t coordinates;

// Requirements to the system
//static SystemRequirements_t system_requirements;		//TODO

// Flag for when a state transition has taken place
static uint8_t state_updated;

//stores the last state before new update
static FSMState_t old_state = fsm_Stuck;

// event flags set after IRQ is processed
extern volatile RadioFlags_t radio_flags;

// flag set by the ISR after RF module generated interrupt
extern volatile uint8_t irq_triggered;

uint32_t on_air_time;

void lora_init_airborne(void) {

	StatusUpdate_t system_init_status;

    lora_init((uint8_t)AIRBORNE_TX_PAYLOAD_SIZE);

    system_init_status.error = 0;
    system_init_status.receiver_mcu = 0;
    system_init_status.icsb1 = 0;
    system_init_status.icsb2 = 0;
    system_init_status.baro1 = 0;
    system_init_status.baro2 = 0;

    system_init_status.imu1 = 0;
    system_init_status.imu2 = 0;
    system_init_status.gps1 = 0;
    system_init_status.gps1_lock = 0;
    system_init_status.gps2 = 0;
    system_init_status.gps2_lock = 0;
    system_init_status.gps3 = 0;
    system_init_status.gps3_lock = 0;

    system_init_status.sd = 0;
    system_init_status.error_code = 0;

    system_init_status.state = fsm_InitialState;

    lora_update_system_status(system_init_status);

    lora_update_coordinates(0.0, 0.0, 0.0);

    on_air_time = 0;

    set_standby(STDBY_RC);
}

void lora_slave_function(void) {

    PayloadGround_t rx_msg;
    PayloadAirborne_t tx_msg;

#if USE_ENCRYPTION == 1
    uint8_t rx_valid;
#endif

    Identifiers_t tx_identifier;

    uint32_t tx_start_timestamp;

    uint8_t rx_success;

    while (1) {

        rx_success = 0;

        memset(rx_msg.Raw, 0, GROUND_TX_PAYLOAD_SIZE);
        memset(tx_msg.Raw, 0, AIRBORNE_TX_PAYLOAD_SIZE);

        do {

            if (state_updated) {

                set_standby(STDBY_RC);

                state_updated = 0;

                process_status_report(&tx_msg);
#if USE_ENCRYPTION == 1
                encrypt_air(tx_msg.Raw);
#endif
                set_payload(tx_msg.Raw, AIRBORNE_TX_PAYLOAD_SIZE);
                set_irq_tx();
                tx_start_timestamp = HAL_GetTick();
                transmit_buffer(TX_TIMEOUT_SINGLE_MODE);	//TODO change if any measure against tx timeout is implemented
                wait_tx_done();
                on_air_time += (HAL_GetTick() - tx_start_timestamp);
                reset_all_irq();
                reset_all_flags();
            }

            set_standby(STDBY_RC);

            set_irq_rx();

#if RX_BOOSTED_MODE == 1
            receive_buffer_boosted(RX_TIMEOUT_SINGLE_MODE);
#else
            receive_buffer(RX_TIMEOUT_SINGLE_MODE);
#endif

            wait_rx_done();

            if (radio_flags.flags.rxDone) {

                reset_rx_done_flag();

                get_fixed_payload(rx_msg.Raw, GROUND_TX_PAYLOAD_SIZE);

#if USE_ENCRYPTION == 1
                rx_valid = decrypt_ground(rx_msg.Raw);

                if (rx_valid) {

                    rx_success = 1;
                }
#else
                if (identifier_check((uint8_t)rx_msg.Data.Raw.PayloadType)) {

                    rx_success = 1;
                }
#endif
            }

            reset_rx_timeout_flag();

        } while (rx_success == 0);

        reset_all_irq();
        reset_all_flags();
        set_standby(STDBY_RC);

        tx_msg = process_msg(rx_msg);

        tx_identifier = rx_msg.Data.Raw.PayloadType;

        if (tx_identifier != INVALID) {

#if USE_ENCRYPTION == 1
            encrypt_air(tx_msg.Raw);
#endif

            set_payload(tx_msg.Raw, AIRBORNE_TX_PAYLOAD_SIZE);
            osDelay(8);
            set_irq_tx();
            tx_start_timestamp = HAL_GetTick();
            transmit_buffer(TX_TIMEOUT_SINGLE_MODE);	//TODO change if any measure against tx timeout is implemented
            wait_tx_done();
            on_air_time += (HAL_GetTick() - tx_start_timestamp);
            /*
            if(radio_flags.flags.txTimeout){
                    system_status.error = 1;
                    lora_init((uint8_t)AIRBORNE_TX_PAYLOAD_SIZE);
            }*/
        }

        reset_all_flags();
    }
}


PayloadAirborne_t process_msg(PayloadGround_t rx_msg) {

    PayloadAirborne_t tx_msg;
    Identifiers_t rx_identifier;

    rx_identifier = rx_msg.Data.Raw.PayloadType;
    tx_msg.Data.Raw.PayloadType = (uint8_t)INVALID;

    switch (rx_identifier) {

        case PING:
            process_ping(&tx_msg);
            break;

        case STATUS_REPORT_REQUEST:
            process_status_report(&tx_msg);
            break;

        case TELEMETRY_REQUEST:
            process_telemetry(&tx_msg);
            break;

        case COUNTDOWN_REQUEST:
            process_countdown(&rx_msg, &tx_msg);
            break;

        case ARMING_REQUEST:
            process_arm(&tx_msg);
            break;

        case EMERGENCY_DEPLOYMENT_REQUEST:
            process_emergency_deployment(&tx_msg);
            break;

        case SOFTWARE_RESET_REQUEST:
            if (rx_msg.Data.FSMCommand.cmd == COMMAND_SOFTWARE_RESET) {
                process_software_reset();
            }
            break;

        case FSM_COMMAND:
            process_fsm_command(&rx_msg, &tx_msg);
            break;

        default:

            break;
    }

    return tx_msg;
}


void process_ping(PayloadAirborne_t *tx_msg) {

    int8_t rx_rssi;
    int8_t rx_snr;
    tx_msg->Data.Raw.PayloadType = PONG;
    get_rssi_snr(&rx_rssi, &rx_snr);
    tx_msg->Data.Ping.RSSI = rx_rssi;
    tx_msg->Data.Ping.SNR = rx_snr;

    write_u32(on_air_time, &(tx_msg->Data.Ping.on_air_time0));

}

void process_status_report(PayloadAirborne_t *tx_msg) {

    tx_msg->Data.Raw.PayloadType = STATUS_REPORT;
    tx_msg->Data.StatusReport.error = system_status.error;
    tx_msg->Data.StatusReport.ready = system_status.ready;
    tx_msg->Data.StatusReport.receiver_mcu = system_status.receiver_mcu;
    tx_msg->Data.StatusReport.icsb1 = system_status.icsb1;
    tx_msg->Data.StatusReport.icsb2 = system_status.icsb2;
    tx_msg->Data.StatusReport.baro1 = system_status.baro1;
    tx_msg->Data.StatusReport.baro2 = system_status.baro2;
    tx_msg->Data.StatusReport.imu1 = system_status.imu1;
    tx_msg->Data.StatusReport.imu2 = system_status.imu2;
    tx_msg->Data.StatusReport.gps1 = system_status.gps1;
    tx_msg->Data.StatusReport.gps1_lock = system_status.gps1_lock;
    tx_msg->Data.StatusReport.gps2 = system_status.gps2;
    tx_msg->Data.StatusReport.gps2_lock = system_status.gps2_lock;
    tx_msg->Data.StatusReport.gps3 = system_status.gps3;
    tx_msg->Data.StatusReport.gps3_lock = system_status.gps3_lock;
    tx_msg->Data.StatusReport.sd = system_status.sd;

    tx_msg->Data.StatusReport.error_code = system_status.error_code;

    tx_msg->Data.StatusReport.fsm_state = (uint8_t)system_status.fsm_state;

    write_u16((uint16_t)(on_air_time & 0xFFFF), &(tx_msg->Data.StatusReport.on_air_time0));

}

void process_telemetry(PayloadAirborne_t *tx_msg) {

    tx_msg->Data.Raw.PayloadType = TELEMETRY_DATA;

    write_s16(coordinates.x, &(tx_msg->Data.Coordinates.x0));
    write_s16(coordinates.y, &(tx_msg->Data.Coordinates.y0));
    write_u16(coordinates.z, &(tx_msg->Data.Coordinates.z0));

}


void process_countdown(PayloadGround_t *rx_msg, PayloadAirborne_t *tx_msg) {

	tx_msg->Data.Raw.PayloadType = COUNTDOWN_ACK;

    fsm_input(Drop);
}

void process_arm(PayloadAirborne_t *tx_msg) {

	tx_msg->Data.Raw.PayloadType = ARMING_ACK;

	tx_msg->Data.FSMCommand.fsm_command = (uint8_t)Transition_Arm;
	tx_msg->Data.FSMCommand.fsm_state = (uint8_t)system_status.fsm_state;

    fsm_input(Arm);
}

void process_emergency_deployment(PayloadAirborne_t *tx_msg) {

	fsm_input(TriggerDeployment);

}

void process_software_reset(void) {
    // system_restart();
}

void process_fsm_command(PayloadGround_t *rx_msg, PayloadAirborne_t *tx_msg) {

    FSMCommand_t command;

    command = rx_msg->Data.FSMCommand.cmd;

    switch (command) {

        case Transition_Calibrate:
            process_calibrate(tx_msg);
            break;

        case Transition_WarmUp:
            process_warmup(tx_msg);
            break;

        case Transition_TriggerApproach:
            process_trigger_approach(tx_msg);
            break;

        case Transition_TouchdownDetected:
            process_touchdown(tx_msg);
            break;

        case Transition_Disarm:
            process_disarm(tx_msg);
            break;

        default:

            break;
    }

    write_u32(on_air_time, &(tx_msg->Data.FSMCommand.on_air_time0));

}

void process_calibrate(PayloadAirborne_t *tx_msg) {

	tx_msg->Data.Raw.PayloadType = FSM_COMMAND_ACK;
	tx_msg->Data.FSMCommand.fsm_command = (uint8_t)Transition_Calibrate;
	tx_msg->Data.FSMCommand.fsm_state = (uint8_t)system_status.fsm_state;

    fsm_input(Calibrate);
}

void process_warmup(PayloadAirborne_t *tx_msg) {

    tx_msg->Data.Raw.PayloadType = FSM_COMMAND_ACK;
    tx_msg->Data.FSMCommand.fsm_command = (uint8_t)Transition_WarmUp;
    tx_msg->Data.FSMCommand.fsm_state = (uint8_t)system_status.fsm_state;

    fsm_input(WarmUp);
}

void process_trigger_approach(PayloadAirborne_t *tx_msg) {

	tx_msg->Data.Raw.PayloadType = FSM_COMMAND_ACK;
	tx_msg->Data.FSMCommand.fsm_command = (uint8_t)Transition_TriggerApproach;
	tx_msg->Data.FSMCommand.fsm_state = (uint8_t)system_status.fsm_state;

    fsm_input(TriggerApproach);
}

void process_touchdown(PayloadAirborne_t *tx_msg) {

    tx_msg->Data.Raw.PayloadType = FSM_COMMAND_ACK;
    tx_msg->Data.FSMCommand.fsm_command = (uint8_t)Transition_TouchdownDetected;
    tx_msg->Data.FSMCommand.fsm_state = (uint8_t)system_status.fsm_state;

    fsm_input(TouchdownDetected);
}

void process_disarm(PayloadAirborne_t *tx_msg) {

    tx_msg->Data.Raw.PayloadType = FSM_COMMAND_ACK;
    tx_msg->Data.FSMCommand.fsm_command = (uint8_t)Transition_Disarm;
    tx_msg->Data.FSMCommand.fsm_state = (uint8_t)system_status.fsm_state;

    fsm_input(Disarm);
}

void lora_update_system_status(StatusUpdate_t status_update) {

    system_status.error = status_update.error;
    system_status.receiver_mcu = status_update.receiver_mcu;
    system_status.icsb1 = status_update.icsb1;
    system_status.icsb2 = status_update.icsb2;
    system_status.baro1 = status_update.baro1;
    system_status.baro2 = status_update.baro2;
    system_status.imu1 = status_update.imu1;
    system_status.imu2 = status_update.imu2;
    system_status.gps1 = status_update.gps1;
    system_status.gps1_lock = status_update.gps1_lock;
    system_status.gps2 = status_update.gps2;
    system_status.gps2_lock = status_update.gps2_lock;
    system_status.gps3 = status_update.gps3;
    system_status.gps3_lock = status_update.gps3_lock;
    system_status.sd = status_update.sd;

    system_status.error_code = status_update.error_code;

    switch (status_update.state) {

    	case InitialState:
			system_status.fsm_state = fsm_InitialState;
			break;

        case CalibratedState:
            system_status.fsm_state = fsm_CalibratedState;
            break;

        case LiveState:
            system_status.fsm_state = fsm_LiveState;
            break;

        case ArmedState:
            system_status.fsm_state = fsm_ArmedState;
            break;

        case DrogueDescentState:
            system_status.fsm_state = fsm_DrogueDescentState;
            break;

        case GuidedDescentState:
            system_status.fsm_state = fsm_GuidedDescentState;
            break;

        case FinalApproachState:
            system_status.fsm_state = fsm_FinalApproachState;
            break;

        case TouchdownState:
            system_status.fsm_state = fsm_TouchdownState;
            break;

        case DisarmedState:
            system_status.fsm_state = fsm_DisarmedState;
            break;
    }

    if (system_status.fsm_state != old_state) {

        state_updated = 1;
    }

    old_state = system_status.fsm_state;

    // system_status.ready;		//TODO
}

void lora_update_coordinates(float x, float y, float z) {

    float x_tmp;
    float y_tmp;
    float z_tmp;

    x_tmp = x * 5;
    y_tmp = y * 5;
    z_tmp = z * 10;

    if (x_tmp >= 32767) {

        x_tmp = 32767;

    } else if (x_tmp <= -32768) {

        x_tmp = -32768;
    }

    if (y_tmp >= 32767) {

        y_tmp = 32767;

    } else if (y_tmp <= -32768) {

        y_tmp = -32768;
    }

    if (z_tmp >= 65535) {

        z_tmp = 65535;

    } else if (z_tmp <= 0) {

        z_tmp = 0;
    }

    coordinates.x = (int16_t)x_tmp;
    coordinates.y = (int16_t)y_tmp;
    coordinates.z = (uint16_t)z_tmp;
}
