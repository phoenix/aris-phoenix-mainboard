/**
 *******************************************************************************
 * @file           servo-driver.c
 * @brief          Contains the driver talking to the deployment servos.
 * @author         Helvijs Kiselis
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include "drivers/servo-driver.h"
#include "config.h"
#include "main.h"
#include "tasks/sd-log.h"

#if !USE_SERVO_HAL

    #if configUSE_TRACE_FACILITY
traceString servo_chn = NULL;
    #endif

void trigger_deployment_servos(void) {
    // Start timers with PWM – initial pulse is set to 60'000 in .ioc. This
    // corresponds to the initial angle that the servos are rotated to. Changing
    // the pulse to 205'000 rotates around the negative axis by about ~160,
    // releasing the rope from the snap shackle

    HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
    HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);

    osDelay(20 / portTICK_RATE_MS);
    htim2.Instance->CCR1 = 205000;
    htim2.Instance->CCR2 = 205000;

    #if configUSE_TRACE_FACILITY
    servo_chn = xTraceRegisterString("Servo Events");
    vTracePrint(servo_chn, "Servo motors actuated!");
    #endif
    sd_log("Servo motors actuated!", EventEntry);

    osDelay(1000 / portTICK_RATE_MS);

    HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_1);
    HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_2);

    return;
}

#endif /* !USE_SERVO_HAL */
