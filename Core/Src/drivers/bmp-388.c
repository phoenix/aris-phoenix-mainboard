/**
 ******************************************************************************
 * @file           bmp-388.c
 * @brief          Implementation of the BMP-388 pressure sensor.
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>

#include "bmp3-bosch.h"
#include "bmp3_defs.h"
#include "bmp3_selftest.h"
#include "config.h"
#include "drivers/bmp-388.h"
#include "tasks/sd-log.h"
#include "tasks/sensor-task.h"

#if !USE_SENSOR_DATA_HAL

/* Tracing support ---------------------------------------------------------- */

    #if configUSE_TRACE_FACILITY
traceString bmp388_chn = NULL;
traceString press_chn = NULL;
    #endif

/* Faulty sensor value detection -------------------------------------------- */

// Why dPa here? Because the BMP3 API returns the readings in that format.

/** Lowest pressure reading that is accepted [dPa] */
    #define BMP388_PRESSURE_LOWER_BOUND (30000 * 100)
/** Highest pressure reading that is accepted in [dPa] */
    #define BMP388_PRESSURE_UPPER_BOUND (110000 * 100)

/* Private functions -------------------------------------------------------- */

/**
 * @brief Wrapper function for the BMP-388 to access the I2C bus.
 *
 * @param reg_addr BMP 388 internal register address
 * @param data Pointer where data should be read from.
 * @param len Data length in bytes
 * @param intf_ptr Pointer to BMP388 handle which is being sampled
 * @return int8_t
 */
int8_t bmp388_bus_write(uint8_t reg_addr, const uint8_t *data, uint32_t len,
                        void *intf_ptr) {
    BMP388_Handle *bmp = (BMP388_Handle *)intf_ptr;
    return i2c_master_write_slave_reg(bmp->bmp388_i2c_addr, reg_addr,
                                      (uint8_t *)data, len);
}

/**
 * @brief Wrapper function for the BMP-388 to access the I2C bus.
 *
 * @param reg_addr BMP 388 internal register address
 * @param data Pointer where read data should be written to.
 * @param len Data length in bytes
 * @param intf_ptr Pointer to BMP388 handle which is being sampled
 * @return int8_t
 */
int8_t bmp388_bus_read(uint8_t reg_addr, uint8_t *data, uint32_t len,
                       void *intf_ptr) {
    BMP388_Handle *bmp = (BMP388_Handle *)intf_ptr;
    return i2c_master_read_slave_reg(bmp->bmp388_i2c_addr, reg_addr, data, len);
}

/**
 * @brief A wrapper for the BMP-388 API to use microsecond delays.
 *
 * @param[in] period Number of microseconds to delay.
 * @param[in] intf_ptr The interface pointer, which contains the I2C port number
 */
void bmp388_delay_us(uint32_t period, void *intf_ptr) {
    // TODO: actually delay in microseconds
    uint32_t delay_ms = period / 1000;
    osDelay(delay_ms > 0 ? delay_ms : 1);
}

/**
 * Run the built-in selftest of the BMP-388 sensor.
 * @param bmp_388 A pointer to a non-null BMP388_Handle including the I2C handle
 * @returns True if the selftest succeeded, false otherwise.
 */
bool bmp388_run_selftest(BMP388_Handle *bmp_388);

/* Public function implementation ------------------------------------------- */

bool bmp388_init(BMP388_Handle *bmp_388) {

    #if configUSE_TRACE_FACILITY
    if (!bmp388_chn) {
        bmp388_chn = xTraceRegisterString("BMP-388 Events");
    }
    vTracePrintF(bmp388_chn, "Initializing BMP-388 with handle at %p", bmp_388);
    #endif

    if (bmp_388 == NULL) {
    #if configUSE_TRACE_FACILITY
        vTracePrint(bmp388_chn,
                    "Invalid arguments passed to BMP-388 initializer");
    #endif
        return false;
    }

    // store pointer to the I2C handle in user data - when the API wants to talk
    // to the peripheral, it will provide that pointer
    bmp_388->bmp_internal.intf = BMP3_I2C_INTF;
    bmp_388->bmp_internal.intf_ptr = (void *)bmp_388;

    // the Bosch driver wants function pointers to a microsecond delay function
    // and bus read/write functions
    bmp_388->bmp_internal.delay_us = &bmp388_delay_us;
    bmp_388->bmp_internal.read = &bmp388_bus_read;
    bmp_388->bmp_internal.write = &bmp388_bus_write;

    // Run self-test before setting all our settings. Why? Because this messes
    // up the ODR and everything.
    if (!bmp388_run_selftest(bmp_388)) {
        return false;
    } else {
        sd_log("BMP-388 selftest successful.", EventEntry);
    }

    int8_t res = bmp3_init(&(bmp_388->bmp_internal));
    if (res != BMP3_INTF_RET_SUCCESS) {
    #if configUSE_TRACE_FACILITY
        vTracePrintF(bmp388_chn,
                     "bmp3_init (Bosch driver) failed with return code (%i)",
                     res);
    #endif
        return false;
    } else {
    #if configUSE_TRACE_FACILITY
        vTracePrint(bmp388_chn, "BMP-388 init succeeded.");
    #endif
    }

    // Configure the sensor for: 50 Hz readouts, standard resolution, 8x
    // pressure oversampling, 1x temperature oversampling, IIR filter coeff 2.
    // These settings are recommended for drones in the BMP-388 datasheet, p.57
    bmp_388->bmp_internal.settings = (struct bmp3_settings){
        .press_en = BMP3_ENABLE,
        .temp_en = BMP3_ENABLE,
        .op_mode = BMP3_MODE_NORMAL,
        .odr_filter = {.press_os = BMP3_OVERSAMPLING_8X,
                       .temp_os = BMP3_NO_OVERSAMPLING,
                       .iir_filter = BMP3_IIR_FILTER_COEFF_1,
                       .odr = BMP3_ODR_50_HZ}};

    // mask all settings to be changed and try changing them in one burst write
    if (bmp3_set_sensor_settings(
            BMP3_SEL_PRESS_EN | BMP3_SEL_TEMP_EN | BMP3_SEL_PRESS_OS |
                BMP3_SEL_TEMP_OS | BMP3_SEL_IIR_FILTER | BMP3_SEL_ODR,
            &(bmp_388->bmp_internal)) != BMP3_INTF_RET_SUCCESS) {
    #if configUSE_TRACE_FACILITY
        vTracePrint(bmp388_chn,
                    "Something failed in changing BMP-388 settings!");
    #endif
        return false;
    } else {
    #if configUSE_TRACE_FACILITY
        vTracePrint(bmp388_chn, "Could successfully set all settings.");
    #endif
    }

    bmp_388->bmp_internal.settings.op_mode = BMP3_MODE_NORMAL;
    if (bmp3_set_op_mode(&(bmp_388->bmp_internal)) != BMP3_OK) {
    #if configUSE_TRACE_FACILITY
        vTracePrint(bmp388_chn, "Error setting operation mode!");
    #endif
    }
    return true;
}

bool bmp388_run_selftest(BMP388_Handle *bmp_388) {
    bool success =
        (bmp3_selftest_check(&bmp_388->bmp_internal) == BMP3_SENSOR_OK);

    if (success) {

    #if configUSE_TRACE_FACILITY
        vTracePrintF(bmp388_chn, "BMP-388 self-test successful.");
    #endif
        sd_log("BMP-388 selftest successful.", EventEntry);

    } else {

    #if configUSE_TRACE_FACILITY
        vTracePrintF(bmp388_chn, "BMP-388 self-test failed!");
    #endif
        sd_log("BMP-388 self test failed!", ErrorEntry);
    }

    return success;
}

bool bmp388_sample(BMP388_Handle *bmp_388, struct BMP388_Readout *readout) {
    if (!bmp_388 || !readout) {
    #if configUSE_TRACE_FACILITY
        vTracePrint(bmp388_chn, "Invalid parameters passed to bmp388_sample()");
    #endif
        return false;
    }

    struct bmp3_data data = {0};
    if (bmp3_get_sensor_data(BMP3_ALL, &data, &(bmp_388->bmp_internal)) ==
        BMP3_INTF_RET_SUCCESS) {

    #if configUSE_TRACE_FACILITY
        // for live visualization on site
        vTracePrintF(bmp388_chn, "Main board pressure reading: %d Pa",
                     data.pressure / 100.0);
    #endif

        if (data.pressure > BMP388_PRESSURE_LOWER_BOUND &&
            data.pressure < BMP388_PRESSURE_UPPER_BOUND) {
            // reading successful and values are within reasonable bounds
            // copy to destination memory
            readout->temperature =
                (int16_t)data.temperature;               // temperature * 100
            readout->pressure = (uint32_t)data.pressure; // pressure * 100

            return true;
        } else {
            // faulty readings also just return false, to ensure that the result
            // isn't used anywhere

    #if configUSE_TRACE_FACILITY
            vTracePrint(bmp388_chn, "BMP-388 returned faulty values.");
    #endif
            sd_log("BMP-388 reading faulty.", ErrorEntry);
            return false;
        }

    } else {
    #if configUSE_TRACE_FACILITY
        vTracePrint(bmp388_chn, "Failed to sample BMP-388!");
    #endif
        return false;
    }
};

#endif
