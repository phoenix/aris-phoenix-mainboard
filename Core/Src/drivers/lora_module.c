/**
 *
 *
 *
 *
 *
 *
 */

// INCLUDES+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#include "drivers/lora_module.h"
#include "cmsis_os.h"
#include "main.h"
#include "tasks/sd-log.h"
#include <stdio.h>
#include <string.h>
// DEFINES++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Set to 1 to activate serial output for debugging
#define LORA_DEBUGGING          0
#define LORA_IRQ_DEBUGGING      0
#define LORA_IRQ_TYPE_DEBUGGING 0

//#if LORA_DEBUGGING == 1

//#endif

/* Set this flag to '1' to use the LoRa modulation or to '0' to use FSK
 * modulation */
#define USE_MODEM_LORA 1
#define USE_MODEM_FSK  !USE_MODEM_LORA

#define RF_FREQUENCY    868000000 // Hz
#define TX_OUTPUT_POWER 14        // 14 dBm

/* Interrupt handling ------------------------------------------------------- */

#define LORA_INTERRUPT_RTOS_FLAG (1U << 7)

#if USE_MODEM_LORA == 1

    #define LORA_BANDWIDTH                                                     \
        LORA_BW_500                            // [0: 125 kHz,
                                               //  1: 250 kHz,
                                               //  2: 500 kHz,
                                               //  3: Reserved]
    #define LORA_SPREADING_FACTOR    LORA_SF10 // [SF7..SF12] new: SF5 & SF6
    #define LORA_LOWDATARATEOPTIMIZE 0
    #define LORA_CODINGRATE                                                    \
        LORA_CR_4_5 // [1: 4/5,
                    //  2: 4/6,
                    //  3: 4/7,
                    //  4: 4/8]
    #define LORA_PREAMBLE_LENGTH                                               \
        12                        // Same for Tx and Rx
                                  // NOTE: should be 12 if SF5/SF6 used
    #define LORA_SYMBOL_TIMEOUT 5 // Symbols
    #define LORA_HEADER_TYPE    LORA_PACKET_FIXED_LENGTH
    #define LORA_FHSS_ENABLED   false
    #define LORA_NB_SYMB_HOP    4
    #define LORA_IQ             LORA_IQ_NORMAL
    #define LORA_CRC_MODE       LORA_CRC_OFF

#elif USE_MODEM_FSK == 1

    #define FSK_FDEV                   25000       // Hz
    #define FSK_DATARATE               19200       // bps
    #define FSK_BANDWIDTH              RX_BW_93800 // Hz
    #define FSK_MODULATION_SHAPPING    MOD_SHAPING_G_BT_05
    #define FSK_PREAMBLE_LENGTH        5 // Same for Tx and Rx
    #define FSK_HEADER_TYPE            RADIO_PACKET_VARIABLE_LENGTH
    #define FSK_CRC_MODE               RADIO_CRC_2_BYTES_CCIT
    #define FSK_ADDR_FILTERING         RADIO_ADDRESSCOMP_FILT_NODE;
    #define FSK_WHITENING_MODE         RADIO_DC_FREE_OFF
    #define FSK_PREAMBLE_DETECTOR_MODE RADIO_PREAMBLE_DETECTOR_OFF
    #define FSK_SYNCWORD_LENGTH        8
#else
    #error "Please define a modem in the compiler options."
#endif

// DEFINES - Register addresses
/*!
 * \brief The address of the register holding the packet configuration
 */
#define REG_LR_PACKETPARAMS 0x0704

/*!
 * \brief The address of the register holding the payload size
 */
#define REG_LR_PAYLOADLENGTH 0x0702

/*!
 * The address of the register giving a 4 bytes random number
 */
#define RANDOM_NUMBER_GENERATORBASEADDR 0x0819

//#define LORA_SPI
// hspi1		//spi between rf board and microcontroller

#define RX_TIMEOUT_VALUE 50000 // in ms

//#define BUFFER_SIZE                                     32 //Define the
// payload size here

//#define TX_PAYLOAD_SIZE
// 4
////Define fixed TX payload size here
//#define RX_PAYLOAD_SIZE
// 4
////Define fixed RX payload size here

#define TX_BUFFER_OFFSET                                                       \
    127 // Defines offset of TX base address Pointer in internal Buffer
#define RX_BUFFER_OFFSET                                                       \
    00 // Defines offset of RX base address Pointer in internal Buffer

// DEFINES Configuration
#define TX_TIMEOUT    0
#define RX_TIMEOUT_US 200000
#define SPI_TIMEOUT   1000

#define XTAL_FREQ 32000000
#define FREQ_DIV  33554432
#define FREQ_STEP                                                              \
    0.95367431640625 // ( ( double )( XTAL_FREQ / ( double )FREQ_DIV ) )
#define FREQ_ERR 0.47683715820312

// DEFINES Pins
#define NSS_GPIO GPIOA
#define NSS_PIN  GPIO_PIN_8

#define NRESET_GPIO GPIOA
#define NRESET_PIN  GPIO_PIN_0

#define ANT_POWER_GPIO GPIOB
#define ANT_POWER_PIN  GPIO_PIN_15

#define DIO1_GPIO GPIOB
#define DIO1_PIN  GPIO_PIN_4

#define BUSY_GPIO GPIOB
#define BUSY_PIN  GPIO_PIN_3

#define XTAL_SEL_GPIO GPIOB
#define XTAL_SEL_PIN  GPIO_PIN_0

#define DEVICE_SEL_GPIO GPIOA
#define DEVICE_SEL_PIN  GPIO_PIN_4

#define FREQ_SEL_GPIO GPIOA
#define FREQ_SEL_PIN  GPIO_PIN_1

//#define USER_LED_GPIO		GPIOA
//#define USER_LED_PIN		GPIO_PIN_5

// SEMTEC MASTER BUTTONS
#define NSS_GPIO GPIOA
#define NSS_PIN  GPIO_PIN_8

#define NSS_GPIO GPIOA
#define NSS_PIN  GPIO_PIN_8

// DEFINES registers
/*!
 * \brief The addresses of the registers holding SyncWords values
 */
#define REG_LR_SYNCWORDBASEADDRESS 0x06C0

/*!
 * \brief The addresses of the register holding LoRa Modem SyncWord value
 */
#define REG_LR_SYNCWORD 0x0740

/*!
 * Syncword for Private LoRa networks
 */
#define LORA_MAC_PRIVATE_SYNCWORD 0x1424

/*!
 * Syncword for Public LoRa networks
 */
#define LORA_MAC_PUBLIC_SYNCWORD 0x3444

/*!
 * The address of the register giving a 4 bytes random number
 */
#define RANDOM_NUMBER_GENERATORBASEADDR 0x0819

/*!
 * The address of the register holding RX Gain value (0x94: power saving, 0x96:
 * rx boosted)
 */
#define REG_RX_GAIN 0x08AC

/*!
 * The address of the register holding frequency error indication
 */
#define REG_FREQUENCY_ERRORBASEADDR 0x076B

/*!
 * Change the value on the device internal trimming capacitor
 */
#define REG_XTA_TRIM 0x0911

/*!
 * Set the current max value in the over current protection
 */
#define REG_OCP 0x08E7

// VARIABLES++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// VARIABLES Typedef			-> see header

// VARIABLES
static SPI_HandleTypeDef *hspi;

extern SPI_HandleTypeDef hspi1;

static RadioConfigurations_t configuration;

//#if LORA_DEBUGGING == 1
extern UART_HandleTypeDef huart2;
//#endif

static uint8_t tx_payload_length = 0;

static uint8_t image_calibrated = 0;
volatile uint8_t irq_triggered = 0;

volatile uint32_t FrequencyError = 0;
uint16_t irqRegs = 0;

volatile RadioFlags_t radio_flags;

RadioOperatingModes_t OperatingMode;

// FUNCTIONS++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// FUNCTIONS High level

void lora_init(uint8_t payload_length) {

    // UART_printf_setup(&huart2);

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "Lora Init\r\n");

#endif

    radio_flags.value = 0;
    tx_payload_length = payload_length;

    lora_pins_init();
    // nss_reset();
    // //----------------------------------------------------------------------------------------------------------

    // wait_busy();

    lora_reset();
    // HAL_Delay(20);
    dummy_delay();

    // lora_reset();

    // wait_busy();

    wakeup(); //-------------------------------------------------------------------------------------------------------------
    set_standby(
        STDBY_RC); //---------------------------------------------------------------------------------------------------
    OperatingMode = MODE_STDBY_RC;

    wait_busy();

    /*
    #if USE_MODEM_LORA == 1
            set_packet_type(PACKET_TYPE_LORA);
    #elif USE_MODEM_GFSK == 1
            set_packet_type(PACKET_TYPE_GFSK);
    #else
            set_packet_type(PACKET_TYPE_NONE);
    #endif
    */

#ifdef USE_CONFIG_PUBLIC_NETOWRK
    // Change LoRa modem Sync Word for Public Networks
    write_reg(REG_LR_SYNCWORD, (LORA_MAC_PUBLIC_SYNCWORD >> 8) & 0xFF);
    write_reg(REG_LR_SYNCWORD + 1, LORA_MAC_PUBLIC_SYNCWORD & 0xFF);
#else
    // Change LoRa modem SyncWord for Private Networks
    write_reg(REG_LR_SYNCWORD, (LORA_MAC_PRIVATE_SYNCWORD >> 8) & 0xFF);
    write_reg(REG_LR_SYNCWORD + 1, LORA_MAC_PRIVATE_SYNCWORD & 0xFF);
#endif

    ant_power_on();

    // HAL_Delay(20);
    dummy_delay();

    set_configuration(&configuration);
    configure_radio(&configuration);
    wait_busy();
    set_buffer_base_addresses(TX_BUFFER_OFFSET, RX_BUFFER_OFFSET);

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "Lora Init COMPLETED\r\n");

#endif
}

void lora_reset(void) {

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "LoRa Reset\r\n");

#endif

    __disable_irq();
    // HAL_Delay(20);
    dummy_delay();
    nreset_reset();
    // HAL_Delay(50);
    dummy_delay();
    nreset_set();
    // HAL_Delay(20);
    dummy_delay();
    __enable_irq();
}

void lora_send_test(void) {

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "Test Transmission START\r\n");

#endif

    uint8_t test_payload[] = {'T', 'E', 'S', 'T'};
    uint16_t length = sizeof(test_payload);
    // uint16_t i;

    // nss_reset();

    set_irq_tx();

    set_payload(test_payload, length);
    transmit_buffer(TX_TIMEOUT); // tx timeout = 0

    wait_tx_done();

    reset_all_flags();

    // nss_set();//------------------------------------------------------------------------------------------------

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "Test TransmissionEND\r\n");

#endif
}

void lora_receive_test_boosted(void) {

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "Boosted Test Reception start\r\n");

#endif

    char buffer[33];
    uint8_t payload_length;
    uint8_t success = 0;

    memset(buffer, 0, 33);

    set_irq_rx();

    receive_buffer_boosted(RX_TIMEOUT_VALUE);

    do {

        if (irq_triggered) {
            irq_triggered = 0;
            process_irqs();
        }

    } while (radio_flags.flags.rxDone == 0 && radio_flags.flags.rxError == 0 &&
             radio_flags.flags.rxTimeout == 0);

    if (radio_flags.flags.rxDone) {
        radio_flags.flags.rxDone = 0;
        success = get_payload((uint8_t *)buffer, &payload_length, 32);
        if (success == 0) {
            buffer[payload_length] = '\0';
            sd_printf(EventEntry, "Message received: %s\t", buffer);
            print_rssi_snr();
            // sd_printf(EventEntry, "Message received\r\n");
        } else {
            sd_printf(EventEntry, "Test Reception over length limit\r\n");
        }
    }

    if (radio_flags.flags.rxError) {
        radio_flags.flags.rxError = 0;
        sd_printf(EventEntry, "RX ERROR\r\n");
    }

    if (radio_flags.flags.rxTimeout) {
        radio_flags.flags.rxTimeout = 0;
        sd_printf(EventEntry, "RX Timeout: Nothing received\r\n");
    }
}

void lora_receive_test(void) {

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "Test Reception start\r\n");

#endif

    char buffer[33];
    uint8_t payload_length;
    uint8_t success = 0;

    memset(buffer, 0, 33);

    set_irq_rx();

    receive_buffer(RX_TIMEOUT_VALUE);

    do {

        if (irq_triggered) {
            irq_triggered = 0;
            process_irqs();
        }

    } while (radio_flags.flags.rxDone == 0 && radio_flags.flags.rxError == 0 &&
             radio_flags.flags.rxTimeout == 0);

    if (radio_flags.flags.rxDone) {
        radio_flags.flags.rxDone = 0;
        success = get_payload((uint8_t *)buffer, &payload_length, 32);
        if (success == 0) {
            buffer[payload_length] = '\0';
            sd_printf(EventEntry, "Message received: %s\t", buffer);
            print_rssi_snr();
            // sd_printf(EventEntry, "Message received\r\n");
        } else {
            sd_printf(EventEntry, "Test Reception over length limit\r\n");
        }
    }

    if (radio_flags.flags.rxError) {
        radio_flags.flags.rxError = 0;
        sd_printf(EventEntry, "RX ERROR\r\n");
    }

    if (radio_flags.flags.rxTimeout) {
        radio_flags.flags.rxTimeout = 0;
        sd_printf(EventEntry, "RX Timeout: Nothing received\r\n");
    }
}

void lora_fixed_receive_test(void) {

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "FIXED Test Reception start\r\n");

#endif

    char buffer[5];
    uint8_t payload_length = 4;

    memset(buffer, 0, 5);

    set_irq_rx();

    receive_buffer(RX_TIMEOUT_VALUE);
    /*
            do{

                    if(irq_triggered){
                            irq_triggered = 0;
                            process_irqs();
                    }

            }while(radio_flags.flags.rxDone == 0 && radio_flags.flags.rxError ==
       0 && radio_flags.flags.rxTimeout == 0);
    */
    wait_rx_done();

    if (radio_flags.flags.rxDone) {
        radio_flags.flags.rxDone = 0;
        get_fixed_payload((uint8_t *)buffer, payload_length);
        buffer[payload_length] = '\0';
        sd_printf(EventEntry, "Message received: %s\t", buffer);
        print_rssi_snr();
        // sd_printf(EventEntry, "Message received\r\n");
    }

    if (radio_flags.flags.rxError) {
        radio_flags.flags.rxError = 0;
        sd_printf(EventEntry, "RX ERROR\r\n");
    }

    if (radio_flags.flags.rxTimeout) {
        radio_flags.flags.rxTimeout = 0;
        sd_printf(EventEntry, "RX Timeout: Nothing received\r\n");
    }
}

void lora_payload_debug_test(void) {

    sd_printf(EventEntry, "Testing buffer functions...\r\n");
    uint8_t payload_set[] = {1, 2, 3, 250};
    uint8_t len = 4;
    uint8_t payload_get[4];
    memset(payload_get, 0, 4);
    write_buffer(TX_BUFFER_OFFSET, (uint8_t *)payload_set, len);
    sd_printf(EventEntry, "Setting payload:\t%d\t%d\t%d\t%d\r\n",
              payload_set[0], payload_set[1], payload_set[2], payload_set[3]);
    read_buffer(TX_BUFFER_OFFSET, (uint8_t *)payload_get, len);
    sd_printf(EventEntry, "Reading payload:\t%d\t%d\t%d\t%d\r\n",
              payload_get[0], payload_get[1], payload_get[2], payload_get[3]);
}

// FUNCTIONS Internal

void lora_pins_init(void) {

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "LoRa Pins Init\r\n");

#endif

    hspi = &hspi1;
    nss_init();
    nreset_init();
    ant_power_init();
    busy_init();
    xtal_sel_init();
}

/*
void set_packet_type(RadioPacketTypes_t packet_type){

        write_command(RADIO_SET_PACKETTYPE, (uint8_t*)&packet_type, 1);

}*/

void set_payload(uint8_t *payload, uint8_t length) {

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "Set Payload\r\n");

#endif

    write_buffer(TX_BUFFER_OFFSET, payload, length);
}

uint8_t get_payload(uint8_t *buffer, uint8_t *size, uint8_t maxSize) {

    uint8_t offset = 0;

    get_rx_buffer_status(size, &offset);
    if (*size > maxSize) {
        return 1;
    }
    read_buffer(offset, buffer, *size);
    return 0;
}

void get_fixed_payload(uint8_t *buffer, uint8_t size) {

    uint8_t offset = RX_BUFFER_OFFSET;

    read_buffer(offset, buffer, size);
}

void get_rx_buffer_status(uint8_t *payloadLength,
                          uint8_t *rxStartBufferPointer) {

    uint8_t status[2];

    read_command((uint8_t)RADIO_GET_RXBUFFERSTATUS, status, 2);

    // In case of LORA fixed header, the payloadLength is obtained by reading
    // the register REG_LR_PAYLOADLENGTH
    if ((configuration.packetType == PACKET_TYPE_LORA) &&
        (read_reg(REG_LR_PACKETPARAMS) >> 7 == 1)) {
        *payloadLength = read_reg(REG_LR_PAYLOADLENGTH);
    } else {
        *payloadLength = status[0];
    }
    *rxStartBufferPointer = status[1];
}

void write_buffer(uint8_t offset, uint8_t *buffer, uint8_t length) {

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "Write Buffer\r\n");

#endif

    // uint16_t i;

    uint8_t data = (uint8_t)RADIO_WRITE_BUFFER;

    nss_reset(); //-------------------------------------------------------------------------------------------------

    HAL_SPI_Transmit(hspi, &data, 1, SPI_TIMEOUT);
    HAL_SPI_Transmit(hspi, &offset, 1, SPI_TIMEOUT);
    // hspi->write(RADIO_WRITE_BUFFER);
    // hspi->write(offset);

    HAL_SPI_Transmit(hspi, buffer, length, SPI_TIMEOUT);
    /*
    for(i=0;i<length;i++){
            hspi->write(buffer[i]);
    }*/

    nss_set(); //---------------------------------------------------------------------------------------------------
}

void read_buffer(uint8_t offset, uint8_t *buffer, uint8_t size) {

    uint8_t zeros[size];

    // uint8_t tmp_buf_send[size+3];
    // uint8_t tmp_buf_receive[size+3];
    uint8_t cmd = (uint8_t)RADIO_READ_BUFFER;
    // tmp_buf_send[0] = (uint8_t)RADIO_READ_BUFFER;
    // tmp_buf_send[1] = offset;

    // memset(&tmp_buf_send[2], 0, size+1);
    memset(zeros, 0, size);

    wait_busy();

    nss_reset();
    HAL_SPI_Transmit(hspi, &cmd, 1, SPI_TIMEOUT);
    HAL_SPI_Transmit(hspi, &offset, 1, SPI_TIMEOUT);
    HAL_SPI_Transmit(hspi, zeros, 1, SPI_TIMEOUT);
    // HAL_SPI_TransmitReceive(hspi, zeros, buffer, size, SPI_TIMEOUT);
    HAL_SPI_Receive(hspi, buffer, size, SPI_TIMEOUT);

    // HAL_SPI_TransmitReceive(hspi, tmp_buf_send, tmp_buf_receive, size+3,
    // SPI_TIMEOUT);

    nss_set();

    // memcpy(buffer, &tmp_buf_receive[3], size);
}

void set_buffer_base_addresses(uint8_t txBaseAddress, uint8_t rxBaseAddress) {
    uint8_t buf[2];

#if LORA_DEBUGGING == 1
    sd_printf(EventEntry, "Set base addresses:\tTX %d\tRX\t%d\r\n",
              txBaseAddress, rxBaseAddress);
#endif

    buf[0] = txBaseAddress;
    buf[1] = rxBaseAddress;
    write_command((uint8_t)RADIO_SET_BUFFERBASEADDRESS, buf, 2);
}

void write_command(RadioCommands_t command, uint8_t *buffer, uint16_t length) {

    uint8_t cmd = (uint8_t)command;

    wait_busy();

    nss_reset(); //---------------------------------------------------------------------------------

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "Write Command: %x\r\n", (uint8_t)command);

#endif

    HAL_SPI_Transmit(hspi, &cmd, 1, SPI_TIMEOUT);

    HAL_SPI_Transmit(hspi, buffer, length, SPI_TIMEOUT);

    nss_set(); //---------------------------------------------------------------------------------
}

void read_command(RadioCommands_t command, uint8_t *buffer, uint16_t size) {

    uint8_t zeros[size];
    uint8_t cmd = (uint8_t)command;
    memset(zeros, 0, size);
    wait_busy();

    nss_reset();
    HAL_SPI_Transmit(hspi, &cmd, 1, SPI_TIMEOUT);
    HAL_SPI_Transmit(hspi, zeros, 1, SPI_TIMEOUT);
    HAL_SPI_TransmitReceive(hspi, zeros, buffer, size, SPI_TIMEOUT);
    nss_set();
}

uint16_t get_irq_status(void) {

    uint8_t irqStatus[2];
    uint16_t status = 0;

    read_command((uint8_t)RADIO_GET_IRQSTATUS, irqStatus, 2);
    status = (uint16_t)(irqStatus[0] << 8) | irqStatus[1];
    return status;
}

void transmit_buffer(uint32_t timeout) {

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "Transmit, Timeout = %d\r\n", timeout);

#endif

    OperatingMode = MODE_TX;

    uint8_t buf[3];
    buf[0] = (uint8_t)((timeout >> 16) & 0xFF);
    buf[1] = (uint8_t)((timeout >> 8) & 0xFF);
    buf[2] = (uint8_t)(timeout & 0xFF);

    write_command(RADIO_SET_TX, buf, 3);
}

void receive_buffer_boosted(uint32_t timeout) {

    uint8_t buf[3];

    OperatingMode = MODE_RX;

#if LORA_DEBUGGING == 1
    sd_printf(EventEntry, "Receive Buffer with boosted gain\r\n");
#endif

    write_reg(REG_RX_GAIN, 0x96); // max LNA gain, increase current by ~2mA for
                                  // around ~3dB in sensivity

    buf[0] = (uint8_t)((timeout >> 16) & 0xFF);
    buf[1] = (uint8_t)((timeout >> 8) & 0xFF);
    buf[2] = (uint8_t)(timeout & 0xFF);
    write_command((uint8_t)RADIO_SET_RX, buf, 3);
}

void receive_buffer(uint32_t timeout) {

    uint8_t buf[3];

    OperatingMode = MODE_RX;

#if LORA_DEBUGGING == 1
    sd_printf(EventEntry, "Receive Buffer with standard gain\r\n");
#endif

    write_reg(REG_RX_GAIN, 0x94); // standard power saving gain

    buf[0] = (uint8_t)((timeout >> 16) & 0xFF);
    buf[1] = (uint8_t)((timeout >> 8) & 0xFF);
    buf[2] = (uint8_t)(timeout & 0xFF);
    write_command((uint8_t)RADIO_SET_RX, buf, 3);
}

void wait_tx_done(void) {

    do {
        osThreadFlagsWait(LORA_INTERRUPT_RTOS_FLAG,
                          osFlagsWaitAny | osFlagsNoClear, osWaitForever);
        osThreadFlagsClear(LORA_INTERRUPT_RTOS_FLAG);
        process_irqs();

    } while (radio_flags.flags.txDone == 0 && radio_flags.flags.txTimeout == 0);
}

void reset_tx_done_flag(void) {

    radio_flags.flags.txDone = 0;
}

void reset_tx_timeout_flag(void) {

    radio_flags.flags.txTimeout = 0;
}

void reset_rx_done_flag(void) {

    radio_flags.flags.rxDone = 0;
}

void reset_rx_timeout_flag(void) {

    radio_flags.flags.rxTimeout = 0;
}

void reset_rx_error_flag(void) {

    radio_flags.flags.rxError = 0;
}

void reset_all_flags(void) {

    radio_flags.value = 0;
}

void wait_rx_done(void) {

    do {

        osThreadFlagsWait(LORA_INTERRUPT_RTOS_FLAG,
                          osFlagsWaitAny | osFlagsNoClear, osWaitForever);
        osThreadFlagsClear(LORA_INTERRUPT_RTOS_FLAG);
        process_irqs();

    } while (radio_flags.flags.rxDone == 0 && radio_flags.flags.rxError == 0 &&
             radio_flags.flags.rxTimeout == 0);
}

// FUNCTIONS Pins Init
void nss_init(void) {

    GPIO_InitTypeDef nss_config;

    nss_config.Pin = NSS_PIN;
    nss_config.Mode = GPIO_MODE_OUTPUT_OD;
    nss_config.Pull = GPIO_PULLUP;
    nss_config.Speed = GPIO_SPEED_FREQ_HIGH;

    HAL_GPIO_DeInit(NSS_GPIO, NSS_PIN);
    // HAL_Delay(20);
    dummy_delay();
    HAL_GPIO_Init(NSS_GPIO, &nss_config);
}

void nreset_init(void) {

    GPIO_InitTypeDef nreset_config;

    nreset_config.Pin = NRESET_PIN;
    nreset_config.Mode = GPIO_MODE_OUTPUT_OD;
    nreset_config.Pull = GPIO_PULLUP;
    nreset_config.Speed = GPIO_SPEED_FREQ_HIGH;

    HAL_GPIO_DeInit(NRESET_GPIO, NRESET_PIN);
    // HAL_Delay(20);
    dummy_delay();
    HAL_GPIO_Init(NRESET_GPIO, &nreset_config);
}

void ant_power_init(void) {

    GPIO_InitTypeDef ant_power_config;

    ant_power_config.Pin = ANT_POWER_PIN;
    ant_power_config.Mode = GPIO_MODE_OUTPUT_PP;
    ant_power_config.Pull = GPIO_PULLDOWN;
    ant_power_config.Speed = GPIO_SPEED_FREQ_HIGH;

    HAL_GPIO_DeInit(ANT_POWER_GPIO, ANT_POWER_PIN);
    // HAL_Delay(20);
    dummy_delay();
    HAL_GPIO_Init(ANT_POWER_GPIO, &ant_power_config);
}

void busy_init(void) {

    GPIO_InitTypeDef busy_config;

    busy_config.Pin = BUSY_PIN;
    busy_config.Mode = GPIO_MODE_INPUT;
    busy_config.Pull = GPIO_NOPULL;
    busy_config.Speed = GPIO_SPEED_FREQ_LOW;

    HAL_GPIO_DeInit(BUSY_GPIO, BUSY_PIN);
    // HAL_Delay(20);
    dummy_delay();
    HAL_GPIO_Init(BUSY_GPIO, &busy_config);
}

void xtal_sel_init(void) {

    GPIO_InitTypeDef xtal_sel_config;

    xtal_sel_config.Pin = XTAL_SEL_PIN;
    xtal_sel_config.Mode = GPIO_MODE_INPUT;
    xtal_sel_config.Pull = GPIO_PULLUP;
    xtal_sel_config.Speed = GPIO_SPEED_FREQ_LOW;

    HAL_GPIO_DeInit(XTAL_SEL_GPIO, XTAL_SEL_PIN);
    // HAL_Delay(20);
    dummy_delay();
    HAL_GPIO_Init(XTAL_SEL_GPIO, &xtal_sel_config);
}

// FUNCTIONS Pins Toggle
void nss_set(void) {

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "NSS Set\r\n");

#endif

    HAL_GPIO_WritePin(NSS_GPIO, NSS_PIN, GPIO_PIN_SET);
}

void nss_reset(void) {

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "NSS Reset\r\n");

#endif

    HAL_GPIO_WritePin(NSS_GPIO, NSS_PIN, GPIO_PIN_RESET);
}

void nreset_set(void) {

    HAL_GPIO_WritePin(NRESET_GPIO, NRESET_PIN, GPIO_PIN_SET);
}

void nreset_reset(void) {

    HAL_GPIO_WritePin(NRESET_GPIO, NRESET_PIN, GPIO_PIN_RESET);
}

void ant_power_on(void) {

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "Antenna Power On\r\n");

#endif

    HAL_GPIO_WritePin(ANT_POWER_GPIO, ANT_POWER_PIN, GPIO_PIN_SET);
}

void ant_power_off(void) {

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "Antenna Power Off\r\n");

#endif

    HAL_GPIO_WritePin(ANT_POWER_GPIO, ANT_POWER_PIN, GPIO_PIN_RESET);
}

// FUNCTIONS Pins read
GPIO_PinState busy_read(void) {

    GPIO_PinState pinstate;
    pinstate = HAL_GPIO_ReadPin(BUSY_GPIO, BUSY_PIN);
    return (pinstate);
}

GPIO_PinState xtal_sel_read(void) {

    GPIO_PinState pinstate;
    pinstate = HAL_GPIO_ReadPin(XTAL_SEL_GPIO, XTAL_SEL_PIN);
    return (pinstate);
}

void wakeup(void) {

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "Wakeup\r\n");

#endif

    uint8_t command = (uint8_t)RADIO_GET_STATUS;
    uint8_t i = 0;

    __disable_irq();
    nss_reset(); //-----------------------------------------------------------------------------------------------------------
    HAL_SPI_Transmit(hspi, &command, 1, SPI_TIMEOUT);
    HAL_SPI_Transmit(hspi, &i, 1, SPI_TIMEOUT);
    nss_set(); //-------------------------------------------------------------------------------------------------------------

    wait_busy();

    __enable_irq();

    ant_power_on();
}

void set_standby(RadioStandbyModes_t standby_config) {

    switch (standby_config) {
        case STDBY_RC:
            OperatingMode = MODE_STDBY_RC;

#if LORA_DEBUGGING == 1
            sd_printf(EventEntry, "Set Standby RC (standard power saving)\r\n");
#endif

            break;

        case STDBY_XOSC:
            OperatingMode = MODE_STDBY_XOSC;

#if LORA_DEBUGGING == 1
            sd_printf(EventEntry,
                      "Set Standby XOSC (time critical standby)\r\n");
#endif

            break;

        default:

#if LORA_DEBUGGING == 1
            sd_printf(EventEntry, "Set Standby CONFIG ERROR\r\n");
#endif
            break;
    }

    write_command(RADIO_SET_STANDBY, (uint8_t *)&standby_config, 1);
}

void set_fs(void) {
#if LORA_DEBUGGING == 1
    sd_printf(EventEntry, "SetFs\r\n");
#endif
    write_command(RADIO_SET_FS, 0, 0);
    OperatingMode = MODE_FS;
}

void dummy_delay(void) {

    volatile uint32_t i;
    volatile uint32_t k;

    for (i = 0; i < 500000; i++) {
        k = k + i;
    }
}

void wait_busy(void) {

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "Wait For BUSY Pin to Reset\r\n");

#endif

    GPIO_PinState pin_state;
    do {
        pin_state = busy_read();
    } while (pin_state == GPIO_PIN_SET);
}

// FUNCTIONS Configuration

void set_configuration(RadioConfigurations_t *config) {

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "Set Configurations\r\n");

#endif

    config->irqRx = IRQ_RX_DONE | IRQ_RX_TX_TIMEOUT;
    config->irqTx = IRQ_TX_DONE | IRQ_RX_TX_TIMEOUT;
    config->rfFrequency = RF_FREQUENCY;
    config->txTimeout = 0;
    config->rxTimeout = (uint32_t)(RX_TIMEOUT_US / 15.625);
    config->txPower = TX_OUTPUT_POWER;
    config->txRampTime = RADIO_RAMP_200_US;
#if USE_MODEM_LORA == 1
    config->packetType = PACKET_TYPE_LORA;
    config->modParams.PacketType = PACKET_TYPE_LORA;
    config->modParams.Params.LoRa.Bandwidth = LORA_BANDWIDTH;
    config->modParams.Params.LoRa.CodingRate = LORA_CODINGRATE;
    config->modParams.Params.LoRa.LowDatarateOptimize =
        LORA_LOWDATARATEOPTIMIZE;
    config->modParams.Params.LoRa.SpreadingFactor = LORA_SPREADING_FACTOR;
    config->packetParams.PacketType = PACKET_TYPE_LORA;
    config->packetParams.Params.LoRa.CrcMode = LORA_CRC_MODE;
    config->packetParams.Params.LoRa.HeaderType = LORA_HEADER_TYPE;
    config->packetParams.Params.LoRa.InvertIQ = LORA_IQ;
    config->packetParams.Params.LoRa.PayloadLength = tx_payload_length;
    config->packetParams.Params.LoRa.PreambleLength = LORA_PREAMBLE_LENGTH;
#elif USE_MODEM_FSK == 1
    config->packetType = PACKET_TYPE_GFSK;
    config->modParams.PacketType = PACKET_TYPE_GFSK;
    config->modParams.Params.Gfsk.Bandwidth = FSK_BANDWIDTH;
    config->modParams.Params.Gfsk.BitRate = 1024000000 / FSK_DATARATE;
    config->modParams.Params.Gfsk.Fdev = FSK_FDEV * 1.048576;
    config->modParams.Params.Gfsk.ModulationShaping = FSK_MODULATION_SHAPPING;
    config->packetParams.PacketType = PACKET_TYPE_GFSK;
    config->packetParams.Params.Gfsk.AddrComp = FSK_ADDR_FILTERING;
    config->packetParams.Params.Gfsk.CrcLength = FSK_CRC_MODE;
    config->packetParams.Params.Gfsk.DcFree = FSK_WHITENING_MODE;
    config->packetParams.Params.Gfsk.HeaderType = FSK_HEADER_TYPE;
    config->packetParams.Params.Gfsk.PayloadLength = BUFFER_SIZE;
    config->packetParams.Params.Gfsk.PreambleLength = FSK_PREAMBLE_LENGTH;
    config->packetParams.Params.Gfsk.PreambleMinDetect =
        FSK_PREAMBLE_DETECTOR_MODE;
    config->packetParams.Params.Gfsk.SyncWordLength = FSK_SYNCWORD_LENGTH;
#endif
}

void configure_radio(RadioConfigurations_t *config) {

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "Configuring RF module\r\n");

#endif

    set_packet_type(config->packetType);
    set_packet_params(&config->packetParams);
    set_modulation_params(&config->modParams);
    set_rf_frequency(config->rfFrequency);
    set_tx_params(config->txPower, config->txRampTime);
    // radio->SetInterruptMode();
    /*
    if(config->packetType == PACKET_TYPE_GFSK){
        uint8_t syncword[8] = {0xF0, 0x0F, 0x55, 0xAA, 0xF0, 0x0F, 0x55, 0xAA};
        radio->SetSyncWord(syncword);
    }*/
}

void set_packet_type(RadioPacketTypes_t packetType) {
    write_command(RADIO_SET_PACKETTYPE, (uint8_t *)&packetType, 1);
}

void set_packet_params(PacketParams_t *packetParams) {

    uint8_t n;
    // uint8_t crcVal = 0;
    uint8_t buf[9] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    /*
    // Check if required configuration corresponds to the stored packet type
    // If not, silently update radio packet type
    if(this->PacketType != packetParams->PacketType)
    {
        this->SetPacketType(packetParams->PacketType);
    }
        */

    switch (packetParams->PacketType) {
        /*case PACKET_TYPE_GFSK:
            if(packetParams->Params.Gfsk.CrcLength == RADIO_CRC_2_BYTES_IBM)
            {
                SetCrcSeed(CRC_IBM_SEED);
                SetCrcPolynomial(CRC_POLYNOMIAL_IBM);
                crcVal = RADIO_CRC_2_BYTES;
            }
            else if(  packetParams->Params.Gfsk.CrcLength ==
           RADIO_CRC_2_BYTES_CCIT )
            {
                SetCrcSeed( CRC_CCITT_SEED );
                SetCrcPolynomial( CRC_POLYNOMIAL_CCITT );
                crcVal = RADIO_CRC_2_BYTES_INV;
            }
            else
            {
                crcVal = packetParams->Params.Gfsk.CrcLength;
            }
            n = 9;
            // convert preamble length from byte to bit
            packetParams->Params.Gfsk.PreambleLength =
           packetParams->Params.Gfsk.PreambleLength << 3;

            buf[0] = ( packetParams->Params.Gfsk.PreambleLength >> 8 ) & 0xFF;
            buf[1] = packetParams->Params.Gfsk.PreambleLength;
            buf[2] = packetParams->Params.Gfsk.PreambleMinDetect;
            buf[3] = ( packetParams->Params.Gfsk.SyncWordLength << 3 ); //
           convert from byte to bit buf[4] = packetParams->Params.Gfsk.AddrComp;
            buf[5] = packetParams->Params.Gfsk.HeaderType;
            buf[6] = packetParams->Params.Gfsk.PayloadLength;
            buf[7] = crcVal;
            buf[8] = packetParams->Params.Gfsk.DcFree;
            break;
            */
        case PACKET_TYPE_LORA:
            n = 6;
            buf[0] = (packetParams->Params.LoRa.PreambleLength >> 8) & 0xFF;
            buf[1] = packetParams->Params.LoRa.PreambleLength;
            buf[2] = packetParams->Params.LoRa.HeaderType;
            buf[3] = packetParams->Params.LoRa.PayloadLength;
            buf[4] = packetParams->Params.LoRa.CrcMode;
            buf[5] = packetParams->Params.LoRa.InvertIQ;
            break;
        default:
        case PACKET_TYPE_NONE:
            return;
    }
    write_command(RADIO_SET_PACKETPARAMS, buf, n);
}

void set_modulation_params(ModulationParams_t *modulationParams) {
    uint8_t n;
    // uint32_t tempVal = 0;
    uint8_t buf[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    /*
    // Check if required configuration corresponds to the stored packet type
    // If not, silently update radio packet type
    if( this->PacketType != modulationParams->PacketType )
    {
        this->SetPacketType( modulationParams->PacketType );
    }
         */

    switch (modulationParams->PacketType) {
        /*case PACKET_TYPE_GFSK:
            n = 8;
            tempVal = ( uint32_t )( 32 * ( ( double )XTAL_FREQ / ( double
           )modulationParams->Params.Gfsk.BitRate ) ); buf[0] = ( tempVal >> 16
           ) & 0xFF; buf[1] = ( tempVal >> 8 ) & 0xFF; buf[2] = tempVal & 0xFF;
            buf[3] = modulationParams->Params.Gfsk.ModulationShaping;
            buf[4] = modulationParams->Params.Gfsk.Bandwidth;
            tempVal = ( uint32_t )( ( double )modulationParams->Params.Gfsk.Fdev
           / ( double )FREQ_STEP ); buf[5] = ( tempVal >> 16 ) & 0xFF; buf[6] =
           ( tempVal >> 8 ) & 0xFF; buf[7] = ( tempVal& 0xFF ); break;
            */
        case PACKET_TYPE_LORA:
            n = 4;
            switch (modulationParams->Params.LoRa.Bandwidth) {
                case LORA_BW_500:
                    modulationParams->Params.LoRa.LowDatarateOptimize = 0x00;
                    break;
                case LORA_BW_250:
                    if (modulationParams->Params.LoRa.SpreadingFactor == 12) {
                        modulationParams->Params.LoRa.LowDatarateOptimize =
                            0x01;
                    } else {
                        modulationParams->Params.LoRa.LowDatarateOptimize =
                            0x00;
                    }
                    break;
                case LORA_BW_125:
                    if (modulationParams->Params.LoRa.SpreadingFactor >= 11) {
                        modulationParams->Params.LoRa.LowDatarateOptimize =
                            0x01;
                    } else {
                        modulationParams->Params.LoRa.LowDatarateOptimize =
                            0x00;
                    }
                    break;
                case LORA_BW_062:
                    if (modulationParams->Params.LoRa.SpreadingFactor >= 10) {
                        modulationParams->Params.LoRa.LowDatarateOptimize =
                            0x01;
                    } else {
                        modulationParams->Params.LoRa.LowDatarateOptimize =
                            0x00;
                    }
                    break;
                case LORA_BW_041:
                    if (modulationParams->Params.LoRa.SpreadingFactor >= 9) {
                        modulationParams->Params.LoRa.LowDatarateOptimize =
                            0x01;
                    } else {
                        modulationParams->Params.LoRa.LowDatarateOptimize =
                            0x00;
                    }
                    break;
                case LORA_BW_031:
                case LORA_BW_020:
                case LORA_BW_015:
                case LORA_BW_010:
                case LORA_BW_007:
                    modulationParams->Params.LoRa.LowDatarateOptimize = 0x01;
                    break;
                default:
                    break;
            }
            buf[0] = modulationParams->Params.LoRa.SpreadingFactor;
            buf[1] = modulationParams->Params.LoRa.Bandwidth;
            buf[2] = modulationParams->Params.LoRa.CodingRate;
            buf[3] = modulationParams->Params.LoRa.LowDatarateOptimize;
            break;
        default:
        case PACKET_TYPE_NONE:
            return;
    }
    write_command(RADIO_SET_MODULATIONPARAMS, buf, n);
}

void set_rf_frequency(uint32_t frequency) {

    uint8_t buf[4];
    uint32_t freq = 0;

    if (image_calibrated == 0) {
        calibrate_image(frequency);
        image_calibrated = 1;
    }

    freq = (uint32_t)((double)frequency / (double)FREQ_STEP);
    buf[0] = (uint8_t)((freq >> 24) & 0xFF);
    buf[1] = (uint8_t)((freq >> 16) & 0xFF);
    buf[2] = (uint8_t)((freq >> 8) & 0xFF);
    buf[3] = (uint8_t)(freq & 0xFF);
    write_command(RADIO_SET_RFFREQUENCY, buf, 4);
}

void calibrate_image(uint32_t freq) {
    uint8_t calFreq[2];

    if (freq > 900000000) {
        calFreq[0] = 0xE1;
        calFreq[1] = 0xE9;
    } else if (freq > 850000000) {
        calFreq[0] = 0xD7;
        calFreq[1] = 0xD8;
    } else if (freq > 770000000) {
        calFreq[0] = 0xC1;
        calFreq[1] = 0xC5;
    } else if (freq > 460000000) {
        calFreq[0] = 0x75;
        calFreq[1] = 0x81;
    } else if (freq > 425000000) {
        calFreq[0] = 0x6B;
        calFreq[1] = 0x6F;
    }
    write_command(RADIO_CALIBRATEIMAGE, calFreq, 2);
}

void set_tx_params(int8_t power, RadioRampTimes_t rampTime) {
    uint8_t buf[2];

    uint8_t xtal;
    xtal = xtal_sel_read();
    // DigitalIn OPT(A3);

    // settings for SX1261
    if (power == 15) {
        set_pa_config(0x06, 0x00, 0x01, 0x01);
    } else {
        set_pa_config(0x04, 0x00, 0x01, 0x01);
    }
    if (power >= 14) {
        power = 14;
    } else if (power < -3) {
        power = -3;
    }
    write_reg(REG_OCP, 0x18); // current max is 80 mA for the whole device

    /*
    else // sx1262 or sx1268
    {
        SetPaConfig( 0x04, 0x07, 0x00, 0x01 );
        if( power > 22 )
        {
            power = 22;
        }
        else if( power < -3 )
        {
            power = -3;
        }
        WriteReg( REG_OCP, 0x38 ); // current max 160mA for the whole device
    }*/

    buf[0] = power;
    if (xtal == GPIO_PIN_RESET) {
        if ((uint8_t)rampTime < RADIO_RAMP_200_US) {
            buf[1] = RADIO_RAMP_200_US;
        } else {
            buf[1] = (uint8_t)rampTime;
        }
    } else {
        buf[1] = (uint8_t)rampTime;
    }
    write_command(RADIO_SET_TXPARAMS, buf, 2);
}

void set_pa_config(uint8_t paDutyCycle, uint8_t HpMax, uint8_t deviceSel,
                   uint8_t paLUT) {
    uint8_t buf[4];

    buf[0] = paDutyCycle;
    buf[1] = HpMax;
    buf[2] = deviceSel;
    buf[3] = paLUT;
    write_command(RADIO_SET_PACONFIG, buf, 4);
}

void write_register(uint16_t address, uint8_t *buffer, uint16_t size) {

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "Write Register\r\n");

#endif

    wait_busy();

    nss_reset(); //-------------------------------------------------------------------------------------------------------------

    uint8_t addr[2];
    addr[0] = (uint8_t)((address & 0xFF00) >> 8);
    addr[1] = (uint8_t)(address & 0x00FF);

    // RadioSpi->write(RADIO_WRITE_REGISTER);
    RadioCommands_t command = (uint8_t)RADIO_WRITE_REGISTER;
    HAL_SPI_Transmit(hspi, &command, 1, SPI_TIMEOUT);

    // RadioSpi->write((address & 0xFF00 ) >> 8);
    // RadioSpi->write(address & 0x00FF);
    HAL_SPI_Transmit(hspi, addr, 2, SPI_TIMEOUT);

    /*
    for(uint16_t i = 0; i < size; i++){
        RadioSpi->write( buffer[i] );
    }*/
    HAL_SPI_Transmit(hspi, buffer, size, SPI_TIMEOUT);

    nss_set(); //-----------------------------------------------------------------------------------------------------------------
}

void write_reg(uint16_t address, uint8_t value) {
    write_register(address, &value, 1);
}

void read_register(uint16_t address, uint8_t *buffer, uint16_t size) {

    uint8_t cmd = (uint8_t)RADIO_READ_REGISTER;
    uint8_t addr[2];
    uint8_t zeros[size];
    memset(zeros, 0, size);

    wait_busy();

    nss_reset();

    // RadioSpi->write(RADIO_READ_REGISTER);
    HAL_SPI_Transmit(hspi, &cmd, 1, SPI_TIMEOUT);

    // RadioSpi->write((address & 0xFF00) >> 8);
    // RadioSpi->write(address & 0x00FF);
    addr[0] = (uint8_t)(address & 0xFF00) >> 8;
    addr[1] = (uint8_t)(address & 0x00FF);
    HAL_SPI_Transmit(hspi, addr, 2, SPI_TIMEOUT);

    // RadioSpi->write(0);
    HAL_SPI_Transmit(hspi, zeros, 1, SPI_TIMEOUT);

    /*for(uint16_t i = 0; i < size; i++)
    {
     buffer[i] = RadioSpi->write( 0 );
    }*/
    HAL_SPI_TransmitReceive(hspi, zeros, buffer, size, SPI_TIMEOUT);

    nss_set();
}

uint8_t read_reg(uint16_t address) {

    uint8_t data;

    read_register(address, &data, 1);
    return data;
}

void set_irq_tx(void) {

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "Set Dio Irq Parameters TX\r\n");

#endif

    set_dio_irq_params(configuration.irqTx, configuration.irqTx, IRQ_RADIO_NONE,
                       IRQ_RADIO_NONE);
}

void set_irq_rx(void) {

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "Set Dio Irq Parameters RX\r\n");

#endif

    set_dio_irq_params(configuration.irqRx, configuration.irqRx, IRQ_RADIO_NONE,
                       IRQ_RADIO_NONE);
}

void set_irq_tx_rx(void) {

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "Set Dio Irq Parameters TX RX\r\n");

#endif

    uint16_t irq_mask = configuration.irqRx | configuration.irqTx;
    set_dio_irq_params(irq_mask, irq_mask, IRQ_RADIO_NONE, IRQ_RADIO_NONE);
}

void reset_all_irq(void) {

#if LORA_DEBUGGING == 1

    sd_printf(EventEntry, "Reset all Dio Irq Parameters\r\n");

#endif

    uint16_t zero_mask = 0x0000;

    set_dio_irq_params(zero_mask, zero_mask, IRQ_RADIO_NONE, IRQ_RADIO_NONE);
}

void set_dio_irq_params(uint16_t irqMask, uint16_t dio1Mask, uint16_t dio2Mask,
                        uint16_t dio3Mask) {

    uint8_t buf[8];

    buf[0] = (uint8_t)((irqMask >> 8) & 0x00FF);
    buf[1] = (uint8_t)(irqMask & 0x00FF);
    buf[2] = (uint8_t)((dio1Mask >> 8) & 0x00FF);
    buf[3] = (uint8_t)(dio1Mask & 0x00FF);
    buf[4] = (uint8_t)((dio2Mask >> 8) & 0x00FF);
    buf[5] = (uint8_t)(dio2Mask & 0x00FF);
    buf[6] = (uint8_t)((dio3Mask >> 8) & 0x00FF);
    buf[7] = (uint8_t)(dio3Mask & 0x00FF);
    write_command((uint8_t)RADIO_CFG_DIOIRQ, buf, 8);
}

/*
//FUNCTIONS Interrupt
//ISR Called by DIO1
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
        if(GPIO_Pin == DIO1_PIN){

                irq_triggered = 1;
        }
}*/

void get_rssi_snr(int8_t *rssi, int8_t *snr) {
    PacketStatus_t pkt_stat;
    get_packet_status(&pkt_stat);
#if USE_MODEM_LORA == 1
    *rssi = pkt_stat.Params.LoRa.RssiPkt;
    *snr = pkt_stat.Params.LoRa.SnrPkt;
#else
    *rssi = pkt_stat.Params.Gfsk.RssiSync;
#endif
}

void print_rssi_snr(void) {

    int8_t rssi;
    int8_t snr;

    get_rssi_snr(&rssi, &snr);

    sd_printf(EventEntry, "RSSI: %d\tSNR: %d\r\n", rssi, snr);
}

uint32_t get_random_number(void) {
    uint8_t buf[] = {0, 0, 0, 0};

    // Set radio in continuous reception
    receive_buffer(0);

    HAL_Delay(1);

    read_register(RANDOM_NUMBER_GENERATORBASEADDR, buf, 4);

    set_standby(STDBY_RC);

    return (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3];
}

/*
void led_blink_complete_success(void){

        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_SET);
        HAL_Delay(1500);
        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_RESET);

}

//two long blinks
void led_blink_partial_success(void){

        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_SET);
        HAL_Delay(600);
        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_RESET);

        HAL_Delay(200);

        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_SET);
        HAL_Delay(600);
        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_RESET);

}

//three shorter blinks
void led_blink_partial_failure(void){

        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_SET);
        HAL_Delay(250);
        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_RESET);

        HAL_Delay(200);

        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_SET);
        HAL_Delay(250);
        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_RESET);

        HAL_Delay(200);

        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_SET);
        HAL_Delay(250);
        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_RESET);

}

//seven short blinks
void led_blink_complete_failure(void){

        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_SET);
        HAL_Delay(100);
        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_RESET);

        HAL_Delay(50);

        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_SET);
        HAL_Delay(100);
        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_RESET);

        HAL_Delay(50);

        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_SET);
        HAL_Delay(100);
        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_RESET);

        HAL_Delay(50);

        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_SET);
        HAL_Delay(100);
        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_RESET);

        HAL_Delay(50);

        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_SET);
        HAL_Delay(100);
        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_RESET);

        HAL_Delay(50);

        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_SET);
        HAL_Delay(100);
        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_RESET);

        HAL_Delay(50);

        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_SET);
        HAL_Delay(100);
        HAL_GPIO_WritePin(USER_LED_GPIO, USER_LED_PIN, GPIO_PIN_RESET);
}



void user_led_init(void){

        GPIO_InitTypeDef led_config;

        led_config.Pin = USER_LED_PIN;
        led_config.Mode = GPIO_MODE_OUTPUT_PP;
        led_config.Pull = GPIO_NOPULL;
        led_config.Speed = GPIO_SPEED_FREQ_MEDIUM;

        HAL_GPIO_DeInit(USER_LED_GPIO, USER_LED_PIN);
        HAL_Delay(20);
        HAL_GPIO_Init(USER_LED_GPIO, &led_config);

}*/

void get_packet_status(PacketStatus_t *pktStatus) {

    uint8_t status[3];

    read_command(RADIO_GET_PACKETSTATUS, status, 3);

#if USE_MODEM_LORA == 1
    pktStatus->packetType = PACKET_TYPE_LORA;
    pktStatus->Params.LoRa.RssiPkt = -status[0] / 2;
    (status[1] < 128)
        ? (pktStatus->Params.LoRa.SnrPkt = status[1] / 4)
        : (pktStatus->Params.LoRa.SnrPkt = ((status[1] - 256) / 4));
    pktStatus->Params.LoRa.SignalRssiPkt = -status[2] / 2;
    pktStatus->Params.LoRa.FreqError = FrequencyError;

#elif USE_MODEM_FSK == 1
    pktStatus->packetType = PACKET_TYPE_GFSK;
    pktStatus->Params.Gfsk.RxStatus = status[0];
    pktStatus->Params.Gfsk.RssiSync = -status[1] / 2;
    pktStatus->Params.Gfsk.RssiAvg = -status[2] / 2;
    pktStatus->Params.Gfsk.FreqError = 0;

#else
    // In that specific case, we set everything in the pktStatus to zeros
    // and reset the packet type accordingly
    pktStatus->packetType = PACKET_TYPE_NONE;
    memset(pktStatus, 0, sizeof(PacketStatus_t));
    pktStatus->packetType = PACKET_TYPE_NONE;
#endif
}

void clear_irq_status(uint16_t irq) {
    uint8_t buf[2];
#if LORA_DEBUGGING == 1
    sd_printf(EventEntry, "Clear Irq Status: 0x%04x\r\n", irq);
#endif
    buf[0] = (uint8_t)(((uint16_t)irq >> 8) & 0x00FF);
    buf[1] = (uint8_t)((uint16_t)irq & 0x00FF);
    write_command(RADIO_CLR_IRQSTATUS, buf, 2);
}

void process_irqs(void) {

    /* -> uncomment if Polling mode is implemented
    if( this->PollingMode == true )
{
    if( this->IrqState == true )
    {
        __disable_irq( );
        this->IrqState = false;
        __enable_irq( );
    }
    else
    {
        return;
    }
}*/
    irq_triggered = 0;

    irqRegs = irqRegs | get_irq_status();
    clear_irq_status(IRQ_RADIO_ALL);

#if LORA_DEBUGGING == 1
    sd_printf(EventEntry, "Processing IRQs: 0x%04x\r\n", irqRegs);
#endif

    if ((irqRegs & IRQ_HEADER_VALID) == IRQ_HEADER_VALID) {
        // LoRa Only
        FrequencyError =
            0x000000 | ((0x0F & read_reg(REG_FREQUENCY_ERRORBASEADDR)) << 16);
        FrequencyError =
            FrequencyError | (read_reg(REG_FREQUENCY_ERRORBASEADDR + 1) << 8);
        FrequencyError =
            FrequencyError | (read_reg(REG_FREQUENCY_ERRORBASEADDR + 2));
    }

    if ((irqRegs & IRQ_TX_DONE) == IRQ_TX_DONE) {

        tx_done();
    }

    if ((irqRegs & IRQ_RX_DONE) == IRQ_RX_DONE) {
        if ((irqRegs & IRQ_CRC_ERROR) == IRQ_CRC_ERROR) {

            rx_error(IRQ_CRC_ERROR_CODE);

        } else {

            rx_done();
        }
    }

    if ((irqRegs & IRQ_CAD_DONE) == IRQ_CAD_DONE) {

        cad_done((irqRegs & IRQ_CAD_ACTIVITY_DETECTED) ==
                 IRQ_CAD_ACTIVITY_DETECTED);
    }

    if ((irqRegs & IRQ_RX_TX_TIMEOUT) == IRQ_RX_TX_TIMEOUT) {
        if ((OperatingMode == MODE_TX)) {
            tx_timeout();
        } else if ((OperatingMode == MODE_RX)) {
            rx_timeout();
        } else {
            assert_param(false);
        }
    }

    /*
        //IRQ_PREAMBLE_DETECTED                   = 0x0004,
        if( irqRegs & IRQ_PREAMBLE_DETECTED )
        {
            if( rxPblSyncWordHeader != NULL )
            {
                rxPblSyncWordHeader( IRQ_PBL_DETECT_CODE);

            }
        }

        //IRQ_SYNCWORD_VALID                      = 0x0008,
        if( irqRegs & IRQ_SYNCWORD_VALID )
        {
            if( rxPblSyncWordHeader != NULL )
            {
                rxPblSyncWordHeader( IRQ_SYNCWORD_VALID_CODE  );
            }
        }

        //IRQ_HEADER_VALID                        = 0x0010,
        if ( irqRegs & IRQ_HEADER_VALID )
        {
            if( rxPblSyncWordHeader != NULL )
            {
                rxPblSyncWordHeader( IRQ_HEADER_VALID_CODE );
            }
        }

        //IRQ_HEADER_ERROR                        = 0x0020,
        if( irqRegs & IRQ_HEADER_ERROR )
        {
            if( rxError != NULL )
            {
                rxError( IRQ_HEADER_ERROR_CODE );
            }
        }
    */
}

void remove_irq(uint16_t irq) {

    irqRegs = irqRegs & (~irq);
}

// Callback functions for events
void tx_done(void) {
    radio_flags.flags.txDone = 1;
    remove_irq(IRQ_TX_DONE);

#if LORA_IRQ_TYPE_DEBUGGING == 1
    sd_printf(EventEntry, "TX Done event triggered\r\n");
#endif
}

void rx_done(void) {
    radio_flags.flags.rxDone = 1;
    remove_irq(IRQ_RX_DONE);

#if LORA_IRQ_TYPE_DEBUGGING == 1
    sd_printf(EventEntry, "RX Done event triggered\r\n");
#endif
}

void tx_timeout(void) {
    radio_flags.flags.txTimeout = 1;
    remove_irq(IRQ_RX_TX_TIMEOUT);
    // debug_if( DEBUG_MESSAGE, "> OnTxTimeout\n\r" );

#if LORA_IRQ_TYPE_DEBUGGING == 1
    sd_printf(EventEntry, "TX Timeout event triggered\r\n");
#endif
}

void rx_timeout(void) {
    radio_flags.flags.rxTimeout = 1;
    remove_irq(IRQ_RX_TX_TIMEOUT);
    // debug_if( DEBUG_MESSAGE, "> OnRxTimeout\n\r" );

#if LORA_IRQ_TYPE_DEBUGGING == 1
    sd_printf(EventEntry, "RX Timeout event triggered\r\n");
#endif
}

void rx_error(IrqErrorCode_t errCode) {
    radio_flags.flags.rxError = 1;
    remove_irq(IRQ_CRC_ERROR);
    // debug_if( DEBUG_MESSAGE, "> OnRxError\n\r" );

#if LORA_IRQ_TYPE_DEBUGGING == 1
    sd_printf(EventEntry, "RX Error event triggered\r\n");
#endif
}

void cad_done(uint8_t cadFlag) {

#if LORA_IRQ_TYPE_DEBUGGING == 1
    sd_printf(EventEntry, "CAD Done event triggered\r\n");
#endif
}

void lora_irq_callback(void) {
    irq_triggered = 1;
    osThreadFlagsSet(lora_taskHandle, LORA_INTERRUPT_RTOS_FLAG);
}
