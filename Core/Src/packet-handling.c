/**
 *******************************************************************************
 * @file           packet-handling.c
 * @brief          Handler interface that acts on incoming packets.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdlib.h>
#include <string.h>

#include "main.h"
#include "packet-handling.h"
#include "system-status.h"
#include "tasks/control-loop.h"
#include "tasks/finite_state_machine.h"
#include "tasks/packets.h"
#include "tasks/sd-log.h"
#include "util/serialization.h"

#if configUSE_TRACE_FACILITY
extern traceString packets_channel;
#endif

void packet_received(Packet *packet) {
// Release allocated payload pointer
#if configUSE_TRACE_FACILITY
    vTracePrintF(packets_channel,
                 "Processing packet from origin %d, size: %u B", packet->origin,
                 packet->payload_size);
#endif

    // Update ICSB status if they're from the ICSB
    switch (packet->origin) {
        case InCanopyBoardLeft:
            global_system_status.icsb_1_last_update = osKernelGetTickCount();
            break;
        case InCanopyBoardRight:
            global_system_status.icsb_2_last_update = osKernelGetTickCount();
            break;
        default: // no action
            break;
    }

    switch (packet->type) {
        case CommandPacket: {
            if (packet->payload) {
                // notify FSM of incoming transition
                enum SystemInput inp = *((uint8_t *)packet->payload);
                osEventFlagsSet(fsm_transitionHandle, inp);
            }
            break;
        }
        case DataPacket: {
            if (packet->payload) {
                process_data_payload(packet->payload, packet->origin);
            }
            break;
        }
        case StatePacket: {
            // Only the ICSBs send state packets, so update the ICSB status
            // TODO: update ICSB status and not only switch it to 1
            // 2nd byte is the battery voltage, log it in mV
            sd_printf(EventEntry, "ICSB Status is %u",
                      *(uint8_t *)packet->payload);
            sd_printf(SensorDataEntry, "BTI1,%i mV",
                      *(((uint8_t *)packet->payload) + 1) * 20);
            break;
        }
        default:
            break;
    }

    // remember that the payload still has to be freed by the handler
    free(packet->payload);
}

void process_data_payload(void *payload, enum Location origin) {
    assert_param(payload != NULL);
#if configUSE_TRACE_FACILITY
    vTracePrint(packets_channel, "Processing data payload.");
#endif

    struct DataPayload pl;
    deserialize_data_payload(payload, &pl);

    switch (pl.sensor_type) {
        case BNO055_Sensor: {
            osMutexAcquire(write_sensor_data_mutexHandle, osWaitForever);

            switch (origin) {
                case InCanopyBoardLeft:
                    memcpy(&newest_sensor_data->icsb_att_readout_1,
                           &pl.bno_readout, sizeof(struct BNO055_Readout));
                    newest_sensor_data->changed_readings |=
                        ICSB_ATT_READOUT_1_CHANGED;
#if configUSE_TRACE_FACILITY
                    vTracePrintF(packets_channel, "Left ICSB data updated.");
#endif
                    sd_printf(
                        SensorDataEntry,
                        "CNA1,%+07i,%+07i,%+07i,%+07i,%+07i,%+07i,%+07i,%+07i,"
                        "%+07i,%+07i",
                        pl.bno_readout.lin_acc.x, pl.bno_readout.lin_acc.y,
                        pl.bno_readout.lin_acc.z, pl.bno_readout.ang_vel.p,
                        pl.bno_readout.ang_vel.q, pl.bno_readout.ang_vel.r,
                        pl.bno_readout.qtr_att.x, pl.bno_readout.qtr_att.y,
                        pl.bno_readout.qtr_att.z, pl.bno_readout.qtr_att.w);
                    break;
                case InCanopyBoardRight:
                    memcpy(&newest_sensor_data->icsb_att_readout_2,
                           &pl.bno_readout, sizeof(struct BNO055_Readout));
                    newest_sensor_data->changed_readings |=
                        ICSB_ATT_READOUT_2_CHANGED;
#if configUSE_TRACE_FACILITY
                    vTracePrintF(packets_channel, "Right ICSB data updated.");
#endif
                    sd_printf(
                        SensorDataEntry,
                        "CNA2,%+07i,%+07i,%+07i,%+07i,%+07i,%+07i,%+07i,%+07i,"
                        "%+07i,%+07i",
                        pl.bno_readout.lin_acc.x, pl.bno_readout.lin_acc.y,
                        pl.bno_readout.lin_acc.z, pl.bno_readout.ang_vel.p,
                        pl.bno_readout.ang_vel.q, pl.bno_readout.ang_vel.r,
                        pl.bno_readout.qtr_att.x, pl.bno_readout.qtr_att.y,
                        pl.bno_readout.qtr_att.z, pl.bno_readout.qtr_att.w);
                    break;
                default:
                    break;
            }
            osMutexRelease(write_sensor_data_mutexHandle);

            break;
        }
        case BMP388_Sensor:
            // do nothing with the data, yet
            sd_printf(SensorDataEntry, "%s,%+07i,%+07i",
                      (origin == InCanopyBoardLeft) ? "CNP1" : "CNP2",
                      pl.bmp_readout.pressure, pl.bmp_readout.temperature);
            break;
        default:
#if configUSE_TRACE_FACILITY
            vTracePrintF(packets_channel,
                         "Received a data payload of an unknown sensor type.");
#endif
            sd_log("Received invalid data payload.", SensorDataEntry);
            break;
    }
}
