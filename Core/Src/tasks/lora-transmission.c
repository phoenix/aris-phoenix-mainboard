/**
 *******************************************************************************
 * @file           lora-transmission.c
 * @brief          Implements the task responsible for LoRa communication.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdbool.h>

#include "cmsis_os.h"

#include "config.h"
#include "drivers/lora_airborne.h"
#include "tasks/lora-transmission.h"

#if !USE_LORA_HAL

void lora_transmission_task_entry(void *arg) {
    // Initialize LoRa interface
    lora_init_airborne();

    // Special slave function for Paratec deployment testing
    lora_slave_function();
}

#endif /* USE_LORA_HAL */
