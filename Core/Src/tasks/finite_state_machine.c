/**
 *******************************************************************************
 * @file           finite_state_machine.c
 * @brief          This file implements a finite state machine keeping
 *                         track of the Guided Recovery system's state.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdbool.h>
#include <stdlib.h>

#include "cmsis_os.h"

#include "config.h"
#include "controllers/motor-controller.h"
#include "drivers/servo-driver.h"
#include "main.h"
#include "tasks/control-loop.h"
#include "tasks/finite_state_machine.h"
#include "tasks/packets.h"
#include "tasks/sd-log.h"
#include "tasks/sensor-task.h"
#include "util/watchdog.h"

/* Tracing support functions ------------------------------------------------ */

#if configUSE_TRACE_FACILITY

/** Tracing channel for the FSM states. Only publish state strings here. */
traceString fsm_state_channel = NULL;
/** Tracing channel for all the other events, e. g. logging data. */
traceString fsm_events = NULL;

#endif

/* Private functions -------------------------------------------------------- */

// These are the finite state machine transition handlers.

/**
 * @brief Perform the transition from the initial state to the calibrated state.
 */
void transition_calibrate(void);

/**
 * @brief Perform the "Warm Up" transition.
 *
 * This function sends a packet containing the "Activate" command to both ICSBs.
 */
void transition_warm_up(void);

void transition_disarm(void);

void transition_arm(void);

void transition_drop(void);

void transition_trigger_deployment(void);

void transition_trigger_approach(void);

void transition_touchdown_detected(void);

/* Private variables and definitions ---------------------------------------- */
/** Number of allowed transitions. Used for iterating over the array. */
#define NUM_TRANSITIONS 10

/** Includes all allowed transitions of the finite state machine. If it isn't in
  the array, it isn't allowed and should lead to the FSM being put in the
  "stuck" state. */
static const struct SystemTransition allowed_transitions[NUM_TRANSITIONS] = {
    {InitialState, CalibratedState, Calibrate, &transition_calibrate},
    {CalibratedState, LiveState, WarmUp, &transition_warm_up},
    {LiveState, ArmedState, Arm, &transition_arm},
    {ArmedState, DisarmedState, Disarm, &transition_disarm},
    {ArmedState, DrogueDescentState, Drop, &transition_drop},
    {ArmedState, GuidedDescentState, TriggerDeployment,
     &transition_trigger_deployment},
    {DrogueDescentState, DisarmedState, Disarm, &transition_disarm},
    {DrogueDescentState, GuidedDescentState, TriggerDeployment,
     &transition_trigger_deployment},
    {GuidedDescentState, FinalApproachState, TriggerApproach,
     &transition_trigger_approach},
    {FinalApproachState, TouchdownState, TouchdownDetected,
     &transition_touchdown_detected}};

/** The current state of the guided recovery system. */
static enum SystemState system_state;
// static to avoid it being changeable outside of the scope of this file

void fsm_task_entry(void *arg) {

#if configUSE_TRACE_FACILITY
    fsm_state_channel = xTraceRegisterString("FSM State");
    fsm_events = xTraceRegisterString("FSM Events");
#endif
    // initialize state machine
    system_state = InitialState;

#if configUSE_TRACE_FACILITY
    const char *state_tracing_strings[10] = {
        "Stuck",     "Initial",       "Calibrated",    "Live",
        "Armed",     "DrogueDescent", "GuidedDescent", "FinalApproach",
        "Touchdown", "Disarmed"};
    vTracePrint(fsm_state_channel,
                state_tracing_strings[(uint8_t)system_state]);
#endif

    // the finite state machine now will start waiting for an event to happen
    const uint32_t all_flags = 0xFFU; // this is 1111 1111b, all input flags
    uint32_t raised_flags = 0x0U;

    // wait for SD to become available and log completed setup
    osEventFlagsWait(setup_flagsHandle, SETUP_FLAG_SD_READY,
                     osFlagsNoClear | osFlagsWaitAll, osWaitForever);

    LOG_AND_TRACE(EventEntry, fsm_events,
                  "Finite State Machine set up successfully and waiting for "
                  "transitions.");

    while (true) {

        raised_flags =
            osEventFlagsWait(fsm_transitionHandle, // wait for this event flag
                             all_flags,      // wait for any transition flag
                             osFlagsWaitAny, // default: any one alone suffices
                             osWaitForever); // stay blocked until flag raised

        LOG_AND_TRACEF(EventEntry, fsm_events,
                       "Transition event flag raised, value: %u", raised_flags);

        bool found_transition = false;
        for (uint_fast8_t i = 0; i < NUM_TRANSITIONS; i++) {
            // check if transition input is within the triggered inputs and
            // current state / transition state match
            if ((allowed_transitions[i].input & raised_flags) &&
                (allowed_transitions[i].initial_state == system_state)) {

                LOG_AND_TRACEF(EventEntry, fsm_events,
                               "Matching transition from (%u) to (%u).",
                               allowed_transitions[i].initial_state,
                               allowed_transitions[i].final_state);

                found_transition = true;

                LOG_AND_TRACE(EventEntry, fsm_events,
                              allowed_transitions[i].handler != NULL
                                  ? "Calling transition handler..."
                                  : "No transition handler found!");

                if (allowed_transitions[i].handler != NULL) {
                    allowed_transitions[i].handler();
                }

                system_state = allowed_transitions[i].final_state;

#if configUSE_TRACE_FACILITY
                vTracePrint(fsm_state_channel,
                            state_tracing_strings[(uint8_t)system_state]);
#endif
            }
        }

        if (!found_transition) {
            // Did not find a transition for this input. In normal state
            // machines, this would mean that the state machine is stuck. We
            // want to avoid a stuck state and do nothing instead.
            /* system_state = Stuck; */
            LOG_AND_TRACE(ErrorEntry, fsm_events, "System would be stuck!");
        }
    }
}

void fsm_input(enum SystemInput input) {
    osEventFlagsSet(fsm_transitionHandle, input);
}

enum SystemState fsm_get_state(void) {
    // This is an int type, which can be read atomically, so no mutex needed
    return system_state;
}

/* Finite State Machine Transition Handlers --------------------------------- */

void transition_calibrate() {
    // Send calibration command to ICSB and to receiver board
    // Report results of calibration
    LOG_AND_TRACE(EventEntry, fsm_events, "Entered CALIBRATE transition.");

    if (!sensor_task_command_calibration()) {
        LOG_AND_TRACE(
            ErrorEntry, fsm_events,
            "Failed to calibrate sensors in Finite State Machine transition!");
    }
}

void transition_warm_up(void) {
    LOG_AND_TRACE(EventEntry, fsm_events, "Entered WARMUP transition.");

    // Pull up the GPIO pin for 1ms to trigger the interrupt on the ESP board
    HAL_GPIO_WritePin(ICSB_WAKEUP_GPIO_Port, ICSB_WAKEUP_Pin, GPIO_PIN_SET);
    osDelay(5);
    HAL_GPIO_WritePin(ICSB_WAKEUP_GPIO_Port, ICSB_WAKEUP_Pin, GPIO_PIN_RESET);

    LOG_AND_TRACE(EventEntry, fsm_events, "Completed WARMUP transition");

    return;
}

void transition_arm() {
    LOG_AND_TRACE(EventEntry, fsm_events, "Entered ARM transition.");
    motors_prepare_for_drop();

    if (height_trigger_arm()) {
        LOG_AND_TRACE(EventEntry, fsm_events,
                      "Height trigger successfully armed.");
    } else {
        LOG_AND_TRACE(ErrorEntry, fsm_events, "Failed to arm height trigger.");
    }
    // Don't start safety timer yet
    // Confirm connection to pressure sensors
    // Send back confirmation of successful arming via LoRa link
}

void transition_disarm() {
    LOG_AND_TRACE(EventEntry, fsm_events, "Entered DISARM transition.");

    // try to stop the watchdog if it is currently running and can still be
    // stopped
    watchdog_disable();

    // stop the safety timer from triggering if it is running
    if (osTimerIsRunning(safety_timerHandle))
        osTimerStop(safety_timerHandle);

    if (height_trigger_disarm()) {
        LOG_AND_TRACE(EventEntry, fsm_events,
                      "Height trigger successfully disarmed.");
    } else {
        LOG_AND_TRACE(ErrorEntry, fsm_events,
                      "Failed to disarm height trigger.");
    }
}

void transition_drop() {
    LOG_AND_TRACE(EventEntry, fsm_events, "Entered DROP transition.");
    // Start safety timer
    osStatus_t status = osTimerStart(
        safety_timerHandle, SAFETY_TIMER_DURATION_MS / portTICK_PERIOD_MS);

    if (status == osOK) {
        LOG_AND_TRACE(EventEntry, fsm_events, "Safety timer started.");
    } else {
        LOG_AND_TRACEF(ErrorEntry, fsm_events,
                       "Failed to start safety timer. Status is (%d)", status);
    }

    watchdog_enable_on_timer(WATCHDOG_PRETIMER_MS);
}

void transition_trigger_deployment() {
    LOG_AND_TRACE(EventEntry, fsm_events,
                  "Entered TRIGGER DEPLOYMENT transition.");

    // The most important task right at the beginning: actuate the servo motors
    // to release the parachute from the tube and let it open
    trigger_deployment_servos();

    // after the servos have been actuated, the watch dog firing should not
    // actuate the servos again
    watchdog_disable();

    // Disable the safety timer, it doesn't have to fire since it would only
    // trigger this transition again.
    if (osTimerIsRunning(safety_timerHandle)) {
        osStatus_t status = osTimerStop(safety_timerHandle);

        if (status == osOK) {
            LOG_AND_TRACE(EventEntry, fsm_events,
                          "Running safety timer was stopped.");
        } else {
            LOG_AND_TRACE(ErrorEntry, fsm_events,
                          "Failed to stop the running safety timer!");
        }
    } else {
        LOG_AND_TRACE(EventEntry, fsm_events,
                      "Safety timer not stopped because it was not running.");
    }

    // Also, the height trigger can be stopped since it already triggered.
    height_trigger_disarm();

    // Wait for five seconds before brakes are released
    osDelay(5000 / portTICK_PERIOD_MS);
    motors_release_brakes();

    osDelay(5);

    // After brakes have been released, the control loop can be started
    state_estimation_set_running(true);

    LOG_AND_TRACE(EventEntry, fsm_events,
                  "Completed TRIGGER DEPLOYMENT transition");
}

void transition_trigger_approach(void) {
    LOG_AND_TRACE(EventEntry, fsm_events,
                  "Entered TRIGGER APPROACH transition.");

    // Stop the State Estimation from running to stop it from interfering with
    // the line pulling we'll do here
    state_estimation_set_running(false);

    // Wait for the control loop to finish its last command to the actuators
    // before taking over control
    osDelay(50 / portTICK_PERIOD_MS);

    // Send input to the motor controller to flare
    motors_flare();
}

void transition_touchdown_detected(void) {
    LOG_AND_TRACE(EventEntry, fsm_events,
                  "Entered TOUCHDOWN DETECTED transition.");

    // Stop the motor controller via a semaphore
    motors_disable();

    // wait 10 seconds, then close off log
    osDelay(1e4 / portTICK_PERIOD_MS);
    sd_stop_log();
}

void safety_timer_fired(void *arg) {
    // this is really bad
    // TODO: consider just changing the PWM signal here
    // trigger_deployment_servos();
    osEventFlagsSet(fsm_transitionHandle, TriggerDeployment);
}
