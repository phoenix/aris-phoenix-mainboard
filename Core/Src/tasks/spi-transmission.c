/**
 *******************************************************************************
 * @file           spi-transmission.c
 * @brief          Implements the SPI master that talks to the receiver board.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include "tasks/spi-transmission.h"
#include "cmsis_os.h"
#include "config.h"
#include "main.h"
#include "tasks/packets.h"
#include "tasks/sd-log.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#if !USE_SPI_HAL

/* Local macros ------------------------------------------------------------- */

    #define SPI_TRANSFER_COMPLETE (1 << 1)

/* Tracing ------------------------------------------------------------------ */

    #if configUSE_TRACE_FACILITY
traceString spi_channel = NULL;
    #endif

/* Local variables ---------------------------------------------------------- */

// This is a very crude way of programming this "queue" with 1 member, but
// thread-safety isn't a big issue here: If the packet isn't sent out in one
// loop iteration, it will be on the next one, which will be delayed by about
// half a second.
// TODO improve this :-) volatile doesn't provide memory barriers
/** Whether there is a packet to send to the ICSB. */
static volatile bool packet_staged = false;
static Packet out_packet = {0};

static uint8_t spi_recv_buf[257] __attribute__((aligned(4)));
static uint8_t spi_send_buf[257] __attribute__((aligned(4)));

/* Public functions --------------------------------------------------------- */

void spi_task_entry(void *arg) {

    #if configUSE_TRACE_FACILITY
    spi_channel = xTraceRegisterString("SPI Transmission Events");
    vTracePrint(spi_channel, "Entered SPI transmission task.");
    #endif /* configUSE_TRACE_FACILITY */

    // make the SPI buffer we send to the SPI peripheral 4-byte aligned, so that
    // we can safely pick it apart the same way it was packed
    // pointers to access these buffers in 32-bits
    uint32_t *data_in = (uint32_t *)spi_recv_buf;
    uint32_t *data_out = (uint32_t *)spi_send_buf;
    size_t spi_buf_len = 256;

    while (1) {

        memset(spi_recv_buf, 0, 257);
        memset(spi_send_buf, 0, 257);

        // block until interrupt from ESP32 unlocks this task
        uint32_t flags =
            osThreadFlagsWait(ESP_INTERRUPT_RECV, osFlagsWaitAny, 75);

        if (flags & (1U << 31)) {
            // highest bit set, indicating that a timeout occurred
            if (HAL_GPIO_ReadPin(ESP_INTR_GPIO_Port, ESP_INTR_Pin) ==
                GPIO_PIN_RESET) {
                continue; // pin is low, we didn't miss an interrupt
            } else {
                LOG_AND_TRACE(
                    ErrorEntry, spi_channel,
                    "Missed an interrupt from ESP32. Set up transaction...");
                // pin is high, continue with the transaction. we did miss the
                // interrupt.
            }
        }

    #if configUSE_TRACE_FACILITY
        vTracePrintF(spi_channel, "Flags after clearing: %u", flags);
        vTracePrint(spi_channel, "SPI task unlocked by GPIO interrupt.");
    #endif

        uint32_t first_word;
        uint32_t second_word;

        if (packet_staged) {
    #if configUSE_TRACE_FACILITY
            vTracePrint(spi_channel,
                        "There is a staged packet. Preparing reply...");
    #endif
            // there is a packet to send. pack header into first two bytes,
            // then payload into the following ones
            first_word = (((uint32_t)(out_packet.origin)) << 24) +
                         (((uint32_t)(out_packet.destination)) << 16) +
                         (((uint32_t)(out_packet.type)) << 8) +
                         ((uint32_t)(out_packet.priority));
            second_word = ((uint32_t)out_packet.payload_size) << 24;

            data_out[0] = first_word;
            data_out[1] = second_word;
            memcpy(data_out + 2, out_packet.payload, out_packet.payload_size);
            free(out_packet.payload);
            packet_staged = false;
        }

        // slave has signaled data ready
        HAL_StatusTypeDef ret = HAL_SPI_TransmitReceive_DMA(
            &hspi2, spi_send_buf, spi_recv_buf, spi_buf_len);
        if (ret == HAL_OK) {
    #if configUSE_TRACE_FACILITY
            vTracePrint(spi_channel, "HAL SPI DMA transmit request is OK.");
    #endif
            // Wait for the SPI transfer to complete. Note that the flags are
            // cleared in a second step. This is due to an undocumented
            // behaviour that osThreadFlagsWait() does also clear flags that
            // aren't waited for. When this happens, a new "ESP_INTR" flag is
            // also cleared and never set again – thus locking the SPI bus.
            // Therefore, we make sure only to clear the "SPI_TRANSFER_COMPLETE"
            // flag when the DMA transfer completes.
            flags = osThreadFlagsWait(SPI_TRANSFER_COMPLETE, osFlagsNoClear,
                                      35 / portTICK_PERIOD_MS);
            osThreadFlagsClear(SPI_TRANSFER_COMPLETE);
    #if configUSE_TRACE_FACILITY
            vTracePrintF(spi_channel, "Flags before clearing: %u", flags);
    #endif
            first_word = data_in[0];
            second_word = data_in[1];
            Packet packet = {.origin = (first_word & 0xff000000) >> 24,
                             .destination = (first_word & 0x00ff0000) >> 16,
                             .type = (first_word & 0x0000ff00) >> 8,
                             .priority = (first_word & 0x000000ff),
                             .payload_size = (second_word & 0xff000000) >> 24,
                             .payload = NULL};
            if ((data_in[1] & 0x00ffffff) != 0x00000000) {
                // because only the first five bytes of the first two words are
                // used for header information, the last three must be zero,
                // otherwise it is an invalid packet
    #if configUSE_TRACE_FACILITY
                vTracePrint(spi_channel, "Received packet does not have three "
                                         "empty bytes. Dropping.");
    #endif
                // just skip the packet
            } else {
                if (packet.payload_size > 0) {
                    packet.payload = malloc(packet.payload_size);
                    memcpy(packet.payload, data_in + 2, packet.payload_size);
                }

                if (packet_add_to_queue(&packet) != 0) {
    #if configUSE_TRACE_FACILITY
                    vTracePrint(spi_channel, "Failed to put packet in queue. "
                                             "Dropped a packet.");
    #endif
                    free(packet.payload);
                }
            }

        } else {
    #if configUSE_TRACE_FACILITY
            vTracePrintF(
                spi_channel,
                "Error: SPI DMA transmit request returned error code %i", ret);
    #endif
            // do nothing
        }
    }
}

int transmit_packet_spi(Packet *packet) {
    if (!packet)
        return -1;
    if (packet_staged) {
        free(packet->payload); // comply with memory invariant
        return -1;             // already a packet staged
    }

    // copy the packet info over, don't free payload memory yet
    memcpy(&out_packet, packet, sizeof(Packet));
    packet_staged = true; // only set this at end, so that copy is
                          // complete

    return 0;
}

/* ISR callbacks ------------------------------------------------------------ */

// These ISR callbacks override the weak symbols in the HAL header files

void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi) {
    if (hspi == &hspi2) {
        osThreadFlagsSet(spi_taskHandle, SPI_TRANSFER_COMPLETE);
    }
}

#endif
