/**
 *******************************************************************************
 * @file           gps-task.c
 * @brief          Task receiving measurements from the GPS receivers
 * @author         Lukas Vogel
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdbool.h>

#include "cmsis_os.h"

#include "drivers/neo-7m.h"
#include "main.h"
#include "system-status.h"
#include "tasks/control-loop.h"
#include "tasks/downlink-task.h"
#include "tasks/gps-task.h"
#include "tasks/sd-log.h"
#include "util/gps-parsing.h"

/* Local macros ------------------------------------------------------------- */

/** Callback integer to indicate MNG1 data is ready */
#define MNG1_READY (1U << 4)
/** Callback integer to indicate MNG1 data is ready */
#define MNG2_READY (1U << 5)

/* Private variables -------------------------------------------------------- */

/** Main tube GPS receiver #1 */
static struct NEO7M_Handle MNG1 = {
    .huart = &huart_MNG1,
    .ready_cb = NEO7M_RxCpltCallback,
    .arg = MNG1_READY,
};

/** Main tube GPS receiver #2 */
static struct NEO7M_Handle MNG2 = {
    .huart = &huart_MNG2,
    .ready_cb = NEO7M_RxCpltCallback,
    .arg = MNG2_READY,
};

/* Tracing functionality ---------------------------------------------------- */
#if configUSE_TRACE_FACILITY
traceString gps_channel = NULL;
#endif

/* Public functions --------------------------------------------------------- */

void gps_task_entry(void *arg) {

#if configUSE_TRACE_FACILITY
    gps_channel = xTraceRegisterString("GPS Events");
    vTracePrint(gps_channel, "Entered GPS task.");
#endif

    // Wait for the SD card to become available
    osEventFlagsWait(setup_flagsHandle, SETUP_FLAG_SD_READY,
                     osFlagsWaitAny | osFlagsNoClear, osWaitForever);

    if (!neo7m_init(&MNG1)) {
#if configUSE_TRACE_FACILITY
        vTracePrint(gps_channel, "Setup of MNG1 failed.");
#endif
        sd_log("Setup of MNG1 failed.", ErrorEntry);
        osDelay(osWaitForever);
    }

    if (!neo7m_init(&MNG2)) {
#if configUSE_TRACE_FACILITY
        vTracePrint(gps_channel, "Setup of MNG2 failed.");
#endif
        sd_log("Setup of MNG2 failed.", ErrorEntry);
        osDelay(osWaitForever);
    }

    sd_log("Setup of GPS task done.", EventEntry);

    struct NEO7M_Readout gps_pos;

    while (true) {

        uint32_t flag = osThreadFlagsWait(MNG1_READY | MNG2_READY,
                                          osFlagsWaitAny, osWaitForever);

        if (flag & MNG1_READY) {
            if (neo7m_get_readout(&MNG1, &gps_pos)) {
                // To write new date into the "newest_sensor_data" location,
                // first acquire the mutex.

                if (gps_pos.has_fix) {
                    // the sensor data is only copied if the sensor has a fix
                    osMutexAcquire(write_sensor_data_mutexHandle,
                                   osWaitForever);
                    memcpy(&newest_sensor_data->geodetic_pos_1, &gps_pos,
                           sizeof(struct NEO7M_Readout));

                    // set flag that the GPS readout 1 has changed
                    newest_sensor_data->changed_readings |=
                        MAIN_GPS_READOUT_1_CHANGED;

                    osMutexRelease(write_sensor_data_mutexHandle);

                    sd_printf(SensorDataEntry, "MNG1,%u,%f,%f,%f",
                              gps_pos.num_satellites, gps_pos.position.latitude,
                              gps_pos.position.longitude,
                              gps_pos.position.elevation);
                } else {
                    // readout successful, but unfortunately no fix
                    sd_printf(SensorDataEntry, "MNG1,nofix");
                }

                // Update global status that the MNG1 now is sending
                global_system_status.mng1_status =
                    MNG1.has_fix ? FixGPSStatus : RespondingGPSStatus;
            } else {
                // sd_log("MNG1 couldn't be parsed!", ErrorEntry);
                // something failed
            }
        }

        if (flag & MNG2_READY) {
            if (neo7m_get_readout(&MNG2, &gps_pos)) {
                // To write new date into the "newest_sensor_data" location,
                // first acquire the mutex.

                if (gps_pos.has_fix) {
                    // the sensor data is only copied if the sensor has a fix
                    osMutexAcquire(write_sensor_data_mutexHandle,
                                   osWaitForever);
                    memcpy(&newest_sensor_data->geodetic_pos_2, &gps_pos,
                           sizeof(struct NEO7M_Readout));

                    // set flag that the GPS readout 1 has changed
                    newest_sensor_data->changed_readings |=
                        MAIN_GPS_READOUT_2_CHANGED;

                    osMutexRelease(write_sensor_data_mutexHandle);

                    sd_printf(SensorDataEntry, "MNG2,%u,%f,%f,%f",
                              gps_pos.num_satellites, gps_pos.position.latitude,
                              gps_pos.position.longitude,
                              gps_pos.position.elevation);
                } else {
                    // readout successful, but unfortunately no fix
                    sd_printf(SensorDataEntry, "MNG2,nofix");
                }

                global_system_status.mng2_status =
                    MNG2.has_fix ? FixGPSStatus : RespondingGPSStatus;
            } else {
                // sd_log("MNG2 couldn't be parsed!", ErrorEntry);
                // something failed
            }
        }
    }
}

void GPS_RxCpltCallback(UART_HandleTypeDef *huart) {
    // Compare against known handles (can do since they are pointers)
    if (huart == &huart_MNG1) {
        neo7m_process(&MNG1);
    } else if (huart == &huart_MNG2) {
        neo7m_process(&MNG2);
    }
}

void NEO7M_RxCpltCallback(int arg) {
    osThreadFlagsSet(gps_taskHandle, (unsigned int)arg);
}
