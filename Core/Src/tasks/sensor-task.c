/**
 *******************************************************************************
 * @file                sensor-task.c
 * @brief               Interface for the sampling of IMU/barometer
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <assert.h>
#include <stdbool.h>
#include <string.h>

#include "config.h"
#include "drivers/bmp-388.h"
#include "drivers/bno-055.h"
#include "main.h"
#include "system-status.h"
#include "tasks/control-loop.h"
#include "tasks/sd-log.h"
#include "tasks/sensor-task.h"

/* Local macros ------------------------------------------------------------- */

#define I2C_DMA_RX_COMPLETE  (1 << 1)
#define I2C_DMA_TX_COMPLETE  (1 << 2)
#define SENSORS_SPL_DEADLINE (1 << 7)

// Commands sent around as thread flags
#define SENSORS_CALIBRATION_START    (1 << 16) ///< Starts the calibration
#define SENSORS_CALIBRATION_COMPLETE (1 << 17) ///< Calibration complete
#define SENSORS_CALIBRATION_ERROR    (1 << 18) ///< Calibration error

/* Tracing support ---------------------------------------------------------- */

#if configUSE_TRACE_FACILITY
traceString sensors_chn = NULL;
#endif

/* Private variables -------------------------------------------------------- */

/** Handle for the BNO055 #1 on I2C bus 1 */
static BNO055_Handle bno055_1 = {
    .afsr = ACCEL_FSR4G,
    .gfsr = GYRO_FSR125,
    .hi2c = &hi2c1,
    .use_primary_addr = true,
    .reset_port = BNO_1_RST_GPIO_Port,
    .reset_pin = BNO_1_RST_Pin,
    .acc_precalib =
        {
            .x = CONFIG_BNO_ACCEL_OFFSET_X_1,
            .y = CONFIG_BNO_ACCEL_OFFSET_Y_1,
            .z = CONFIG_BNO_ACCEL_OFFSET_Z_1,
            .r = CONFIG_BNO_ACCEL_OFFSET_R_1,
        },
    .gyro_precalib =
        {
            .x = CONFIG_BNO_GYRO_OFFSET_X_1,
            .y = CONFIG_BNO_GYRO_OFFSET_Y_1,
            .z = CONFIG_BNO_GYRO_OFFSET_Z_1,
        },
    .mag_precalib =
        {
            .x = CONFIG_BNO_MAG_OFFSET_X_1,
            .y = CONFIG_BNO_MAG_OFFSET_Y_1,
            .z = CONFIG_BNO_MAG_OFFSET_Z_1,
            .r = CONFIG_BNO_MAG_OFFSET_R_1,
        },
};

/** Handle for the BNO055 #2 on I2C bus 1 */
static BNO055_Handle bno055_2 = {
    .afsr = ACCEL_FSR4G,
    .gfsr = GYRO_FSR125,
    .hi2c = &hi2c1,
    .use_primary_addr = false,
    .reset_port = BNO_2_RST_GPIO_Port,
    .reset_pin = BNO_2_RST_Pin,
    .acc_precalib =
        {
            .x = CONFIG_BNO_ACCEL_OFFSET_X_2,
            .y = CONFIG_BNO_ACCEL_OFFSET_Y_2,
            .z = CONFIG_BNO_ACCEL_OFFSET_Z_2,
            .r = CONFIG_BNO_ACCEL_OFFSET_R_2,
        },
    .gyro_precalib =
        {
            .x = CONFIG_BNO_GYRO_OFFSET_X_2,
            .y = CONFIG_BNO_GYRO_OFFSET_Y_2,
            .z = CONFIG_BNO_GYRO_OFFSET_Z_2,
        },
    .mag_precalib =
        {
            .x = CONFIG_BNO_MAG_OFFSET_X_2,
            .y = CONFIG_BNO_MAG_OFFSET_Y_2,
            .z = CONFIG_BNO_MAG_OFFSET_Z_2,
            .r = CONFIG_BNO_MAG_OFFSET_R_2,
        },
};

static BMP388_Handle bmp388_1 = {
    .bmp388_i2c_addr = 0x77,
    .hi2c = &hi2c1,
};

static BMP388_Handle bmp388_2 = {
    .bmp388_i2c_addr = 0x76,
    .hi2c = &hi2c1,
};

/** Handle of the task that is currently waiting on a response from the
 * calibration command. */
static osThreadId_t callback_taskHandle = NULL;

static osThreadId_t callback_i2cTransaction = NULL;

static volatile bool bno055_are_in_repair = false;

/* Private functions -------------------------------------------------------- */

/**
 * @brief Attempt to calibrate the sensors. Reports success.
 * @note This method is private, because it should only be called from the
 *  sensor task to ensure that only one task is accessing the sensors.
 * This method wakes up the BNO-055 sensor and waits for its calibration status
 * to turn to "3". It then reports success or times out waiting for that.
 * @return true
 * @return false
 */
bool sensors_calibrate(void);

void sensors_fix_bno(void);

/* Public functions --------------------------------------------------------- */

void sensor_task_entry(void *arg) {

#if configUSE_TRACE_FACILITY
    sensors_chn = xTraceRegisterString("Sensor Events");
#endif

    // wait for SD card to become available
    osEventFlagsWait(setup_flagsHandle, SETUP_FLAG_SD_READY,
                     osFlagsWaitAll | osFlagsNoClear, osWaitForever);

    /* Sensor initialization ------------------------------------------------ */

    LOG_AND_TRACE(EventEntry, sensors_chn, "Starting sensor setup.");

    sd_log("Setting up sensors.", EventEntry);
    if (!bno055_setup(&bno055_1)) {
        LOG_AND_TRACE(ErrorEntry, sensors_chn, "Failed to setup MNA1!");
        assert_param(false);
    } else {
        global_system_status.mna1_status = SetupCompleteSensorStatus;
    }

    if (!bno055_setup(&bno055_2)) {
        LOG_AND_TRACE(ErrorEntry, sensors_chn, "Failed to setup MNA2!");
        assert_param(false);
    } else {
        global_system_status.mna2_status = SetupCompleteSensorStatus;
    }

    if (!bmp388_init(&bmp388_1)) {
        LOG_AND_TRACE(ErrorEntry, sensors_chn, "Failed to setup MNP1!");
        assert_param(false);
    } else {
        global_system_status.mnp1_status = SetupCompleteSensorStatus;
    }

    if (!bmp388_init(&bmp388_2)) {
        LOG_AND_TRACE(ErrorEntry, sensors_chn, "Failed to setup MNP2!");
        assert_param(false);
    } else {
        global_system_status.mnp2_status = SetupCompleteSensorStatus;
    }

    // Pre-calibrate the sensors by pushing known calibration values
    if (!bno055_push_calibration(&bno055_1)) {
        LOG_AND_TRACE(ErrorEntry, sensors_chn,
                      "Could not push pre-calibration for MNA1!");
    }
    if (!bno055_push_calibration(&bno055_2)) {
        LOG_AND_TRACE(ErrorEntry, sensors_chn,
                      "Could not push pre-calibration for MNA2!");
    }

    LOG_AND_TRACE(EventEntry, sensors_chn, "Sensor initialization complete.");

    // wait for "calibration", then attempt to calibrate the sensors
    bool successfully_calibrated = false;
    do {
        LOG_AND_TRACE(EventEntry, sensors_chn,
                      "Waiting for \"Calibration\" flag on sensors task...");

        osThreadFlagsWait(SENSORS_CALIBRATION_START, osFlagsWaitAll,
                          osWaitForever);

        sd_log("Starting sensor calibration...", EventEntry);
        successfully_calibrated = sensors_calibrate();

        osThreadFlagsSet(callback_taskHandle, successfully_calibrated
                                                  ? SENSORS_CALIBRATION_COMPLETE
                                                  : SENSORS_CALIBRATION_ERROR);
    } while (!successfully_calibrated);

    // Calibrated the sensors, update the status
    global_system_status.mna1_status = ReadySensorStatus;
    global_system_status.mna2_status = ReadySensorStatus;
    global_system_status.mnp1_status = ReadySensorStatus;
    global_system_status.mnp2_status = ReadySensorStatus;

    // notify other threads waiting for the sensors to be ready for sampling
    osEventFlagsSet(setup_flagsHandle, SETUP_FLAG_SENSORS_CALIBRATED);

    // readouts
    struct BNO055_Readout bno055_readout_1 = {0};
    struct BNO055_Readout bno055_readout_2 = {0};
    struct BMP388_Readout bmp388_readout_1 = {0};
    struct BMP388_Readout bmp388_readout_2 = {0};

    uint8_t bno_1_cons_fails = 0;
    uint8_t bno_2_cons_fails = 0;

    while (true) {

        uint32_t successful_measurements = 0x00;

        // wait until it's time to sample
        osThreadFlagsWait(SENSORS_SPL_DEADLINE, osFlagsWaitAll, osWaitForever);

        if (global_system_status.mna1_status == ReadySensorStatus) {
            // Check whether the status is "ready", because it it isn't we
            // ignore taking this measurement for now
            if (bno055_sample(&bno055_1, &bno055_readout_1)) {
                successful_measurements |= MAIN_ATT_READOUT_1_CHANGED;
                bno_1_cons_fails = 0;
            } else {
                LOG_AND_TRACE(ErrorEntry, sensors_chn, "MNA1 sampling error");
                // if the BNO has been gone for more than 10 samples, error
                if (++bno_1_cons_fails >= 10) {
                    global_system_status.mna1_status =
                        TemporaryErrorSensorStatus;
                }
            }
        }

        if (global_system_status.mna2_status == ReadySensorStatus) {
            if (bno055_sample(&bno055_2, &bno055_readout_2)) {
                successful_measurements |= MAIN_ATT_READOUT_2_CHANGED;
                bno_2_cons_fails = 0;
            } else {
                LOG_AND_TRACE(ErrorEntry, sensors_chn, "MNA2 sampling error");
                if (++bno_2_cons_fails >= 10) {
                    global_system_status.mna2_status =
                        TemporaryErrorSensorStatus;
                }
            }
        }

        // Don't guard against sensor failures here, because it can always be
        // that the clock or data line flakes out, but the pressure sensors
        // always come back from that
        if (bmp388_sample(&bmp388_1, &bmp388_readout_1)) {
            successful_measurements |= MAIN_PRS_READOUT_1_CHANGED;
            global_system_status.mnp1_status = ReadySensorStatus;
        } else {
            LOG_AND_TRACE(ErrorEntry, sensors_chn, "MNP1 sampling error");
            global_system_status.mnp1_status = TemporaryErrorSensorStatus;
        }


        if (bmp388_sample(&bmp388_2, &bmp388_readout_2)) {
            successful_measurements |= MAIN_PRS_READOUT_2_CHANGED;
            global_system_status.mnp2_status = ReadySensorStatus;
        } else {
            LOG_AND_TRACE(ErrorEntry, sensors_chn, "MNP2 sampling error");
            global_system_status.mnp2_status = TemporaryErrorSensorStatus;
        }

        // make sure that all sensor data is written in one fell swoop
        osMutexAcquire(write_sensor_data_mutexHandle, osWaitForever);

        // only copy the data where the sampling was actually successful
        if (successful_measurements & MAIN_ATT_READOUT_1_CHANGED)
            memcpy(&newest_sensor_data->main_att_readout_1, &bno055_readout_1,
                   sizeof(struct BNO055_Readout));
        if (successful_measurements & MAIN_ATT_READOUT_2_CHANGED)
            memcpy(&newest_sensor_data->main_att_readout_2, &bno055_readout_2,
                   sizeof(struct BNO055_Readout));
        if (successful_measurements & MAIN_PRS_READOUT_1_CHANGED)
            memcpy(&newest_sensor_data->main_tube_pressure_1, &bmp388_readout_1,
                   sizeof(struct BMP388_Readout));
        if (successful_measurements & MAIN_PRS_READOUT_2_CHANGED)
            memcpy(&newest_sensor_data->main_tube_pressure_2, &bmp388_readout_2,
                   sizeof(struct BMP388_Readout));

        // hand off to the state estimation which readings were successful
        newest_sensor_data->changed_readings |= successful_measurements;

        // now ready for readout by the sensor task
        osMutexRelease(write_sensor_data_mutexHandle);
        if (successful_measurements & MAIN_ATT_READOUT_1_CHANGED) {
            sd_printf(SensorDataEntry,
                      "MNA1,%+07i,%+07i,%+07i,%+07i,%+07i,%+07i,%+07i,%+07i,%+"
                      "07i,%+07i",
                      bno055_readout_1.lin_acc.x, bno055_readout_1.lin_acc.y,
                      bno055_readout_1.lin_acc.z, bno055_readout_1.ang_vel.p,
                      bno055_readout_1.ang_vel.q, bno055_readout_1.ang_vel.r,
                      bno055_readout_1.qtr_att.x, bno055_readout_1.qtr_att.y,
                      bno055_readout_1.qtr_att.z, bno055_readout_1.qtr_att.w);
        }
        if (successful_measurements & MAIN_ATT_READOUT_2_CHANGED) {
            sd_printf(SensorDataEntry,
                      "MNA2,%+07i,%+07i,%+07i,%+07i,%+07i,%+07i,%+07i,%+07i,%+"
                      "07i,%+07i",
                      bno055_readout_2.lin_acc.x, bno055_readout_2.lin_acc.y,
                      bno055_readout_2.lin_acc.z, bno055_readout_2.ang_vel.p,
                      bno055_readout_2.ang_vel.q, bno055_readout_2.ang_vel.r,
                      bno055_readout_2.qtr_att.x, bno055_readout_2.qtr_att.y,
                      bno055_readout_2.qtr_att.z, bno055_readout_2.qtr_att.w);
        }
        if (successful_measurements & MAIN_PRS_READOUT_1_CHANGED) {
            sd_printf(SensorDataEntry, "MNP1,+%07i,+%07i",
                      bmp388_readout_1.pressure, bmp388_readout_1.temperature);
        }

        if (successful_measurements & MAIN_PRS_READOUT_2_CHANGED) {
            sd_printf(SensorDataEntry, "MNP2,+%07i,+%07i",
                      bmp388_readout_2.pressure, bmp388_readout_2.temperature);
        }

        if (global_system_status.mna1_status != ReadySensorStatus &&
            global_system_status.mna2_status != ReadySensorStatus &&
            !bno055_are_in_repair) {
            sensors_fix_bno();
            bno055_are_in_repair = true;
        }
    }
}

void sensor_spl_timer_fired(void *argument) {
    osThreadFlagsSet(sensor_taskHandle, SENSORS_SPL_DEADLINE);
}

bool sensor_task_command_calibration(void) {
    // make caller get the callback
    callback_taskHandle = osThreadGetId();
    // notify the task to start calibration
    osThreadFlagsSet(sensor_taskHandle, SENSORS_CALIBRATION_START);

    uint32_t calib_flags = osThreadFlagsWait(
        SENSORS_CALIBRATION_COMPLETE | SENSORS_CALIBRATION_ERROR,
        osFlagsWaitAny, SENSOR_CALIBRATION_TIMEOUT_MS / portTICK_PERIOD_MS);

// TODO: check for timeout on the thread flag waiting
#if configUSE_TRACE_FACILITY
    vTracePrintF(sensors_chn, "Sensor calibration returned to FSM with flag %u",
                 calib_flags);
#endif

    if (calib_flags & SENSORS_CALIBRATION_ERROR) {
        sd_log("Sensor calibration timed out.", ErrorEntry);
        return false;
    }

    sd_log("Calibration of sensors successful.", EventEntry);
    return true;
}

void sensor_task_start_sampling(void) {
    // start the timer and the sampling
    osTimerStart(sensor_spl_timerHandle,
                 configTICK_RATE_HZ / SENSOR_SAMPLING_RATE);
}

/* Private function implementation ------------------------------------------ */

bool sensors_calibrate() {

    // first, wake BNO-055 from low-power mode
    if (!bno055_wakeup(&bno055_1) || !bno055_wakeup(&bno055_2)) {
        // already failed calibration :-)
        return false;
    }

    // Because we already push a calibration profile on startup, we decided not
    // to wait for the calibration status to reach "3", but simply log what the
    // calibration matrix says now.

    bno055_print_calibration(&bno055_1);
    bno055_print_calibration(&bno055_2);

    return true;

    /*
    uint32_t current_tick = osKernelGetTickCount();
    uint32_t timeout_tick =
        current_tick +
        (SENSOR_CALIBRATION_TIMEOUT_MS - 5000) / portTICK_PERIOD_MS;
    uint8_t fully_calibrated_ticks =
        0; // subsequent ticks where CALIB_STAT was 3

    while (true) {
        if (current_tick > timeout_tick)
            return false;

        // get current system calibration status
        if (bno055_is_calibrated(&bno055_1) &&
            bno055_is_calibrated(&bno055_2)) {
            fully_calibrated_ticks++;
        } else {
            // What did it cost you? Everything. Back to the beginning.
            fully_calibrated_ticks = 0;
        }

        if (fully_calibrated_ticks > 100) {
            // print the values so that we're quicker in calibrating next time
            bno055_print_calibration(&bno055_1);
            bno055_print_calibration(&bno055_2);
            return true;
        }

        current_tick += 50 / portTICK_PERIOD_MS;
        osDelayUntil(current_tick);
    }
    */
}

/* Hardware helper methods for sensors -------------------------------------- */

int8_t i2c_master_read_slave_reg(uint8_t dev_addr, uint8_t reg_addr,
                                 uint8_t *data, uint8_t cnt) {

    if (osMutexAcquire(i2c_bus_mutexHandle, 35) != osOK) {
        LOG_AND_TRACE(ErrorEntry, sensors_chn, "Could not acquire I2C mutex.");
        return -1;
    }

    callback_i2cTransaction = osThreadGetId();

    // we now have the mutex

    // First make sure that the I2C bus state is "ready"
    if (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY) {
        LOG_AND_TRACE(ErrorEntry, sensors_chn, "I2C is not ready!");
        osMutexRelease(i2c_bus_mutexHandle);
        return -1;
    }

    // Try to queue the DMA transfer. This should succeed in most cases.
    HAL_StatusTypeDef status = HAL_I2C_Mem_Read_IT(
        &hi2c1, dev_addr << 1,
        reg_addr,      // shift device address for HAL driver
        1, data, cnt); // read in cnt number of bytes in 1 byte portions
                       // 10ms timeout on the readout
    if (status != HAL_OK) {
        LOG_AND_TRACE(ErrorEntry, sensors_chn,
                      "Couldn't queue a register read transfer (via IT)!");
        osMutexRelease(i2c_bus_mutexHandle);
        return -1;
    }

    // The DMA transfer is happening in the background. Wait here until it is
    // complete, or timeout after 35 milliseconds.
    osThreadFlagsWait(I2C_DMA_RX_COMPLETE, osFlagsWaitAll | osFlagsNoClear,
                      35 / portTICK_PERIOD_MS);
    osThreadFlagsClear(I2C_DMA_RX_COMPLETE);

    int8_t ret =
        (int8_t)(i2c_check_no_errors() ? 0 : -1); // return 0 on success
    osMutexRelease(i2c_bus_mutexHandle);
    return ret;
}

int8_t i2c_master_write_slave_reg(uint8_t dev_addr, uint8_t reg_addr,
                                  uint8_t *data, uint8_t cnt) {
    if (osMutexAcquire(i2c_bus_mutexHandle, 35) != osOK) {
        LOG_AND_TRACE(ErrorEntry, sensors_chn, "Could not acquire I2C mutex.");
        return -1;
    }

    callback_i2cTransaction = osThreadGetId();

    // First make sure that the I2C bus state is "ready"
    if (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY) {
        LOG_AND_TRACE(ErrorEntry, sensors_chn, "I2C is not ready!");
        osMutexRelease(i2c_bus_mutexHandle);
        return -1;
    }

    // Try to queue the DMA transfer. This should succeed in most cases.
    HAL_StatusTypeDef status =
        HAL_I2C_Mem_Write_IT(&hi2c1, dev_addr << 1, reg_addr, 1, data, cnt);
    if (status != HAL_OK) {
        LOG_AND_TRACE(ErrorEntry, sensors_chn,
                      "Couldn't queue a register write transfer (via IT)!");
        osMutexRelease(i2c_bus_mutexHandle);
        return -1;
    }

    // The DMA transfer is happening in the background. Wait here until it is
    // complete, or timeout after 35 milliseconds.
    osThreadFlagsWait(I2C_DMA_TX_COMPLETE, osFlagsWaitAll | osFlagsNoClear,
                      35 / portTICK_PERIOD_MS);
    osThreadFlagsClear(I2C_DMA_TX_COMPLETE);

    // Return, but check for errors first.
    int8_t ret =
        (int8_t)(i2c_check_no_errors() ? 0 : -1); // return 0 on success
    osMutexRelease(i2c_bus_mutexHandle);
    return ret;
}

void HAL_I2C_MemTxCpltCallback(I2C_HandleTypeDef *hi2c) {
    if (hi2c == &hi2c1) {
        osThreadFlagsSet(callback_i2cTransaction, I2C_DMA_TX_COMPLETE);
        // this is an ISR, so no yielding here – context switch should happen
        // immediately after returning from ISR
    }
}

void HAL_I2C_MemRxCpltCallback(I2C_HandleTypeDef *hi2c) {
    if (hi2c == &hi2c1) {
        osThreadFlagsSet(callback_i2cTransaction, I2C_DMA_RX_COMPLETE);
        // this is an ISR, so no yielding here – context switch should happen
        // immediately after returning from ISR
    }
}

/**
 * @brief Check whether an error occurred in the I2C transfer.
 * @returns True if no error occurred.
 */
bool i2c_check_no_errors() {
    uint32_t error_flags = HAL_I2C_GetError(&hi2c1);

    if (error_flags == HAL_I2C_ERROR_NONE) {
        return true;
    } else {
        LOG_AND_TRACEF(ErrorEntry, sensors_chn, "Error flags on I2C bus: %u",
                       error_flags);
        return false;
    }
}

#define FIX_BNO_FLAG (1U << 19)

void sensor_fix_task_entry(void *arg) {

    while (true) {
        uint32_t flags =
            osThreadFlagsWait(FIX_BNO_FLAG, osFlagsWaitAny, osWaitForever);

        if (flags & FIX_BNO_FLAG) {

            osDelay(1000 / portTICK_PERIOD_MS);

            if (!bno055_setup(&bno055_1)) {
                LOG_AND_TRACE(ErrorEntry, sensors_chn,
                              "Failed to recover MNA1!");
                osDelay(1000 / portTICK_PERIOD_MS);
            } else {
                global_system_status.mna1_status = SetupCompleteSensorStatus;
            }

            if (!bno055_setup(&bno055_2)) {
                LOG_AND_TRACE(ErrorEntry, sensors_chn,
                              "Failed to recover MNA2!");
                osDelay(1000 / portTICK_PERIOD_MS);
            } else {
                global_system_status.mna2_status = SetupCompleteSensorStatus;
            }

            if (global_system_status.mna1_status == SetupCompleteSensorStatus) {
                bno055_push_calibration(&bno055_1);
            } else {
                global_system_status.mna1_status = PermanentErrorSensorStatus;
            }

            if (global_system_status.mna2_status == SetupCompleteSensorStatus) {
                bno055_push_calibration(&bno055_2);
            } else {
                global_system_status.mna2_status = PermanentErrorSensorStatus;
            }

            if (global_system_status.mna1_status !=
                PermanentErrorSensorStatus) {
                global_system_status.mna1_status = ReadySensorStatus;
            }
            if (global_system_status.mna2_status !=
                PermanentErrorSensorStatus) {
                global_system_status.mna2_status = ReadySensorStatus;
            }

            bno055_are_in_repair = false;
        }
    }
}

void sensors_fix_bno(void) {
    osThreadFlagsSet(sensor_fix_taskHandle, FIX_BNO_FLAG);
}
