/**
 *******************************************************************************
 * @file           control-loop.c
 * @brief          Implementation file for the state estimation task.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <math.h>
#include <stdbool.h>
#include <string.h>

#include "cmsis_os.h"

#include "controllers/attitude-controller.h"
#include "controllers/bodyrate-controller.h"
#include "controllers/guidance.h"
#include "controllers/input-handling.h"
#include "controllers/motor-controller.h"
#include "controllers/state-estimation.h"
#include "controllers/system-identification-input.h"
#include "controllers/wind-estimation.h"

#if USE_SENSOR_DATA_HAL
    #include "hal/nine-dof-model.h"
#endif

#include "config.h"
#include "data/data.h"
#include "drivers/lora_airborne.h"
#include "main.h"
#include "tasks/control-loop.h"
#include "tasks/finite_state_machine.h"
#include "tasks/sd-log.h"
#include "tasks/sensor-task.h"
#include "util/watchdog.h"

/* Macros ------------------------------------------------------------------- */

/** Flag used to indicate that the full state estimation should run */
#define RUN_FULL_CONTROL_LOOP (1 << 2)
/** Flag used to stop the full state estimation and clear the
 * RUN_FULL_CONTROL_LOOP flag. */
#define STOP_FULL_CONTROL_LOOP (1 << 3)

/** Flag used to indicate that the height trigger should be enabled. */
#define HEIGHT_TRIGGER_ENABLED (1 << 4)
/** Flag used to stop the height trigger and clear all other flags. */
#define HEIGHT_TRIGGER_STOP (1 << 5)

/* Private variables -------------------------------------------------------- */

/**
 * @brief The local variable that receives all the newest sensor data. The
 * newest_sensor_data pointer is exported to other parts of the program to
 * access.
 */
struct CompleteSensorData input_sensor_data = {0};

/**
 * @brief The local variable that the newest sensor data is copied into. This
 * variable is always safe to access within this task – the data from
 * input_sensor_data is copied over.
 */
struct CompleteSensorData sensor_data = {0};

/**
 * @brief The local variable that holds the system's state. The estimated_state
 * pointer is exported to other parts of the program to access.
 */
struct State state = {0};

/* Imported globals --------------------------------------------------------- */

/**
 * @brief A handle to the timer firing when a new iteration of the state
 * estimation is due.
 */
extern osTimerId_t state_estimation_timerHandle;

/* Public variables --------------------------------------------------------- */

struct CompleteSensorData *newest_sensor_data = &input_sensor_data;

struct State *estimated_state = &state;

/* Tracing support ---------------------------------------------------------- */

#if configUSE_TRACE_FACILITY
traceString st_est_chn = NULL;
#endif

/* Private functions -------------------------------------------------------- */

/**
 * @brief Wait for a sensor sample to arrive and copy it.
 *
 * This function blocks until the timer fires, then copies the available data
 * and resets the updated sensor data.
 * @param dest The destination memory location.
 * @returns true if an error occurred. The sensor_data is only reliable if this
 *  returns true.
 */
bool sensor_data_receive(struct CompleteSensorData *dest);

/**
 * @brief Preprocess the sensor data (averaging, sensor selection, ...) for SE
 * @param data A pointer to the collected sensor data.
 * @param st_est_input Output location of the preprocessed data.
 */
void sensor_data_preprocess(struct CompleteSensorData *data,
                            struct StateEstimationInput *st_est_input);

/* Public functions --------------------------------------------------------- */

void control_loop_entry(void *arg) {

    // Set up the tracing channel for the state estimation task
#if configUSE_TRACE_FACILITY
    st_est_chn = xTraceRegisterString("State Estimation Events");
    vTracePrint(st_est_chn, "Entered state estimation task.");
#endif

    // local variables holding current state and state estimation input
    struct StateEstimationInput st_est_input = {0};
    st_est_input.gps_position.north = 0.0f;
    st_est_input.gps_position.east = 0.0f;
    st_est_input.gps_position.down = -2500.0f;

    struct BodyRateControllerOutput bdrt_ctrl_output = {0};
    struct AttitudeControllerOutput att_ctrl_output = {0};
    struct ReferenceAttitude guidance_output = {0};
    struct Wind wind_est = {0};
    struct InputHandlingOutput clean_input = {0};

    // Initialize NED System
    initNedSystem();

    // Initialize State Estimation
    state_estimation_init();

#if !USE_SYS_ID_INPUT
    // Initialize Controllers
    guidance_init();
    attitude_controller_init();
    bodyrate_controller_init();
    input_handling_init();

    wind_estimation_init();
#endif

#if USE_SYS_ID_INPUT
    system_identification_input_init();
#endif

#if USE_SENSOR_DATA_HAL
    // If we're using the sensor data HAL, we get it from the linearized 9-DoF
    // model instead of the actual sensors. We have to initialize the model as
    // well here.
    nine_dof_model_init();
#endif

    // wait until sensors are calibrated and then make it start sampling
    osEventFlagsWait(setup_flagsHandle, SETUP_FLAG_SENSORS_CALIBRATED,
                     osFlagsWaitAll | osFlagsNoClear, osWaitForever);
    sensor_task_start_sampling();

    // wait half the sampling time to start the state estimation, give the
    // sensor sampling task enough time to collect & write the data
    osDelay(configTICK_RATE_HZ / (2 * STATE_ESTIMATION_RATE));

    // Set up the timer releasing this task at the specified rate in config.h
    osTimerStart(state_estimation_timerHandle,
                 configTICK_RATE_HZ / STATE_ESTIMATION_RATE);

    LOG_AND_TRACE(EventEntry, st_est_chn,
                  "Started the state estimation timer.");

    while (true) {

        // wait for deadline to arrive and copy data over into sensor_data
        if (!sensor_data_receive(&sensor_data)) {
            LOG_AND_TRACE(
                ErrorEntry, st_est_chn,
                "Failed to retrieve sensor data. Skip this iteration...");
            continue; // try again next deadline
        }

        sensor_data_preprocess(&sensor_data, &st_est_input);

        uint32_t current_flags = osThreadFlagsGet();

        // test whether flags need to be cleared
        if (current_flags & HEIGHT_TRIGGER_STOP) {
            LOG_AND_TRACE(EventEntry, st_est_chn, "Clear height trigger flag");
            osThreadFlagsClear(HEIGHT_TRIGGER_ENABLED | HEIGHT_TRIGGER_STOP);
        }

        if (current_flags & STOP_FULL_CONTROL_LOOP) {
            LOG_AND_TRACE(EventEntry, st_est_chn,
                          "Clear full control loop flag");

            // Clear all flags & stop the full state estimation from running
            osThreadFlagsClear(STOP_FULL_CONTROL_LOOP | RUN_FULL_CONTROL_LOOP);
            current_flags = 0;
        }

        // run height trigger, if flag is still set
        if (current_flags & HEIGHT_TRIGGER_ENABLED) {
            if (st_est_input.altitude_updated &&
                st_est_input.altitude_from_baro <=
                    DEPLOYMENT_HEIGHT_METERS_AGL) {
                // deployment height reached
                // signal deployment to state machine
                osEventFlagsSet(fsm_transitionHandle,
                                (uint8_t)TriggerDeployment);
            }
        }

        watchdog_refresh();

        // Log Euler angles and other state estimation input
        sd_printf(SensorDataEntry, "STINP_ICSB,%f,%f,%f,%f,%f,%f,%f,%f,%f",
                  (double)st_est_input.lin_accel_icsb.x,
                  (double)st_est_input.lin_accel_icsb.y,
                  (double)st_est_input.lin_accel_icsb.z,
                  (double)st_est_input.ang_vel_icsb.p,
                  (double)st_est_input.ang_vel_icsb.q,
                  (double)st_est_input.ang_vel_icsb.r,
                  (double)st_est_input.heading_icsb.roll,
                  (double)st_est_input.heading_icsb.pitch,
                  (double)st_est_input.heading_icsb.yaw);

        sd_printf(SensorDataEntry,
                  "STINP_MAIN,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f",
                  (double)st_est_input.lin_accel_main.x,
                  (double)st_est_input.lin_accel_main.y,
                  (double)st_est_input.lin_accel_main.z,
                  (double)st_est_input.ang_vel_main.p,
                  (double)st_est_input.ang_vel_main.q,
                  (double)st_est_input.ang_vel_main.r,
                  (double)st_est_input.heading_main.roll,
                  (double)st_est_input.heading_main.pitch,
                  (double)st_est_input.heading_main.yaw,
                  (double)st_est_input.altitude_from_baro,
                  (double)st_est_input.gps_position.north,
                  (double)st_est_input.gps_position.east,
                  (double)st_est_input.gps_position.down);

        // run control loop, if flag is still set
        if (current_flags & RUN_FULL_CONTROL_LOOP) {
            // run full state estimation in here
            if (!state_estimation_run(&st_est_input, &state, &clean_input,
                                      &guidance_output)) {
                LOG_AND_TRACE(ErrorEntry, st_est_chn,
                              "State Estimation returned an error.");
                // TODO: What to do in this case?
            }

            // LOG current state
            sd_printf(SensorDataEntry,
                      "STATE,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,"
                      "%.4f,%.4f",
                      (double)state.position.north, (double)state.position.east,
                      (double)state.position.down, (double)state.velocity.u,
                      (double)state.velocity.v, (double)state.velocity.w,
                      (double)state.angular_velocity.p,
                      (double)state.angular_velocity.q,
                      (double)state.angular_velocity.r,
                      (double)state.attitude.roll, (double)state.attitude.pitch,
                      (double)state.attitude.yaw);

#if !USE_SYS_ID_INPUT
            // run all parts of control system in the right order
            //
            // Estimate wind speed
            wind_estimation_run(&state, &wind_est);

            // Run guidance algorithm
            guidance_run(&state, &wind_est, &guidance_output);

            // Check if controllers are enabled
            if (guidance_output.controller_enabled) {
                // run attitude controller (outer loop)
                attitude_controller_run(&state, &guidance_output,
                                        &bdrt_ctrl_output, &att_ctrl_output);

                // run body rate controller (inner loop)
                bodyrate_controller_run(&state, &att_ctrl_output,
                                        &bdrt_ctrl_output);
            }
#endif

#if USE_SYS_ID_INPUT
            // Get Sys ID input
            system_identification_input_run(&guidance_output);

            // Check if controllers are enabled
            if (guidance_output.controller_enabled) {
                // run attitude controller (outer loop)
                attitude_controller_run(&state, &guidance_output,
                                        &bdrt_ctrl_output, &att_ctrl_output);

                // run body rate controller (inner loop)
                bodyrate_controller_run(&state, &att_ctrl_output,
                                        &bdrt_ctrl_output);
            }
#endif

            // Run filters over steering inputs
            input_handling_run(&bdrt_ctrl_output, &clean_input);

            // LOG system inputs
            sd_printf(SensorDataEntry, "INPUT,%.4f,%.4f,%.4f,%.4f",
                      (double)clean_input.delta_a_clean,
                      (double)clean_input.delta_f_clean,
                      (double)clean_input.Delta_L, (double)clean_input.Delta_R);

#if USE_SENSOR_DATA_HAL
            // run model. this stores the output in a location where the sensor
            // HAL fetches it from
            nine_dof_model_run(&clean_input);
#endif

            // hand over the steering inputs to the motor controller
            motor_controller_run(&clean_input);
        }

        osMutexAcquire(access_state_mutexHandle, osWaitForever);
        // copy calculated results into the state struct
        osMutexRelease(access_state_mutexHandle);

        // post to queue or semaphore that state has been calculated
    }
}

void st_est_timer_fired(void *argument) {
    // note that this isn't an ISR, but we still shouldn't block the timer
    // task

    int32_t arg = (int32_t)argument;

    switch (arg) {
        case STATE_EST_TIMER_FIRED:
            osThreadFlagsSet(control_loopHandle, STATE_EST_TIMER_FIRED);
            break;
        default:
#if configUSE_TRACE_FACILITY
            vTracePrint(st_est_chn, "Unknown timer fired!");
#endif
            break;
    }
}

void state_estimation_set_running(bool running) {
    LOG_AND_TRACEF(EventEntry, st_est_chn, "Enable full state estimation: %d",
                   running);
    // keep in mind that this function runs from another task's context
    if (running) {
        osThreadFlagsSet(control_loopHandle, RUN_FULL_CONTROL_LOOP);
    } else {
        osThreadFlagsSet(control_loopHandle, STOP_FULL_CONTROL_LOOP);
    }
}

bool height_trigger_arm(void) {
    LOG_AND_TRACE(EventEntry, st_est_chn, "Enabling height trigger...");
    // check that highest bit is not set
    return (osThreadFlagsSet(control_loopHandle, HEIGHT_TRIGGER_ENABLED) &
            (1U << 31)) == 0;
}

bool height_trigger_disarm(void) {
    LOG_AND_TRACE(EventEntry, st_est_chn, "Disabling height trigger...");
    return (osThreadFlagsSet(control_loopHandle, HEIGHT_TRIGGER_STOP) &
            (1U << 31)) == 0;
}

/* Private functions -------------------------------------------------------- */

bool sensor_data_receive(struct CompleteSensorData *dest) {
    assert_param(dest != NULL);
    // wait for deadline to arrive
    osThreadFlagsWait(STATE_EST_TIMER_FIRED, osFlagsWaitAll, osWaitForever);

    // acquire mutex to ensure nobody's writing during data copying
    osMutexAcquire(write_sensor_data_mutexHandle, osWaitForever);

    memcpy(dest, newest_sensor_data, sizeof(struct CompleteSensorData));
    newest_sensor_data->changed_readings = 0x00; // reset all flags
    // all things read, can hand off control now
    osMutexRelease(write_sensor_data_mutexHandle);

    // Log which measurements have been taken
    LOG_AND_TRACEF(SensorDataEntry, st_est_chn, "CHGDR,%u",
                   dest->changed_readings);

    // sensor_data now contains the newest data, so successful
    return true;
}

void sensor_data_preprocess(struct CompleteSensorData *data,
                            struct StateEstimationInput *st_est_input) {

    static union Quaternion main_assembly_correction = {
        .x = 0.0f,
        .y = 0.0f,
        .z = sinf(HEADING_CORRECTION_ASSEMBLY * 0.5f),
        .w = cosf(HEADING_CORRECTION_ASSEMBLY * 0.5f),
    };

    assert_param(data != NULL);
    assert_param(st_est_input != NULL);

    // Calculate NED from geodetic coordinates if GPS was updated
    union GeodeticPosition gps_position = {0};
    bool GPS_updated = false;

    if (data->changed_readings & (MAIN_GPS_READOUT_1_CHANGED) &&
        (data->changed_readings & MAIN_GPS_READOUT_2_CHANGED)) {
        gps_position.latitude = (data->geodetic_pos_1.position.latitude +
                                 data->geodetic_pos_2.position.latitude) /
                                2.0;
        gps_position.longitude = (data->geodetic_pos_1.position.longitude +
                                  data->geodetic_pos_2.position.longitude) /
                                 2.0;
        gps_position.elevation = (data->geodetic_pos_1.position.elevation +
                                  data->geodetic_pos_2.position.elevation) /
                                 2.0;
        GPS_updated = true;
    } else if (data->changed_readings & MAIN_GPS_READOUT_1_CHANGED) {
        gps_position.latitude = data->geodetic_pos_1.position.latitude;
        gps_position.longitude = data->geodetic_pos_1.position.longitude;
        gps_position.elevation = data->geodetic_pos_1.position.elevation;
        GPS_updated = true;
    } else if (data->changed_readings & MAIN_GPS_READOUT_2_CHANGED) {
        gps_position.latitude = data->geodetic_pos_2.position.latitude;
        gps_position.longitude = data->geodetic_pos_2.position.longitude;
        gps_position.elevation = data->geodetic_pos_2.position.elevation;
        GPS_updated = true;
    }

    if (GPS_updated) {
        union Position ned_position = {0};
        geo2ned(&gps_position, &ned_position);

        st_est_input->gps_position.north = ned_position.north;
        st_est_input->gps_position.east = ned_position.east;
        st_est_input->gps_position.down = ned_position.down;
    }

    st_est_input->gps_position_updated = GPS_updated;

    // Convert average pressure from barometers to altitude AGL
    RawPressure average_pressure = 0;
    if ((data->changed_readings & MAIN_PRS_READOUT_1_CHANGED) &&
        (data->changed_readings & MAIN_PRS_READOUT_2_CHANGED)) {
        // both measurements available
        average_pressure = (data->main_tube_pressure_1.pressure +
                            data->main_tube_pressure_2.pressure) /
                           2;
    } else if (data->changed_readings & MAIN_PRS_READOUT_1_CHANGED) {
        average_pressure = data->main_tube_pressure_1.pressure;
    } else if (data->changed_readings & MAIN_PRS_READOUT_2_CHANGED) {
        average_pressure = data->main_tube_pressure_2.pressure;
    }

    if (average_pressure != 0) {
        st_est_input->altitude_from_baro =
            convert_raw_pressure_to_altitude(average_pressure);
        st_est_input->altitude_updated = true;
    } else {
        st_est_input->altitude_updated = false;
    }

    // Average the IMU data from the main board
    union Heading main_heading = {{0, 0, 0}};
    union LinearAcceleration main_lin_acc = {{0, 0, 0}};
    union AngularVelocity main_ang_vel = {{0, 0, 0}};

    st_est_input->main_imu_updated = false;

    if ((data->changed_readings & MAIN_ATT_READOUT_1_CHANGED) &&
        (data->changed_readings & MAIN_ATT_READOUT_2_CHANGED)) {
        // both got updated, take average
        union Heading main_heading_1 = {{0, 0, 0}};
        union Heading main_heading_2 = {{0, 0, 0}};
        convert_raw_quaternion_to_heading(&data->main_att_readout_1.qtr_att,
                                          &main_heading_1,
                                          &main_assembly_correction);
        convert_raw_quaternion_to_heading(&data->main_att_readout_2.qtr_att,
                                          &main_heading_2,
                                          &main_assembly_correction);

        union LinearAcceleration main_lin_acc_1 = {{0, 0, 0}};
        union LinearAcceleration main_lin_acc_2 = {{0, 0, 0}};
        convert_raw_acceleration(&data->main_att_readout_1.lin_acc,
                                 &main_lin_acc_1, &main_assembly_correction);
        convert_raw_acceleration(&data->main_att_readout_2.lin_acc,
                                 &main_lin_acc_2, &main_assembly_correction);

        union AngularVelocity main_ang_vel_1 = {{0, 0, 0}};
        union AngularVelocity main_ang_vel_2 = {{0, 0, 0}};
        convert_raw_angular_velocity(&data->main_att_readout_1.ang_vel,
                                     &main_ang_vel_1,
                                     &main_assembly_correction);
        convert_raw_angular_velocity(&data->main_att_readout_2.ang_vel,
                                     &main_ang_vel_2,
                                     &main_assembly_correction);

        for (uint_fast8_t i = 0; i < 3; i++) {
            main_heading.array[i] =
                mean_angle(main_heading_1.array[i], main_heading_2.array[i]);
            main_lin_acc.array[i] =
                (main_lin_acc_1.array[i] + main_lin_acc_2.array[i]) / 2.0f;
            main_ang_vel.array[i] =
                (main_ang_vel_1.array[i] + main_ang_vel_2.array[i]) / 2.0f;
        }

        st_est_input->main_imu_updated = true;

    } else if (data->changed_readings & MAIN_ATT_READOUT_1_CHANGED) {
        convert_raw_quaternion_to_heading(&data->main_att_readout_1.qtr_att,
                                          &main_heading,
                                          &main_assembly_correction);
        convert_raw_acceleration(&data->main_att_readout_1.lin_acc,
                                 &main_lin_acc, &main_assembly_correction);
        convert_raw_angular_velocity(&data->main_att_readout_1.ang_vel,
                                     &main_ang_vel, &main_assembly_correction);
        st_est_input->main_imu_updated = true;
    } else if (data->changed_readings & MAIN_ATT_READOUT_2_CHANGED) {
        convert_raw_quaternion_to_heading(&data->main_att_readout_2.qtr_att,
                                          &main_heading,
                                          &main_assembly_correction);
        convert_raw_acceleration(&data->main_att_readout_2.lin_acc,
                                 &main_lin_acc, &main_assembly_correction);
        convert_raw_angular_velocity(&data->main_att_readout_2.ang_vel,
                                     &main_ang_vel, &main_assembly_correction);
        st_est_input->main_imu_updated = true;
    }

    st_est_input->heading_main = main_heading;
    st_est_input->lin_accel_main = main_lin_acc;
    st_est_input->ang_vel_main = main_ang_vel;

    // Average the attitude data from the ICSB
    union Heading icsb_heading = {{0, 0, 0}};
    union LinearAcceleration icsb_lin_acc = {{0, 0, 0}};
    union AngularVelocity icsb_ang_vel = {{0, 0, 0}};

    st_est_input->icsb_imu_updated = false;

    if ((data->changed_readings & ICSB_ATT_READOUT_1_CHANGED) &&
        (data->changed_readings & ICSB_ATT_READOUT_2_CHANGED)) {
        // both got updated, take average
        union Heading icsb_heading_1 = {{0, 0, 0}};
        union Heading icsb_heading_2 = {{0, 0, 0}};
        convert_raw_quaternion_to_heading(&data->icsb_att_readout_1.qtr_att,
                                          &icsb_heading_1, NULL);
        convert_raw_quaternion_to_heading(&data->icsb_att_readout_2.qtr_att,
                                          &icsb_heading_2, NULL);

        union LinearAcceleration icsb_lin_acc_1 = {{0, 0, 0}};
        union LinearAcceleration icsb_lin_acc_2 = {{0, 0, 0}};
        convert_raw_acceleration(&data->icsb_att_readout_1.lin_acc,
                                 &icsb_lin_acc_1, NULL);
        convert_raw_acceleration(&data->icsb_att_readout_2.lin_acc,
                                 &icsb_lin_acc_2, NULL);

        union AngularVelocity icsb_ang_vel_1 = {{0, 0, 0}};
        union AngularVelocity icsb_ang_vel_2 = {{0, 0, 0}};
        convert_raw_angular_velocity(&data->icsb_att_readout_1.ang_vel,
                                     &icsb_ang_vel_1, NULL);
        convert_raw_angular_velocity(&data->icsb_att_readout_2.ang_vel,
                                     &icsb_ang_vel_2, NULL);

        for (uint_fast8_t i = 0; i < 3; i++) {
            icsb_heading.array[i] =
                (icsb_heading_1.array[i] + icsb_heading_2.array[i]) / 2.0f;
            icsb_lin_acc.array[i] =
                (icsb_lin_acc_1.array[i] + icsb_lin_acc_2.array[i]) / 2.0f;
            icsb_ang_vel.array[i] =
                (icsb_ang_vel_1.array[i] + icsb_ang_vel_2.array[i]) / 2.0f;
        }
        st_est_input->icsb_imu_updated = true;

    } else if (data->changed_readings & ICSB_ATT_READOUT_1_CHANGED) {
        convert_raw_quaternion_to_heading(&data->icsb_att_readout_1.qtr_att,
                                          &icsb_heading, NULL);
        convert_raw_acceleration(&data->icsb_att_readout_1.lin_acc,
                                 &icsb_lin_acc, NULL);
        convert_raw_angular_velocity(&data->icsb_att_readout_1.ang_vel,
                                     &icsb_ang_vel, NULL);

        st_est_input->icsb_imu_updated = true;

    } else if (data->changed_readings & ICSB_ATT_READOUT_2_CHANGED) {
        convert_raw_quaternion_to_heading(&data->icsb_att_readout_2.qtr_att,
                                          &icsb_heading, NULL);
        convert_raw_acceleration(&data->icsb_att_readout_2.lin_acc,
                                 &icsb_lin_acc, NULL);
        convert_raw_angular_velocity(&data->icsb_att_readout_2.ang_vel,
                                     &icsb_ang_vel, NULL);
        st_est_input->icsb_imu_updated = true;
    }

    st_est_input->heading_icsb = icsb_heading;
    st_est_input->lin_accel_icsb = icsb_lin_acc;
    st_est_input->ang_vel_icsb = icsb_ang_vel;

    // ICSB pressure readings are not forwarded to the control loop

    // Last task: Update position that the LoRa downlink reports
    lora_update_coordinates(st_est_input->gps_position.north,
                            st_est_input->gps_position.east,
                            st_est_input->altitude_from_baro);
}
