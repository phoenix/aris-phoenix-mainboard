/**
 *******************************************************************************
 * @file           packets.c
 * @brief          Main program body
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include "FreeRTOS.h"
#include "queue.h"
#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tasks/packets.h"

/* Private variables -------------------------------------------------------- */

#if configUSE_TRACE_FACILITY
traceString packets_channel = NULL;
#endif

/* Private functions -------------------------------------------------------- */

/**
 * Process a packet.
 *
 * The processing is done on the packet task.
 * @param packet A pointer to the packet to process.
 * @returns true if the processing was successful.
 */
int packet_process(Packet *packet);

/* RTOS functionality ------------------------------------------------------- */

QueueHandle_t packet_queue;

void packet_task_entry(void *arg) {
#if configUSE_TRACE_FACILITY
    packets_channel = xTraceRegisterString("Packets Events");
#endif
    assert_param(arg != NULL);
    struct PacketTaskConfig *config = (struct PacketTaskConfig *)arg;

    uint_fast8_t num_tx = config->num_transmission_handlers;
    enum Location here = config->here;

#if configUSE_TRACE_FACILITY
    vTracePrintF(packets_channel,
                 "Configuration contains %u transmission handlers.", num_tx);
#endif

    struct PacketTransmissionHandler *tx_hdls =
        malloc(num_tx * sizeof(struct PacketTransmissionHandler));
    memcpy(tx_hdls, config->tx_handlers,
           num_tx * sizeof(struct PacketTransmissionHandler));

    void (*reception_handler)(Packet * packet) = config->reception_handler;

    if (num_tx == 0 || reception_handler == NULL) {
#if configUSE_TRACE_FACILITY
        vTracePrintF(packets_channel,
                     "Number of reception or transmission handlers is zero.");
#endif
    }

    packet_queue = xQueueCreate(30, sizeof(Packet));
    assert_param(packet_queue != NULL);

    // start handling packets

    Packet packet;
    while (1) {
        if (xQueueReceive(packet_queue, &packet, portMAX_DELAY) == pdPASS) {
            UBaseType_t available_spaces = uxQueueSpacesAvailable(packet_queue);
#if configUSE_TRACE_FACILITY
            vTracePrintF(packets_channel, "%u packets available in queue.",
                         available_spaces);
#endif
            if (packet.destination == here) {
                // packet received, look for handler for this sender
                if (reception_handler != NULL) {
                    reception_handler(&packet);
                } else {
#if configUSE_TRACE_FACILITY
                    vTracePrint(packets_channel,
                                "Received packet, but no reception handler "
                                "configured.");
#endif
                }
                continue;
            }

            // else: send packet. loop through transmission handlers to find
            // suitable
            uint_fast8_t found_tx = 0;
            for (uint_fast8_t i = 0; i < num_tx; i++) {
                if (tx_hdls[i].destination == packet.destination) {
#if configUSE_TRACE_FACILITY
                    vTracePrint(packets_channel,
                                "Calling transmission handler.");
#endif
                    if (tx_hdls[i].transmitter(&packet) != 0) {
#if configUSE_TRACE_FACILITY
                        vTracePrint(packets_channel,
                                    "Transmission handler returned an error.");
#endif
                    }
#if configUSE_TRACE_FACILITY
                    vTracePrintF(packets_channel,
                                 "Transmission handler returned.");
#endif

                    found_tx = 1;
                    break;
                }
            }

            if (found_tx == 0) {
#if configUSE_TRACE_FACILITY
                vTracePrint(
                    packets_channel,
                    "No suitable transmission handler found. Drop packet...");
#endif
                free(packet.payload);
            }

        } else {
#if configUSE_TRACE_FACILITY
            vTracePrint(packets_channel, "Error retrieving item from queue.");
#endif
        }
    }
}

int packet_add_to_queue(Packet *packet) {
    assert_param(packet_queue != NULL);
    assert_param(packet != NULL);

    BaseType_t ret;
    if (packet->priority == PriorityHigh) {
#if configUSE_TRACE_FACILITY
        vTracePrint(packets_channel,
                    "Packet has priority. Inserted at front of queue.");
#endif
        ret = xQueueSendToFront(packet_queue, packet, 100);
    } else {
        ret = xQueueSendToBack(packet_queue, packet, 100);
    }

    if (ret == errQUEUE_FULL) {
#if configUSE_TRACE_FACILITY
        vTracePrint(packets_channel,
                    "Couldn't add packet to queue because it is full!");
#endif
        return -1;
    }

    return 0;
}
