/**
 *******************************************************************************
 * @file           sd-log.c
 * @brief          Implements the SD logging task and its setup functions.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bsp_driver_sd.h"
#include "cmsis_os.h"
#include "fatfs.h"
#include "message_buffer.h"
#include "stm32f4xx_hal.h"

#include "config.h"
#include "drivers/sd-driver.h"
#include "main.h"
#include "system-status.h"
#include "tasks/sd-log.h"

/* Debugging and tracing ---------------------------------------------------- */

#if configUSE_TRACE_FACILITY
traceString sd_channel = NULL;
#endif

/* Local variables ---------------------------------------------------------- */

/**
 * The current status of the SD card. If it is not initialized, all writes to
 * it will be discarded, thus ensuring that the application will not block on
 * the logging queue.
 */
static volatile bool sd_initialized = false;

/** Buffer used for all log entries. */
MessageBufferHandle_t log_buffer = NULL;

/* Public function implementation --------------------------------------------*/

void sd_log_task_entry(void *argument) {

#if configUSE_TRACE_FACILITY
    sd_channel = xTraceRegisterString("SD Card Events");
    vTracePrint(sd_channel, "Entered SD Log task.");
    vTraceSetMessageBufferName(log_buffer, "SD Log Buffer");
#endif /* configUSE_TRACE_FACILITY */

    /* SD Card Setup -------------------------------------------------------- */

    // Set up SD card as early as possible so that all tasks can start logging
    // as soon as they get activated. For creation of the log buffer, see main.c
    // where the memory gets allocated for it
    bool sd_mounted = sd_card_mount();

#if configUSE_TRACE_FACILITY
    if (sd_mounted) {
        vTracePrint(sd_channel, "SD successfully mounted.");
    } else {
        vTracePrint(sd_channel, "SD could not be mounted.");
    }
#endif

    if (sd_mounted) {
        global_system_status.sd_status = MountedSDStatus;

        // SD is only ready when the setup could successfully be completed
        if (sd_setup_logging())
            global_system_status.sd_status = ReadySDStatus;
    } else {
        // set the SD status to "not present"
        // this will immediately be visible in the LoRa status
        global_system_status.sd_status = NotPresentSDStatus;
    }

    // Unlock the ability to print log messages
    sd_start_log();
    system_print_startup_report();

    // Three buffers used for the three different files that we write to:
    // EVENTS.LOG, ERROR.LOG and DATA.LOG
    char events_buf[SD_CARD_WRITE_CHUNK];
    char errors_buf[SD_CARD_WRITE_CHUNK];
    char data_buf[SD_CARD_WRITE_CHUNK];

    // These store the current index where new data is copied into the buffer.
    uint16_t events_buf_idx = 0;
    uint16_t errors_buf_idx = 0;
    uint16_t data_buf_idx = 0;

    // Indicate to the rest of the code that the SD card is ready
    osEventFlagsSet(setup_flagsHandle, SETUP_FLAG_SD_READY);

    // Can store an entire write chunk plus the "entry type" byte
    char write_buf[SD_CARD_WRITE_CHUNK + 2];

    uint8_t writes_since_sync = 0;
    while (true) {
        // empty the writing buffer, ensures that however many bytes are copied
        // in here, it is zero-terminated
        memset(write_buf, 0, SD_CARD_WRITE_CHUNK + 2);

        // block here to wait for logging item to be processed
        size_t bytes = xMessageBufferReceive(
            log_buffer, write_buf, SD_CARD_WRITE_CHUNK, osWaitForever);

#if configUSE_TRACE_FACILITY
        vTracePrintF(sd_channel, "Received %u bytes from log buffer.", bytes);
#endif /* configUSE_TRACE_FACILITY */

        if (bytes > 1) {
            // get which log buffer we should write to from first byte

            enum SDLogEntryType type = write_buf[0];
            size_t msg_len = bytes - 1; // subtract that first byte we read

            // check that there's something to write and that we can fit it in a
            // write chunk
            if (msg_len > 0 && msg_len < 512 - 1) {
                switch (type) {
                    case EventEntry:
                        if (events_buf_idx + msg_len < 512 - 1) {
                            // enough space in the buffer to put everything in
                            strncpy(events_buf + events_buf_idx, write_buf + 1,
                                    msg_len);
                            events_buf_idx += msg_len;
                        } else {
                            // null-terminate string
                            events_buf[events_buf_idx++] = '\0';
                            // write it out
                            if (sd_write(events_buf, EventFile)) {
                                global_system_status.sd_status = ReadySDStatus;
                            } else {
                                global_system_status.sd_status = NotPresentSDStatus;
                            }
                            writes_since_sync++;
                            // reset the buffer
                            strncpy(events_buf, write_buf + 1, msg_len);
                            events_buf_idx = msg_len;
                        }
                        break;
                    case ErrorEntry:
                        if (errors_buf_idx + msg_len < 512 - 1) {
                            // enough space in the buffer to put everything in
                            strncpy(errors_buf + errors_buf_idx, write_buf + 1,
                                    msg_len);
                            errors_buf_idx += msg_len;
                        } else {
                            // null-terminate string
                            errors_buf[errors_buf_idx++] = '\0';
                            // write it out
                            if (sd_write(errors_buf, ErrorFile)) {
                                global_system_status.sd_status = ReadySDStatus;
                            } else {
                                global_system_status.sd_status = NotPresentSDStatus;
                            }
                            writes_since_sync++;
                            // reset the buffer
                            strncpy(errors_buf, write_buf + 1, msg_len);
                            errors_buf_idx = msg_len;
                        }
                        break;
                    case SensorDataEntry:
                        if (data_buf_idx + msg_len < 512 - 1) {
                            // enough space in the buffer to put everything in
                            strncpy(data_buf + data_buf_idx, write_buf + 1,
                                    msg_len);
                            data_buf_idx += msg_len;
                        } else {
                            // null-terminate string
                            data_buf[data_buf_idx++] = '\0';
                            // write it out
                            if (sd_write(data_buf, SensorDataFile)) {
                                global_system_status.sd_status = ReadySDStatus;
                            } else {
                                global_system_status.sd_status = NotPresentSDStatus;
                            }
                            writes_since_sync++;
                            // reset the buffer
                            strncpy(data_buf, write_buf + 1, msg_len);
                            data_buf_idx = msg_len;
                        }
                        break;
                    case EndLogEntry:
                        events_buf[events_buf_idx] = '\0';
                        errors_buf[errors_buf_idx] = '\0';
                        data_buf[data_buf_idx] = '\0';
                        sd_write(events_buf, EventFile);
                        sd_write(errors_buf, ErrorFile);
                        sd_write(data_buf, SensorDataFile);
                        events_buf_idx = 0;
                        errors_buf_idx = 0;
                        data_buf_idx = 0;
                        break;
                    default:
#if configUSE_TRACE_FACILITY
                        vTracePrintF(sd_channel, "Unknown entry type %u", type);
#endif
                        break;
                }

                if (writes_since_sync >= SD_CARD_SYNC_THRESHOLD) {
                    sd_sync();
                }
            }
        }
    }
}

/* Private function implementation -------------------------------------------*/

bool sd_log(const char *buf, enum SDLogEntryType type) {

    // Check, in a safe way, whether the logging string is <= 255 bytes and that
    // it's null-terminated. By the way, nice function that is not C standard.
    size_t len = strnlen(buf, 256);
    if (len >= 256) {
        // I know that this looks funny, but it does not lead to recursion. This
        // string has <255 bytes :-)
        LOG_AND_TRACE(ErrorEntry, sd_channel,
                      "Logging string exceeded 255 bytes or not terminated!");
        return false;
    }

    if (sd_initialized) {
        // 255 characters, 1 comma, 10 tick count places, \r\n and \0 is 269,
        // plus one byte for the SDLogEntryType makes 270 characters. Add an
        // extra one to be sure and check that we don't overflow 270.
        char log_msg[271];
        log_msg[0] = (uint8_t)type; // 1 byte: entry type
        snprintf(log_msg + 1, 11, "%10lu", osKernelGetTickCount()); // 10B: tick
        log_msg[11] = ',';                                          // comma
        strncpy(log_msg + 12, buf, len);        // buffer, <= 255B
        strncpy(log_msg + 12 + len, "\r\n", 3); // CRLF with terminator
        size_t actual_len = strnlen(log_msg, 271);

        if (actual_len >= 271) {
            LOG_AND_TRACE(ErrorEntry, sd_channel,
                          "Log message printing out of bounds.");
            return false;
        }

        // It is unsafe to write to logging queue from multiple buffers, so
        // perform a non-blocking write in a critical section.
        taskENTER_CRITICAL();
        if (xMessageBufferSend(log_buffer, log_msg, actual_len, 0) !=
            actual_len) {
            taskEXIT_CRITICAL();
#if configUSE_TRACE_FACILITY
            vTracePrint(sd_channel, "Failed to send to log message buffer.");
#endif
            return false;
        }
        taskEXIT_CRITICAL();
#if configUSE_TRACE_FACILITY
        vTracePrintF(sd_channel, "Successfully added %u bytes to log buffer.",
                     len + 13);
#endif
        return true;
    } else {
#if configUSE_TRACE_FACILITY
        vTracePrint(sd_channel, "Did not add to SD log buffer since SD"
                                " is not initialized.");
#endif
        return false;
    }
}

bool sd_printf(enum SDLogEntryType type, const char *fmt, ...) {
    va_list argp;
    va_start(argp, fmt);

    char buf[256];
    uint8_t bytes_written = vsnprintf(buf, 256, fmt, argp);
    if (bytes_written < 0) {
        LOG_AND_TRACE(ErrorEntry, sd_channel, "vsnprintf failed in sd_printf");
        return false;
    }

#if configUSE_TRACE_FACILITY
    vTracePrintF(sd_channel, "vsnprintf wrote %u bytes.", bytes_written);
#endif
    return sd_log(buf, type);
}

void sd_log_task_pause_logging(void) {
#if configUSE_TRACE_FACILITY
    vTracePrintF(sd_channel, "Received notice to pause SD logging.");
#endif /* configUSE_TRACE_FACILITY */

    sd_initialized = false; // prevent further writes

    osDelay(1000 / portTICK_PERIOD_MS); // wait 1s to write data to SD

    // unmount disk
    if (!sd_card_unmount()) {
#if configUSE_TRACE_FACILITY
        vTracePrint(sd_channel, "SD unmount returned error");
#endif /* configUSE_TRACE_FACILITY */
    } else {
#if configUSE_TRACE_FACILITY
        vTracePrint(sd_channel, "Successfully unmounted SD card.");
#endif /* configUSE_TRACE_FACILITY */
    }
}

void sd_start_log(void) {
    sd_initialized = true;
    sd_log("------ Start of Log ------", EventEntry);
}

void sd_stop_log(void) {
    sd_log("------ End of Log ------", EventEntry);
    sd_log("\r\n\r\n\r\n", EndLogEntry);
    sd_log_task_pause_logging();
}
