/**
 *******************************************************************************
 * @file           downlink-task.c
 * @brief          Implementation that regularly updates the LoRa status
 *                      report of certain system functions (e.g. GPS lock)
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdbool.h>

#include "cmsis_os.h"

#include "config.h"
#include "drivers/lora_airborne.h"
#include "main.h"
#include "system-status.h"
#include "tasks/downlink-task.h"
#include "tasks/finite_state_machine.h"
#include "tasks/sd-log.h"

/* Tracing support ---------------------------------------------------------- */

#if configUSE_TRACE_FACILITY
traceString downlink_chn = NULL;
#endif

/* Public function implementation ------------------------------------------- */

void downlink_task_entry(void *arg) {

#if configUSE_TRACE_FACILITY
    downlink_chn = xTraceRegisterString("Downlink Events");
#endif

    uint32_t next_tick = osKernelGetTickCount();

    // wait for the SD to become ready
    osEventFlagsWait(setup_flagsHandle, SETUP_FLAG_SD_READY,
                     osFlagsWaitAll | osFlagsNoClear, osWaitForever);

    while (true) {

#if configUSE_TRACE_FACILITY
        vTracePrint(downlink_chn, "Updating current system state.");
#endif
        sd_log("Updating current system state.", EventEntry);

        StatusUpdate_t status = system_get_lora_compatible_status();

        // Send the system status out
        lora_update_system_status(status);

        next_tick += configTICK_RATE_HZ / DOWNLINK_UPDATE_FREQ;
        osDelayUntil(next_tick);
    }
}
