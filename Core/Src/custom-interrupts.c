/**
 *******************************************************************************
 * @file           custom-interrupts.c
 * @brief          Custom interrupt handlers for multiple peripherals
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include "config.h"
#include "drivers/epos4.h"
#include "drivers/lora_module.h"
#include "main.h"
#include "tasks/gps-task.h"
#include "tasks/spi-transmission.h"

#if USE_SD_CARD_HAL
extern void sd_uart_transfer_complete(void);
#endif

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {

    // the UART interrupt comes from a GPS receive process via DMA, hand off to
    // the respective handler in the NEO7M driver
    if (huart == &huart_MNG1 || huart == &huart_MNG2) {
#if !USE_SENSOR_DATA_HAL
        GPS_RxCpltCallback(huart);
#endif
    }

#if !USE_MOTOR_HAL
    // Call the motor controller
    if (huart == &huart_ACT1 || huart == &huart_ACT2) {
        EPOS_UART_RxCplt_Callback();
    }
#endif
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart) {

    // Callback for the "SD" card (i.e., the USART2 DMA)
#if USE_SD_CARD_HAL
    if (huart == &huart2) {
        sd_uart_transfer_complete();
    }
#endif

#if !USE_MOTOR_HAL
    // Call the motor controller
    if (huart == &huart_ACT1 || huart == &huart_ACT2) {
        EPOS_UART_TxCplt_Callback();
    }
#endif
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart) {

#if !USE_MOTOR_HAL
    if (huart == &huart_ACT1 || huart == &huart_ACT2) {
        // unlock both of them
        EPOS_UART_RxCplt_Callback();
        EPOS_UART_TxCplt_Callback();
    }
#endif
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
    if (GPIO_Pin == ESP_INTR_Pin) {
        osThreadFlagsSet(spi_taskHandle, ESP_INTERRUPT_RECV);
    } else if (GPIO_Pin == B1_Pin) {
        osThreadFlagsSet(lora_taskHandle, LORA_INTR_RECEIVED);
    } else if (GPIO_Pin == DIO1_Pin) {
        lora_irq_callback();
    }
}