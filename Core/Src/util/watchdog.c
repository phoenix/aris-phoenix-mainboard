/**
 *******************************************************************************
 * @file           watchdog.c
 * @brief          Interface for managing the safety watchdog.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdbool.h>
#include <stdint.h>

#include "cmsis_os.h"

#include "main.h"
#include "system-status.h"
#include "util/backup-registers.h"
#include "util/watchdog.h"

/* Local macros ------------------------------------------------------------- */

// Two entirely random numbers

/** Indicates the deployment should trigger on reset */
#define WATCHDOG_TRIGGER_ENABLE 4209273102
/** Indicates the deployment shouldn't trigger on reset */
#define WATCHDOG_TRIGGER_NOOOOO 3575159735

/* Public function implementation ------------------------------------------- */

void watchdog_enable_on_timer(uint32_t ms) {
    osTimerStart(watchdog_pretimerHandle, ms / portTICK_PERIOD_MS);
}

/**
 * @brief Disable the watchdog (if the timer has not elapsed) or stop deployment
 *
 * This function will try to stop the timer that was started with @see
 * watchdog_enable_on_timer. If the timer has already elapsed, the IWDG can't be
 * stopped, but the RTC backup register will be cleared such that on potential
 * reset, the deployment will NOT happen.
 */
void watchdog_disable(void) {
    if (osTimerIsRunning(watchdog_pretimerHandle)) {
        osTimerStop(watchdog_pretimerHandle);
    }
    rtc_backup_register_write(BACKUP_REGISTER_WATCHDOG_STATE,
                              WATCHDOG_TRIGGER_NOOOOO);
}

bool watchdog_should_trigger_deployment(void) {

    // Check if the system has been reset by a watchdog
    if (system_get_reset_reason() == WatchdogResetReason) {
        // If in here, a watchdog reset the system.
        uint32_t watchdog_reg_value;
        if (rtc_backup_register_read(BACKUP_REGISTER_WATCHDOG_STATE,
                                     &watchdog_reg_value)) {
            // successfully read register
            return WATCHDOG_TRIGGER_ENABLE == watchdog_reg_value;
        } else {
            // something went wrong, opt to return false in this case
            return false;
        }
    }

    // System not reset by watchdog, clear the "watchdog should be enabled
    // register"
    rtc_backup_register_write(BACKUP_REGISTER_WATCHDOG_STATE,
                              WATCHDOG_TRIGGER_NOOOOO);

    return false;
}

void watchdog_refresh(void) {
    HAL_IWDG_Refresh(&hiwdg);
}

/* Timer callback ----------------------------------------------------------- */

void watchdog_pretimer_fired(void *arg) {
    rtc_backup_register_write(BACKUP_REGISTER_WATCHDOG_STATE,
                              WATCHDOG_TRIGGER_ENABLE);
    watchdog_init();
}
