/**
 *******************************************************************************
 * @file           serialization.c
 * @brief          Helper for serialization of data payloads.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <string.h>

#include "main.h"
#include "util/serialization.h"

void serialize_data_payload(struct DataPayload *pl, uint8_t *buf) {
    // I don't care about endianness
    assert_param(pl != NULL);
    assert_param(buf != NULL);

    if (pl->sensor_type == BNO055_Sensor) {
        buf[0] = (uint8_t)BNO055_Sensor;
        memcpy(buf + 1, &pl->bno_readout.tick, sizeof(uint32_t));
        memcpy(buf + 5, pl->bno_readout.ang_vel.array, 3 * sizeof(uint16_t));
        memcpy(buf + 11, pl->bno_readout.qtr_att.array, 4 * sizeof(uint16_t));
        memcpy(buf + 19, pl->bno_readout.lin_acc.array, 3 * sizeof(uint16_t));
    } else {
        buf[0] = (uint8_t)BMP388_Sensor;
        memcpy(buf + 1, &pl->bmp_readout.pressure, sizeof(RawPressure));
        memcpy(buf + 5, &pl->bmp_readout.temperature, sizeof(RawTemperature));
    }
}

void deserialize_data_payload(uint8_t *buf, struct DataPayload *pl) {
    assert_param(pl != NULL);
    assert_param(buf != NULL);

    if ((enum SensorType)buf[0] == BNO055_Sensor) {
        pl->sensor_type = BNO055_Sensor;
        memcpy(&pl->bno_readout.tick, buf + 1, sizeof(uint32_t));
        memcpy(pl->bno_readout.ang_vel.array, buf + 5, 3 * sizeof(uint16_t));
        memcpy(pl->bno_readout.qtr_att.array, buf + 11, 4 * sizeof(uint16_t));
        memcpy(pl->bno_readout.lin_acc.array, buf + 19, 3 * sizeof(uint16_t));
    } else {
        pl->sensor_type = BMP388_Sensor;
        memcpy(&pl->bmp_readout.pressure, buf + 1, sizeof(RawPressure));
        memcpy(&pl->bmp_readout.temperature, buf + 5, sizeof(RawTemperature));
    }
}