/**
 *******************************************************************************
 * @file           maths.c
 * @brief          Utility functions and definitions for maths
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <math.h>
#include <stdint.h>

#include "util/maths.h"

/*******************************************************************************
 * Quaternions
 ******************************************************************************/

float quaternion_norm(union Quaternion *q) {
    if (!q)
        return -1.0f;

    return sqrtf(sq(q->x) + sq(q->y) + sq(q->z) + sq(q->w));
}

bool normalize_quaternion(union Quaternion *q) {
    if (!q)
        return false;

    float norm = quaternion_norm(q);
    if (norm <= 0.0f)
        return false;

    float norm_factor = 1 / norm;

    // Catch a non-normalizable quaternion before making everything nan
    if (!isnormal(norm_factor) || isnan(norm_factor) || isinff(norm_factor)) {
        return false;
    }

    q->x *= norm_factor;
    q->y *= norm_factor;
    q->z *= norm_factor;
    q->w *= norm_factor;

    return true;
}

bool conjugate_quaternion(union Quaternion *q) {
    if (!q)
        return false;

    // The real part is kept the same, and all imaginary parts are inverted
    q->x = -q->x;
    q->y = -q->y;
    q->z = -q->z;

    return true;
}

union Quaternion hamilton_product(union Quaternion *q1, union Quaternion *q2) {
    union Quaternion ret = {0};

    // if either of the quaternions are NULL, [0 0 0 0] is returned
    if (!q1 || !q2)
        return ret;

    ret.w = q1->w * q2->w - q1->x * q2->x - q1->y * q2->y - q1->z * q2->z;
    ret.x = q1->w * q2->x + q1->x * q2->w + q1->y * q2->z - q1->z * q2->y;
    ret.y = q1->w * q2->y - q1->x * q2->z + q1->y * q2->w + q1->z * q2->x;
    ret.z = q1->w * q2->z + q1->x * q2->y - q1->y * q2->x + q1->z * q2->w;

    return ret;
}

bool rotate_by_quaternion(union Vector3 *vec, union Quaternion *q) {
    if (!vec || !q)
        return false;

    // Set vector part of the quaternion to the vector we want to rotate
    union Quaternion v = {
        .w = 0,
        .x = vec->x,
        .y = vec->y,
        .z = vec->z,
    };

    // Calculate the inverse of the quaternion of q
    union Quaternion q_inv = *q;
    conjugate_quaternion(&q_inv);

    // Calculate q * v * conj(q)
    union Quaternion intermediate_prod = hamilton_product(&v, &q_inv);
    union Quaternion res = hamilton_product(q, &intermediate_prod);

    // Return vector part of the result
    vec->x = res.x;
    vec->y = res.y;
    vec->z = res.z;

    return true;
}

/*******************************************************************************
 * Angle mathematics: Conversion and mean
 ******************************************************************************/

double deg2rad(double angle) {
    return (angle / 180.0) * M_PI;
}

double rad2deg(double radians) {
    return (radians / M_PI) * 180.0;
}

float mean_angle(float a, float b) {
    return atan2f(0.5f * (sinf(a) + sinf(b)), 0.5f * (cosf(a) + cosf(b)));
}

float wrap_to_pi(float angle) {
    return atan2f(sinf(angle), cosf(angle));
}

float wrap_to_2pi(float angle) {
    float wrapped = fmodf(angle, M_2PIf);
    if (wrapped < 0)
        wrapped += M_2PIf;
    return wrapped;
}
