/**
 *******************************************************************************
 * @file           backup-registers.c
 * @brief          Interface for persisting variables through a system reset
 *                      by using RTC backup registers.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdbool.h>
#include <stdint.h>

#include "stm32f446xx.h"
#include "util/backup-registers.h"

bool rtc_backup_register_write(uint8_t reg_no, uint32_t value) {
    if (reg_no > 19)
        return false;

    // Calculate the target register address
    volatile uint32_t *write_addr = &RTC->BKP0R;
    write_addr += reg_no;

    // Write to the register
    *write_addr = value;

    return true;
}

bool rtc_backup_register_read(uint8_t reg_no, uint32_t *value) {
    if (reg_no > 19 || !value)
        return false;

    // Calculate the target register address
    volatile uint32_t *read_addr = &RTC->BKP0R;
    read_addr += reg_no;

    // Read from the register
    *value = *read_addr;

    return true;
}
