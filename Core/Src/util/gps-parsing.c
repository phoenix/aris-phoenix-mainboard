/**
 *******************************************************************************
 * @file           gps-parsing.c
 * @brief          Contains parsing for NMEA-formatted GPS messages.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "main.h"
#include "util/gps-parsing.h"

/* Local macros ------------------------------------------------------------- */

#define DIGIT_TO_VAL(_x) (_x - '0')

/* Private functions -------------------------------------------------------- */

static uint32_t grab_fields(char *src, uint8_t mult);
uint32_t GPS_coord_to_degrees(const char *coordinateString);

/* Public function implementation ------------------------------------------- */

bool nmea_parse_new_char(uint8_t c, struct NMEA_State *state) {
    assert_param(state != NULL);

    uint8_t frameOK = 0;
    uint8_t svSatNum = 0, svPacketIdx = 0, svSatParam = 0;

    switch (c) {
        case '$':
            state->param = 0;
            state->offset = 0;
            state->parity = 0;
            break;
        case ',':
        case '*':
            state->string[state->offset] = 0;
            if (state->param == 0) { // frame identification
                state->gps_frame = NO_FRAME;
                if (0 == strcmp(state->string, "GPGGA") ||
                    0 == strcmp(state->string, "GNGGA")) {
                    state->gps_frame = FRAME_GGA;
                } else if (0 == strcmp(state->string, "GPRMC") ||
                           0 == strcmp(state->string, "GNRMC")) {
                    state->gps_frame = FRAME_RMC;
                } else if (0 == strcmp(state->string, "GPGSV")) {
                    state->gps_frame = FRAME_GSV;
                }
            }

            switch (state->gps_frame) {
                case FRAME_GGA: //************* GPGGA FRAME parsing
                    switch (state->param) {
                            //          case 1:             // Time
                            //          information
                            //              break;
                        case 2:
                            state->gps_Msg.latitude =
                                GPS_coord_to_degrees(state->string);
                            break;
                        case 3:
                            if (state->string[0] == 'S')
                                state->gps_Msg.latitude *= -1;
                            break;
                        case 4:
                            state->gps_Msg.longitude =
                                GPS_coord_to_degrees(state->string);
                            break;
                        case 5:
                            if (state->string[0] == 'W')
                                state->gps_Msg.longitude *= -1;
                            break;
                        case 6:
                            if (state->string[0] > '0') {
                                // we have a GPS fix
                                state->GPS_FIX = 1;
                            } else {
                                // we don't have a GPS fix
                                state->GPS_FIX = 0;
                            }
                            break;
                        case 7:
                            state->gps_Msg.numSat =
                                grab_fields(state->string, 0);
                            break;
                        case 8:
                            state->gps_Msg.hdop =
                                grab_fields(state->string, 1) * 100; // hdop
                            break;
                        case 9:
                            state->gps_Msg.altitudeCm =
                                grab_fields(state->string, 1) *
                                10; // altitude in centimeters. Note: NMEA
                                    // delivers altitude with 1 or 3
                                    // decimals. It's safer to cut at 0.1m
                                    // and multiply by 10
                            break;
                    }
                    break;
                case FRAME_RMC: //************* GPRMC FRAME parsing
                    switch (state->param) {
                        case 1:
                            state->gps_Msg.time = grab_fields(
                                state->string, 2); // UTC time hhmmss.ss
                            break;
                        case 7:
                            state->gps_Msg.speed =
                                ((grab_fields(state->string, 1) * 5144L) /
                                 1000L); // speed in cm/s added by Mis
                            break;
                        case 8:
                            state->gps_Msg.ground_course = (grab_fields(
                                state->string, 1)); // ground course deg * 10
                            break;
                        case 9:
                            state->gps_Msg.date =
                                grab_fields(state->string, 0); // date dd/mm/yy
                            break;
                    }
                    break;
                case FRAME_GSV:
                    switch (state->param) {
                            /*case 1:
                                  // Total number of messages of this type
                               in this cycle break; */
                        case 2:
                            // Message number
                            state->svMessageNum = grab_fields(state->string, 0);
                            break;
                        case 3:
                            // Total number of SVs visible
                            // TODO: not sure what this does
                            // state->GPS_numCh = grab_fields(state->string, 0);
                            break;
                    }
                    if (state->param < 4)
                        break;

                    svPacketIdx = (state->param - 4) / 4 +
                                  1; // satellite number in packet, 1-4
                    svSatNum =
                        svPacketIdx + (4 * (state->svMessageNum -
                                            1)); // global satellite number
                    svSatParam = state->param - 3 -
                                 (4 * (svPacketIdx -
                                       1)); // parameter number for satellite

                    if (svSatNum > GPS_SV_MAXSATS)
                        break;

                    switch (svSatParam) {
                        case 1:
                            // SV PRN number
                            state->GPS_svinfo_chn[svSatNum - 1] = svSatNum;
                            state->GPS_svinfo_svid[svSatNum - 1] =
                                grab_fields(state->string, 0);
                            break;
                            /*case 2:
                                  // Elevation, in degrees, 90 maximum
                                  break;
                              case 3:
                                  // Azimuth, degrees from True North, 000
                              through 359 break; */
                        case 4:
                            // SNR, 00 through 99 dB (null when not
                            // tracking)
                            state->GPS_svinfo_cno[svSatNum - 1] =
                                grab_fields(state->string, 0);
                            state->GPS_svinfo_quality[svSatNum - 1] =
                                0; // only used by ublox
                            break;
                    }

                    state->GPS_svInfoReceivedCount++;

                    break;
                default:
                    break;
            }

            state->param++;
            state->offset = 0;
            if (c == '*')
                state->checksum_param = 1;
            else
                state->parity ^= c;
            break;
        case '\r':
        case '\n':
            if (state->checksum_param) { // parity checksum
                uint8_t checksum =
                    16 * ((state->string[0] >= 'A')
                              ? state->string[0] - 'A' + 10
                              : state->string[0] - '0') +
                    ((state->string[1] >= 'A') ? state->string[1] - 'A' + 10
                                               : state->string[1] - '0');
                if (checksum == state->parity) {
                    state->GPS_packetCount++;
                    switch (state->gps_frame) {
                        case FRAME_GGA:
                            frameOK = 1;
                            if (state->GPS_FIX) {
                                state->gpsSol.llh.lat = state->gps_Msg.latitude;
                                state->gpsSol.llh.lon =
                                    state->gps_Msg.longitude;
                                state->gpsSol.numSat = state->gps_Msg.numSat;
                                state->gpsSol.llh.altCm =
                                    state->gps_Msg.altitudeCm;
                                state->gpsSol.hdop = state->gps_Msg.hdop;
                            }
                            break;
                        case FRAME_RMC:
                            state->gpsSol.groundSpeed = state->gps_Msg.speed;
                            state->gpsSol.groundCourse =
                                state->gps_Msg.ground_course;
                            break;
                        default:
                            break;
                    } // end switch
                } else {
                    //*gpsPacketLogChar = LOG_ERROR;
                }
            }
            state->checksum_param = 0;
            break;
        default:
            if (state->offset < 15)
                state->string[state->offset++] = c;
            if (!state->checksum_param)
                state->parity ^= c;
    }
    return frameOK;
}

/* Private function implementation ------------------------------------------ */

static uint32_t grab_fields(char *src,
                            uint8_t mult) { // convert string to uint32
    uint32_t i;
    uint32_t tmp = 0;
    int isneg = 0;
    for (i = 0; src[i] != 0; i++) {
        if ((i == 0) && (src[0] == '-')) { // detect negative sign
            isneg = 1;
            continue; // jump to next character if the first one was a negative
                      // sign
        }
        if (src[i] == '.') {
            i++;
            if (mult == 0) {
                break;
            } else {
                src[i + mult] = 0;
            }
        }
        tmp *= 10;
        if (src[i] >= '0' && src[i] <= '9') {
            tmp += src[i] - '0';
        }
        if (i >= 15) {
            return 0; // out of bounds
        }
    }
    return isneg ? -tmp : tmp; // handle negative altitudes
}

uint32_t GPS_coord_to_degrees(const char *coordinateString) {
    const char *fieldSeparator, *remainingString;
    uint8_t degress = 0, minutes = 0;
    uint16_t fractionalMinutes = 0;
    uint8_t digitIndex;

    // scan for decimal point or end of field
    for (fieldSeparator = coordinateString;
         isdigit((unsigned char)*fieldSeparator); fieldSeparator++) {
        if (fieldSeparator >= coordinateString + 15)
            return 0; // stop potential fail
    }
    remainingString = coordinateString;

    // convert degrees
    while ((fieldSeparator - remainingString) > 2) {
        if (degress)
            degress *= 10;
        degress += DIGIT_TO_VAL(*remainingString++);
    }
    // convert minutes
    while (fieldSeparator > remainingString) {
        if (minutes)
            minutes *= 10;
        minutes += DIGIT_TO_VAL(*remainingString++);
    }
    // convert fractional minutes
    // expect up to four digits, result is in
    // ten-thousandths of a minute
    if (*fieldSeparator == '.') {
        remainingString = fieldSeparator + 1;
        for (digitIndex = 0; digitIndex < 4; digitIndex++) {
            fractionalMinutes *= 10;
            if (isdigit((unsigned char)*remainingString))
                fractionalMinutes += *remainingString++ - '0';
        }
    }
    return degress * 10000000UL +
           (minutes * 1000000UL + fractionalMinutes * 100UL) / 6;
}
