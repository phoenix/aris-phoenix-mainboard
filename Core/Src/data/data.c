/**
 *******************************************************************************
 * @file           data.c
 * @brief          Implements conversion functions for the data that was
 * 						collected.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <math.h>
#include <stdlib.h>

#include "config.h"
#include "data/data.h"
#include "main.h"
#include "tasks/sd-log.h"
#include "util/maths.h"

/** Correction for yaw angle since the BNO055 outputs zero yaw for west */
#define YAW_CORRECTION 1.5f * M_PIf
/** Correction for roll angle since the BNO055 outputs zero roll for upside-down
 */
#define ROLL_CORRECTION 1.0f * M_PIf

/* Private definitions ------------------------------------------------------ */

// Geodetic system parameters
static double kSemimajorAxis = 6378137;
static double kSemiminorAxis = 6356752.3142;
static double kFirstEccentricitySquared = 6.69437999014 * 0.001;
static double kSecondEccentricitySquared = 6.73949674228 * 0.001;
// static double kFlattening = 1 / 298.257223563;

static double init_ecef_x, init_ecef_y, init_ecef_z;
static double ecef_to_ned_matrix[3][3];
static double ned_to_ecef_matrix[3][3];

// Divisors for the conversion. Taken from the Bosch reference implementation
// header file.

#define BNO055_ACCEL_DIV_MSQ (100.0f)
#define BNO055_GYRO_DIV_RPS  (900.0f)
#define BNO055_EULER_DIV_RAD (900.0f)
#define BMP388_PRES_DIV_PA   (100.0f)
#define BNO055_QUAT_DIV      (16384.0f)

void convert_raw_heading(union RawHeading *in, union Heading *out) {
    assert(in != NULL);
    assert(out != NULL);

    out->roll = (float)(in->roll) / BNO055_EULER_DIV_RAD;
    out->pitch = (float)(in->pitch) / BNO055_EULER_DIV_RAD;
    out->yaw = (float)(in->yaw) / BNO055_EULER_DIV_RAD;
}

void convert_raw_acceleration(union RawLinearAcceleration *in,
                              union LinearAcceleration *out,
                              union Quaternion *correction) {
    assert(in != NULL);
    assert(out != NULL);

    // According to the BNO-055 reference implementation, see line 3210
    // in bosch-sensors.c, just interpret as float and divide by a constant to
    // get the float value from the raw value.
    out->x = (float)(in->x) / BNO055_ACCEL_DIV_MSQ;
    out->y = (float)(in->y) / BNO055_ACCEL_DIV_MSQ;
    out->z = (float)(in->z) / BNO055_ACCEL_DIV_MSQ;

    // Correct assembly inaccuracies if a correcting quaternion is passed
    if (correction != NULL) {
        rotate_by_quaternion((union Vector3 *)out, correction);
    }

    return;
}

void convert_raw_angular_velocity(union RawAngularVelocity *in,
                                  union AngularVelocity *out,
                                  union Quaternion *correction) {
    assert(in != NULL);
    assert(out != NULL);

    // according to the BNO-055 reference implementation, see line 3850
    // in bosch-sensors.c, just interpret as float and divide by a constant

    out->p = (float)(in->p) / BNO055_GYRO_DIV_RPS;
    out->q = (float)(in->q) / BNO055_GYRO_DIV_RPS;
    out->r = (float)(in->r) / BNO055_GYRO_DIV_RPS;

    // Correct [p; q; r] to account for inaccurate assembly
    if (correction != NULL) {
        rotate_by_quaternion((union Vector3 *)out, correction);
    }

    return;
}

void convert_raw_quaternion_to_heading(union RawQuaternion *in,
                                       union Heading *out,
                                       union Quaternion *correction) {

    // Step 1: Convert raw register data into float-based unit quaternion
    union Quaternion q = {0};
    for (uint_fast8_t i = 0; i < 4; i++) {
        q.array[i] = in->array[i] / BNO055_QUAT_DIV;
    }

    // Step 2: Normalize the quaternion
    if (!normalize_quaternion(&q)) {
        sd_log("Tried to convert non-normalizable quaternion!", ErrorEntry);
        return;
    }

    // Step 3: Axis remap because the BNO055 returns the quaternion in a weird
    // coordinate frame
    float x_temp = q.x;
    q.x = q.z;
    q.y = q.y;
    q.z = x_temp;

    // Step 3: Correct heading (due to assembly inaccuracies)
    // q =  assembly_correction * q where '*' is the Hamilton product
    if (correction != NULL) {
        q = hamilton_product(correction, &q);
    }

    // Step 4: Compute intrinsic z-y'-x'' rotation angles
    float roll = wrap_to_pi(
        atan2f(2 * (q.w * q.x + q.y * q.z), 1 - 2 * (q.x * q.x + q.y * q.y)) +
        ROLL_CORRECTION);
    float pitch = asinf(2 * (q.w * q.y - q.z * q.x));
    float yaw =
        atan2f(2 * (q.w * q.z + q.x * q.y), 1 - 2 * (q.y * q.y + q.z * q.z)) +
        YAW_CORRECTION;

    // Step 5: Check if all of them are valid numbers and save to out if true
    if (isfinite(roll) && isfinite(pitch) && isfinite(yaw)) {
        out->roll = roll;
        out->pitch = pitch;
        out->yaw = yaw;
    } else {
        sd_log("Roll, Pitch or Yaw is not finite!", ErrorEntry);
    }
}

void convert_heading_to_raw_quaternion(
    union Heading *in, union RawQuaternion *out,
    union Quaternion *quat_assembly_correction) {
    assert_param(in != NULL);
    assert_param(out != NULL);

    float roll_in = in->roll - ROLL_CORRECTION;
    float pitch_in = in->pitch;
    float yaw_in = in->yaw - YAW_CORRECTION;

    float s_x = sinf(roll_in * 0.5f);
    float s_y = sinf(pitch_in * 0.5f);
    float s_z = sinf(yaw_in * 0.5f);

    float c_x = cosf(roll_in * 0.5f);
    float c_y = cosf(pitch_in * 0.5f);
    float c_z = cosf(yaw_in * 0.5f);

    union Quaternion q = {.x = s_x * c_y * c_z - c_x * s_y * s_z,
                          .y = c_x * s_y * c_z + s_x * c_y * s_z,
                          .z = c_x * c_y * s_z - s_x * s_y * c_z,
                          .w = c_x * c_y * c_z + s_x * s_y * s_z};

    // Rotate quaternion to create measurement with BNO-zero point and
    // inaccurate assembly
    if (quat_assembly_correction != NULL) {
        union Quaternion quat_assembly_correction_inv =
            *quat_assembly_correction;
        conjugate_quaternion(&quat_assembly_correction_inv);

        // q_rotated = assembly_correction_inv * q
        q = hamilton_product(&quat_assembly_correction_inv, &q);
    }

    // Inverse axis remap
    float z_temp = q.z;
    q.z = q.x;
    q.y = q.y;
    q.x = z_temp;

    out->w = q.w * BNO055_QUAT_DIV;
    out->x = q.x * BNO055_QUAT_DIV;
    out->y = q.y * BNO055_QUAT_DIV;
    out->z = q.z * BNO055_QUAT_DIV;
}

AltitudeAGL convert_raw_pressure_to_altitude(RawPressure pressure) {
    // international barometric formula
    return 44330.769230769f *
               (1.0f - (float)pow(pressure / (BMP388_PRES_DIV_PA *
                                              SEA_LEVEL_PRESSURE_PA),
                                  0.190294957f)) -
           GROUND_LEVEL_METERS;
}


bool initNedSystem() {
    double lat_rad = deg2rad(LAT_0);
    double lng_rad = deg2rad(LNG_0);

    double xi =
        sqrt(1 - kFirstEccentricitySquared * sin(lat_rad) * sin(lat_rad));
    init_ecef_x = (kSemimajorAxis / xi + (double)GROUND_LEVEL_METERS) *
                  cos(lat_rad) * cos(lng_rad);
    init_ecef_y = (kSemimajorAxis / xi + (double)GROUND_LEVEL_METERS) *
                  cos(lat_rad) * sin(lng_rad);
    init_ecef_z = (kSemimajorAxis / xi * (1 - kFirstEccentricitySquared) +
                   (double)GROUND_LEVEL_METERS) *
                  sin(lat_rad);

    double phiP =
        atan2(init_ecef_z, sqrt(pow(init_ecef_x, 2) + pow(init_ecef_y, 2)));

    /**
     * Transformation matrix ECEF -> NED
     */
    const double sLat_ecef_ned = sin(phiP);
    const double sLon_ecef_ned = sin(lng_rad);
    const double cLat_ecef_ned = cos(phiP);
    const double cLon_ecef_ned = cos(lng_rad);

    ecef_to_ned_matrix[0][0] = -sLat_ecef_ned * cLon_ecef_ned;
    ecef_to_ned_matrix[0][1] = -sLat_ecef_ned * sLon_ecef_ned;
    ecef_to_ned_matrix[0][2] = cLat_ecef_ned;
    ecef_to_ned_matrix[1][0] = -sLon_ecef_ned;
    ecef_to_ned_matrix[1][1] = cLon_ecef_ned;
    ecef_to_ned_matrix[1][2] = 0.0;
    ecef_to_ned_matrix[2][0] = cLat_ecef_ned * cLon_ecef_ned;
    ecef_to_ned_matrix[2][1] = cLat_ecef_ned * sLon_ecef_ned;
    ecef_to_ned_matrix[2][2] = sLat_ecef_ned;

    /**
     * Transformation matrix NED -> ECEF
     */
    const double sLat_ned_ecef = sin(lat_rad);
    const double sLon_ned_ecef = sin(lng_rad);
    const double cLat_ned_ecef = cos(lat_rad);
    const double cLon_ned_ecef = cos(lng_rad);

    ned_to_ecef_matrix[0][0] = -sLat_ned_ecef * cLon_ned_ecef;
    ned_to_ecef_matrix[0][1] = -sLon_ned_ecef;
    ned_to_ecef_matrix[0][2] = cLat_ned_ecef * cLon_ned_ecef;
    ned_to_ecef_matrix[1][0] = -sLat_ned_ecef * sLon_ned_ecef;
    ned_to_ecef_matrix[1][1] = cLon_ned_ecef;
    ned_to_ecef_matrix[1][2] = cLat_ned_ecef * sLon_ned_ecef;
    ned_to_ecef_matrix[2][0] = cLat_ned_ecef;
    ned_to_ecef_matrix[2][1] = 0.0;
    ned_to_ecef_matrix[2][2] = sLat_ned_ecef;

    return true;
}

void geo2ned(union GeodeticPosition *geo_position,
             union Position *ned_position) {
    double lat = deg2rad(geo_position->latitude);
    double lng = deg2rad(geo_position->longitude);
    double alt = geo_position->elevation;

    double xi = sqrt(1 - kFirstEccentricitySquared * sin(lat) * sin(lat));

    double x = (kSemimajorAxis / xi + alt) * cos(lat) * cos(lng);
    double y = (kSemimajorAxis / xi + alt) * cos(lat) * sin(lng);
    double z = (kSemimajorAxis / xi * (1 - kFirstEccentricitySquared) + alt) *
               sin(lat);

    double vect[3];
    vect[0] = x - init_ecef_x;
    vect[1] = y - init_ecef_y;
    vect[2] = z - init_ecef_z;

    double ret[3];
    ret[0] = ecef_to_ned_matrix[0][0] * vect[0] +
             ecef_to_ned_matrix[0][1] * vect[1] +
             ecef_to_ned_matrix[0][2] * vect[2];
    ret[1] = ecef_to_ned_matrix[1][0] * vect[0] +
             ecef_to_ned_matrix[1][1] * vect[1] +
             ecef_to_ned_matrix[1][2] * vect[2];
    ret[2] = ecef_to_ned_matrix[2][0] * vect[0] +
             ecef_to_ned_matrix[2][1] * vect[1] +
             ecef_to_ned_matrix[2][2] * vect[2];

    ned_position->north = ret[0];
    ned_position->east = ret[1];
    ned_position->down = -ret[2];
}

void ned2geo(union Position *ned_position,
             union GeodeticPosition *geo_position) {
    float up = -ned_position->down;
    double ret[3];

    ret[0] = ned_to_ecef_matrix[0][0] * (double)ned_position->north +
             ned_to_ecef_matrix[0][1] * (double)ned_position->east +
             ned_to_ecef_matrix[0][2] * (double)up;
    ret[1] = ned_to_ecef_matrix[1][0] * (double)ned_position->north +
             ned_to_ecef_matrix[1][1] * (double)ned_position->east +
             ned_to_ecef_matrix[1][2] * (double)up;
    ret[2] = ned_to_ecef_matrix[2][0] * (double)ned_position->north +
             ned_to_ecef_matrix[2][1] * (double)ned_position->east +
             ned_to_ecef_matrix[2][2] * (double)up;

    double x = ret[0] + init_ecef_x;
    double y = ret[1] + init_ecef_y;
    double z = ret[2] + init_ecef_z;

    double r = sqrt(x * x + y * y);
    double Esq =
        kSemimajorAxis * kSemimajorAxis - kSemiminorAxis * kSemiminorAxis;
    double F = 54 * kSemiminorAxis * kSemiminorAxis * z * z;
    double G = r * r + (1 - kFirstEccentricitySquared) * z * z -
               kFirstEccentricitySquared * Esq;
    double C =
        (kFirstEccentricitySquared * kFirstEccentricitySquared * F * r * r) /
        pow(G, 3);
    double S = cbrt(1 + C + sqrt(C * C + 2 * C));
    double P = F / (3 * pow((S + 1 / S + 1), 2) * G * G);
    double Q =
        sqrt(1 + 2 * kFirstEccentricitySquared * kFirstEccentricitySquared * P);
    double r_0 =
        -(P * kFirstEccentricitySquared * r) / (1 + Q) +
        sqrt(0.5 * kSemimajorAxis * kSemimajorAxis * (1 + 1.0 / Q) -
             P * (1 - kFirstEccentricitySquared) * z * z / (Q * (1 + Q)) -
             0.5 * P * r * r);
    double U = sqrt(pow((r - kFirstEccentricitySquared * r_0), 2) + z * z);
    double V = sqrt(pow((r - kFirstEccentricitySquared * r_0), 2) +
                    (1 - kFirstEccentricitySquared) * z * z);
    double Z_0 = kSemiminorAxis * kSemiminorAxis * z / (kSemimajorAxis * V);

    geo_position->elevation =
        U * (1 - kSemiminorAxis * kSemiminorAxis / (kSemimajorAxis * V));
    geo_position->latitude =
        rad2deg(atan((z + kSecondEccentricitySquared * Z_0) / r));
    geo_position->longitude = rad2deg(atan2(y, x));
}

#if USE_SENSOR_DATA_HAL

uint32_t convert_altitude_to_raw_pressure(float down) {
    return (uint32_t)(
        (float)pow(1 - (GROUND_LEVEL_METERS - down) / 44330.769230769f,
                   1 / 0.190294957f) *
        BMP388_PRES_DIV_PA * SEA_LEVEL_PRESSURE_PA);
}

union RawHeading unconvert_raw_heading(union Heading in) {
    union RawHeading raw_heading = {};
    raw_heading.roll = (int16_t)(in.roll * BNO055_EULER_DIV_RAD);
    raw_heading.pitch = (int16_t)(in.pitch * BNO055_EULER_DIV_RAD);
    raw_heading.yaw = (int16_t)(in.yaw * BNO055_EULER_DIV_RAD);
    return raw_heading;
}

union RawLinearAcceleration
unconvert_raw_acceleration(union LinearAcceleration in,
                           union Quaternion *quat_assembly_correction) {
    // Rotate quaternion to create measurement with BNO-zero point and
    // inaccurate assembly
    if (quat_assembly_correction != NULL) {
        union Quaternion quat_assembly_correction_inv =
            *quat_assembly_correction;
        conjugate_quaternion(&quat_assembly_correction_inv);
        rotate_by_quaternion((union Vector3 *)&in,
                             &quat_assembly_correction_inv);
    }

    union RawLinearAcceleration raw_lin_accel = {};
    raw_lin_accel.x = (int16_t)(in.x * BNO055_ACCEL_DIV_MSQ);
    raw_lin_accel.y = (int16_t)(in.y * BNO055_ACCEL_DIV_MSQ);
    raw_lin_accel.z = (int16_t)(in.z * BNO055_ACCEL_DIV_MSQ);

    return raw_lin_accel;
}

union RawAngularVelocity
unconvert_raw_angular_velocity(union AngularVelocity in,
                               union Quaternion *quat_assembly_correction) {


    if (quat_assembly_correction != NULL) {
        // Rotate quaternion to create measurement with BNO-zero point and
        // inaccurate assembly
        union Quaternion quat_assembly_correction_inv =
            *quat_assembly_correction;
        conjugate_quaternion(&quat_assembly_correction_inv);
        rotate_by_quaternion((union Vector3 *)&in,
                             &quat_assembly_correction_inv);
    }

    union RawAngularVelocity raw_ang_vel = {};
    raw_ang_vel.p = (int16_t)(in.p * BNO055_GYRO_DIV_RPS);
    raw_ang_vel.q = (int16_t)(in.q * BNO055_GYRO_DIV_RPS);
    raw_ang_vel.r = (int16_t)(in.r * BNO055_GYRO_DIV_RPS);
    return raw_ang_vel;
}

#endif
