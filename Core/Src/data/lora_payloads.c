/**
 *
 *
 *
 *
 *
 */

#include "data/lora_payloads.h"
#include "drivers/lora_module.h"
#include <stdio.h>
#include <string.h>


void signature_whitening(uint8_t* msg, uint8_t length){

	uint8_t whitening_msg[] = {'P', 'H', 'O', 'E', 'N', 'I', 'X'};
	uint8_t i;

	for (i=0; i<length; i+=1){

		msg[i] ^= whitening_msg[i%(sizeof(whitening_msg))];

	}

}



void encrypt_air(uint8_t* msg){

	set_checksum(msg, AIRBORNE_TX_PAYLOAD_SIZE);

	//TODO ENCRYPTION

	signature_whitening(msg, AIRBORNE_TX_PAYLOAD_SIZE);

}



uint8_t decrypt_air(uint8_t* msg){

	uint8_t valid = 0;
	uint8_t identifier;

	signature_whitening(msg, AIRBORNE_TX_PAYLOAD_SIZE);

	//TODO DECRYPTION

	identifier = msg[0];

	valid = (check_checksum(msg, AIRBORNE_TX_PAYLOAD_SIZE) && identifier_check(identifier));

	return valid;

}



void encrypt_ground(uint8_t* msg){

	set_checksum(msg, GROUND_TX_PAYLOAD_SIZE);

	//TODO ENCRYPTION

	signature_whitening(msg, GROUND_TX_PAYLOAD_SIZE);

}


uint8_t decrypt_ground(uint8_t* msg){

	uint8_t valid = 0;
	uint8_t identifier;

	signature_whitening(msg, GROUND_TX_PAYLOAD_SIZE);

	//TODO DECRYPTION

	identifier = msg[0];

	valid = (check_checksum(msg, GROUND_TX_PAYLOAD_SIZE) && identifier_check(identifier));

	return valid;

}


uint8_t identifier_check(uint8_t identifier) {

    uint8_t mask = 0x01;
    uint8_t i;

    for(i=0; i<4; i++){

    	if(((identifier>>i) & mask) != ((identifier>>(7-i)) & mask)){

    		return 0;

    	}
    }

    return 1;
}



void set_checksum(uint8_t* msg, uint8_t length){

	uint8_t checksum = 0;
	uint8_t i;

	for(i=0; i<(length-2); i+=1){

		checksum ^= msg[i];

	}
	msg[length-1] = checksum;

}


uint8_t check_checksum(uint8_t* msg, uint8_t length){

	uint8_t valid = 0;

	uint8_t checksum = 0;
	uint8_t i;

	for(i=0; i<(length-2); i+=1){

		checksum ^= msg[i];

	}

	if(checksum == msg[length-1]){

		valid = 1;

	}

	return valid;

}

void write_u16(uint16_t data, uint8_t* dest){

	dest[0] = (uint8_t) (data & 0xFF);
	dest[1] = (uint8_t) ((data>>8) & 0xFF);

}

uint16_t read_u16(uint8_t* source){

	uint16_t data = 0;
	data |= (uint8_t)source[1];
	data = (data << 8) & 0xFF00;
	data |= (uint8_t)source[0];

	return data;

}

void write_u32(uint32_t data, uint8_t* dest){

	dest[0] = (uint8_t) (data & 0xFF);
	dest[1] = (uint8_t) ((data>>8) & 0xFF);
	dest[2] = (uint8_t) ((data>>16) & 0xFF);
	dest[3] = (uint8_t) ((data>>24) & 0xFF);

}

uint32_t read_u32(uint8_t* source){

	uint32_t data = 0;
	data |= (uint8_t)source[3];
	data = (data << 8) & 0xFFFFFF00;
	data |= (uint8_t)source[2];
	data = (data << 8) & 0xFFFFFF00;
	data |= (uint8_t)source[1];
	data = (data << 8) & 0xFFFFFF00;
	data |= (uint8_t)source[0];

	return data;

}



void write_s16(int16_t data, uint8_t* dest){

	dest[0] = (uint8_t) (data & 0xFF);
	dest[1] = (uint8_t) ((data>>8) & 0xFF);

}

int16_t read_s16(uint8_t* source){

	int16_t data = 0;
	data |= (uint8_t)source[1];
	data = (data << 8) & 0xFF00;
	data |= (uint8_t)source[0];

	return data;

}

void write_s32(int32_t data, uint8_t* dest){

	dest[0] = (uint8_t) (data & 0xFF);
	dest[1] = (uint8_t) ((data>>8) & 0xFF);
	dest[2] = (uint8_t) ((data>>16) & 0xFF);
	dest[3] = (uint8_t) ((data>>24) & 0xFF);

}

int32_t read_s32(uint8_t* source){

	int32_t data = 0;
	data |= (uint8_t)source[3];
	data = (data << 8) & 0xFFFFFF00;
	data |= (uint8_t)source[2];
	data = (data << 8) & 0xFFFFFF00;
	data |= (uint8_t)source[1];
	data = (data << 8) & 0xFFFFFF00;
	data |= (uint8_t)source[0];

	return data;

}
