/**
 *******************************************************************************
 * @file           system-status.c
 * @brief          A framework that keeps an overview over what's working on
 *                      the system and access to low-level system functions.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include "cmsis_os.h"
#include "core_cm4.h"
#include "stm32f446xx.h"

#include "config.h"
#include "system-status.h"
#include "tasks/sd-log.h"

/* Public variables --------------------------------------------------------- */

// Initialize the system status to the default values
struct SystemStatus global_system_status = {0};

/* Public function implementation ------------------------------------------- */

StatusUpdate_t system_get_lora_compatible_status(void) {

    TickType_t current_tick = osKernelGetTickCount();

    StatusUpdate_t status = {
        // SD is marked as connected when it could successfully be mounted  or
        // better
        .sd = global_system_status.sd_status >= ReadySDStatus,

        // GPS is marked "okay" when they are responding
        .gps1 = global_system_status.mng1_status >= RespondingGPSStatus,
        .gps1_lock = global_system_status.mng1_status == FixGPSStatus,
        .gps2 = global_system_status.mng2_status >= RespondingGPSStatus,
        .gps2_lock = global_system_status.mng2_status == FixGPSStatus,

        // IMUs are marked "okay" when calibration was successful and no error
        .imu1 = global_system_status.mna1_status == ReadySensorStatus,
        .imu2 = global_system_status.mna2_status == ReadySensorStatus,
        .baro1 = global_system_status.mnp1_status == ReadySensorStatus,
        .baro2 = global_system_status.mnp2_status == ReadySensorStatus,

        // In-Canopy Sensor Boards are connected as long as not more
        // than 75 ticks pass between updates
        .icsb1 = current_tick - global_system_status.icsb_1_last_update < 75,
        .icsb2 = current_tick - global_system_status.icsb_2_last_update < 75,

        .state = fsm_get_state(),
    };

    return status;
}

enum ResetReason system_get_reset_reason() {

    volatile uint32_t csr = RCC->CSR;

    // Check if we already checked what the reset reason was. If we did, return
    // result from last time.
    if (global_system_status.reset_reason == UnknownResetReason) {
        enum ResetReason reason = UnknownResetReason;

        if (csr & RCC_CSR_LPWRRSTF) {
            reason = LowPowerResetReason;
        } else if (csr & RCC_CSR_WWDGRSTF || csr & RCC_CSR_IWDGRSTF) {
            reason = WatchdogResetReason;
        } else if (csr & RCC_CSR_SFTRSTF) {
            reason = SoftwareResetReason;
        } else if (csr & RCC_CSR_PORRSTF) {
            reason = PowerOnPowerDownResetReason;
        } else if (csr & RCC_CSR_PINRSTF) {
            reason = ResetPinResetReason;
        } else if (csr & RCC_CSR_BORRSTF) {
            reason = BrownOutResetReason;
        }

        // Clear the register so on next read the reset is cleared
        RCC->CSR |= RCC_CSR_RMVF;
        global_system_status.reset_reason = reason;
        return reason;
    } else {
        return global_system_status.reset_reason;
    }
}

void system_print_startup_report(void) {

    // Print the best guess of what the reset reason was
    const char *reset_reason = "Unknown";

    switch (global_system_status.reset_reason) {
        case LowPowerResetReason:
            reset_reason = "Low Power";
            break;
        case WatchdogResetReason:
            reset_reason = "Watchdog Reset";
            break;
        case SoftwareResetReason:
            reset_reason = "Software Reset";
            break;
        case PowerOnPowerDownResetReason:
            reset_reason = "Power-On-Power-Down Reset";
            break;
        case ResetPinResetReason:
            reset_reason = "Reset Pin";
            break;
        case BrownOutResetReason:
            reset_reason = "Brownout";
            break;
        default:
            break;
    }

    sd_printf(EventEntry, "System restarted. Reason: %s", reset_reason);

    // Print the current configuration we're flying as a log entry.
    sd_printf(EventEntry, "Sea level pressure [Pa]: %u", SEA_LEVEL_PRESSURE_PA);
    sd_printf(EventEntry, "Ground level [m]: %f", (double)GROUND_LEVEL_METERS);
    sd_printf(EventEntry, "Deployment height [m]: %u",
              (double)DEPLOYMENT_HEIGHT_METERS_AGL);
    sd_printf(EventEntry, "Target longitude/latitude [°/°]: %f, %f",
              (double)LAT_0, (double)LNG_0);
    sd_printf(EventEntry, "Safety timer duration [s]: %d",
              SAFETY_TIMER_DURATION_MS / 1000);
    sd_printf(EventEntry, "Watchdog pretimer[s]: %u",
              WATCHDOG_PRETIMER_MS / 1000);

    return;
}

void system_restart() {
    // Now that's easy. But no, we don't actually want to do this. Log that this
    // was commanded.
    sd_log("A system reset was erroneously commanded.", ErrorEntry);
    // NVIC_SystemReset();
}
