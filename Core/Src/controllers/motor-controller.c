/**
 *******************************************************************************
 * @file           motor-controller.c
 * @brief          Motor controller that takes the input handling output and
 *                   sends it to the EPOS driver.
 * @author         : Pascal Sutter
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include "controllers/motor-controller.h"
#include "drivers/epos4.h"
#include "main.h"
#include "tasks/sd-log.h"

/* Local variables ---------------------------------------------------------- */

int8_t Motor_Status_Right;
int8_t Motor_Status_Left;
int32_t Measured_Motor_Position_Left = 0;
int32_t Measured_Motor_Position_Right = 0;
int32_t Motor_Position_Right;
int32_t Motor_Position_Left;

// Define different Positions of lines
float ReleaseBrakes = 0.10; // Distance needed to Release brakes
float Flare_Right = 0.30;   // Distance for flare manoeuver
float Flare_Left = 0.40;

// Position Mode
int32_t PPM_Velocity = 10000;      // max 10'000 - can go up to 15k max!
int32_t PPM_Acceleration = 200000; // unlimited, 200'000 good value
int32_t PPM_Deceleration = 200000; // unlimited, 200'000 good value

// Homing Mode
int32_t HMM_Acceleration = 10000; // unlimited, keep it low - 2000
int32_t HMM_SpeedSwitch = 10000;  // speed of homing
int32_t HMM_SpeedZero = 10;       // speed near its home position
int32_t HMM_HomeOffset =
    -0.50 * (-618 / 0.08); // distance from Brake Position to Equilibrium Point
int32_t HMM_CurrentThreshold = 500; // 500mA default
int32_t HMM_HomePosition = 0;       // Define new home position

/* Public functions --------------------------------------------------------- */

bool motors_prepare_for_drop(void) {

    // During arming, the actuators are activated, which makes sure that the
    // a) the steering lines do not move during deployment and b) the motor
    // already has torque so it cannot be damaged by shocks on the lines

    if ((Motor_Status_Right = EnableMotor(&huart1)) == EPOS4_OK) {
        sd_printf(EventEntry, "Right motor enabled.");
    } else {
        sd_printf(EventEntry, "Right motor could not be enabled.");
    }

    if ((Motor_Status_Left = EnableMotor(&huart6)) == EPOS4_OK) {
        sd_printf(EventEntry, "Left motor enabled.");
    } else {
        sd_printf(ErrorEntry, "Left motor could not be enabled.");
    }

    return Motor_Status_Right == EPOS4_OK && Motor_Status_Left == EPOS4_OK;
}

bool motors_release_brakes(void) {
    /* Calibration Phase right after Deployment of main parachute
     * Releasing of brakes, go to equilibrium position and set it to zero - Zero
     * position
     * ..... */

    /*Set current actuator position to zero*/
    Motor_Status_Right = SetHomingModeHHM(
        &huart1, HMM_Acceleration, HMM_SpeedSwitch, HMM_SpeedZero, 0,
        HMM_CurrentThreshold, HMM_HomePosition);
    Motor_Status_Left = SetHomingModeHHM(
        &huart6, HMM_Acceleration, HMM_SpeedSwitch, HMM_SpeedZero, 0,
        HMM_CurrentThreshold, HMM_HomePosition);
    Motor_Status_Right = FindHome(&huart1);
    Motor_Status_Left = FindHome(&huart6);
    osDelay(100); // important Delay

    Motor_Status_Right = GetPosition(&huart1, &Measured_Motor_Position_Right);
    Motor_Status_Left = GetPosition(&huart6, &Measured_Motor_Position_Left);
    sd_printf(SensorDataEntry, "ACTR,%d", Measured_Motor_Position_Right - 3090);
    sd_printf(SensorDataEntry, "ACTL,%d", Measured_Motor_Position_Left - 3090);

    /*Set Position Profile Mode and release brakes */
    Motor_Status_Right = SetPositionProfilePPM(
        &huart1, PPM_Velocity, PPM_Acceleration, PPM_Deceleration);
    Motor_Status_Left = SetPositionProfilePPM(
        &huart6, PPM_Velocity, PPM_Acceleration, PPM_Deceleration);

    Motor_Status_Right =
        MoveToPosition(&huart1, ReleaseBrakes * (-618 / 0.08f));
    Motor_Status_Left = MoveToPosition(&huart6, ReleaseBrakes * (-618 / 0.08f));
    osDelay(1800); // important Delay

    Motor_Status_Right = GetPosition(&huart1, &Measured_Motor_Position_Right);
    Motor_Status_Left = GetPosition(&huart6, &Measured_Motor_Position_Left);
    sd_printf(SensorDataEntry, "ACTR,%d", Measured_Motor_Position_Right - 3090);
    sd_printf(SensorDataEntry, "ACTL,%d", Measured_Motor_Position_Left - 3090);

    /* Set Homing Mode again and release the line - set new position to Zero*/
    Motor_Status_Right = SetHomingModeHHM(
        &huart1, HMM_Acceleration, HMM_SpeedSwitch, HMM_SpeedZero,
        HMM_HomeOffset, HMM_CurrentThreshold, HMM_HomePosition);
    Motor_Status_Left = SetHomingModeHHM(
        &huart6, HMM_Acceleration, HMM_SpeedSwitch, HMM_SpeedZero,
        HMM_HomeOffset, HMM_CurrentThreshold, HMM_HomePosition);

    Motor_Status_Right = FindHome(&huart1);
    Motor_Status_Left = FindHome(&huart6);
    osDelay(6000); // important Delay

    Motor_Status_Right = GetPosition(&huart1, &Measured_Motor_Position_Right);
    Motor_Status_Left = GetPosition(&huart6, &Measured_Motor_Position_Left);
    sd_printf(SensorDataEntry, "ACTR,%d", Measured_Motor_Position_Right);
    sd_printf(SensorDataEntry, "ACTL,%d", Measured_Motor_Position_Left);

    Motor_Status_Right = SetPositionProfilePPM(
        &huart1, PPM_Velocity, PPM_Acceleration, PPM_Deceleration);
    Motor_Status_Left = SetPositionProfilePPM(
        &huart6, PPM_Velocity, PPM_Acceleration, PPM_Deceleration);
    return true;
}

bool motor_controller_run(struct InputHandlingOutput *reference) {
    /* Guided Descent Phase - Actuators get input values from controls and move
     * to the right position Right steering inputs --> huart1 Left steering
     * inputs --> huart6
     * ..... */

    float delta_left = reference->Delta_L;
    float delta_right = reference->Delta_R;

    Motor_Position_Right = (float)((delta_right) * (-618 / 0.08f));
    Motor_Status_Right = MoveToPosition(&huart1, Motor_Position_Right);
    Motor_Status_Right = GetPosition(&huart1, &Measured_Motor_Position_Right);

    Motor_Position_Left = (float)((delta_left) * (-618 / 0.08f));
    Motor_Status_Left = MoveToPosition(&huart6, Motor_Position_Left);
    Motor_Status_Left = GetPosition(&huart6, &Measured_Motor_Position_Left);

    sd_printf(SensorDataEntry, "ACTR,%d", Measured_Motor_Position_Right);
    sd_printf(SensorDataEntry, "ACTL,%d", Measured_Motor_Position_Left);

    return true;
}

bool motors_flare(void) {
    /* Flare activated remotely by Lora - during trigger approach state
     * ...*/
    Motor_Status_Right = MoveToPosition(&huart1, Flare_Right * (-618 / 0.08f));
    Motor_Status_Left = MoveToPosition(&huart6, Flare_Left * (-618 / 0.08f));
    osDelay(5000);

    Motor_Status_Right = GetPosition(&huart1, &Measured_Motor_Position_Right);
    Motor_Status_Left = GetPosition(&huart6, &Measured_Motor_Position_Left);
    sd_printf(SensorDataEntry, "ACTR,%d", Measured_Motor_Position_Right);
    sd_printf(SensorDataEntry, "ACTL,%d", Measured_Motor_Position_Left);


    return true;
}

bool motors_disable(void) {
    /*After landing the actuators go in standby mode again
     * ...*/
    if ((Motor_Status_Right = DisableMotor(&huart1)) == EPOS4_OK) {
        sd_log("Disabled right motor.", EventEntry);
    } else {
        sd_log("Could not disable right motor!", ErrorEntry);
    }

    if ((Motor_Status_Right = InitDisableMotor(&huart1)) == EPOS4_OK) {
        sd_log("Init-Disabled right motor.", EventEntry);
    } else {
        sd_log("Could not init-disable right motor!", ErrorEntry);
    }

    if ((Motor_Status_Left = DisableMotor(&huart6)) == EPOS4_OK) {
        sd_log("Disabled left motor.", EventEntry);
    } else {
        sd_log("Could not disable left motor!", ErrorEntry);
    }

    if ((Motor_Status_Left = InitDisableMotor(&huart6)) == EPOS4_OK) {
        sd_log("Init-Disabled left motor.", EventEntry);
    } else {
        sd_log("Could not init-disable left motor!", ErrorEntry);
    }
    return true;
}
