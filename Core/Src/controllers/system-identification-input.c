/**
 *******************************************************************************
 * @file           system-identification-input.c
 * @brief          Provides preprogrammed input to the motor controller
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdbool.h>

#include "controllers/system-identification-input.h"

#include "SystemIdentificationInput.h"

#include "tasks/sd-log.h"

void oneStep_SystemIdentificationInput(void) {
    static boolean_T OverrunFlag = false;

    /* Disable interrupts here */

    /* Check for overrun */
    if (OverrunFlag) {
        rtmSetErrorStatus(SystemIdentificationInput_M, "Overrun");
        return;
    }

    OverrunFlag = true;

    /* Save FPU context here (if necessary) */
    /* Re-enable timer or interrupt here */
    /* Set model inputs here */

    /* Step the model */
    SystemIdentificationInput_step();

    /* Get model outputs here */

    /* Indicate task complete */
    OverrunFlag = false;

    /* Disable interrupts here */
    /* Restore FPU context here (if necessary) */
    /* Enable interrupts here */
}

/**
 * Run system identification input once
 */
bool system_identification_input_run(
    struct ReferenceAttitude *reference_attitude) {

#if EXTENDED_CONTROL_LOOP_LOGGING
    sd_printf(EventEntry, "--- RUN SYSTEM ID INPUT");
#endif

    // run input handling filters
    oneStep_SystemIdentificationInput();

    // set outputs
    reference_attitude->controller_enabled =
        SystemIdentificationInput_Y.controller_enabled;
    reference_attitude->guidance_mode = 2;
    reference_attitude->pitch_ref = SystemIdentificationInput_Y.pitch_ref;
    reference_attitude->yaw_ref = SystemIdentificationInput_Y.yaw_ref;

#if EXTENDED_CONTROL_LOOP_LOGGING
    sd_printf(EventEntry, "--- DID RUN SYSTEM ID INPUT");
#endif

    return true;
}

/**
 * Initialize system identification input
 */
bool system_identification_input_init() {
    SystemIdentificationInput_initialize();
    return true;
}

/**
 * Terminate system identification input
 */
bool system_identification_input_terminate() {
    SystemIdentificationInput_terminate();
    return true;
}
