/**
 *******************************************************************************
 * @file           bodyrate-controller.c
 * @brief          Header file for the bodyrate controller.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdbool.h>

#include "controllers/bodyrate-controller.h"

#include "BodyRateController.h"

#include "tasks/sd-log.h"

void oneStep_BodyRateController(void) {
    static boolean_T OverrunFlag = false;

    /* Disable interrupts here */

    /* Check for overrun */
    if (OverrunFlag) {
        rtmSetErrorStatus(BodyRateController_M, "Overrun");
        return;
    }

    OverrunFlag = true;

    /* Save FPU context here (if necessary) */
    /* Re-enable timer or interrupt here */
    /* Set model inputs here */

    /* Step the model */
    BodyRateController_step();

    /* Get model outputs here */

    /* Indicate task complete */
    OverrunFlag = false;

    /* Disable interrupts here */
    /* Restore FPU context here (if necessary) */
    /* Enable interrupts here */
}

/**
 * Run body rate controller once
 */
bool bodyrate_controller_run(const struct State *estimated_state,
                             const struct AttitudeControllerOutput *ref_rate,
                             struct BodyRateControllerOutput *out) {
    // Stores whether the bodyrate controller should actually run on the next
    // function call. This is for running the bodyrate controller at half the
    // rate.
#if EXTENDED_CONTROL_LOOP_LOGGING
    sd_printf(EventEntry, "--- RUN BODY RATE CONTROLLER");
#endif

    // Set inputs

    BodyRateController_U.p_ref = ref_rate->p_ref;
    BodyRateController_U.q_ref = ref_rate->q_ref;

    BodyRateController_U.omega[0] = estimated_state->angular_velocity.p;
    BodyRateController_U.omega[1] = estimated_state->angular_velocity.q;
    BodyRateController_U.omega[2] = estimated_state->angular_velocity.r;

    // Run Body rate controller
    oneStep_BodyRateController();

    // Set output
    out->delta_a = BodyRateController_Y.delta_a;
    out->delta_f = BodyRateController_Y.delta_f;
    out->i_1 = BodyRateController_Y.i_1;
    out->i_2 = BodyRateController_Y.i_2;

#if EXTENDED_CONTROL_LOOP_LOGGING
    sd_printf(EventEntry, "--- DID RUN BODY RATE CONTROLLER");
#endif

    return true;
}

/**
 * Initialize body rate controller
 */
bool bodyrate_controller_init() {
    BodyRateController_initialize();
    return true;
}

/**
 * Terminate body rate controller
 */
bool bodyrate_controller_terminate() {
    BodyRateController_terminate();
    return true;
}
