/**
 *******************************************************************************
 * @file           wind-estimation.c
 * @brief          Bridge to the Matlab generated wind estimator code.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdlib.h>

#include "controllers/state-estimation.h"
#include "controllers/wind-estimation.h"

#include "config.h"
#include "data/data.h"
#include "main.h"

#include "WindEstimation.h"

#include "tasks/sd-log.h"

void oneStep_WindEstimation(void) {
    static boolean_T OverrunFlag = false;

    /* Disable interrupts here */

    /* Check for overrun */
    if (OverrunFlag) {
        rtmSetErrorStatus(WindEstimation_M, "Overrun");
        return;
    }

    OverrunFlag = true;

    /* Save FPU context here (if necessary) */
    /* Re-enable timer or interrupt here */
    /* Set model inputs here */

    /* Step the model */
    WindEstimation_step();

    /* Get model outputs here */

    /* Indicate task complete */
    OverrunFlag = false;

    /* Disable interrupts here */
    /* Restore FPU context here (if necessary) */
    /* Enable interrupts here */
}

/**
 * Run wind estimation once
 */
bool wind_estimation_run(const struct State *estimated_state,
                         struct Wind *wind_est) {
    // check that both pointers are valid
    assert_param(estimated_state != NULL);
    assert_param(wind_est != NULL);

    static int wind_est_exec_counter = 1;
    wind_est_exec_counter--;

    if (wind_est_exec_counter < 1) {
#if EXTENDED_CONTROL_LOOP_LOGGING
        sd_printf(EventEntry, "- RUN WIND ESTIMATION");
#endif

        // set inputs
        WindEstimation_U.Phi[0] = estimated_state->attitude.roll;
        WindEstimation_U.Phi[1] = estimated_state->attitude.pitch;
        WindEstimation_U.Phi[2] = estimated_state->attitude.yaw;

        WindEstimation_U.v[0] = estimated_state->velocity.u;
        WindEstimation_U.v[1] = estimated_state->velocity.v;
        WindEstimation_U.v[2] = estimated_state->velocity.w;

        oneStep_WindEstimation();

        wind_est->x = WindEstimation_Y.wind_estimation[0];
        wind_est->y = WindEstimation_Y.wind_estimation[1];
        wind_est->z = WindEstimation_Y.wind_estimation[2];

        wind_est_exec_counter = 10;
#if EXTENDED_CONTROL_LOOP_LOGGING
        sd_printf(EventEntry, "- DID RUN WIND ESTIMATION");
#endif
    }

    return true;
}

/**
 * Initialize wind estimation
 */
bool wind_estimation_init() {
    WindEstimation_initialize();
    return true;
}

/**
 * Terminate wind estimation
 */
bool wind_estimation_terminate() {
    WindEstimation_terminate();
    return true;
}
