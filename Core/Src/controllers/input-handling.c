/**
 *******************************************************************************
 * @file           input-handling.c
 * @brief          Source file for input handling filters
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdbool.h>

#include "controllers/input-handling.h"

#include "InputHandling.h"

#include "tasks/sd-log.h"

void oneStep_InputHandling(void) {
    static boolean_T OverrunFlag = false;

    /* Disable interrupts here */

    /* Check for overrun */
    if (OverrunFlag) {
        rtmSetErrorStatus(InputHandling_M, "Overrun");
        return;
    }

    OverrunFlag = true;

    /* Save FPU context here (if necessary) */
    /* Re-enable timer or interrupt here */
    /* Set model inputs here */

    /* Step the model */
    InputHandling_step();

    /* Get model outputs here */

    /* Indicate task complete */
    OverrunFlag = false;

    /* Disable interrupts here */
    /* Restore FPU context here (if necessary) */
    /* Enable interrupts here */
}

/**
 * Run input handling filter once
 */
bool input_handling_run(
    const struct BodyRateControllerOutput *body_rate_ctrl_out,
    struct InputHandlingOutput *out) {
#if EXTENDED_CONTROL_LOOP_LOGGING
    sd_printf(EventEntry, "--- RUN INPUT HANDLING");
#endif

    // set inputs
    InputHandling_U.delta_a = body_rate_ctrl_out->delta_a;
    InputHandling_U.delta_f = body_rate_ctrl_out->delta_f;

    // run input handling filters
    oneStep_InputHandling();

    // set outputs
    out->delta_a_clean = InputHandling_Y.delta_a_clean;
    out->delta_f_clean = InputHandling_Y.delta_f_clean;
    out->Delta_L = InputHandling_Y.Delta_L;
    out->Delta_R = InputHandling_Y.Delta_R;

#if EXTENDED_CONTROL_LOOP_LOGGING
    sd_printf(EventEntry, "--- DID RUN INPUT HANDLING");
#endif

    return true;
}

/**
 * Initialize input handling filter
 */
bool input_handling_init() {
    InputHandling_initialize();
    return true;
}

/**
 * Terminate input handling filter
 */
bool input_handling_terminate() {
    InputHandling_terminate();
    return true;
}
