/**
 *******************************************************************************
 * @file           guidance.c
 * @brief          Bridge to the Matlab generated guidance code.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include "controllers/guidance.h"

#include "Guidance.h"

#include "tasks/sd-log.h"

void oneStep_Guidance(void) {
    static boolean_T OverrunFlag = false;

    /* Disable interrupts here */

    /* Check for overrun */
    if (OverrunFlag) {
        rtmSetErrorStatus(Guidance_M, "Overrun");
        return;
    }

    OverrunFlag = true;

    /* Save FPU context here (if necessary) */
    /* Re-enable timer or interrupt here */
    /* Set model inputs here */

    /* Step the model */
    Guidance_step();

    /* Get model outputs here */

    /* Indicate task complete */
    OverrunFlag = false;

    /* Disable interrupts here */
    /* Restore FPU context here (if necessary) */
    /* Enable interrupts here */
}

/**
 * Run guidance algorithm once
 */
bool guidance_run(const struct State *estimated_state,
                  const struct Wind *estimated_wind,
                  struct ReferenceAttitude *out) {

    static int guidance_exec_counter = 1;
    guidance_exec_counter--;

    if (guidance_exec_counter < 1) {
#if EXTENDED_CONTROL_LOOP_LOGGING
        sd_printf(EventEntry, "- RUN GUIDANCE");
#endif

        // set inputs
        Guidance_U.Phi[0] = estimated_state->attitude.roll;
        Guidance_U.Phi[1] = estimated_state->attitude.pitch;
        Guidance_U.Phi[2] = estimated_state->attitude.yaw;

        Guidance_U.v[0] = estimated_state->velocity.u;
        Guidance_U.v[1] = estimated_state->velocity.v;
        Guidance_U.v[2] = estimated_state->velocity.w;

        Guidance_U.x[0] = estimated_state->position.north;
        Guidance_U.x[1] = estimated_state->position.east;
        Guidance_U.x[2] = estimated_state->position.down;

        Guidance_U.wind_estimation[0] = estimated_wind->x;
        Guidance_U.wind_estimation[1] = estimated_wind->y;
        Guidance_U.wind_estimation[2] = estimated_wind->z;

        // run Guidance
        oneStep_Guidance();

        // set outputs
        out->controller_enabled = Guidance_Y.controller_enabled;
        out->pitch_ref = Guidance_Y.pitch_ref;
        out->yaw_ref = Guidance_Y.yaw_ref;
        out->guidance_mode = Guidance_Y.guidance_mode;

        guidance_exec_counter = 10;
#if EXTENDED_CONTROL_LOOP_LOGGING
        sd_printf(EventEntry, "- DID RUN GUIDANCE");
#endif
    }

    return true;
}

/**
 * Initialize guidance algorithm
 */
bool guidance_init() {
    Guidance_initialize();
    return true;
}

/**
 * Terminate guidance algorithm
 */
bool guidance_terminate() {
    Guidance_terminate();
    return true;
}
