/**
 *******************************************************************************
 * @file           attitude-controller.c
 * @brief          Header file for the attitude controller.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdbool.h>

#include "controllers/attitude-controller.h"

#include "AttitudeController.h"

#include "tasks/sd-log.h"

/**
 * Generated by MATLAB
 */
void oneStep_AttitudeController(void) {
    static boolean_T OverrunFlag = false;

    /* Disable interrupts here */

    /* Check for overrun */
    if (OverrunFlag) {
        rtmSetErrorStatus(AttitudeController_M, "Overrun");
        return;
    }

    OverrunFlag = true;

    /* Save FPU context here (if necessary) */
    /* Re-enable timer or interrupt here */
    /* Set model inputs here */

    /* Step the model */
    AttitudeController_step();

    /* Get model outputs here */

    /* Indicate task complete */
    OverrunFlag = false;

    /* Disable interrupts here */
    /* Restore FPU context here (if necessary) */
    /* Enable interrupts here */
}

bool attitude_controller_run(
    const struct State *state, const struct ReferenceAttitude *ref_att,
    const struct BodyRateControllerOutput *body_rate_out,
    struct AttitudeControllerOutput *out) {
    // Stores whether the attitude controller should actually run on the next
    // function call. This is for running the attitude controller at half the
    // rate.
    static bool should_run_next = true;

    if (should_run_next) {
#if EXTENDED_CONTROL_LOOP_LOGGING
        sd_printf(EventEntry, "-- RUN ATTITUDE CONTROLLER");
#endif

        // set inputs
        AttitudeController_U.pitch_ref = ref_att->pitch_ref;
        AttitudeController_U.yaw_ref = ref_att->yaw_ref;

        AttitudeController_U.Phi[0] = state->attitude.roll;
        AttitudeController_U.Phi[1] = state->attitude.pitch;
        AttitudeController_U.Phi[2] = state->attitude.yaw;

        AttitudeController_U.v[0] = state->velocity.u;
        AttitudeController_U.v[1] = state->velocity.v;
        AttitudeController_U.v[2] = state->velocity.w;

        AttitudeController_U.omega[0] = state->angular_velocity.p;
        AttitudeController_U.omega[1] = state->angular_velocity.q;
        AttitudeController_U.omega[2] = state->angular_velocity.r;

        AttitudeController_U.i_1 = body_rate_out->i_1;
        AttitudeController_U.i_2 = body_rate_out->i_2;

        // run body rate controller
        oneStep_AttitudeController();

        // set outputs
        out->p_ref = AttitudeController_Y.p_ref;
        out->q_ref = AttitudeController_Y.q_ref;
#if EXTENDED_CONTROL_LOOP_LOGGING
        sd_printf(EventEntry, "-- DID RUN ATTITUDE CONTROLLER");
#endif
    }

    should_run_next = !should_run_next;
    return true;
}

/**
 * Initialize attitude controller
 */
bool attitude_controller_init() {
    AttitudeController_initialize();
    return true;
}

/**
 * Terminate attitude controller
 */
bool attitude_controller_terminate() {
    AttitudeController_terminate();
    return true;
}
