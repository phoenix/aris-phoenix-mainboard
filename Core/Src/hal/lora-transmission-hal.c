/**
 *******************************************************************************
 * @file           lora-transmission-hal.c
 * @brief          Implements a HAL responsible for LoRa communication.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdbool.h>
#include <stdlib.h>

#include "cmsis_os.h"

#include "config.h"
#include "tasks/finite_state_machine.h"
#include "tasks/lora-transmission.h"
#include "tasks/packets.h"

#if USE_LORA_HAL

    #if configUSE_TRACE_FACILITY
traceString lora_chn = NULL;
    #endif

void lora_transmission_task_entry(void *arg) {

    #if configUSE_TRACE_FACILITY
    lora_chn = xTraceRegisterString("LoRA HAL");
    #endif
    int transition = 0;

    while (true) {
        // wait for interrupt to be received
        osThreadFlagsWait(LORA_INTR_RECEIVED, osFlagsWaitAny, osWaitForever);
        enum SystemInput next_input = (enum SystemInput)(1 << transition);

        Packet transition_packet = {.destination = MainControllerBoard,
                                    .origin = ReceiverBoard,
                                    .type = CommandPacket,
                                    .payload_size = 1,
                                    .priority = 1,
                                    .payload = malloc(1)};

        *((uint8_t *)transition_packet.payload) = (uint8_t)next_input;
    #if configUSE_TRACE_FACILITY
        vTracePrintF(lora_chn, "Input to the FSM: %d", next_input);
    #endif
        transition++;

        packet_add_to_queue(&transition_packet);
    }
}

#endif /* USE_LORA_HAL */
