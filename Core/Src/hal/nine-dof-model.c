/**
 *******************************************************************************
 * @file           nine-dof-model.c
 * @brief          Bridge file to the linearized 9 DoF model for the HAL
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrtinitiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdbool.h>

#include "NineDofModel_Linear.h"
#include "config.h"
#include "hal/nine-dof-model.h"
#include "tasks/gps-task.h"
#include "tasks/sd-log.h"

#if USE_SENSOR_DATA_HAL

/* Private variables -------------------------------------------------------- */

/**
 * @brief The local variable that stores the calculated model sensor data.
 */
struct CompleteSensorData local_model_sensor_data;

/* Public variables --------------------------------------------------------- */
struct CompleteSensorData *model_sensor_data = &local_model_sensor_data;

/* Public functions --------------------------------------------------------- */

void oneStep_NineDofModel(void) {
    static boolean_T OverrunFlag = false;

    /* Disable interrupts here */

    /* Check for overrun */
    if (OverrunFlag) {
        rtmSetErrorStatus(NineDofModel_Linear_M, "Overrun");
        return;
    }

    OverrunFlag = true;

    /* Save FPU context here (if necessary) */
    /* Re-enable timer or interrupt here */
    /* Set model inputs here */

    /* Step the model */
    NineDofModel_Linear_step();

    /* Get model outputs here */

    /* Indicate task complete */
    OverrunFlag = false;

    /* Disable interrupts here */
    /* Restore FPU context here (if necessary) */
    /* Enable interrupts here */
}

/**
 * Run Model once
 */

bool nine_dof_model_run(struct InputHandlingOutput *sys_input) {

    #if EXTENDED_CONTROL_LOOP_LOGGING
    sd_printf(EventEntry, "---- RUN MODEL");
    #endif

    static union Quaternion main_assembly_correction = {
        .x = 0.0f,
        .y = 0.0f,
        .z = sinf(HEADING_CORRECTION_ASSEMBLY * 0.5f),
        .w = cosf(HEADING_CORRECTION_ASSEMBLY * 0.5f),
    };

    static uint8_t iterations_since_last_gps_push = 0;

    struct CompleteSensorData *out = model_sensor_data;

    // Set inputs
    NineDofModel_Linear_U.delta_a = sys_input->delta_a_clean;
    NineDofModel_Linear_U.delta_f = sys_input->delta_f_clean;

    // Run Body rate controller
    oneStep_NineDofModel();

    sd_printf(SensorDataEntry,
              "MEAS,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%."
              "4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f",
              (double)NineDofModel_Linear_Y.body_meas[0],
              (double)NineDofModel_Linear_Y.body_meas[1],
              (double)NineDofModel_Linear_Y.body_meas[2],
              (double)NineDofModel_Linear_Y.body_meas[3],
              (double)NineDofModel_Linear_Y.body_meas[4],
              (double)NineDofModel_Linear_Y.body_meas[5],
              (double)NineDofModel_Linear_Y.body_meas[6],
              (double)NineDofModel_Linear_Y.body_meas[7],
              (double)NineDofModel_Linear_Y.body_meas[8],
              (double)NineDofModel_Linear_Y.body_meas[9],
              (double)NineDofModel_Linear_Y.body_meas[10],
              (double)NineDofModel_Linear_Y.body_meas[11],
              (double)NineDofModel_Linear_Y.para_meas[0],
              (double)NineDofModel_Linear_Y.para_meas[1],
              (double)NineDofModel_Linear_Y.para_meas[2],
              (double)NineDofModel_Linear_Y.para_meas[3],
              (double)NineDofModel_Linear_Y.para_meas[4],
              (double)NineDofModel_Linear_Y.para_meas[5],
              (double)NineDofModel_Linear_Y.para_meas[6],
              (double)NineDofModel_Linear_Y.para_meas[7],
              (double)NineDofModel_Linear_Y.para_meas[8]);

    // Compute raw acceleration ICSB
    union LinearAcceleration true_accel_ICSB = {};
    true_accel_ICSB.x = NineDofModel_Linear_Y.para_meas[0];
    true_accel_ICSB.y = NineDofModel_Linear_Y.para_meas[1];
    true_accel_ICSB.z = NineDofModel_Linear_Y.para_meas[2];
    union RawLinearAcceleration raw_lin_accel_ICSB =
        unconvert_raw_acceleration(true_accel_ICSB, NULL);

    // Compute raw angular velocities ICSB
    union AngularVelocity true_ang_vel_ICSB = {};
    true_ang_vel_ICSB.p = NineDofModel_Linear_Y.para_meas[3];
    true_ang_vel_ICSB.q = NineDofModel_Linear_Y.para_meas[4];
    true_ang_vel_ICSB.r = NineDofModel_Linear_Y.para_meas[5];
    union RawAngularVelocity raw_ang_vel_ICSB =
        unconvert_raw_angular_velocity(true_ang_vel_ICSB, NULL);

    // Compute raw heading ICSB
    union Heading true_heading_ICSB = {};
    true_heading_ICSB.roll = NineDofModel_Linear_Y.para_meas[6];
    true_heading_ICSB.pitch = NineDofModel_Linear_Y.para_meas[7];
    true_heading_ICSB.yaw = NineDofModel_Linear_Y.para_meas[8];
    union RawQuaternion raw_quat_ICSB;
    convert_heading_to_raw_quaternion(&true_heading_ICSB, &raw_quat_ICSB, NULL);

    // Compute raw acceleration BODY
    union LinearAcceleration true_accel_BODY = {};
    true_accel_BODY.x = NineDofModel_Linear_Y.body_meas[3];
    true_accel_BODY.y = NineDofModel_Linear_Y.body_meas[4];
    true_accel_BODY.z = NineDofModel_Linear_Y.body_meas[5];
    union RawLinearAcceleration raw_lin_accel_BODY =
        unconvert_raw_acceleration(true_accel_BODY, &main_assembly_correction);

    // Compute raw angular velocities BODY
    union AngularVelocity true_ang_vel_BODY = {};
    true_ang_vel_BODY.p = NineDofModel_Linear_Y.body_meas[6];
    true_ang_vel_BODY.q = NineDofModel_Linear_Y.body_meas[7];
    true_ang_vel_BODY.r = NineDofModel_Linear_Y.body_meas[8];
    union RawAngularVelocity raw_ang_vel_BODY = unconvert_raw_angular_velocity(
        true_ang_vel_BODY, &main_assembly_correction);

    // Compute raw heading BODY
    union Heading true_heading_BODY = {};
    true_heading_BODY.roll = NineDofModel_Linear_Y.body_meas[9];
    true_heading_BODY.pitch = NineDofModel_Linear_Y.body_meas[10];
    true_heading_BODY.yaw = NineDofModel_Linear_Y.body_meas[11];
    union RawQuaternion raw_quat_BODY;
    convert_heading_to_raw_quaternion(&true_heading_BODY, &raw_quat_BODY,
                                      &main_assembly_correction);

    uint32_t pressure =
        convert_altitude_to_raw_pressure(NineDofModel_Linear_Y.body_meas[2]);

    union Position ned_pos = {0};
    ned_pos.north = NineDofModel_Linear_Y.body_meas[0];
    ned_pos.east = NineDofModel_Linear_Y.body_meas[1];
    ned_pos.down = NineDofModel_Linear_Y.body_meas[2];

    // Set sensor data
    // GPS 1
    ned2geo(&ned_pos, &out->geodetic_pos_1.position);

    // GPS 2 (= GPS 1)
    out->geodetic_pos_2.position.latitude =
        out->geodetic_pos_1.position.latitude;
    out->geodetic_pos_2.position.longitude =
        out->geodetic_pos_1.position.longitude;
    out->geodetic_pos_2.position.elevation =
        out->geodetic_pos_1.position.elevation;

    // Set flag that GPS was updated every 20 iterations = 1 Hz
    if (iterations_since_last_gps_push >= 20) {
        NEO7M_RxCpltCallback(1U << 4);
        NEO7M_RxCpltCallback(1U << 5);
        iterations_since_last_gps_push = 0;
    }

    // Pressure / Temperature
    out->main_tube_pressure_1.pressure = pressure;
    out->main_tube_pressure_2.pressure = pressure;
    out->main_tube_pressure_1.temperature = 150;
    out->main_tube_pressure_2.temperature = 150;

    out->icsb_pressure_left.pressure = pressure;
    out->icsb_pressure_right.pressure = pressure;
    out->icsb_pressure_left.temperature = 150;
    out->icsb_pressure_right.temperature = 150;

    // Heading
    out->icsb_att_readout_1.qtr_att = raw_quat_ICSB;
    out->icsb_att_readout_2.qtr_att = raw_quat_ICSB;

    out->main_att_readout_1.qtr_att = raw_quat_BODY;
    out->main_att_readout_2.qtr_att = raw_quat_BODY;

    // Angular velocity
    out->icsb_att_readout_1.ang_vel = raw_ang_vel_ICSB;
    out->icsb_att_readout_2.ang_vel = raw_ang_vel_ICSB;

    out->main_att_readout_1.ang_vel = raw_ang_vel_BODY;
    out->main_att_readout_2.ang_vel = raw_ang_vel_BODY;

    // Linear acceleration
    out->icsb_att_readout_1.lin_acc = raw_lin_accel_ICSB;
    out->icsb_att_readout_2.lin_acc = raw_lin_accel_ICSB;

    out->main_att_readout_1.lin_acc = raw_lin_accel_BODY;
    out->main_att_readout_2.lin_acc = raw_lin_accel_BODY;

    // All readings changed
    out->changed_readings = 2047;

    #if EXTENDED_CONTROL_LOOP_LOGGING
    sd_printf(EventEntry, "--- DID RUN MODEL");
    #endif

    iterations_since_last_gps_push++;
    return true;
}

/**
 * Initialize Model
 */
bool nine_dof_model_init() {
    NineDofModel_Linear_initialize();
    return true;
}

/**
 * Terminate Model
 */
bool nine_dof_model_terminate() {
    NineDofModel_Linear_terminate();
    return true;
}

#endif
