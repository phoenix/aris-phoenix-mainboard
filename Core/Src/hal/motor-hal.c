/**
 *******************************************************************************
 * @file           motor-hal.c
 * @brief          HAL for the motor controllers
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include "config.h"
#include "drivers/epos4.h"

#if USE_MOTOR_HAL

    #include "drivers/epos4.h"

// Enable Motor, if unsuccessful it returns an EPOS4_Error.
// Has to be called at beginning for further motor commands
EPOS4_Status_t EnableMotor(UART_HandleTypeDef *huart) {
    return EPOS4_OK;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

EPOS4_Status_t DisableMotor(UART_HandleTypeDef *huart) {
    return EPOS4_OK;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

EPOS4_Status_t InitDisableMotor(UART_HandleTypeDef *huart) {
    return EPOS4_OK;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// Cyclic Synchronous Position Mode (CPM) -> faster, less stable --> 0x08,
// smaller inputs, higher frequency
EPOS4_Status_t SetCyclicPositionMode(UART_HandleTypeDef *huart) {
    return EPOS4_OK;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// This function writes the desired position into the EPOS4 position register
// Afterwards it tells the EPOS 4 to update the position and then we disable
// The movement again. Position [increments], 360° == 618 increments
EPOS4_Status_t MoveToPosition(UART_HandleTypeDef *huart, int32_t position) {

    return EPOS4_OK;
}

EPOS4_Status_t GetPosition(UART_HandleTypeDef *huart, int32_t *position) {
    *position = 0;
    return EPOS4_OK;
}

EPOS4_Status_t SetPositionProfilePPM(UART_HandleTypeDef *huart,
                                     int32_t velocity, int32_t acceleration,
                                     int32_t deceleration) {
    return EPOS4_OK;
}

/*Set homing parameter and define Homing position+method*/
EPOS4_Status_t SetHomingModeHHM(UART_HandleTypeDef *huart,
                                uint32_t homing_acceleration,
                                uint32_t speed_switch, uint32_t speed_index,
                                int32_t home_offset, uint16_t current_threshold,
                                int32_t home_position) {

    return EPOS4_OK;
}

EPOS4_Status_t FindHome(UART_HandleTypeDef *huart) {
    return EPOS4_OK;
}

#endif
