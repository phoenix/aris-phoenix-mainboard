/**
 *******************************************************************************
 * @file           servo-hal.c
 * @brief          Contains a pseudo servo driver. Does not do anything.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include "cmsis_os.h"

#include "config.h"
#include "drivers/servo-driver.h"

#if USE_SERVO_HAL

    #if configUSE_TRACE_FACILITY
traceString servo_chn = NULL;
    #endif

void trigger_deployment_servos(void) {
    #if configUSE_TRACE_FACILITY
    servo_chn = xTraceRegisterString("Servo Events");
    vTracePrint(servo_chn, "Servo motors actuated!");
    #endif
    return;
}

#endif /* USE_SERVO_HAL */
