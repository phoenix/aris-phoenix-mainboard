/**
 *******************************************************************************
 * @file           sd-hal.c
 * @brief          Hardware abstraction layer for the SD card.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdbool.h>
#include <string.h>

#include "config.h"
#include "drivers/sd-driver.h"
#include "main.h"

#if USE_SD_CARD_HAL

// Imported USART handle
extern UART_HandleTypeDef huart2;

    // Macro for the notification received upon complete DMA transfer
    #define SD_HAL_DMA_TX_COMLETE (1 << 1)

bool sd_card_mount(void) {
    return true;
}

bool sd_card_unmount(void) {
    return true;
}

/* Files and folders -------------------------------------------------------- */

bool sd_setup_logging(void) {
    return true;
}

bool sd_write(char *buf, enum SDFile target_file) {
    HAL_UART_Transmit_DMA(&huart2, (uint8_t *)buf, strlen(buf));
    osThreadFlagsWait(SD_HAL_DMA_TX_COMLETE, osFlagsWaitAll, osWaitForever);
    return true;
}

bool sd_sync(void) {
    return true;
}

/* Information about the file system ---------------------------------------- */

float free_diskspace_percentage(void) {
    return 1;
}

int count_subdirectories(char *path) {
    return 0;
}

/* Callback for the DMA transfer -------------------------------------------- */

void sd_uart_transfer_complete() {
    osThreadFlagsSet(sd_log_taskHandle, SD_HAL_DMA_TX_COMLETE);
}

#endif /* USE_SD_CARD_HAL */