/**
 *******************************************************************************
 * @file           spi-transmission-hal.c
 * @brief          A HAL that simulates the SPI transmission task. Starts
 *                      reading the data from the nine-dof model as soon as the
 *                      ICSB are activated.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include "cmsis_os.h"
#include "config.h"
#include "data/data.h"
#include "hal/nine-dof-model.h"
#include "main.h"
#include "tasks/packets.h"
#include "tasks/sd-log.h"
#include "tasks/spi-transmission.h"
#include "util/serialization.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#if USE_SPI_HAL

/* Private variables -------------------------------------------------------- */
static volatile bool send_icsb_hal_packets = true;

/* Tracing ------------------------------------------------------------------ */

    #if configUSE_TRACE_FACILITY
traceString spi_channel = NULL;
    #endif

/* Public functions --------------------------------------------------------- */

void spi_task_entry(void *arg) {

    #if configUSE_TRACE_FACILITY
    spi_channel = xTraceRegisterString("SPI Transmission Events");
    vTracePrint(spi_channel, "Entered SPI transmission HAL.");
    #endif /* configUSE_TRACE_FACILITY */

    uint32_t tick = osKernelGetTickCount();

    while (true) {

        tick += 50 / portTICK_PERIOD_MS;

        if (send_icsb_hal_packets) {
    #if configUSE_TRACE_FACILITY
            vTracePrint(spi_channel, "SPI HAL task pushes ICSB packet.");
    #endif

            if (model_sensor_data->changed_readings &
                ICSB_ATT_READOUT_1_CHANGED) {

                // Set up a "fake ICSB packet"
                Packet icsb_data = {
                    .destination = MainControllerBoard,
                    .origin = InCanopyBoardLeft,
                    .priority = 0,
                    .type = DataPacket,
                    .payload_size = 23,
                    .payload = malloc(23),
                };

                // this is the payload of the fake packet
                struct DataPayload data = {
                    .sensor_type = BNO055_Sensor,
                    .bno_readout = model_sensor_data->icsb_att_readout_1,
                };

                if (icsb_data.payload) {
                    serialize_data_payload(&data, icsb_data.payload);

                    if (packet_add_to_queue(&icsb_data) == 0) {
    #if configUSE_TRACE_FACILITY
                        vTracePrint(spi_channel,
                                    "Successfully pushed ICSB HAL packet.");
    #endif
                    } else {
                        free(icsb_data.payload);
                    }
                }
            }
        }

        osDelayUntil(tick);
    }
}

int transmit_packet_spi(Packet *packet) {
    if (!packet)
        return -1;

    if (packet->type == CommandPacket) {
        if (packet->payload != NULL) {
            enum Command cmd = ((struct CommandPayload *)packet->payload)->cmd;

            if (cmd == CommandActivate) {
                // start pushing data
                send_icsb_hal_packets = true;
            }

            // memory invariant is still important, even in HAL :-)
            free(packet->payload);
            return 0;
        }
        return -1;
    }

    return 0;
}

#endif
