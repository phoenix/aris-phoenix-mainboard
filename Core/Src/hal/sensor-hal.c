/**
 *******************************************************************************
 * @file           sensor-hal.c
 * @brief          Contains a task that simulates the arrival of new sensor
 *                        data from both main tube and ICSB sensors.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdbool.h>
#include <string.h>

#include "cmsis_os.h"
#include "config.h"

#include "data/data.h"
#include "drivers/bmp-388.h"
#include "drivers/bno-055.h"
#include "drivers/neo-7m.h"
#include "main.h"
#include "tasks/control-loop.h"
#include "tasks/sensor-task.h"

#include "tasks/sd-log.h"

#include "hal/nine-dof-model.h"

#if USE_SENSOR_DATA_HAL

/* BNO-055 HAL -------------------------------------------------------------- */

bool bno055_setup(BNO055_Handle *bno055) {
    assert_param(bno055 != NULL);
    return true;
}

bool bno055_reset(BNO055_Handle *bno055) {
    assert_param(bno055 != NULL);
    osDelay(1);
    return true;
}

bool bno055_lowpower_enable(BNO055_Handle *bno055) {
    assert_param(bno055 != NULL);
    return true;
}

bool bno055_wakeup(BNO055_Handle *bno055) {
    assert_param(bno055 != NULL);
    return true;
}

bool bno055_sample(BNO055_Handle *bno055, struct BNO055_Readout *dest) {
    assert_param(bno055 != NULL);
    memcpy(dest, &model_sensor_data->main_att_readout_1,
           sizeof(struct BNO055_Readout));
    return true;
}

bool bno055_is_calibrated(BNO055_Handle *bno055) {
    return true;
}

bool bno055_print_calibration(BNO055_Handle *bno055) {
    return true;
}

bool bno055_push_calibration(BNO055_Handle *bno055) {
    return true;
}

/* BMP-388 HAL -------------------------------------------------------------- */

bool bmp388_init(BMP388_Handle *bmp388) {
    assert(bmp388 != NULL);
    return true;
}

bool bmp388_sample(BMP388_Handle *bmp_388, struct BMP388_Readout *readout) {
    assert_param(bmp_388 != NULL);
    assert_param(readout != NULL);
    memcpy(readout, &model_sensor_data->main_tube_pressure_1,
           sizeof(struct BMP388_Readout));
    return true;
}

bool bmp388_run_selftest(BMP388_Handle *bmp_388) {
    return true;
}

/* NEO-7M ------------------------------------------------------------------- */

bool neo7m_init(struct NEO7M_Handle *neo7m) {
    assert_param(neo7m != NULL);
    return true;
}

bool neo7m_get_readout(struct NEO7M_Handle *neo7m,
                       struct NEO7M_Readout *readout) {
    assert_param(neo7m != NULL);
    assert_param(readout != NULL);

    readout->num_satellites = 2;
    readout->has_fix = true;

    readout->position.latitude =
        model_sensor_data->geodetic_pos_1.position.latitude;
    readout->position.longitude =
        model_sensor_data->geodetic_pos_1.position.longitude;
    readout->position.elevation =
        model_sensor_data->geodetic_pos_1.position.elevation;

    // TODO: memcpy shifting all positions by 1
    //		 lat = 0
    //       lng = lat
    //       elevation = lng
    // memcpy(&readout->position, &model_sensor_data->geodetic_pos_1,
    //       sizeof(struct NEO7M_Readout));

    return true;
}

#endif /* USE_SENSOR_DATA_HAL */
