var union_radio_flags__t =
[
    [ "flags", "union_radio_flags__t.html#a4416d8b8550f00ac4bcf437836141249", null ],
    [ "reserved", "union_radio_flags__t.html#a8e3fa6f4cfe4589ef5b9c0f8b2556f78", null ],
    [ "rxDone", "union_radio_flags__t.html#a8ae0427ee61e86657a04ba6ba8afe465", null ],
    [ "rxError", "union_radio_flags__t.html#a2de67e2195491a2a3501dcb6344e19dc", null ],
    [ "rxTimeout", "union_radio_flags__t.html#abd9a49f0c8c1a1f91b3d0e1051ad23ad", null ],
    [ "txDone", "union_radio_flags__t.html#a6c2eb8381890171fc05fdc95379ecd41", null ],
    [ "txTimeout", "union_radio_flags__t.html#ae71fe0adaec0ba7ea31f77dfc1bff68c", null ],
    [ "value", "union_radio_flags__t.html#a04712216fc5a11a45346e0a23ca71661", null ]
];