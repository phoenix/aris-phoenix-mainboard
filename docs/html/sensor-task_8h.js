var sensor_task_8h =
[
    [ "i2c_check_no_errors", "sensor-task_8h.html#ad8dee9bd28bd8cfc7f21c213a3e02256", null ],
    [ "i2c_master_read_slave_reg", "sensor-task_8h.html#ae6ec823e1518099c205ccf2d28137b18", null ],
    [ "i2c_master_write_slave_reg", "sensor-task_8h.html#ab4a8d5c39986c6dbed7c4215ad7bb77f", null ],
    [ "sensor_spl_timer_fired", "sensor-task_8h.html#a121175ac309cbe89a2556b491181b3db", null ],
    [ "sensor_task_command_calibration", "sensor-task_8h.html#a4288c900a480be2869c7574b3dbff86c", null ],
    [ "sensor_task_entry", "group__tasks.html#gac257f94997c6c9fc1a8465cda42a3121", null ],
    [ "sensor_task_start_sampling", "sensor-task_8h.html#ae09da06bbbca35be7b7599d319264284", null ]
];