var struct_radio_configurations__t =
[
    [ "irqRx", "struct_radio_configurations__t.html#a7e3784864ff721813de549f6d0357a27", null ],
    [ "irqTx", "struct_radio_configurations__t.html#ab892e28f160303eaa7eb909ef9ae84c7", null ],
    [ "modParams", "struct_radio_configurations__t.html#a6f039a7acf4443f29d12cdfdfe230b7f", null ],
    [ "packetParams", "struct_radio_configurations__t.html#aee5ce46c22f284ba9f2fba99cfedf1b3", null ],
    [ "packetType", "struct_radio_configurations__t.html#a620c4538d4b447a6a755f349e5d93bf5", null ],
    [ "rfFrequency", "struct_radio_configurations__t.html#a441ca8561f0a53541c9a7089c3b29272", null ],
    [ "rxTimeout", "struct_radio_configurations__t.html#a4a8eb4500e04119728a16906020408f2", null ],
    [ "txPower", "struct_radio_configurations__t.html#a989034d9e4955b99d3fd5eb1c474794d", null ],
    [ "txRampTime", "struct_radio_configurations__t.html#a5fe19930c1f893128a6602e06c82b3a9", null ],
    [ "txTimeout", "struct_radio_configurations__t.html#afd9d6a6422b0f177572a6b829ef88d2a", null ]
];