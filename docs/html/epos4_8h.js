var epos4_8h =
[
    [ "EPOS4_Status_t", "group__actuator__drivers.html#ga66ef68997d236f4be2452898de38cc74", [
      [ "EPOS4_OK", "group__actuator__drivers.html#gga66ef68997d236f4be2452898de38cc74a1162bc4edbf0199243f046ceb6c5a3ba", null ],
      [ "EPOS4_ERROR", "group__actuator__drivers.html#gga66ef68997d236f4be2452898de38cc74a205daad8dc1da340bc3b23c59aa20504", null ]
    ] ],
    [ "calculateCRC", "group__actuator__drivers.html#gaf63722d746335e1285e394f94f551d16", null ],
    [ "DisableMotor", "group__actuator__drivers.html#gaf0a4e28b448ca8dc8e317e01bbc02d91", null ],
    [ "EnableMotor", "group__actuator__drivers.html#gaf422ddf3331c29b45dd89ac2fecabc12", null ],
    [ "EPOS_UART_RxCplt_Callback", "group__actuator__drivers.html#ga6b4250bce8f9f1f213065c88259de2a0", null ],
    [ "EPOS_UART_TxCplt_Callback", "group__actuator__drivers.html#gaa1e3dc2ee1b6de2cfe4b669bab856fae", null ],
    [ "FindHome", "group__actuator__drivers.html#ga21e14fc602673b0791945244ca4245b8", null ],
    [ "GetPosition", "group__actuator__drivers.html#gac88ff15f0b6c755c80f6ae454f5e95ba", null ],
    [ "InitDisableMotor", "group__actuator__drivers.html#ga0629d5394937e18d3294918632efbaa3", null ],
    [ "MoveToPosition", "group__actuator__drivers.html#gacafd45031f7525bc9e8fa4c5250a8907", null ],
    [ "ReadCommand", "group__actuator__drivers.html#ga8b134fa0a4ec49de0696bdd6ec3412b9", null ],
    [ "SetCyclicPositionMode", "group__actuator__drivers.html#gad602100bdb9132f888d48a992687a172", null ],
    [ "SetHomingModeHHM", "group__actuator__drivers.html#ga4723d59b7d40b9387ec1762e34769187", null ],
    [ "SetPositionProfilePPM", "group__actuator__drivers.html#ga6f5d4e3cab1c851cbbd1755d5351a8d6", null ],
    [ "WriteCommand", "group__actuator__drivers.html#ga42d498402fc2783bce4fc419f7581699", null ],
    [ "huart1", "group__actuator__drivers.html#ga2cf715bef37f7e8ef385a30974a5f0d5", null ],
    [ "huart6", "group__actuator__drivers.html#ga38905dda1d2fbff006c69db48eddba9d", null ]
];