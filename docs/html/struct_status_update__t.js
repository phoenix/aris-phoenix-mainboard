var struct_status_update__t =
[
    [ "baro1", "struct_status_update__t.html#a6dd0e66a49aa16be544bdb5a2b5c25c2", null ],
    [ "baro2", "struct_status_update__t.html#a2140e52dee65a43f676ffb546ac58e46", null ],
    [ "error", "struct_status_update__t.html#af2c749fe68afeb7c9ed4a5fdf48e4b58", null ],
    [ "error_code", "struct_status_update__t.html#a549568085bb4e5da93bad452ecb71ff5", null ],
    [ "gps1", "struct_status_update__t.html#a7809782c2aaf5aa71c566f8ef43bbd03", null ],
    [ "gps1_lock", "struct_status_update__t.html#a63113aff4a8f74f3cfba8b51d25a497e", null ],
    [ "gps2", "struct_status_update__t.html#af3d778b5dd179821d0e3e920c01670f8", null ],
    [ "gps2_lock", "struct_status_update__t.html#ae56138026494b630910522cd0339650c", null ],
    [ "gps3", "struct_status_update__t.html#a95ed6e7ae8569586dfe85eefd69c18e7", null ],
    [ "gps3_lock", "struct_status_update__t.html#af1d6b80884be827b97cac83b9572114e", null ],
    [ "icsb1", "struct_status_update__t.html#adcdb456fb7c740b820df2b748b945c19", null ],
    [ "icsb2", "struct_status_update__t.html#a91895b2990fd979345001c2fb671d6a2", null ],
    [ "imu1", "struct_status_update__t.html#a19588dfee288c0cff9c9aa78cd22fcb8", null ],
    [ "imu2", "struct_status_update__t.html#ac070391f62fac8e60ebc4437d9648db6", null ],
    [ "receiver_mcu", "struct_status_update__t.html#a79aaac4df511f233eadaa6dccaa3748a", null ],
    [ "reserved", "struct_status_update__t.html#a02b540a30620c3449922001e116db833", null ],
    [ "sd", "struct_status_update__t.html#abf2d4a2b57c5b0ae7796b0ee15e3bc42", null ],
    [ "state", "struct_status_update__t.html#a8a05e1d722ab581dfba528a3ce77f1c0", null ]
];