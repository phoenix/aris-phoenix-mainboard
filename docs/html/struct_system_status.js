var struct_system_status =
[
    [ "icsb_1_last_update", "struct_system_status.html#ab05742f7955e0c3b7dde5f6269a7e67f", null ],
    [ "icsb_1_status", "struct_system_status.html#a9cabdf9080aa557fe5837bf1f9aad805", null ],
    [ "icsb_2_last_update", "struct_system_status.html#a3293b1830e36e0eecfa7e2f0051ea4fd", null ],
    [ "icsb_2_status", "struct_system_status.html#a334cd3bb0ce0a11121cce548e4df3574", null ],
    [ "mna1_status", "struct_system_status.html#a83fb1f7ed243b3ff414b1b79bdb874e7", null ],
    [ "mna2_status", "struct_system_status.html#a891c761580e3a6cfd77c6dc9565e06ea", null ],
    [ "mng1_status", "struct_system_status.html#a7dde664fa6ac61d0df0b9987aa00444c", null ],
    [ "mng2_status", "struct_system_status.html#a112768afbb210678c16b808bd4003eca", null ],
    [ "mnp1_status", "struct_system_status.html#a7d5ddc8814ac5d009f05ba28b8c1853c", null ],
    [ "mnp2_status", "struct_system_status.html#ae065b1a3cd720ed0f0b1233213bf4616", null ],
    [ "reset_reason", "struct_system_status.html#a84e733988003f87c734314d2919be375", null ],
    [ "sd_status", "struct_system_status.html#aeebaa55c7b8c177bd0900be93e5df6ea", null ]
];