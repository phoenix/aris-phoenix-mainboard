var config_8h =
[
    [ "CONFIG_BNO055_ACCEL_PRECALIBRATION", "config_8h.html#afb304ea036a9ab87b58ca1d4028fcb89", null ],
    [ "CONFIG_BNO055_PRECALIBRATION", "config_8h.html#a77b4b53f543678a1517f2fb229d6a6b2", null ],
    [ "CONFIG_BNO_ACCEL_OFFSET_R_1", "config_8h.html#a55d802bad3acbf8d934fdb2380cced1d", null ],
    [ "CONFIG_BNO_ACCEL_OFFSET_R_2", "config_8h.html#abcb0b40bac836645df27841fe67610cc", null ],
    [ "CONFIG_BNO_ACCEL_OFFSET_X_1", "config_8h.html#a9abc580e22f8ce5b59f958da93c07c57", null ],
    [ "CONFIG_BNO_ACCEL_OFFSET_X_2", "config_8h.html#af612ad9f50cc7bdd3bb83871e771502b", null ],
    [ "CONFIG_BNO_ACCEL_OFFSET_Y_1", "config_8h.html#a97f3e5293aa2518d43b27fa72a3f5ef6", null ],
    [ "CONFIG_BNO_ACCEL_OFFSET_Y_2", "config_8h.html#ad8fa67c2c1b18bf0f992cd94877c6eff", null ],
    [ "CONFIG_BNO_ACCEL_OFFSET_Z_1", "config_8h.html#abb3966cb45c5d9cc68b8966e90f72a8d", null ],
    [ "CONFIG_BNO_ACCEL_OFFSET_Z_2", "config_8h.html#abf8ae0b3b798964649bcb14549895d3b", null ],
    [ "CONFIG_BNO_GYRO_OFFSET_X_1", "config_8h.html#a8a2e01a86d6cd458430142c2bd5ef71f", null ],
    [ "CONFIG_BNO_GYRO_OFFSET_X_2", "config_8h.html#ab2dd81e320817c4c75e2f71f6ce5fd02", null ],
    [ "CONFIG_BNO_GYRO_OFFSET_Y_1", "config_8h.html#a36cecf3691b3068e41b9e03c5c86e913", null ],
    [ "CONFIG_BNO_GYRO_OFFSET_Y_2", "config_8h.html#af65c8d21ab3f9aae5fc24f544aeae027", null ],
    [ "CONFIG_BNO_GYRO_OFFSET_Z_1", "config_8h.html#ac1f3fdf0a79c9da9de2de00bccbac6db", null ],
    [ "CONFIG_BNO_GYRO_OFFSET_Z_2", "config_8h.html#a4132ca7e41a33c0a49c1341b9c773f69", null ],
    [ "CONFIG_BNO_MAG_OFFSET_R_1", "config_8h.html#a735486ddf1d1ac818bc1a1d5e08838d9", null ],
    [ "CONFIG_BNO_MAG_OFFSET_R_2", "config_8h.html#a01fafee798678b464b60eb2c641810be", null ],
    [ "CONFIG_BNO_MAG_OFFSET_X_1", "config_8h.html#aae3f2177070b74324b7cdc587900e578", null ],
    [ "CONFIG_BNO_MAG_OFFSET_X_2", "config_8h.html#a276a44ed304f5a324633f82f5c07930c", null ],
    [ "CONFIG_BNO_MAG_OFFSET_Y_1", "config_8h.html#afef207b25ce580bbe92fbc6f5afd923b", null ],
    [ "CONFIG_BNO_MAG_OFFSET_Y_2", "config_8h.html#ac1bd44ccd3f1656c431fbfa501b13a13", null ],
    [ "CONFIG_BNO_MAG_OFFSET_Z_1", "config_8h.html#ab887696d03c373b25a3a4a7ea533f905", null ],
    [ "CONFIG_BNO_MAG_OFFSET_Z_2", "config_8h.html#a96ac63dcf5150d461385e30f4771180b", null ],
    [ "CONTROLLER_POST_DROP_WAIT", "config_8h.html#a78cbd3ac6d48edc1c7c00d239d21e46e", null ],
    [ "DEPLOYMENT_HEIGHT_METERS_AGL", "config_8h.html#aa628ce09f2c67123f0e438662658304d", null ],
    [ "DOWNLINK_UPDATE_FREQ", "config_8h.html#af8aa15e921e8827e22079f791e2a8a05", null ],
    [ "ENABLE_DEBUG_PRINTING", "config_8h.html#adedd46adb80b5d24c05a536431351201", null ],
    [ "EXTENDED_CONTROL_LOOP_LOGGING", "config_8h.html#a5ba52fac5927ebe46315b1a64efa75a5", null ],
    [ "GROUND_LEVEL_METERS", "config_8h.html#a48890ad3187eef6347eb047a0211fb3b", null ],
    [ "HEADING_CORRECTION_ASSEMBLY", "config_8h.html#a947677fd90e1b2d5e82de72f7277d7f1", null ],
    [ "LAT_0", "config_8h.html#a87265d813e11fd0cbcdce6449fff9ef9", null ],
    [ "LNG_0", "config_8h.html#ad58eceeba10b204bab2915074a8c79a5", null ],
    [ "SAFETY_TIMER_DURATION_MS", "config_8h.html#a0287bffc4c11a92ac80f41cc93d59f1c", null ],
    [ "SD_CARD_STREAM_BUFFER_SIZE", "config_8h.html#a22839b54e6024c5e8a4c86ec950ec463", null ],
    [ "SD_CARD_SYNC_THRESHOLD", "config_8h.html#a7b5c8858d7b75b2510fc98d2e677148b", null ],
    [ "SD_CARD_WRITE_CHUNK", "config_8h.html#a42d0ad53b9e16deaf8772e2e2a834532", null ],
    [ "SEA_LEVEL_PRESSURE_PA", "config_8h.html#a7c5035ff6775507d3f2f70eb26ef00d7", null ],
    [ "SENSOR_CALIBRATION_TIMEOUT_MS", "config_8h.html#a52218f6f060f6d7f83bd29147b8f6e1d", null ],
    [ "SENSOR_SAMPLING_RATE", "config_8h.html#a0b4341e108100d2ef5406bfaf27e68dd", null ],
    [ "STATE_ESTIMATION_RATE", "config_8h.html#a8b33348445212eaf6ab9877a8d8bf40e", null ],
    [ "TRACING_STARTS_ON_LAUNCH", "config_8h.html#a6c743ceef35e64988d87b7b8730c4ec6", null ],
    [ "USE_LORA_HAL", "config_8h.html#ab0b9d81c968031d918ff1c7ab4870d5c", null ],
    [ "USE_MOTOR_HAL", "config_8h.html#af4374f1bf06ea9b8c525ff2f2cf9f7c4", null ],
    [ "USE_SD_CARD_HAL", "config_8h.html#a536ce9c74d81b7d7b7d9047314349d14", null ],
    [ "USE_SENSOR_DATA_HAL", "config_8h.html#abe1e587d36d94d2ed6e15c20032b585d", null ],
    [ "USE_SERVO_HAL", "config_8h.html#a7bd38ce15324f9d87d201e048dbdf96a", null ],
    [ "USE_SPI_HAL", "config_8h.html#af4c785ee16f4e5ade842742c4c2f9098", null ],
    [ "USE_SYS_ID_INPUT", "config_8h.html#a2241b6e0cca996145d542d35a0dc1b96", null ],
    [ "WATCHDOG_PRETIMER_MS", "config_8h.html#a0117f047eb59cdaa1e1e92fb7452cca3", null ]
];