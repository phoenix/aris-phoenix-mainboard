var modules =
[
    [ "Drivers", "group__drivers.html", "group__drivers" ],
    [ "Tasks", "group__tasks.html", "group__tasks" ],
    [ "Hardware Abstraction Layer", "group__hal__functions.html", null ],
    [ "Bridge to MATLAB-generated C code", "group__controllers.html", "group__controllers" ],
    [ "CMSIS", "group___c_m_s_i_s.html", "group___c_m_s_i_s" ]
];