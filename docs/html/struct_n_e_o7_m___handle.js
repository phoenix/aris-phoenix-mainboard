var struct_n_e_o7_m___handle =
[
    [ "arg", "struct_n_e_o7_m___handle.html#a83f8223f5fb301197db6316e026da616", null ],
    [ "has_fix", "struct_n_e_o7_m___handle.html#aab94df476815491b4946dea79579a1e1", null ],
    [ "huart", "struct_n_e_o7_m___handle.html#a4bfc6a99048ba36a3aed236d6c2ee35c", null ],
    [ "nmea", "struct_n_e_o7_m___handle.html#ad2622683a189b675a2d35a78fe5b758a", null ],
    [ "num_satellites", "struct_n_e_o7_m___handle.html#abe63fa6745ecd44725b50d7beb4f11ff", null ],
    [ "proc_buf", "struct_n_e_o7_m___handle.html#acfdd6d2724b576363336008589f24d86", null ],
    [ "ready", "struct_n_e_o7_m___handle.html#a7cec022e321677691a1c28bd4c5dea1d", null ],
    [ "ready_cb", "struct_n_e_o7_m___handle.html#adfbf9782abf204a4870d662fc7653858", null ],
    [ "recv_buf", "struct_n_e_o7_m___handle.html#a07b516a4b26c583188b6d2ff317eb9cd", null ],
    [ "recv_index", "struct_n_e_o7_m___handle.html#a7479d501e6df965f644338067c764b23", null ],
    [ "recv_tmp", "struct_n_e_o7_m___handle.html#a8bfdfb110a6f62897b26cfb632ac1459", null ]
];