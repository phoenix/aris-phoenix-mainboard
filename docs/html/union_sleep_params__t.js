var union_sleep_params__t =
[
    [ "Fields", "union_sleep_params__t.html#ac989ca7aae4b72fb69e70bbbd48778a2", null ],
    [ "Reserved", "union_sleep_params__t.html#a2b0064b069c104821ce150ad635a7462", null ],
    [ "Reset", "union_sleep_params__t.html#a9e004ab756f8325f7c7fe6b8a8a1a06f", null ],
    [ "Value", "union_sleep_params__t.html#aad0a5b261ca1516b99d12e67c368a96c", null ],
    [ "WakeUpRTC", "union_sleep_params__t.html#a6353413541d5336c545abe5e082d6eab", null ],
    [ "WarmStart", "union_sleep_params__t.html#a7fdf0c869e0b5e9beea12f61f99e3ae5", null ]
];