var structgps_data_nmea__s =
[
    [ "altitudeCm", "structgps_data_nmea__s.html#a10b1ae08d54130d303df5ee221693fc2", null ],
    [ "date", "structgps_data_nmea__s.html#a745bcf84087c9e157c42a47505552f97", null ],
    [ "ground_course", "structgps_data_nmea__s.html#a3f85ec0e7e8d6b71427236b2c33d2931", null ],
    [ "hdop", "structgps_data_nmea__s.html#aafeea97febbd080e0d7151c239f140c5", null ],
    [ "latitude", "structgps_data_nmea__s.html#ac2596fd3d15e2d164a0282f4185e7b85", null ],
    [ "longitude", "structgps_data_nmea__s.html#a5882f2a75b00995fd5653402b8c0584a", null ],
    [ "numSat", "structgps_data_nmea__s.html#a9ac8b69516d7ddbf1e3679552e6d54cc", null ],
    [ "speed", "structgps_data_nmea__s.html#ac3db9cac6a859cae7a8523edbe19eada", null ],
    [ "time", "structgps_data_nmea__s.html#a6963ded6abd9dee9c12a47139db390f9", null ]
];