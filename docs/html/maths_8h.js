var maths_8h =
[
    [ "Vector3", "union_vector3.html", "union_vector3" ],
    [ "Quaternion", "union_quaternion.html", "union_quaternion" ],
    [ "M_2PIf", "maths_8h.html#a141633e3d1acfc6b738a942d83920bb9", null ],
    [ "M_PIf", "maths_8h.html#acbb42dc053fedc161079f0a4d20a64e8", null ],
    [ "sq", "maths_8h.html#ae7616788b30810a219d9cdee95904ba4", null ],
    [ "conjugate_quaternion", "maths_8h.html#a305978bede58a4cf29a3b43859ce6caf", null ],
    [ "deg2rad", "maths_8h.html#a5b0996d58889c10b20c0567d64fec705", null ],
    [ "hamilton_product", "maths_8h.html#a8bf78345d28916297eae1344969f4505", null ],
    [ "mean_angle", "maths_8h.html#a7833320323cc219e2441adcd17ed44bd", null ],
    [ "normalize_quaternion", "maths_8h.html#a2384cea8480d333b1ceae04bfbe2b627", null ],
    [ "quaternion_norm", "maths_8h.html#acd740d56900921169db8b9bcf1240b61", null ],
    [ "rad2deg", "maths_8h.html#a32bc85d5e25e4374b31308af4f530377", null ],
    [ "rotate_by_quaternion", "maths_8h.html#ac864c8f0df4d61c011a311d32e896d4c", null ],
    [ "wrap_to_2pi", "maths_8h.html#a579c635063537db3babe08a3e2143b49", null ],
    [ "wrap_to_pi", "maths_8h.html#ab57253029b29dd8904938274c3e36b93", null ]
];