var data_8c =
[
    [ "BMP388_PRES_DIV_PA", "data_8c.html#a75462f2856cedceeee01e8a438dffd8a", null ],
    [ "BNO055_ACCEL_DIV_MSQ", "data_8c.html#abde3f484caff9a525c0cff8949fb12a1", null ],
    [ "BNO055_EULER_DIV_RAD", "data_8c.html#ac787b35c4a5c1c6572c7bfba6d370d6d", null ],
    [ "BNO055_GYRO_DIV_RPS", "data_8c.html#a6083623f29dff76612e61fc05857d042", null ],
    [ "BNO055_QUAT_DIV", "data_8c.html#a9bc6874db56470ecf139232ec6bd9631", null ],
    [ "ROLL_CORRECTION", "data_8c.html#a8a08e2598b56dad9e8bb1ba3997aa176", null ],
    [ "YAW_CORRECTION", "data_8c.html#ac3a826a1927c0d52a0d8bcffa829c449", null ],
    [ "convert_heading_to_raw_quaternion", "data_8c.html#aabb658a7358daef431a50de277e5be4f", null ],
    [ "convert_raw_acceleration", "data_8c.html#a40c0c972469104e069e89d7d18016b6f", null ],
    [ "convert_raw_angular_velocity", "data_8c.html#af164bac76d592c51296dfbf887e9f208", null ],
    [ "convert_raw_heading", "data_8c.html#a99323a553c91e826bc370e6d40705571", null ],
    [ "convert_raw_pressure_to_altitude", "data_8c.html#a562b309e5c4b69b9c97558d46f6c5813", null ],
    [ "convert_raw_quaternion_to_heading", "data_8c.html#a3811ea83affa130c69fddd596c1f287f", null ],
    [ "geo2ned", "data_8c.html#a9952a7c34d1e736eb7ba9fb3a7a6c9f7", null ],
    [ "initNedSystem", "data_8c.html#af833e4eb6e4ba1e803570e45d60b27ef", null ],
    [ "ned2geo", "data_8c.html#a022a1689eee829f2ad2227e34af24a01", null ]
];