var group__sensor__drivers =
[
    [ "BMP388_Struct", "struct_b_m_p388___struct.html", [
      [ "bmp388_i2c_addr", "struct_b_m_p388___struct.html#a81bbc1ff350217ab77adc6e01f8664da", null ],
      [ "bmp_internal", "struct_b_m_p388___struct.html#a4c59bce0cd91a4c9938c19c30d07bf4f", null ],
      [ "hi2c", "struct_b_m_p388___struct.html#af986bf4ea9a1215eeead839c3bbf14ed", null ]
    ] ],
    [ "BNO055_Struct", "struct_b_n_o055___struct.html", [
      [ "acc_precalib", "struct_b_n_o055___struct.html#ac97d5cf3c9c1648e9bb7212fb606ba34", null ],
      [ "afsr", "struct_b_n_o055___struct.html#a8a0ceb949805689a321d498546cbe485", null ],
      [ "bno_priv", "struct_b_n_o055___struct.html#a8ba6da81bbd0ec6a6d9ea783125c6e0f", null ],
      [ "gfsr", "struct_b_n_o055___struct.html#a8e85c89664ac69d3024991b0ec0dc404", null ],
      [ "gyro_precalib", "struct_b_n_o055___struct.html#a2941fdf3c9e797d15ba5ae10dc899aae", null ],
      [ "hi2c", "struct_b_n_o055___struct.html#a49a7ee0ab901109217d80476b3c8236c", null ],
      [ "mag_precalib", "struct_b_n_o055___struct.html#a4bfcbf8f8d8fe524c34cce90e92d2ea0", null ],
      [ "reset_pin", "struct_b_n_o055___struct.html#a44010ac4b90448fa5f51c764cbde87a6", null ],
      [ "reset_port", "struct_b_n_o055___struct.html#a6f5af082b77e6b40a47f8840e437c253", null ],
      [ "rst_pin_setup", "struct_b_n_o055___struct.html#a01bca0d89f663f06747110d57d52a600", null ],
      [ "use_primary_addr", "struct_b_n_o055___struct.html#a6135095b16d9b2f7d65dca5ff20307fc", null ]
    ] ],
    [ "BMP388_Handle", "group__sensor__drivers.html#ga79a3ddbecd35a1449363305069c7b2fb", null ],
    [ "BNO055_Handle", "group__sensor__drivers.html#ga154c9265fea112b65d2d6ddb92609e88", null ],
    [ "AccelFullscaleRange", "group__sensor__drivers.html#ga3e58d5e2df17e9f35d8826d2ea08972a", [
      [ "ACCEL_FSR2G", "group__sensor__drivers.html#gga3e58d5e2df17e9f35d8826d2ea08972aa9655d943d8550b67251401dc359d73dd", null ],
      [ "ACCEL_FSR4G", "group__sensor__drivers.html#gga3e58d5e2df17e9f35d8826d2ea08972aaf50801e1f72c793f566b11b86c99835a", null ],
      [ "ACCEL_FSR8G", "group__sensor__drivers.html#gga3e58d5e2df17e9f35d8826d2ea08972aa17d5321488f779bcea386d5e1e48669a", null ],
      [ "ACCEL_FSR16G", "group__sensor__drivers.html#gga3e58d5e2df17e9f35d8826d2ea08972aa206c9b51333e6d0cf8c5d0901414ee18", null ]
    ] ],
    [ "GyroFullscaleRange", "group__sensor__drivers.html#gabf3bdd237936e004be319913f060f155", [
      [ "GYRO_FSR2000", "group__sensor__drivers.html#ggabf3bdd237936e004be319913f060f155ababc835568ad2e6471c3a6bdc41d3bc5", null ],
      [ "GYRO_FSR1000", "group__sensor__drivers.html#ggabf3bdd237936e004be319913f060f155ae00fa6c78d7ce0dd88b17cd015198637", null ],
      [ "GYRO_FSR500", "group__sensor__drivers.html#ggabf3bdd237936e004be319913f060f155ade65f2e35a80fcb98053655b3fe1a8af", null ],
      [ "GYRO_FSR250", "group__sensor__drivers.html#ggabf3bdd237936e004be319913f060f155aa020f99dea251b140034600157701964", null ],
      [ "GYRO_FSR125", "group__sensor__drivers.html#ggabf3bdd237936e004be319913f060f155a98380caf49bd6c89c453339ea14bcf11", null ]
    ] ],
    [ "bmp388_init", "group__sensor__drivers.html#ga2df01c429bec0633865bbb5dde0331e2", null ],
    [ "bmp388_sample", "group__sensor__drivers.html#gac3bedad8ea421ee1d766d6cd74ac33ae", null ],
    [ "bno055_is_calibrated", "group__sensor__drivers.html#ga9198f1b90606409b3fe27f3b5d989926", null ],
    [ "bno055_lowpower_enable", "group__sensor__drivers.html#ga23063f4cb9a7c4d94b5ccde348efbddc", null ],
    [ "bno055_print_calibration", "group__sensor__drivers.html#gaa95fda186d56dad0f1b2a20a5854bc01", null ],
    [ "bno055_push_calibration", "group__sensor__drivers.html#ga067e5c8eb822cf99e427b97af2c5fb6c", null ],
    [ "bno055_reset", "group__sensor__drivers.html#gac6d4bda9b9fb6a98922f994f29d3c32d", null ],
    [ "bno055_sample", "group__sensor__drivers.html#ga74682bbb4f9708b2b2d04c4bee378a1a", null ],
    [ "bno055_setup", "group__sensor__drivers.html#gaf96524ed85ce9cabe5746fb541bdb103", null ],
    [ "bno055_wakeup", "group__sensor__drivers.html#ga07157932158227180828825473d13946", null ]
];