var struct_b_n_o055___struct =
[
    [ "acc_precalib", "struct_b_n_o055___struct.html#ac97d5cf3c9c1648e9bb7212fb606ba34", null ],
    [ "afsr", "struct_b_n_o055___struct.html#a8a0ceb949805689a321d498546cbe485", null ],
    [ "bno_priv", "struct_b_n_o055___struct.html#a8ba6da81bbd0ec6a6d9ea783125c6e0f", null ],
    [ "gfsr", "struct_b_n_o055___struct.html#a8e85c89664ac69d3024991b0ec0dc404", null ],
    [ "gyro_precalib", "struct_b_n_o055___struct.html#a2941fdf3c9e797d15ba5ae10dc899aae", null ],
    [ "hi2c", "struct_b_n_o055___struct.html#a49a7ee0ab901109217d80476b3c8236c", null ],
    [ "mag_precalib", "struct_b_n_o055___struct.html#a4bfcbf8f8d8fe524c34cce90e92d2ea0", null ],
    [ "reset_pin", "struct_b_n_o055___struct.html#a44010ac4b90448fa5f51c764cbde87a6", null ],
    [ "reset_port", "struct_b_n_o055___struct.html#a6f5af082b77e6b40a47f8840e437c253", null ],
    [ "rst_pin_setup", "struct_b_n_o055___struct.html#a01bca0d89f663f06747110d57d52a600", null ],
    [ "use_primary_addr", "struct_b_n_o055___struct.html#a6135095b16d9b2f7d65dca5ff20307fc", null ]
];