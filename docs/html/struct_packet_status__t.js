var struct_packet_status__t =
[
    [ "FreqError", "struct_packet_status__t.html#ac35a69292eb6698369b00a69f27cf46f", null ],
    [ "Gfsk", "struct_packet_status__t.html#a874afe09bc0a8ad322a274e96dc86983", null ],
    [ "LoRa", "struct_packet_status__t.html#a7baca7478a1c5e06c3d80a10f1ec18e0", null ],
    [ "packetType", "struct_packet_status__t.html#a3ef54bfe6c9061cf1b47d4ec98543164", null ],
    [ "Params", "struct_packet_status__t.html#ad1fafe65580383259dba35a241d54af6", null ],
    [ "RssiAvg", "struct_packet_status__t.html#a8a27bde9e410bdf0de3557f3010e0e82", null ],
    [ "RssiPkt", "struct_packet_status__t.html#a2c67217bdeb4494d6cfee21a765f6dd4", null ],
    [ "RssiSync", "struct_packet_status__t.html#af1d9c35a9f9c7fac52acb9dae48c0206", null ],
    [ "RxStatus", "struct_packet_status__t.html#a6e997a7d9fea83ba6435c25510b003ef", null ],
    [ "SignalRssiPkt", "struct_packet_status__t.html#a1345a8883ecd30b4ffdbc396d1c577eb", null ],
    [ "SnrPkt", "struct_packet_status__t.html#a7562e60e21e95924c782fe31c989c223", null ]
];