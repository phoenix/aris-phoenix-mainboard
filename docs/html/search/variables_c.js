var searchData=
[
  ['newest_5fsensor_5fdata_2357',['newest_sensor_data',['../control-loop_8h.html#a38bf886fd67f49271d1f52b1a5e07dd7',1,'newest_sensor_data():&#160;control-loop.c'],['../control-loop_8c.html#a38bf886fd67f49271d1f52b1a5e07dd7',1,'newest_sensor_data():&#160;control-loop.c']]],
  ['nmea_2358',['nmea',['../struct_n_e_o7_m___handle.html#ad2622683a189b675a2d35a78fe5b758a',1,'NEO7M_Handle']]],
  ['north_2359',['north',['../union_position.html#a49223890807ed59b559257ff31a4c5bd',1,'Position']]],
  ['num_5fsatellites_2360',['num_satellites',['../struct_n_e_o7_m___readout.html#a23a55097f826f7bdc81f50a2fd668afa',1,'NEO7M_Readout::num_satellites()'],['../struct_n_e_o7_m___handle.html#abe63fa6745ecd44725b50d7beb4f11ff',1,'NEO7M_Handle::num_satellites()']]],
  ['num_5ftransmission_5fhandlers_2361',['num_transmission_handlers',['../struct_packet_task_config.html#afd513519fe508a70a70e3bcb0736133a',1,'PacketTaskConfig']]],
  ['numsat_2362',['numSat',['../structgps_data_nmea__s.html#a9ac8b69516d7ddbf1e3679552e6d54cc',1,'gpsDataNmea_s::numSat()'],['../structgps_solution_data__s.html#ab332c887349e3dff173928a1fcd60c03',1,'gpsSolutionData_s::numSat()']]]
];
