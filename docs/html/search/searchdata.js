var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstuvwxyz",
  1: "abcdghilmnpqrsvw",
  2: "abcdefgilmnpsw",
  3: "_abcdefghilmnopqrstuwx",
  4: "_abcdefghilmnopqrstuvwxyz",
  5: "abgprt",
  6: "acefgilprs",
  7: "abcdefgiklmnprstuw",
  8: "abcdefghilmnprstuvwxy",
  9: "abcdghlst",
  10: "d"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};

