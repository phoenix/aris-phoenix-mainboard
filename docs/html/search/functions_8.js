var searchData=
[
  ['hal_5fgpio_5fexti_5fcallback_1882',['HAL_GPIO_EXTI_Callback',['../custom-interrupts_8c.html#a0cd91fd3a9608559c2a87a8ba6cba55f',1,'custom-interrupts.c']]],
  ['hal_5fi2c_5fmemrxcpltcallback_1883',['HAL_I2C_MemRxCpltCallback',['../sensor-task_8c.html#ac16a95413b35f05c5ce725fefd8531a5',1,'sensor-task.c']]],
  ['hal_5fi2c_5fmemtxcpltcallback_1884',['HAL_I2C_MemTxCpltCallback',['../sensor-task_8c.html#a874f6104d2cdbced9f2ab6e941ec58f0',1,'sensor-task.c']]],
  ['hal_5fi2c_5fmspdeinit_1885',['HAL_I2C_MspDeInit',['../stm32f4xx__hal__msp_8c.html#a2ec8d9b09854c732e2feed549278f048',1,'stm32f4xx_hal_msp.c']]],
  ['hal_5fi2c_5fmspinit_1886',['HAL_I2C_MspInit',['../stm32f4xx__hal__msp_8c.html#abe01a202c27b23fc150aa66af3130073',1,'stm32f4xx_hal_msp.c']]],
  ['hal_5finittick_1887',['HAL_InitTick',['../stm32f4xx__hal__timebase__tim_8c.html#a879cdb21ef051eb81ec51c18147397d5',1,'stm32f4xx_hal_timebase_tim.c']]],
  ['hal_5fmspinit_1888',['HAL_MspInit',['../stm32f4xx__hal__msp_8c.html#ae4fb8e66865c87d0ebab74a726a6891f',1,'stm32f4xx_hal_msp.c']]],
  ['hal_5fresumetick_1889',['HAL_ResumeTick',['../stm32f4xx__hal__timebase__tim_8c.html#a24e0ee9dae1ec0f9d19200f5575ff790',1,'stm32f4xx_hal_timebase_tim.c']]],
  ['hal_5frtc_5fmspdeinit_1890',['HAL_RTC_MspDeInit',['../stm32f4xx__hal__msp_8c.html#a8767bc3a4d472d39a688090ab10ba6ce',1,'stm32f4xx_hal_msp.c']]],
  ['hal_5frtc_5fmspinit_1891',['HAL_RTC_MspInit',['../stm32f4xx__hal__msp_8c.html#aee6eddaa309c8c9829f1ca794d8f99c5',1,'stm32f4xx_hal_msp.c']]],
  ['hal_5fsd_5fmspdeinit_1892',['HAL_SD_MspDeInit',['../stm32f4xx__hal__msp_8c.html#aad3ad0f8145fca4a29dbe8beef5db085',1,'stm32f4xx_hal_msp.c']]],
  ['hal_5fsd_5fmspinit_1893',['HAL_SD_MspInit',['../stm32f4xx__hal__msp_8c.html#a11b692d44079cb65eb037202d627ae96',1,'stm32f4xx_hal_msp.c']]],
  ['hal_5fspi_5fmspdeinit_1894',['HAL_SPI_MspDeInit',['../stm32f4xx__hal__msp_8c.html#abadc4d4974af1afd943e8d13589068e1',1,'stm32f4xx_hal_msp.c']]],
  ['hal_5fspi_5fmspinit_1895',['HAL_SPI_MspInit',['../stm32f4xx__hal__msp_8c.html#a17f583be14b22caffa6c4e56dcd035ef',1,'stm32f4xx_hal_msp.c']]],
  ['hal_5fspi_5ftxrxcpltcallback_1896',['HAL_SPI_TxRxCpltCallback',['../spi-transmission_8c.html#a04e63f382f172164c8e7cae4ff5d883c',1,'spi-transmission.c']]],
  ['hal_5fsuspendtick_1897',['HAL_SuspendTick',['../stm32f4xx__hal__timebase__tim_8c.html#aaf651af2afe688a991c657f64f8fa5f9',1,'stm32f4xx_hal_timebase_tim.c']]],
  ['hal_5ftim_5fmsppostinit_1898',['HAL_TIM_MspPostInit',['../main_8h.html#ae70bce6c39d0b570a7523b86738cec4b',1,'HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim):&#160;stm32f4xx_hal_msp.c'],['../stm32f4xx__hal__msp_8c.html#ae70bce6c39d0b570a7523b86738cec4b',1,'HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim):&#160;stm32f4xx_hal_msp.c']]],
  ['hal_5ftim_5fperiodelapsedcallback_1899',['HAL_TIM_PeriodElapsedCallback',['../main_8c.html#a8a3b0ad512a6e6c6157440b68d395eac',1,'main.c']]],
  ['hal_5ftim_5fpwm_5fmspdeinit_1900',['HAL_TIM_PWM_MspDeInit',['../stm32f4xx__hal__msp_8c.html#af5f524564b3a99a9f87f45227309e866',1,'stm32f4xx_hal_msp.c']]],
  ['hal_5ftim_5fpwm_5fmspinit_1901',['HAL_TIM_PWM_MspInit',['../stm32f4xx__hal__msp_8c.html#a24d5b9c609d8b753d95508419f4c1901',1,'stm32f4xx_hal_msp.c']]],
  ['hal_5fuart_5ferrorcallback_1902',['HAL_UART_ErrorCallback',['../custom-interrupts_8c.html#a0e0456ea96d55db31de947fb3e954f18',1,'custom-interrupts.c']]],
  ['hal_5fuart_5fmspdeinit_1903',['HAL_UART_MspDeInit',['../stm32f4xx__hal__msp_8c.html#a718f39804e3b910d738a0e1e46151188',1,'stm32f4xx_hal_msp.c']]],
  ['hal_5fuart_5fmspinit_1904',['HAL_UART_MspInit',['../stm32f4xx__hal__msp_8c.html#a0e553b32211877322f949b14801bbfa7',1,'stm32f4xx_hal_msp.c']]],
  ['hal_5fuart_5frxcpltcallback_1905',['HAL_UART_RxCpltCallback',['../custom-interrupts_8c.html#ae494a9643f29b87d6d81e5264e60e57b',1,'custom-interrupts.c']]],
  ['hal_5fuart_5ftxcpltcallback_1906',['HAL_UART_TxCpltCallback',['../custom-interrupts_8c.html#abcdf9b59049eccbc87d54042f9235b1a',1,'custom-interrupts.c']]],
  ['hamilton_5fproduct_1907',['hamilton_product',['../maths_8h.html#a8bf78345d28916297eae1344969f4505',1,'hamilton_product(union Quaternion *a, union Quaternion *b):&#160;maths.c'],['../maths_8c.html#a9ad1129259c6006a08a0e84af9a9733a',1,'hamilton_product(union Quaternion *q1, union Quaternion *q2):&#160;maths.c']]],
  ['hardfault_5fhandler_1908',['HardFault_Handler',['../stm32f4xx__it_8h.html#a2bffc10d5bd4106753b7c30e86903bea',1,'HardFault_Handler(void):&#160;stm32f4xx_it.c'],['../stm32f4xx__it_8c.html#a2bffc10d5bd4106753b7c30e86903bea',1,'HardFault_Handler(void):&#160;stm32f4xx_it.c']]],
  ['height_5ftrigger_5farm_1909',['height_trigger_arm',['../control-loop_8h.html#af4acffa8c6b9718c5e2158a70643c3db',1,'height_trigger_arm(void):&#160;control-loop.c'],['../control-loop_8c.html#af4acffa8c6b9718c5e2158a70643c3db',1,'height_trigger_arm(void):&#160;control-loop.c']]],
  ['height_5ftrigger_5fdisarm_1910',['height_trigger_disarm',['../control-loop_8h.html#ab439403a3505c6f917e40c6c23404f04',1,'height_trigger_disarm(void):&#160;control-loop.c'],['../control-loop_8c.html#ab439403a3505c6f917e40c6c23404f04',1,'height_trigger_disarm(void):&#160;control-loop.c']]]
];
