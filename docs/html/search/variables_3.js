var searchData=
[
  ['changed_5freadings_2167',['changed_readings',['../struct_complete_sensor_data.html#ac3742f703377e5fd49be6d3c1c0e2be2',1,'CompleteSensorData']]],
  ['checksum_2168',['checksum',['../union_payload_airborne__t.html#a4a684691a13c37ca84029583f87dbea9',1,'PayloadAirborne_t::checksum()'],['../union_payload_ground__t.html#a6fe8b2b13af86f15a405415e79dc10fb',1,'PayloadGround_t::checksum()']]],
  ['checksum_5fparam_2169',['checksum_param',['../struct_n_m_e_a___state.html#a79f73478619c9e01772d2abfb48abb31',1,'NMEA_State']]],
  ['cmd_2170',['cmd',['../union_payload_ground__t.html#a1a7eed953f06f3b3d386a3d8d1e93e95',1,'PayloadGround_t::cmd()'],['../struct_command_payload.html#adb9ead1935288b4421817e907d4aa8ff',1,'CommandPayload::cmd()']]],
  ['codingrate_2171',['CodingRate',['../struct_modulation_params__t.html#a840e886c63e2d6f5be04c5e928e7112b',1,'ModulationParams_t']]],
  ['control_5floop_5fattributes_2172',['control_loop_attributes',['../main_8c.html#aab2202e436bd6898f89ff74788b164c0',1,'main.c']]],
  ['control_5floophandle_2173',['control_loopHandle',['../main_8h.html#ad8c3276c2a134fb4eb5c0e956cd9c4e8',1,'control_loopHandle():&#160;main.c'],['../main_8c.html#ad8c3276c2a134fb4eb5c0e956cd9c4e8',1,'control_loopHandle():&#160;main.c']]],
  ['controller_5fenabled_2174',['controller_enabled',['../struct_reference_attitude.html#a2c3e7410ce98709fcafb7da80cf97a36',1,'ReferenceAttitude']]],
  ['coordinates_2175',['Coordinates',['../union_payload_airborne__t.html#a01156a2efd0df0cb88a11fd70c5fa28e',1,'PayloadAirborne_t']]],
  ['countdown_2176',['Countdown',['../union_payload_ground__t.html#a0705c31b6cbda64a0937b1c994e71b66',1,'PayloadGround_t']]],
  ['crclength_2177',['CrcLength',['../struct_packet_params__t.html#a0b1bdc1ef1ea606a5e85f1bb3f8235e0',1,'PacketParams_t']]],
  ['crcmode_2178',['CrcMode',['../struct_packet_params__t.html#a1c2a6db8a2982e86797c0fe80e1660b4',1,'PacketParams_t']]],
  ['crcok_2179',['CrcOk',['../struct_rx_counter__t.html#a529035221bcec41e2ef23dc17644c1cc',1,'RxCounter_t']]]
];
