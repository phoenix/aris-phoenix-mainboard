var searchData=
[
  ['q_1024',['q',['../union_raw_angular_velocity.html#a3c6203ef43e1f077ce90b57e442c3a1f',1,'RawAngularVelocity::q()'],['../union_angular_velocity.html#af573e72c5c2985b3192fcbae4da72477',1,'AngularVelocity::q()']]],
  ['q_5fref_1025',['q_ref',['../struct_attitude_controller_output.html#a486d667ed90fd58214c462f9c2eb7cb1',1,'AttitudeControllerOutput']]],
  ['qtr_5fatt_1026',['qtr_att',['../struct_b_n_o055___readout.html#a9a83d1d8b170c05343b60bf35f5969e0',1,'BNO055_Readout']]],
  ['quaternion_1027',['Quaternion',['../union_quaternion.html',1,'']]],
  ['quaternion_5fnorm_1028',['quaternion_norm',['../maths_8h.html#acd740d56900921169db8b9bcf1240b61',1,'quaternion_norm(union Quaternion *q):&#160;maths.c'],['../maths_8c.html#acd740d56900921169db8b9bcf1240b61',1,'quaternion_norm(union Quaternion *q):&#160;maths.c']]]
];
