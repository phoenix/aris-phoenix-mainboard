var nine_dof_model_8c =
[
    [ "nine_dof_model_init", "group__hal__functions.html#gafbbd5a40f6403731ad60666f9626700f", null ],
    [ "nine_dof_model_run", "group__hal__functions.html#ga8b97e285012a601491abf14495a18757", null ],
    [ "nine_dof_model_terminate", "group__hal__functions.html#gae90ce113b23e9d210dad023ba3db1725", null ],
    [ "oneStep_NineDofModel", "group__hal__functions.html#gaea1659271309b4873f76b6ff1ab81661", null ],
    [ "local_model_sensor_data", "nine-dof-model_8c.html#a0c57c20036637ff37faab7e9cf702f8c", null ],
    [ "model_sensor_data", "group__hal__functions.html#gaaa7569755d8351973a4c17c85d472246", null ]
];