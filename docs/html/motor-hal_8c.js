var motor_hal_8c =
[
    [ "DisableMotor", "group__actuator__drivers.html#gaf0a4e28b448ca8dc8e317e01bbc02d91", null ],
    [ "EnableMotor", "group__actuator__drivers.html#gaf422ddf3331c29b45dd89ac2fecabc12", null ],
    [ "FindHome", "group__actuator__drivers.html#ga21e14fc602673b0791945244ca4245b8", null ],
    [ "GetPosition", "group__actuator__drivers.html#gac88ff15f0b6c755c80f6ae454f5e95ba", null ],
    [ "InitDisableMotor", "group__actuator__drivers.html#ga0629d5394937e18d3294918632efbaa3", null ],
    [ "MoveToPosition", "group__actuator__drivers.html#gacafd45031f7525bc9e8fa4c5250a8907", null ],
    [ "SetCyclicPositionMode", "group__actuator__drivers.html#gad602100bdb9132f888d48a992687a172", null ],
    [ "SetHomingModeHHM", "group__actuator__drivers.html#ga4723d59b7d40b9387ec1762e34769187", null ],
    [ "SetPositionProfilePPM", "group__actuator__drivers.html#ga6f5d4e3cab1c851cbbd1755d5351a8d6", null ]
];