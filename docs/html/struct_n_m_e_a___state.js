var struct_n_m_e_a___state =
[
    [ "checksum_param", "struct_n_m_e_a___state.html#a79f73478619c9e01772d2abfb48abb31", null ],
    [ "GPS_FIX", "struct_n_m_e_a___state.html#abca54b89d9bcfb26d36adf9d8dbf6008", null ],
    [ "gps_frame", "struct_n_m_e_a___state.html#a661f67e304aa3c55222827bef56f7909", null ],
    [ "gps_Msg", "struct_n_m_e_a___state.html#a7fd701148509b8e027872a0d6001838b", null ],
    [ "GPS_numCh", "struct_n_m_e_a___state.html#ae1666e883485e94fee3253981d687268", null ],
    [ "GPS_packetCount", "struct_n_m_e_a___state.html#a2c0f0b4ddb50bd3140bc0601c01af906", null ],
    [ "GPS_svinfo_chn", "struct_n_m_e_a___state.html#a346ddd422cac0716b24b640f64986f4d", null ],
    [ "GPS_svinfo_cno", "struct_n_m_e_a___state.html#a6f9a03508161276b9900d4143465784a", null ],
    [ "GPS_svinfo_quality", "struct_n_m_e_a___state.html#ad3f9bcd584856ca2fc920802f53e6567", null ],
    [ "GPS_svinfo_svid", "struct_n_m_e_a___state.html#a6cce8e4a9f21462c6748051655f65d50", null ],
    [ "GPS_svInfoReceivedCount", "struct_n_m_e_a___state.html#a95f85aba51f4a9c400d641581d523641", null ],
    [ "GPS_update", "struct_n_m_e_a___state.html#a4dcc9f2d5e70e535ec620ce29bd800e4", null ],
    [ "gpsSol", "struct_n_m_e_a___state.html#ad6863e7794e67f491856c04c2a1fd558", null ],
    [ "offset", "struct_n_m_e_a___state.html#a9961e35fee205c0dc914ed4bd612e3ce", null ],
    [ "param", "struct_n_m_e_a___state.html#a4011743c9deeb82631b6a3d399ad3213", null ],
    [ "parity", "struct_n_m_e_a___state.html#a7432ff0183aa6a8894d32a337a7d693e", null ],
    [ "string", "struct_n_m_e_a___state.html#a9e1ef4ec8371546640f079a427a90973", null ],
    [ "svMessageNum", "struct_n_m_e_a___state.html#a32418b9a9e152c79199683e8b9056096", null ]
];