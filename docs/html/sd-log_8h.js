var sd_log_8h =
[
    [ "LOG_AND_TRACE", "sd-log_8h.html#a658b92eff4c31e35ebc874fd372c5f72", null ],
    [ "LOG_AND_TRACEF", "sd-log_8h.html#adeaa1ab690150697f6f138d23b2a9dc3", null ],
    [ "SDLogEntryType", "sd-log_8h.html#ab165bb600df79d5c067ecfa54e77d3ba", [
      [ "EventEntry", "sd-log_8h.html#ab165bb600df79d5c067ecfa54e77d3baaa296b33ab8eba43f66135b5dda824ee4", null ],
      [ "ErrorEntry", "sd-log_8h.html#ab165bb600df79d5c067ecfa54e77d3baa192e2e40ac5915d3edf4d0a497d6eea9", null ],
      [ "SensorDataEntry", "sd-log_8h.html#ab165bb600df79d5c067ecfa54e77d3baa8ccbf19df0f4c0f3fe737dd55947c15e", null ],
      [ "EndLogEntry", "sd-log_8h.html#ab165bb600df79d5c067ecfa54e77d3baae48452abe22b9a65eec691899f0cf041", null ]
    ] ],
    [ "sd_log", "sd-log_8h.html#afee737fa50c3c25aada1494d5f61cab7", null ],
    [ "sd_log_task_entry", "group__tasks.html#ga514b2e456a79ba592a8316a6753e71f0", null ],
    [ "sd_log_task_pause_logging", "sd-log_8h.html#a23c2813bf06c943f45b50693832dc04c", null ],
    [ "sd_printf", "sd-log_8h.html#acd9c1feebb4366faad208b17b584e22b", null ],
    [ "sd_start_log", "sd-log_8h.html#a226999dc5e549db454ff1f7c6e1353a7", null ],
    [ "sd_stop_log", "sd-log_8h.html#a8865886cbdd1285a3483417ca84877d0", null ]
];