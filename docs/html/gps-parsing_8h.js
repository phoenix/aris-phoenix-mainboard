var gps_parsing_8h =
[
    [ "gpsDataNmea_s", "structgps_data_nmea__s.html", "structgps_data_nmea__s" ],
    [ "gpsLocation_s", "structgps_location__s.html", "structgps_location__s" ],
    [ "gpsSolutionData_s", "structgps_solution_data__s.html", "structgps_solution_data__s" ],
    [ "NMEA_State", "struct_n_m_e_a___state.html", "struct_n_m_e_a___state" ],
    [ "GPS_SV_MAXSATS", "gps-parsing_8h.html#aed6b57f546a7ec9ecdce9f99c25fd2e1", null ],
    [ "gpsDataNmea_t", "gps-parsing_8h.html#a70eccfecf1f6cc59ef63e99e9444188c", null ],
    [ "gpsLocation_t", "gps-parsing_8h.html#a1d177a940fdb3d080b1c2e2755c85870", null ],
    [ "gpsSolutionData_t", "gps-parsing_8h.html#a23acd6f33e1f71560a027653b0f6a329", null ],
    [ "GPSFrame", "gps-parsing_8h.html#ad62cd7746de512b41c699cfa489e4a3a", [
      [ "NO_FRAME", "gps-parsing_8h.html#ad62cd7746de512b41c699cfa489e4a3aa86d8adc4c0423a641dc4cdeaa20bbb74", null ],
      [ "FRAME_GGA", "gps-parsing_8h.html#ad62cd7746de512b41c699cfa489e4a3aad7767184019b806ebefc7ea1d99ff8f9", null ],
      [ "FRAME_RMC", "gps-parsing_8h.html#ad62cd7746de512b41c699cfa489e4a3aa7026fe90e6265c9efa27cbc17672c0bb", null ],
      [ "FRAME_GSV", "gps-parsing_8h.html#ad62cd7746de512b41c699cfa489e4a3aa6ae2db19a4950d1ff4639db1eeaf78aa", null ]
    ] ],
    [ "nmea_parse_new_char", "gps-parsing_8h.html#ac1b8da30eebffa7b5ee75b7d32c3efed", null ]
];