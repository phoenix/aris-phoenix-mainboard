var system_status_8h =
[
    [ "SystemStatus", "struct_system_status.html", "struct_system_status" ],
    [ "GPSStatus", "system-status_8h.html#a2a217c77991abe0b32b64e214aa87128", [
      [ "NotRespondingGPSStatus", "system-status_8h.html#a2a217c77991abe0b32b64e214aa87128a41d830e80544db8759d9b5edb7dfe791", null ],
      [ "RespondingGPSStatus", "system-status_8h.html#a2a217c77991abe0b32b64e214aa87128afa3e997f1836e0ba649aaa48db0e4a56", null ],
      [ "FixGPSStatus", "system-status_8h.html#a2a217c77991abe0b32b64e214aa87128a6728166f8b179362178492d8c4612148", null ]
    ] ],
    [ "ICSBStatus", "system-status_8h.html#a50d2ddbe96002a285287bb081f5c0dfb", [
      [ "NotConnectedICSBStatus", "system-status_8h.html#a50d2ddbe96002a285287bb081f5c0dfbaeb1cdc73f5834179d5e879e7c950be4d", null ],
      [ "CalibratedICSBStatus", "system-status_8h.html#a50d2ddbe96002a285287bb081f5c0dfba1612187916b01b9807aeeba63d6b1a8f", null ],
      [ "ActiveICSBStatus", "system-status_8h.html#a50d2ddbe96002a285287bb081f5c0dfbafdd81afa9c3a242845fcfd181ed11181", null ],
      [ "SleepingICSBStatus", "system-status_8h.html#a50d2ddbe96002a285287bb081f5c0dfba138bac7d3a32332aa7d2a5f05c6e767b", null ]
    ] ],
    [ "ResetReason", "system-status_8h.html#a06d9333e5541d14a05e83e78d4a07e11", [
      [ "UnknownResetReason", "system-status_8h.html#a06d9333e5541d14a05e83e78d4a07e11a7ee456c81c4123bb46a71da315efe192", null ],
      [ "SoftwareResetReason", "system-status_8h.html#a06d9333e5541d14a05e83e78d4a07e11a7ea91fd9cef541b8a1abcb576349bb40", null ],
      [ "LowPowerResetReason", "system-status_8h.html#a06d9333e5541d14a05e83e78d4a07e11a1939cdf423b7bc898558edc4d44eecc7", null ],
      [ "WatchdogResetReason", "system-status_8h.html#a06d9333e5541d14a05e83e78d4a07e11a78f4e0f4c5c5daa3373428a7b4ce2382", null ],
      [ "PowerOnPowerDownResetReason", "system-status_8h.html#a06d9333e5541d14a05e83e78d4a07e11a3b36e82d9b52d41ac4e5cb05af09c207", null ],
      [ "ResetPinResetReason", "system-status_8h.html#a06d9333e5541d14a05e83e78d4a07e11ad4711a39f372ce43f51f0814194492b7", null ],
      [ "BrownOutResetReason", "system-status_8h.html#a06d9333e5541d14a05e83e78d4a07e11a0120f95c3ae4cf28e1986024bff41c5b", null ]
    ] ],
    [ "SDStatus", "system-status_8h.html#a8e3454b87453013ba2de62f017c0fa9f", [
      [ "UnknownSDStatus", "system-status_8h.html#a8e3454b87453013ba2de62f017c0fa9fa44fb9db3af6bb8919930bde71d5c0cbe", null ],
      [ "NotPresentSDStatus", "system-status_8h.html#a8e3454b87453013ba2de62f017c0fa9fa0e3c6bfa3c750a898dd09b521a3da4c8", null ],
      [ "MountedSDStatus", "system-status_8h.html#a8e3454b87453013ba2de62f017c0fa9fafbc91a20d10270b4d456c41a60e4d53a", null ],
      [ "ReadySDStatus", "system-status_8h.html#a8e3454b87453013ba2de62f017c0fa9fa83aa095c47bd6b93b8ba8eb2f12ede38", null ]
    ] ],
    [ "SensorStatus", "system-status_8h.html#aa348cf223e558076864814ee88920cec", [
      [ "NotRespondingSensorStatus", "system-status_8h.html#aa348cf223e558076864814ee88920ceca1b5a595cbd624e1b0041d212f3d194f4", null ],
      [ "InSetupSensorStatus", "system-status_8h.html#aa348cf223e558076864814ee88920ceca80ec4e5a9483a8178349670cd2e9e30b", null ],
      [ "SetupCompleteSensorStatus", "system-status_8h.html#aa348cf223e558076864814ee88920ceca9cf8e0d9c9973780053061e2e24b812c", null ],
      [ "ReadySensorStatus", "system-status_8h.html#aa348cf223e558076864814ee88920ceca3ea2382fc12ee655a07cfea2a6d5911c", null ],
      [ "TemporaryErrorSensorStatus", "system-status_8h.html#aa348cf223e558076864814ee88920ceca6376a62ef235fabcc1157275f16bf59b", null ],
      [ "PermanentErrorSensorStatus", "system-status_8h.html#aa348cf223e558076864814ee88920ceca6de42a9d46ce1352e0a927247fafe0e0", null ]
    ] ],
    [ "system_get_lora_compatible_status", "system-status_8h.html#ac7d6a71640aee795e639c9c565d99274", null ],
    [ "system_get_reset_reason", "system-status_8h.html#a299c256a03a2e7c008ba82777934932a", null ],
    [ "system_print_startup_report", "system-status_8h.html#af62ba9f6101f35ede81772a0d8a2e01f", null ],
    [ "system_restart", "system-status_8h.html#a80e0119dbfcaaa43025fe2135d3d1efc", null ],
    [ "global_system_status", "system-status_8h.html#a5c6b509ddbe5d72272ad00de6bdf9c1f", null ]
];