/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Mainboard Project PHOENIX", "index.html", [
    [ "Documentation for Mainboard of Project PHOENIX", "index.html", [
      [ "Overview", "index.html#overview_sec", null ]
    ] ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", "globals_eval" ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_free_r_t_o_s_config_8h.html",
"epos4_8c.html#a61b7c6b217fdcc66618c54b662ef3991",
"group__actuator__drivers.html#ga2b4c305e4d6809a4675bdc277fa6a0bb",
"group__lora__drivers.html#ga56a15ec949d9dd7f166378ad23c04556",
"group__lora__drivers.html#gafd5d55e01b19ae715ec4793d604ae87a",
"group__lora__drivers.html#gga82c98e353fc8a7955a8e92a62f66445da133094575a15d9ed64912392980adb12",
"lora__airborne_8c.html#a87fc037c15107c4b8be8975c512096f9",
"main_8h.html#a4236607013564473712d6463fe98f07b",
"stm32f4xx__hal__conf_8h.html#a4ad07ad8fa6f8639ab8ef362390d86c7",
"struct_modulation_params__t.html#ae370e9a419267b8a396d2ccd1db947e0",
"system-status_8h.html#a06d9333e5541d14a05e83e78d4a07e11a3b36e82d9b52d41ac4e5cb05af09c207"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';