var group__tasks =
[
    [ "control_loop_entry", "group__tasks.html#ga15504ce858eaa03c3d6d78a47bd3b2ba", null ],
    [ "downlink_task_entry", "group__tasks.html#ga74e4cc0af039a64a0c0254e25b15c1af", null ],
    [ "fsm_task_entry", "group__tasks.html#gaffd6748a060d34dfe76555c75451d0fe", null ],
    [ "gps_task_entry", "group__tasks.html#ga884e9de2eb465d59f5792d573e958388", null ],
    [ "lora_transmission_task_entry", "group__tasks.html#gab372f3f5212f243aee7f4c90348fae17", null ],
    [ "packet_task_entry", "group__tasks.html#ga1d9e99c9d401443a65c6393ab7d1bfed", null ],
    [ "sd_log_task_entry", "group__tasks.html#ga514b2e456a79ba592a8316a6753e71f0", null ],
    [ "sensor_task_entry", "group__tasks.html#gac257f94997c6c9fc1a8465cda42a3121", null ],
    [ "spi_task_entry", "group__tasks.html#ga9c77a96b51d24cb2e03b492671e0ba98", null ]
];