var struct_system_status__t =
[
    [ "baro1", "struct_system_status__t.html#ad6f0dd7a695dc25fde9ae3e844ab4fd1", null ],
    [ "baro2", "struct_system_status__t.html#ac5e78f496b1ab218875a84c2f26bb110", null ],
    [ "error", "struct_system_status__t.html#a120ae36c32d9d6b10d292f5c37293eb0", null ],
    [ "error_code", "struct_system_status__t.html#a1847e95f5427eb44a5f1c4427cc555f1", null ],
    [ "fsm_state", "struct_system_status__t.html#ac9fe9c9605b9bfe78f9ef1f2b303a793", null ],
    [ "gps1", "struct_system_status__t.html#ad0d1c6b5bdf3857145b479feff493016", null ],
    [ "gps1_lock", "struct_system_status__t.html#a39e2beb990d976d40fd0c78f364061b0", null ],
    [ "gps2", "struct_system_status__t.html#ae00f259f9c5050224c61c80e5448c08f", null ],
    [ "gps2_lock", "struct_system_status__t.html#aae96366e79e5948451911290affdc2e2", null ],
    [ "gps3", "struct_system_status__t.html#a6c470181debbccfa893266240c29d9ed", null ],
    [ "gps3_lock", "struct_system_status__t.html#a2a22730ee0823d3b7df7bdcd8e95116d", null ],
    [ "icsb1", "struct_system_status__t.html#a6f90aae7a1a50e00f68cf66f9abde2cb", null ],
    [ "icsb2", "struct_system_status__t.html#a190566e5b18cb8c0de732587315d613f", null ],
    [ "imu1", "struct_system_status__t.html#a6a53c8cde46e641135d22b3df2332b31", null ],
    [ "imu2", "struct_system_status__t.html#a0be210add0b32d8fcd959ee035cd1d20", null ],
    [ "ready", "struct_system_status__t.html#a831bae120fef6ece7fc62054efd6c8e0", null ],
    [ "receiver_mcu", "struct_system_status__t.html#aaa481047bfdc922109b0341e506da8dd", null ],
    [ "sd", "struct_system_status__t.html#a63d91f822a9648054369fa5726297d39", null ]
];