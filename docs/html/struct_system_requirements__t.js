var struct_system_requirements__t =
[
    [ "baro", "struct_system_requirements__t.html#acc3403189c4d1d68ee1a6b8ada2931e9", null ],
    [ "gps_active", "struct_system_requirements__t.html#a57f73d773652f7b8d28eebeb499015ae", null ],
    [ "gps_lock", "struct_system_requirements__t.html#a7b6e1dfa47d97bbbb8a923d936f6fa74", null ],
    [ "icsb", "struct_system_requirements__t.html#a4e0288ba3e936526593500b75a8c109e", null ],
    [ "imu", "struct_system_requirements__t.html#a7c048048b350599d66b34a8b9b718126", null ],
    [ "receiver_mcu", "struct_system_requirements__t.html#a4a39a06ca0cd5b98209a079ccb35fa5b", null ],
    [ "reserved", "struct_system_requirements__t.html#a85e5c92f269ff1bcc12d2a1544e86dc9", null ],
    [ "sd", "struct_system_requirements__t.html#a680255b5ac6667291f2eaa12715c484e", null ]
];