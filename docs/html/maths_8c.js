var maths_8c =
[
    [ "conjugate_quaternion", "maths_8c.html#a305978bede58a4cf29a3b43859ce6caf", null ],
    [ "deg2rad", "maths_8c.html#a5b0996d58889c10b20c0567d64fec705", null ],
    [ "hamilton_product", "maths_8c.html#a9ad1129259c6006a08a0e84af9a9733a", null ],
    [ "mean_angle", "maths_8c.html#a7833320323cc219e2441adcd17ed44bd", null ],
    [ "normalize_quaternion", "maths_8c.html#a2384cea8480d333b1ceae04bfbe2b627", null ],
    [ "quaternion_norm", "maths_8c.html#acd740d56900921169db8b9bcf1240b61", null ],
    [ "rad2deg", "maths_8c.html#a32bc85d5e25e4374b31308af4f530377", null ],
    [ "rotate_by_quaternion", "maths_8c.html#ac864c8f0df4d61c011a311d32e896d4c", null ],
    [ "wrap_to_2pi", "maths_8c.html#a579c635063537db3babe08a3e2143b49", null ],
    [ "wrap_to_pi", "maths_8c.html#ab57253029b29dd8904938274c3e36b93", null ]
];