var union_radio_error__t =
[
    [ "__pad0__", "union_radio_error__t.html#a34a957972698334a25bd39c7accd24e9", null ],
    [ "AdcCalib", "union_radio_error__t.html#ac2c5a9c4e89f78ca54356892f5db7e46", null ],
    [ "BuckStart", "union_radio_error__t.html#a53dfe016ba559b316f540c2ae26fb692", null ],
    [ "Fields", "union_radio_error__t.html#a8bd3fe5ed438ed004c7f0218dcbdac01", null ],
    [ "ImgCalib", "union_radio_error__t.html#a362f286886d40cc9d343c6608c1db80e", null ],
    [ "PaRamp", "union_radio_error__t.html#ae68d93787626e37ba2bb2fe508767ae1", null ],
    [ "PllCalib", "union_radio_error__t.html#a17487080ac7d313579774764ba3f9a1d", null ],
    [ "PllLock", "union_radio_error__t.html#a75ec0854acfe452ae1b0d69ee9fe2f24", null ],
    [ "Rc13mCalib", "union_radio_error__t.html#aaf90f54d5d721ec6ebe31110ab0c5661", null ],
    [ "Rc64kCalib", "union_radio_error__t.html#add20db72f17f78e08ac9f7f4f3cd6651", null ],
    [ "Value", "union_radio_error__t.html#ae661e0b62ee2ff135017636971c5c88d", null ],
    [ "XoscStart", "union_radio_error__t.html#a48bf328804b7fb9894673dc6a58cf27b", null ]
];